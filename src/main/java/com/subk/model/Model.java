package com.subk.model;

public class Model {

	private String applicationid;
	private String pdfType;

	public String getApplicationid() {
		return applicationid;
	}

	public void setApplicationid(String applicationid) {
		this.applicationid = applicationid;
	}

	public String getPdfType() {
		return pdfType;
	}

	public void setPdfType(String pdfType) {
		this.pdfType = pdfType;
	}

	@Override
	public String toString() {
		return "Model [applicationid=" + applicationid + ", pdfType=" + pdfType + "]";
	}

}
