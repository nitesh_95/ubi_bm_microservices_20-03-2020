package com.subk.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MasterOccupation")
public class MasterOccupation {
	@Id
	@Column(name = "Code")
	private String code;
	@Column(name = "Description")
	private String description;
	
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "MasterOccupation [code=" + code + ", description=" + description + "]";
	}
	
}
