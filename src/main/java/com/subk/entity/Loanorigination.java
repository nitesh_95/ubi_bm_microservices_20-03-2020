package com.subk.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "Loanorigination")
public class Loanorigination {
	@Id
	@Column(name = "Applicationid")
	private String applicationid;
	@Column(name = "Bcupdateddatetime")
	private String bcupdateddatetime;
	@Transient
	private String branchname;
	@Column(name = "Custtypecode")
	private String custtypecode;
	@Column(name = "Mobileno")
	private String mobileno;
	@Column(name = "Bcverificationstatus")
	private String bcverificationstatus;
	@Column(name = "Applicationno")
	private String applicationno;
	@Column(name = "Groupid")
	private String groupid;
	@Column(name = "Groupname")
	private String groupname;
	@Column(name = "Custinfo")
	private String custinfo;
	@Column(name = "Newcust")
	private String newcust;
	@Column(name = "Subkid")
	private String subkid;
	@Column(name = "Sbaccno")
	private String sbaccno;
	@Column(name = "Custid")
	private String custid;
	@Column(name = "Bcomobile")
	private String bcomobile;
	@Column(name = "Bcopin")
	private String bcopin;
	@Column(name = "Savingacinfo")
	private String savingacinfo;
	@Column(name = "Bcoid")
	private String bcoid;
	@Column(name = "Bconame")
	private String bconame;
	@Column(name = "Custtitle")
	private String custtitle;
	@Column(name = "Custtitlecode")
	private String custtitlecode;
	@Column(name = "Appl1name")
	private String appl1name;
	@Column(name = "Familyexp")
	private String familyexp;
	@Column(name = "Networthcode")
	private String networthcode;
	@Column(name = "Constitutioncode")
	private String constitutioncode;
	@Column(name = "Castecode")
	private String castecode;
	@Column(name = "Communitycode")
	private String communitycode;
	@Column(name = "Schemecode")
	private String schemecode;
	@Column(name = "Introducerstatuscode")
	private String introducerstatuscode;
	@Column(name = "Annualturnovercode")
	private String annualturnovercode;
	@Column(name = "Maritalstatutcode")
	private String maritalstatutcode;
	@Column(name = "Glsubheadcode")
	private String glsubheadcode;
	@Column(name = "Srcofincomecode")
	private String srcofincomecode;
	@Column(name = "Nomineedetails")
	private String nomineedetails;
	@Column(name = "Applicantaddress")
	private String applicantaddress;
	@Column(name = "Permpincode")
	private String permpincode;
	@Column(name = "Electrbill")
	private String electrbill;
	@Column(name = "Permdistrict")
	private String permdistrict;
	@Column(name = "Studyexp")
	private String studyexp;
	@Column(name = "Permstate")
	private String permstate;
	@Column(name = "Blockcode")
	private String blockcode;
	@Column(name = "Insuranceprem")
	private String insuranceprem;
	@Column(name = "Fuelchrgs")
	private String fuelchrgs;
	@Column(name = "Permlandmark")
	private String permlandmark;
	@Column(name = "Televchrgs")
	private String televchrgs;
	@Column(name = "Permstreetno")
	private String permstreetno;
	@Column(name = "Internetchrgs")
	private String internetchrgs;
	@Column(name = "Jlggroupname")
	private String jlggroupname;
	@Column(name = "Jlgleader")
	private String jlgleader;
	@Column(name = "Familysust")
	private String familysust;
	@Column(name = "Appl1careof")
	private String appl1careof;
	@Column(name = "Mobilechrgs")
	private String mobilechrgs;
	@Column(name = "Permhouseno")
	private String permhouseno;
	@Column(name = "Bankremarks")
	private String bankremarks;
	@Column(name = "Uhremarks")
	private String uhremarks;
	@Column(name = "Uhverificationstatus")
	private String uhverificationstatus;
	@Column(name = "Teremarks")
	private String teremarks;
	@Column(name = "Teverificationstatus")
	private String teverificationstatus;
	@Column(name = "Fxremarks")
	private String fxremarks;
	@Column(name = "Fxverificationstatus")
	private String fxverificationstatus;
	@Column(name = "Permvillcity")
	private String permvillcity;
//	@Column(name = "UhRecommendedLoanamount")
//	private String UhRecommendedLoanamount;
	@Column(name = "Fxupdateddatetime")
	private String fxupdateddatetime;
	@Column(name = "Teupdateddatetime")
	private String teupdateddatetime;
	@Column(name = "Uhupdateddatetime")
	private String uhupdateddatetime;
	@Column(name = "Bankverificationstatus")
	private String bankverificationstatus;
	@Column(name = "Bankupdateddatetime")
	private String bankupdateddatetime;
	@Column(name = "Solid")
	private String solid;
	@Column(name = "Loanamountsactioned")
	private String loanamountsactioned;
	@Column(name = "Dbaction")
	private String dbaction;
	@Column(name = "Dobdate")
	private String dobdate;
	@Column(name = "Appl1Address")
	private String appl1address;
	@Column(name = "UhRecommendedNoofmonths")
	private String uhRecommendedNoofmonths;
	@Column(name = "UhRecommendedInstalmentamount")
	private String uhRecommendedInstalmentamount;
	@Column(name = "Applicantphotopath")
	private String applicantphotopath;
	@Column(name = "Panchtax")
	private String panchtax;
	@Column(name = "Telephone")
	private String telephone;
	@Column(name = "Miscexp")
	private String miscexp;
	@Column(name = "Totalyrlyfamexp")
	private String totalyrlyfamexp;
	@Column(name = "Yrlysurplus")
	private String yrlysurplus;
	@Column(name = "Idproof")
	private String idproof;
	@Column(name = "Idproofimage")
	private String idproofimage;
	@Column(name = "Avgweeklysurplus")
	private String avgweeklysurplus;
	@Column(name = "Addressproof")
	private String addressproof;
	@Column(name = "Addressproofimage")
	private String addressproofimage;
	@Column(name = "Loanamount")
	private String loanamount;
	@Column(name = "Pmsbypmjjbycovrg")
	private String pmsbypmjjbycovrg;
	@Column(name = "Approachfor")
	private String approachfor;
	@Column(name = "Pmsbypmjjbyvalidity")
	private String pmsbypmjjbyvalidity;
	@Column(name = "Presentfinancer")
	private String presentfinancer;
	@Column(name = "Appl1occupation")
	private String appl1occupation;
	@Column(name = "Repayperiod")
	private String repayperiod;
	@Column(name = "Apprxyrlyapp1income")
	private String apprxyrlyapp1income;
	@Column(name = "Weeklyinstalment")
	private String weeklyinstalment;
	@Column(name = "Familymembsoccupation")
	private String familymembsoccupation;
	@Column(name = "Bankmitraremarks")
	private String bankmitraremarks;
	@Column(name = "Yrlyothfamilymembincome")
	private String yrlyothfamilymembincome;
	@Column(name = "Bankmanagremarks")
	private String bankmanagremarks;
	@Column(name = "Totalyrlyfamilyincome")
	private String totalyrlyfamilyincome;
	@Column(name = "Bcrecomendation")
	private String bcrecomendation;
	@Column(name = "Bcremarks")
	private String bcremarks;
	@Column(name = "Newsubkid")
	private String newsubkid;
	@Column(name = "Appl1image")
	private String appl1image;
	@Column(name = "permhusfathname")
	private String permhusfathname;
	@Column(name = "permreltype")
	private String permreltype;
	@Column(name = "Permtelephone")
	private String permtelephone;
	@Column(name = "Commhusfathname")
	private String commhusfathname;
	@Column(name = "Commrelationtype")
	private String commrelationtype;
	@Column(name = "Commhouseno")
	private String commhouseno;
	@Column(name = "Commstreetno")
	private String commstreetno;
	@Column(name = "Commlandmark")
	private String commlandmark;
	@Column(name = "Commvillcity")
	private String commvillcity;
	@Column(name = "Commdistrict")
	private String commdistrict;
	@Column(name = "Commstate")
	private String commstate;
	@Column(name = "Commpincode")
	private String commpincode;
	@Column(name = "Commtelephone")
	private String commtelephone;
	@Column(name = "Areavillagecode")
	private String areavillagecode;
	@Column(name = "Areablockcode")
	private String areablockcode;
	@Column(name = "Areadistcode")
	private String areadistcode;
	@Column(name = "Areastatecode")
	private String areastatecode;
	@Column(name = "Fhgtype")
	private String fhgtype;
	@Column(name = "Fhgname")
	private String fhgname;
	@Column(name = "Operationmode")
	private String operationmode;
	@Column(name = "Sex")
	private String sex;
	@Column(name = "Pangirno")
	private String pangirno;
	@Column(name = "Annualturnover")
	private String annualturnover;
	@Column(name = "Category")
	private String category;
	@Column(name = "Qualification")
	private String qualification;
	@Column(name = "Estatement")
	private String estatement;
	@Column(name = "Houseloan")
	private String houseloan;
	@Column(name = "chequebook")
	private String chequebook;
	@Column(name = "Vehicleloan")
	private String vehicleloan;
	@Column(name = "Mobilebanking")
	private String mobilebanking;
	@Column(name = "Mutualfund")
	private String mutualfund;
	@Column(name = "Internetbanking")
	private String internetbanking;
	@Column(name = "Lifeinsurance")
	private String lifeinsurance;
	@Column(name = "Creditcard")
	private String creditcard;
	@Column(name = "Pension")
	private String pension;
	@Column(name = "Addonothers")
	private String addonothers;
	@Column(name = "Crosssellothers")
	private String crosssellothers;
	@Column(name = "atmrequest")
	private String atmrequest;
	@Column(name = "Smsalert")
	private String smsalert;
	@Column(name = "Emailid")
	private String emailid;
	@Column(name = "Idprooftype")
	private String idprooftype;
	@Column(name = "Addressprooftype")
	private String addressprooftype;
	@Column(name = "Introducertitle")
	private String introducertitle;
	@Column(name = "Introducername")
	private String introducername;
	@Column(name = "Introducercustid")
	private String introducercustid;
	@Column(name = "Introduceraccno")
	private String introduceraccno;
	@Column(name = "Introducerknownyear")
	private String introducerknownyear;
	@Column(name = "Nomineeaccnature")
	private String nomineeaccnature;
	@Column(name = "Nomineeage")
	private String nomineeage;
	@Column(name = "Nomineeaccno")
	private String nomineeaccno;
	@Column(name = "Nomineedob")
	private String nomineedob;
	@Column(name = "Nomineeaddetails")
	private String nomineeaddetails;
	@Column(name = "Nomineegaurdianname")
	private String nomineegaurdianname;
	@Column(name = "Nomineename")
	private String nomineename;
	@Column(name = "Nomineeaddress")
	private String nomineeaddress;
	@Column(name = "Nomineeguardianage")
	private String nomineeguardianage;
	@Column(name = "Nomineerelation")
	private String nomineerelation;
	@Column(name = "Nomineeguardianadd")
	private String nomineeguardianadd;
	@Column(name = "Minornomiguartitle")
	private String minornomiguartitle;
	@Column(name = "Minornomiguarname")
	private String minornomiguarname;
	@Column(name = "Minornomiguarage")
	private String minornomiguarAge;
	@Column(name = "Minornomiguarreltype")
	private String minornomiguarreltype;
	@Column(name = "Minornomiguarrelcode")
	private String minornomiguarrelcode;
	@Column(name = "Nomineecity")
	private String nomineecity;
	@Column(name = "Witness1name")
	private String witness1name;
	@Column(name = "Witness2name")
	private String witness2name;
	@Column(name = "Witness1add")
	private String witness1add;
	@Column(name = "Witness2add")
	private String witness2add;
	@Column(name = "Witness1place")
	private String witness1place;
	@Column(name = "Witness2place")
	private String witness2place;
	@Column(name = "Witness1telephone")
	private String witness1telephone;
	@Column(name = "Witness2telephone")
	private String witness2telephone;
	@Column(name = "Appl2name")
	private String appl2name;
	@Column(name = "Appl2pincode")
	private String appl2pincode;
	@Column(name = "Appl2fhgname")
	private String appl2fhgname;
	@Column(name = "Appl2telephone")
	private String appl2telephone;
	@Column(name = "Appl2careof")
	private String appl2careof;
	@Column(name = "Appl2mobile")
	private String appl2mobile;
	@Column(name = "Appl2houseno")
	private String appl2houseno;
	@Column(name = "Appl2sex")
	private String appl2sex;
	@Column(name = "Appl2dob")
	private String appl2dob;
	@Column(name = "Appl2landmark")
	private String appl2landmark;
	@Column(name = "Appl2occupation")
	private String appl2occupation;
	@Column(name = "Appl2villcity")
	private String appl2villcity;
	@Column(name = "Appl2category")
	private String appl2category;
	@Column(name = "Appl2district")
	private String appl2district;
	@Column(name = "Appl2idproof")
	private String appl2idproof;
	@Column(name = "Appl2state")
	private String appl2state;
	@Column(name = "Appl2addproof")
	private String appl2addproof;
	@Column(name = "Custpin")
	private String custpin;
	@Column(name = "Confirmcustpin")
	private String confirmcustpin;
	@Column(name = "Familymemb1name")
	private String familymemb1name;
	@Column(name = "Familymemb1relation")
	private String familymemb1relation;
	@Column(name = "Familymemb1sex")
	private String familymemb1sex;
	@Column(name = "Familymemb1age")
	private String familymemb1age;
	@Column(name = "Familymemb1Occup")
	private String familymemb1Occup;
	@Column(name = "Familymemb1qualification")
	private String familymemb1qualification;
	@Column(name = "Familymemb2name")
	private String familymemb2name;
	@Column(name = "Familymemb2relation")
	private String familymemb2relation;
	@Column(name = "Familymemb2sex")
	private String familymemb2sex;
	@Column(name = "Familymemb2age")
	private String familymemb2age;
	@Column(name = "Familymemb2occup")
	private String familymemb2occup;
	@Column(name = "Familymemb2qualification")
	private String familymemb2qualification;
	@Column(name = "Familymemb3name")
	private String familymemb3name;
	@Column(name = "Familymemb3relation")
	private String familymemb3relation;
	@Column(name = "Familymemb3sex")
	private String familymemb3sex;
	@Column(name = "Familymemb3age")
	private String familymemb3age;
	@Column(name = "Familymemb3occup")
	private String familymemb3occup;
	@Column(name = "Familymemb3qualification")
	private String familymemb3qualification;
	@Column(name = "Loanamountdue")
	private String loanamountdue;
	@Column(name = "Livestock")
	private String livestock;
	@Column(name = "Agrlimpl")
	private String agrlimpl;
	@Column(name = "Activity")
	private String activity;
	@Column(name = "Assetvehicle")
	private String assetvehicle;
	@Column(name = "Loanreuired")
	private String loanreuired;
	@Column(name = "Assetanyother")
	private String assetanyother;
	@Column(name = "Loanaccnomineename")
	private String loanaccnomineename;
	@Column(name = "Loansource")
	private String loansource;
	@Column(name = "Loanaccnomineerelation")
	private String loanaccnomineerelation;
	@Column(name = "Loanpurpose")
	private String loanpurpose;
	@Column(name = "Loanaccnomineeage")
	private String loanaccnomineeage;
	@Column(name = "Amountborrowed")
	private String amountborrowed;
	@Column(name = "Loanaccnomineeresident")
	private String loanaccnomineeresident;
	@Column(name = "Isfpcaptured")
	private String isfpcaptured;
//	@Column(name = "Loanamountsactioned")
//	private String foanamountsactioned;
	@Column(name = "Noofinstalments")
	private String noofinstalments;
	@Column(name = "Interestrate")
	private String interestrate;
	@Column(name = "Instalmentamount")
	private String instalmentamount;
	@Column(name = "Activitycode")
	private String activitycode;
	@Column(name = "Runningsince")
	private String runningsince;
	@Column(name = "Yearsofexperience")
	private String yearsofexperience;
	@Column(name = "Savingopenedstatus")
	private String savingopenedstatus;
	@Column(name = "Savingopenedcreateddate")
	private String savingopenedcreateddate;
	@Column(name = "Loanopenedstatus")
	private String loanopenedstatus;
	@Column(name = "Loandisbursedcreateddate")
	private String loandisbursedcreateddate;
	@Column(name = "Loandisbursedstatus")
	private String loandisbursedstatus;
	@Column(name = "Loanopenedcreateddate")
	private String loanopenedcreateddate;
	@Column(name = "Permvillage")
	private String permvillage;
	@Column(name = "Commvillage")
	private String commvillage;
	@Column(name = "Permsubdistrict")
	private String permsubdistrict;
//	@Column(name = "UhRecommendedLoanamount")
//	private String uhRecommendedLoanamount;
	@Column(name = "apppushstatus")
	private String apppushstatus;
	@Column(name = "Savingaccnumber")
	private String savingaccnumber;
	@Column(name = "Bankcustid")
	private String bankcustid;
	@Column(name = "mclr")
	private String mclr;
	@Column(name = "mclrMaleFemale")
	private String mclrMaleFemale;
	@Column(name = "EkycApplicantphoto")
	private String ekycApplicantphoto;

	public String getCusttypecode() {
		return custtypecode;
	}

	public void setCusttypecode(String custtypecode) {
		this.custtypecode = custtypecode;
	}

	public String getBcverificationstatus() {
		return bcverificationstatus;
	}

	public void setBcverificationstatus(String bcverificationstatus) {
		this.bcverificationstatus = bcverificationstatus;
	}

	public String getApplicationno() {
		return applicationno;
	}

	public void setApplicationno(String applicationno) {
		this.applicationno = applicationno;
	}

	public String getGroupid() {
		return groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public String getCustinfo() {
		return custinfo;
	}

	public void setCustinfo(String custinfo) {
		this.custinfo = custinfo;
	}

	public String getNewcust() {
		return newcust;
	}

	public void setNewcust(String newcust) {
		this.newcust = newcust;
	}

	public String getSubkid() {
		return subkid;
	}

	public void setSubkid(String subkid) {
		this.subkid = subkid;
	}

	public String getSbaccno() {
		return sbaccno;
	}

	public void setSbaccno(String sbaccno) {
		this.sbaccno = sbaccno;
	}

	public String getCustid() {
		return custid;
	}

	public void setCustid(String custid) {
		this.custid = custid;
	}

	public String getBcomobile() {
		return bcomobile;
	}

	public void setBcomobile(String bcomobile) {
		this.bcomobile = bcomobile;
	}

	public String getBcopin() {
		return bcopin;
	}

	public void setBcopin(String bcopin) {
		this.bcopin = bcopin;
	}

	public String getSavingacinfo() {
		return savingacinfo;
	}

	public void setSavingacinfo(String savingacinfo) {
		this.savingacinfo = savingacinfo;
	}

	public String getBcoid() {
		return bcoid;
	}

	public void setBcoid(String bcoid) {
		this.bcoid = bcoid;
	}

	public String getBconame() {
		return bconame;
	}

	public void setBconame(String bconame) {
		this.bconame = bconame;
	}

	public String getCusttitle() {
		return custtitle;
	}

	public void setCusttitle(String custtitle) {
		this.custtitle = custtitle;
	}

	public String getCusttitlecode() {
		return custtitlecode;
	}

	public void setCusttitlecode(String custtitlecode) {
		this.custtitlecode = custtitlecode;
	}

	public String getFamilyexp() {
		return familyexp;
	}

	public void setFamilyexp(String familyexp) {
		this.familyexp = familyexp;
	}

	public String getNetworthcode() {
		return networthcode;
	}

	public void setNetworthcode(String networthcode) {
		this.networthcode = networthcode;
	}

	public String getConstitutioncode() {
		return constitutioncode;
	}

	public void setConstitutioncode(String constitutioncode) {
		this.constitutioncode = constitutioncode;
	}

	public String getCastecode() {
		return castecode;
	}

	public void setCastecode(String castecode) {
		this.castecode = castecode;
	}

	public String getCommunitycode() {
		return communitycode;
	}

	public void setCommunitycode(String communitycode) {
		this.communitycode = communitycode;
	}

	public String getSchemecode() {
		return schemecode;
	}

	public void setSchemecode(String schemecode) {
		this.schemecode = schemecode;
	}

	public String getIntroducerstatuscode() {
		return introducerstatuscode;
	}

	public void setIntroducerstatuscode(String introducerstatuscode) {
		this.introducerstatuscode = introducerstatuscode;
	}

	public String getAnnualturnovercode() {
		return annualturnovercode;
	}

	public void setAnnualturnovercode(String annualturnovercode) {
		this.annualturnovercode = annualturnovercode;
	}

	public String getMaritalstatutcode() {
		return maritalstatutcode;
	}

	public void setMaritalstatutcode(String maritalstatutcode) {
		this.maritalstatutcode = maritalstatutcode;
	}

	public String getGlsubheadcode() {
		return glsubheadcode;
	}

	public void setGlsubheadcode(String glsubheadcode) {
		this.glsubheadcode = glsubheadcode;
	}

	public String getSrcofincomecode() {
		return srcofincomecode;
	}

	public void setSrcofincomecode(String srcofincomecode) {
		this.srcofincomecode = srcofincomecode;
	}

	public String getNomineedetails() {
		return nomineedetails;
	}

	public void setNomineedetails(String nomineedetails) {
		this.nomineedetails = nomineedetails;
	}

	public String getApplicantaddress() {
		return applicantaddress;
	}

	public void setApplicantaddress(String applicantaddress) {
		this.applicantaddress = applicantaddress;
	}

	public String getPermpincode() {
		return permpincode;
	}

	public void setPermpincode(String permpincode) {
		this.permpincode = permpincode;
	}

	public String getElectrbill() {
		return electrbill;
	}

	public void setElectrbill(String electrbill) {
		this.electrbill = electrbill;
	}

	public String getPermdistrict() {
		return permdistrict;
	}

	public void setPermdistrict(String permdistrict) {
		this.permdistrict = permdistrict;
	}

	public String getStudyexp() {
		return studyexp;
	}

	public void setStudyexp(String studyexp) {
		this.studyexp = studyexp;
	}

	public String getPermstate() {
		return permstate;
	}

	public void setPermstate(String permstate) {
		this.permstate = permstate;
	}

	public String getBlockcode() {
		return blockcode;
	}

	public void setBlockcode(String blockcode) {
		this.blockcode = blockcode;
	}

	public String getInsuranceprem() {
		return insuranceprem;
	}

	public void setInsuranceprem(String insuranceprem) {
		this.insuranceprem = insuranceprem;
	}

	public String getFuelchrgs() {
		return fuelchrgs;
	}

	public void setFuelchrgs(String fuelchrgs) {
		this.fuelchrgs = fuelchrgs;
	}

	public String getPermlandmark() {
		return permlandmark;
	}

	public void setPermlandmark(String permlandmark) {
		this.permlandmark = permlandmark;
	}

	public String getTelevchrgs() {
		return televchrgs;
	}

	public void setTelevchrgs(String televchrgs) {
		this.televchrgs = televchrgs;
	}

	public String getPermstreetno() {
		return permstreetno;
	}

	public void setPermstreetno(String permstreetno) {
		this.permstreetno = permstreetno;
	}

	public String getInternetchrgs() {
		return internetchrgs;
	}

	public void setInternetchrgs(String internetchrgs) {
		this.internetchrgs = internetchrgs;
	}

	public String getJlggroupname() {
		return jlggroupname;
	}

	public void setJlggroupname(String jlggroupname) {
		this.jlggroupname = jlggroupname;
	}

	public String getJlgleader() {
		return jlgleader;
	}

	public void setJlgleader(String jlgleader) {
		this.jlgleader = jlgleader;
	}

	public String getFamilysust() {
		return familysust;
	}

	public void setFamilysust(String familysust) {
		this.familysust = familysust;
	}

	public String getAppl1careof() {
		return appl1careof;
	}

	public void setAppl1careof(String appl1careof) {
		this.appl1careof = appl1careof;
	}

	public String getMobilechrgs() {
		return mobilechrgs;
	}

	public void setMobilechrgs(String mobilechrgs) {
		this.mobilechrgs = mobilechrgs;
	}

	public String getPermhouseno() {
		return permhouseno;
	}

	public void setPermhouseno(String permhouseno) {
		this.permhouseno = permhouseno;
	}

	public String getUhremarks() {
		return uhremarks;
	}

	public void setUhremarks(String uhremarks) {
		this.uhremarks = uhremarks;
	}

	public String getUhverificationstatus() {
		return uhverificationstatus;
	}

	public void setUhverificationstatus(String uhverificationstatus) {
		this.uhverificationstatus = uhverificationstatus;
	}

	public String getTeremarks() {
		return teremarks;
	}

	public void setTeremarks(String teremarks) {
		this.teremarks = teremarks;
	}

	public String getTeverificationstatus() {
		return teverificationstatus;
	}

	public void setTeverificationstatus(String teverificationstatus) {
		this.teverificationstatus = teverificationstatus;
	}

	public String getFxremarks() {
		return fxremarks;
	}

	public void setFxremarks(String fxremarks) {
		this.fxremarks = fxremarks;
	}

	public String getFxverificationstatus() {
		return fxverificationstatus;
	}

	public void setFxverificationstatus(String fxverificationstatus) {
		this.fxverificationstatus = fxverificationstatus;
	}

	public String getDbaction() {
		return dbaction;
	}

	public void setDbaction(String dbaction) {
		this.dbaction = dbaction;
	}

	public String getPanchtax() {
		return panchtax;
	}

	public void setPanchtax(String panchtax) {
		this.panchtax = panchtax;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getMiscexp() {
		return miscexp;
	}

	public void setMiscexp(String miscexp) {
		this.miscexp = miscexp;
	}

	public String getTotalyrlyfamexp() {
		return totalyrlyfamexp;
	}

	public void setTotalyrlyfamexp(String totalyrlyfamexp) {
		this.totalyrlyfamexp = totalyrlyfamexp;
	}

	public String getYrlysurplus() {
		return yrlysurplus;
	}

	public void setYrlysurplus(String yrlysurplus) {
		this.yrlysurplus = yrlysurplus;
	}

	public String getIdproof() {
		return idproof;
	}

	public void setIdproof(String idproof) {
		this.idproof = idproof;
	}

	public String getIdproofimage() {
		return idproofimage;
	}

	public void setIdproofimage(String idproofimage) {
		this.idproofimage = idproofimage;
	}

	public String getBranchname() {
		return branchname;
	}

	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}

	public String getAvgweeklysurplus() {
		return avgweeklysurplus;
	}

	public void setAvgweeklysurplus(String avgweeklysurplus) {
		this.avgweeklysurplus = avgweeklysurplus;
	}

	public String getAddressproof() {
		return addressproof;
	}

	public void setAddressproof(String addressproof) {
		this.addressproof = addressproof;
	}

	public String getAddressproofimage() {
		return addressproofimage;
	}

	public void setAddressproofimage(String addressproofimage) {
		this.addressproofimage = addressproofimage;
	}

	public String getLoanamount() {
		return loanamount;
	}

	public void setLoanamount(String loanamount) {
		this.loanamount = loanamount;
	}

	public String getPmsbypmjjbycovrg() {
		return pmsbypmjjbycovrg;
	}

	public void setPmsbypmjjbycovrg(String pmsbypmjjbycovrg) {
		this.pmsbypmjjbycovrg = pmsbypmjjbycovrg;
	}

	public String getApproachfor() {
		return approachfor;
	}

	public void setApproachfor(String approachfor) {
		this.approachfor = approachfor;
	}

	public String getPmsbypmjjbyvalidity() {
		return pmsbypmjjbyvalidity;
	}

	public void setPmsbypmjjbyvalidity(String pmsbypmjjbyvalidity) {
		this.pmsbypmjjbyvalidity = pmsbypmjjbyvalidity;
	}

	public String getPresentfinancer() {
		return presentfinancer;
	}

	public void setPresentfinancer(String presentfinancer) {
		this.presentfinancer = presentfinancer;
	}

	public String getAppl1occupation() {
		return appl1occupation;
	}

	public void setAppl1occupation(String appl1occupation) {
		this.appl1occupation = appl1occupation;
	}

	public String getRepayperiod() {
		return repayperiod;
	}

	public void setRepayperiod(String repayperiod) {
		this.repayperiod = repayperiod;
	}

	public String getApprxyrlyapp1income() {
		return apprxyrlyapp1income;
	}

	public void setApprxyrlyapp1income(String apprxyrlyapp1income) {
		this.apprxyrlyapp1income = apprxyrlyapp1income;
	}

	public String getWeeklyinstalment() {
		return weeklyinstalment;
	}

	public void setWeeklyinstalment(String weeklyinstalment) {
		this.weeklyinstalment = weeklyinstalment;
	}

	public String getFamilymembsoccupation() {
		return familymembsoccupation;
	}

	public void setFamilymembsoccupation(String familymembsoccupation) {
		this.familymembsoccupation = familymembsoccupation;
	}

	public String getBankmitraremarks() {
		return bankmitraremarks;
	}

	public void setBankmitraremarks(String bankmitraremarks) {
		this.bankmitraremarks = bankmitraremarks;
	}

	public String getYrlyothfamilymembincome() {
		return yrlyothfamilymembincome;
	}

	public void setYrlyothfamilymembincome(String yrlyothfamilymembincome) {
		this.yrlyothfamilymembincome = yrlyothfamilymembincome;
	}

	public String getBankmanagremarks() {
		return bankmanagremarks;
	}

	public void setBankmanagremarks(String bankmanagremarks) {
		this.bankmanagremarks = bankmanagremarks;
	}

	public String getTotalyrlyfamilyincome() {
		return totalyrlyfamilyincome;
	}

	public void setTotalyrlyfamilyincome(String totalyrlyfamilyincome) {
		this.totalyrlyfamilyincome = totalyrlyfamilyincome;
	}

	public String getBcrecomendation() {
		return bcrecomendation;
	}

	public void setBcrecomendation(String bcrecomendation) {
		this.bcrecomendation = bcrecomendation;
	}

	public String getBcremarks() {
		return bcremarks;
	}

	public void setBcremarks(String bcremarks) {
		this.bcremarks = bcremarks;
	}

	public String getNewsubkid() {
		return newsubkid;
	}

	public void setNewsubkid(String newsubkid) {
		this.newsubkid = newsubkid;
	}

	public String getAppl1image() {
		return appl1image;
	}

	public void setAppl1image(String appl1image) {
		this.appl1image = appl1image;
	}

	public String getPermhusfathname() {
		return permhusfathname;
	}

	public void setPermhusfathname(String permhusfathname) {
		this.permhusfathname = permhusfathname;
	}

	public String getPermreltype() {
		return permreltype;
	}

	public void setPermreltype(String permreltype) {
		this.permreltype = permreltype;
	}

	public String getPermtelephone() {
		return permtelephone;
	}

	public void setPermtelephone(String permtelephone) {
		this.permtelephone = permtelephone;
	}

	public String getCommhusfathname() {
		return commhusfathname;
	}

	public void setCommhusfathname(String commhusfathname) {
		this.commhusfathname = commhusfathname;
	}

	public String getCommrelationtype() {
		return commrelationtype;
	}

	public void setCommrelationtype(String commrelationtype) {
		this.commrelationtype = commrelationtype;
	}

	public String getCommhouseno() {
		return commhouseno;
	}

	public void setCommhouseno(String commhouseno) {
		this.commhouseno = commhouseno;
	}

	public String getCommstreetno() {
		return commstreetno;
	}

	public void setCommstreetno(String commstreetno) {
		this.commstreetno = commstreetno;
	}

	public String getCommlandmark() {
		return commlandmark;
	}

	public void setCommlandmark(String commlandmark) {
		this.commlandmark = commlandmark;
	}

	public String getCommvillcity() {
		return commvillcity;
	}

	public void setCommvillcity(String commvillcity) {
		this.commvillcity = commvillcity;
	}

	public String getCommdistrict() {
		return commdistrict;
	}

	public void setCommdistrict(String commdistrict) {
		this.commdistrict = commdistrict;
	}

	public String getCommstate() {
		return commstate;
	}

	public void setCommstate(String commstate) {
		this.commstate = commstate;
	}

	public String getCommpincode() {
		return commpincode;
	}

	public void setCommpincode(String commpincode) {
		this.commpincode = commpincode;
	}

	public String getCommtelephone() {
		return commtelephone;
	}

	public void setCommtelephone(String commtelephone) {
		this.commtelephone = commtelephone;
	}

	public String getAreavillagecode() {
		return areavillagecode;
	}

	public void setAreavillagecode(String areavillagecode) {
		this.areavillagecode = areavillagecode;
	}

	public String getAreablockcode() {
		return areablockcode;
	}

	public void setAreablockcode(String areablockcode) {
		this.areablockcode = areablockcode;
	}

	public String getAreadistcode() {
		return areadistcode;
	}

	public void setAreadistcode(String areadistcode) {
		this.areadistcode = areadistcode;
	}

	public String getAreastatecode() {
		return areastatecode;
	}

	public void setAreastatecode(String areastatecode) {
		this.areastatecode = areastatecode;
	}

	public String getFhgtype() {
		return fhgtype;
	}

	public void setFhgtype(String fhgtype) {
		this.fhgtype = fhgtype;
	}

	public String getFhgname() {
		return fhgname;
	}

	public void setFhgname(String fhgname) {
		this.fhgname = fhgname;
	}

	public String getOperationmode() {
		return operationmode;
	}

	public void setOperationmode(String operationmode) {
		this.operationmode = operationmode;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPangirno() {
		return pangirno;
	}

	public void setPangirno(String pangirno) {
		this.pangirno = pangirno;
	}

	public String getAnnualturnover() {
		return annualturnover;
	}

	public void setAnnualturnover(String annualturnover) {
		this.annualturnover = annualturnover;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getEstatement() {
		return estatement;
	}

	public void setEstatement(String estatement) {
		this.estatement = estatement;
	}

	public String getHouseloan() {
		return houseloan;
	}

	public void setHouseloan(String houseloan) {
		this.houseloan = houseloan;
	}

	public String getChequebook() {
		return chequebook;
	}

	public void setChequebook(String chequebook) {
		this.chequebook = chequebook;
	}

	public String getVehicleloan() {
		return vehicleloan;
	}

	public void setVehicleloan(String vehicleloan) {
		this.vehicleloan = vehicleloan;
	}

	public String getMobilebanking() {
		return mobilebanking;
	}

	public void setMobilebanking(String mobilebanking) {
		this.mobilebanking = mobilebanking;
	}

	public String getMutualfund() {
		return mutualfund;
	}

	public void setMutualfund(String mutualfund) {
		this.mutualfund = mutualfund;
	}

	public String getInternetbanking() {
		return internetbanking;
	}

	public void setInternetbanking(String internetbanking) {
		this.internetbanking = internetbanking;
	}

	public String getLifeinsurance() {
		return lifeinsurance;
	}

	public void setLifeinsurance(String lifeinsurance) {
		this.lifeinsurance = lifeinsurance;
	}

	public String getCreditcard() {
		return creditcard;
	}

	public void setCreditcard(String creditcard) {
		this.creditcard = creditcard;
	}

	public String getPension() {
		return pension;
	}

	public void setPension(String pension) {
		this.pension = pension;
	}

	public String getAddonothers() {
		return addonothers;
	}

	public void setAddonothers(String addonothers) {
		this.addonothers = addonothers;
	}

	public String getCrosssellothers() {
		return crosssellothers;
	}

	public void setCrosssellothers(String crosssellothers) {
		this.crosssellothers = crosssellothers;
	}

	public String getAtmrequest() {
		return atmrequest;
	}

	public void setAtmrequest(String atmrequest) {
		this.atmrequest = atmrequest;
	}

	public String getSmsalert() {
		return smsalert;
	}

	public void setSmsalert(String smsalert) {
		this.smsalert = smsalert;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getIdprooftype() {
		return idprooftype;
	}

	public void setIdprooftype(String idprooftype) {
		this.idprooftype = idprooftype;
	}

	public String getAddressprooftype() {
		return addressprooftype;
	}

	public void setAddressprooftype(String addressprooftype) {
		this.addressprooftype = addressprooftype;
	}

	public String getIntroducertitle() {
		return introducertitle;
	}

	public void setIntroducertitle(String introducertitle) {
		this.introducertitle = introducertitle;
	}

	public String getIntroducername() {
		return introducername;
	}

	public void setIntroducername(String introducername) {
		this.introducername = introducername;
	}

	public String getIntroducercustid() {
		return introducercustid;
	}

	public void setIntroducercustid(String introducercustid) {
		this.introducercustid = introducercustid;
	}

	public String getIntroduceraccno() {
		return introduceraccno;
	}

	public void setIntroduceraccno(String introduceraccno) {
		this.introduceraccno = introduceraccno;
	}

	public String getIntroducerknownyear() {
		return introducerknownyear;
	}

	public void setIntroducerknownyear(String introducerknownyear) {
		this.introducerknownyear = introducerknownyear;
	}

	public String getNomineeaccnature() {
		return nomineeaccnature;
	}

	public void setNomineeaccnature(String nomineeaccnature) {
		this.nomineeaccnature = nomineeaccnature;
	}

	public String getNomineeage() {
		return nomineeage;
	}

	public void setNomineeage(String nomineeage) {
		this.nomineeage = nomineeage;
	}

	public String getNomineeaccno() {
		return nomineeaccno;
	}

	public void setNomineeaccno(String nomineeaccno) {
		this.nomineeaccno = nomineeaccno;
	}

	public String getNomineedob() {
		return nomineedob;
	}

	public void setNomineedob(String nomineedob) {
		this.nomineedob = nomineedob;
	}

	public String getNomineeaddetails() {
		return nomineeaddetails;
	}

	public void setNomineeaddetails(String nomineeaddetails) {
		this.nomineeaddetails = nomineeaddetails;
	}

	public String getNomineegaurdianname() {
		return nomineegaurdianname;
	}

	public void setNomineegaurdianname(String nomineegaurdianname) {
		this.nomineegaurdianname = nomineegaurdianname;
	}

	public String getNomineename() {
		return nomineename;
	}

	public void setNomineename(String nomineename) {
		this.nomineename = nomineename;
	}

	public String getNomineeaddress() {
		return nomineeaddress;
	}

	public void setNomineeaddress(String nomineeaddress) {
		this.nomineeaddress = nomineeaddress;
	}

	public String getNomineeguardianage() {
		return nomineeguardianage;
	}

	public void setNomineeguardianage(String nomineeguardianage) {
		this.nomineeguardianage = nomineeguardianage;
	}

	public String getNomineerelation() {
		return nomineerelation;
	}

	public void setNomineerelation(String nomineerelation) {
		this.nomineerelation = nomineerelation;
	}

	public String getNomineeguardianadd() {
		return nomineeguardianadd;
	}

	public void setNomineeguardianadd(String nomineeguardianadd) {
		this.nomineeguardianadd = nomineeguardianadd;
	}

	public String getMinornomiguartitle() {
		return minornomiguartitle;
	}

	public void setMinornomiguartitle(String minornomiguartitle) {
		this.minornomiguartitle = minornomiguartitle;
	}

	public String getMinornomiguarname() {
		return minornomiguarname;
	}

	public void setMinornomiguarname(String minornomiguarname) {
		this.minornomiguarname = minornomiguarname;
	}

	public String getMinornomiguarAge() {
		return minornomiguarAge;
	}

	public void setMinornomiguarAge(String minornomiguarAge) {
		this.minornomiguarAge = minornomiguarAge;
	}

	public String getMinornomiguarreltype() {
		return minornomiguarreltype;
	}

	public void setMinornomiguarreltype(String minornomiguarreltype) {
		this.minornomiguarreltype = minornomiguarreltype;
	}

	public String getMinornomiguarrelcode() {
		return minornomiguarrelcode;
	}

	public void setMinornomiguarrelcode(String minornomiguarrelcode) {
		this.minornomiguarrelcode = minornomiguarrelcode;
	}

	public String getNomineecity() {
		return nomineecity;
	}

	public void setNomineecity(String nomineecity) {
		this.nomineecity = nomineecity;
	}

	public String getWitness1name() {
		return witness1name;
	}

	public void setWitness1name(String witness1name) {
		this.witness1name = witness1name;
	}

	public String getWitness2name() {
		return witness2name;
	}

	public void setWitness2name(String witness2name) {
		this.witness2name = witness2name;
	}

	public String getWitness1add() {
		return witness1add;
	}

	public void setWitness1add(String witness1add) {
		this.witness1add = witness1add;
	}

	public String getWitness2add() {
		return witness2add;
	}

	public void setWitness2add(String witness2add) {
		this.witness2add = witness2add;
	}

	public String getWitness1place() {
		return witness1place;
	}

	public void setWitness1place(String witness1place) {
		this.witness1place = witness1place;
	}

	public String getWitness2place() {
		return witness2place;
	}

	public void setWitness2place(String witness2place) {
		this.witness2place = witness2place;
	}

	public String getWitness1telephone() {
		return witness1telephone;
	}

	public void setWitness1telephone(String witness1telephone) {
		this.witness1telephone = witness1telephone;
	}

	public String getWitness2telephone() {
		return witness2telephone;
	}

	public void setWitness2telephone(String witness2telephone) {
		this.witness2telephone = witness2telephone;
	}

	public String getAppl2name() {
		return appl2name;
	}

	public void setAppl2name(String appl2name) {
		this.appl2name = appl2name;
	}

	public String getAppl2pincode() {
		return appl2pincode;
	}

	public void setAppl2pincode(String appl2pincode) {
		this.appl2pincode = appl2pincode;
	}

	public String getAppl2fhgname() {
		return appl2fhgname;
	}

	public void setAppl2fhgname(String appl2fhgname) {
		this.appl2fhgname = appl2fhgname;
	}

	public String getAppl2telephone() {
		return appl2telephone;
	}

	public void setAppl2telephone(String appl2telephone) {
		this.appl2telephone = appl2telephone;
	}

	public String getAppl2careof() {
		return appl2careof;
	}

	public void setAppl2careof(String appl2careof) {
		this.appl2careof = appl2careof;
	}

	public String getAppl2mobile() {
		return appl2mobile;
	}

	public void setAppl2mobile(String appl2mobile) {
		this.appl2mobile = appl2mobile;
	}

	public String getAppl2houseno() {
		return appl2houseno;
	}

	public void setAppl2houseno(String appl2houseno) {
		this.appl2houseno = appl2houseno;
	}

	public String getAppl2sex() {
		return appl2sex;
	}

	public void setAppl2sex(String appl2sex) {
		this.appl2sex = appl2sex;
	}

	public String getAppl2dob() {
		return appl2dob;
	}

	public void setAppl2dob(String appl2dob) {
		this.appl2dob = appl2dob;
	}

	public String getAppl2landmark() {
		return appl2landmark;
	}

	public void setAppl2landmark(String appl2landmark) {
		this.appl2landmark = appl2landmark;
	}

	public String getAppl2occupation() {
		return appl2occupation;
	}

	public void setAppl2occupation(String appl2occupation) {
		this.appl2occupation = appl2occupation;
	}

	public String getAppl2villcity() {
		return appl2villcity;
	}

	public void setAppl2villcity(String appl2villcity) {
		this.appl2villcity = appl2villcity;
	}

	public String getAppl2category() {
		return appl2category;
	}

	public void setAppl2category(String appl2category) {
		this.appl2category = appl2category;
	}

	public String getAppl2district() {
		return appl2district;
	}

	public void setAppl2district(String appl2district) {
		this.appl2district = appl2district;
	}

	public String getAppl2idproof() {
		return appl2idproof;
	}

	public void setAppl2idproof(String appl2idproof) {
		this.appl2idproof = appl2idproof;
	}

	public String getAppl2state() {
		return appl2state;
	}

	public void setAppl2state(String appl2state) {
		this.appl2state = appl2state;
	}

	public String getAppl2addproof() {
		return appl2addproof;
	}

	public void setAppl2addproof(String appl2addproof) {
		this.appl2addproof = appl2addproof;
	}

	public String getCustpin() {
		return custpin;
	}

	public void setCustpin(String custpin) {
		this.custpin = custpin;
	}

	public String getConfirmcustpin() {
		return confirmcustpin;
	}

	public void setConfirmcustpin(String confirmcustpin) {
		this.confirmcustpin = confirmcustpin;
	}

	public String getFamilymemb1name() {
		return familymemb1name;
	}

	public void setFamilymemb1name(String familymemb1name) {
		this.familymemb1name = familymemb1name;
	}

	public String getFamilymemb1relation() {
		return familymemb1relation;
	}

	public void setFamilymemb1relation(String familymemb1relation) {
		this.familymemb1relation = familymemb1relation;
	}

	public String getFamilymemb1sex() {
		return familymemb1sex;
	}

	public void setFamilymemb1sex(String familymemb1sex) {
		this.familymemb1sex = familymemb1sex;
	}

	public String getFamilymemb1age() {
		return familymemb1age;
	}

	public void setFamilymemb1age(String familymemb1age) {
		this.familymemb1age = familymemb1age;
	}

	public String getFamilymemb1Occup() {
		return familymemb1Occup;
	}

	public void setFamilymemb1Occup(String familymemb1Occup) {
		this.familymemb1Occup = familymemb1Occup;
	}

	public String getFamilymemb1qualification() {
		return familymemb1qualification;
	}

	public void setFamilymemb1qualification(String familymemb1qualification) {
		this.familymemb1qualification = familymemb1qualification;
	}

	public String getFamilymemb2name() {
		return familymemb2name;
	}

	public void setFamilymemb2name(String familymemb2name) {
		this.familymemb2name = familymemb2name;
	}

	public String getFamilymemb2relation() {
		return familymemb2relation;
	}

	public void setFamilymemb2relation(String familymemb2relation) {
		this.familymemb2relation = familymemb2relation;
	}

	public String getFamilymemb2sex() {
		return familymemb2sex;
	}

	public void setFamilymemb2sex(String familymemb2sex) {
		this.familymemb2sex = familymemb2sex;
	}

	public String getFamilymemb2age() {
		return familymemb2age;
	}

	public void setFamilymemb2age(String familymemb2age) {
		this.familymemb2age = familymemb2age;
	}

	public String getFamilymemb2occup() {
		return familymemb2occup;
	}

	public void setFamilymemb2occup(String familymemb2occup) {
		this.familymemb2occup = familymemb2occup;
	}

	public String getFamilymemb2qualification() {
		return familymemb2qualification;
	}

	public void setFamilymemb2qualification(String familymemb2qualification) {
		this.familymemb2qualification = familymemb2qualification;
	}

	public String getFamilymemb3name() {
		return familymemb3name;
	}

	public void setFamilymemb3name(String familymemb3name) {
		this.familymemb3name = familymemb3name;
	}

	public String getFamilymemb3relation() {
		return familymemb3relation;
	}

	public void setFamilymemb3relation(String familymemb3relation) {
		this.familymemb3relation = familymemb3relation;
	}

	public String getFamilymemb3sex() {
		return familymemb3sex;
	}

	public void setFamilymemb3sex(String familymemb3sex) {
		this.familymemb3sex = familymemb3sex;
	}

	public String getFamilymemb3age() {
		return familymemb3age;
	}

	public void setFamilymemb3age(String familymemb3age) {
		this.familymemb3age = familymemb3age;
	}

	public String getFamilymemb3occup() {
		return familymemb3occup;
	}

	public void setFamilymemb3occup(String familymemb3occup) {
		this.familymemb3occup = familymemb3occup;
	}

	public String getFamilymemb3qualification() {
		return familymemb3qualification;
	}

	public void setFamilymemb3qualification(String familymemb3qualification) {
		this.familymemb3qualification = familymemb3qualification;
	}

	public String getLoanamountdue() {
		return loanamountdue;
	}

	public void setLoanamountdue(String loanamountdue) {
		this.loanamountdue = loanamountdue;
	}

	public String getLivestock() {
		return livestock;
	}

	public void setLivestock(String livestock) {
		this.livestock = livestock;
	}

	public String getAgrlimpl() {
		return agrlimpl;
	}

	public void setAgrlimpl(String agrlimpl) {
		this.agrlimpl = agrlimpl;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getAssetvehicle() {
		return assetvehicle;
	}

	public void setAssetvehicle(String assetvehicle) {
		this.assetvehicle = assetvehicle;
	}

	public String getLoanreuired() {
		return loanreuired;
	}

	public void setLoanreuired(String loanreuired) {
		this.loanreuired = loanreuired;
	}

	public String getAssetanyother() {
		return assetanyother;
	}

	public void setAssetanyother(String assetanyother) {
		this.assetanyother = assetanyother;
	}

	public String getLoanaccnomineename() {
		return loanaccnomineename;
	}

	public void setLoanaccnomineename(String loanaccnomineename) {
		this.loanaccnomineename = loanaccnomineename;
	}

	public String getLoansource() {
		return loansource;
	}

	public void setLoansource(String loansource) {
		this.loansource = loansource;
	}

	public String getLoanaccnomineerelation() {
		return loanaccnomineerelation;
	}

	public void setLoanaccnomineerelation(String loanaccnomineerelation) {
		this.loanaccnomineerelation = loanaccnomineerelation;
	}

	public String getLoanpurpose() {
		return loanpurpose;
	}

	public void setLoanpurpose(String loanpurpose) {
		this.loanpurpose = loanpurpose;
	}

	public String getLoanaccnomineeage() {
		return loanaccnomineeage;
	}

	public void setLoanaccnomineeage(String loanaccnomineeage) {
		this.loanaccnomineeage = loanaccnomineeage;
	}

	public String getAmountborrowed() {
		return amountborrowed;
	}

	public void setAmountborrowed(String amountborrowed) {
		this.amountborrowed = amountborrowed;
	}

	public String getLoanaccnomineeresident() {
		return loanaccnomineeresident;
	}

	public void setLoanaccnomineeresident(String loanaccnomineeresident) {
		this.loanaccnomineeresident = loanaccnomineeresident;
	}

	public String getIsfpcaptured() {
		return isfpcaptured;
	}

	public void setIsfpcaptured(String isfpcaptured) {
		this.isfpcaptured = isfpcaptured;
	}

//	public String getFoanamountsactioned() {
//		return foanamountsactioned;
//	}
//
//	public void setFoanamountsactioned(String foanamountsactioned) {
//		this.foanamountsactioned = foanamountsactioned;
//	}

	public String getNoofinstalments() {
		return noofinstalments;
	}

	public void setNoofinstalments(String noofinstalments) {
		this.noofinstalments = noofinstalments;
	}

	public String getInterestrate() {
		return interestrate;
	}

	public void setInterestrate(String interestrate) {
		this.interestrate = interestrate;
	}

	public String getInstalmentamount() {
		return instalmentamount;
	}

	public void setInstalmentamount(String instalmentamount) {
		this.instalmentamount = instalmentamount;
	}

	public String getActivitycode() {
		return activitycode;
	}

	public void setActivitycode(String activitycode) {
		this.activitycode = activitycode;
	}

	public String getRunningsince() {
		return runningsince;
	}

	public void setRunningsince(String runningsince) {
		this.runningsince = runningsince;
	}

	public String getYearsofexperience() {
		return yearsofexperience;
	}

	public void setYearsofexperience(String yearsofexperience) {
		this.yearsofexperience = yearsofexperience;
	}

	public String getSavingopenedstatus() {
		return savingopenedstatus;
	}

	public void setSavingopenedstatus(String savingopenedstatus) {
		this.savingopenedstatus = savingopenedstatus;
	}

	public String getSavingopenedcreateddate() {
		return savingopenedcreateddate;
	}

	public void setSavingopenedcreateddate(String savingopenedcreateddate) {
		this.savingopenedcreateddate = savingopenedcreateddate;
	}

	public String getLoanopenedstatus() {
		return loanopenedstatus;
	}

	public void setLoanopenedstatus(String loanopenedstatus) {
		this.loanopenedstatus = loanopenedstatus;
	}

	public String getLoandisbursedcreateddate() {
		return loandisbursedcreateddate;
	}

	public void setLoandisbursedcreateddate(String loandisbursedcreateddate) {
		this.loandisbursedcreateddate = loandisbursedcreateddate;
	}

	public String getLoandisbursedstatus() {
		return loandisbursedstatus;
	}

	public void setLoandisbursedstatus(String loandisbursedstatus) {
		this.loandisbursedstatus = loandisbursedstatus;
	}

	public String getLoanopenedcreateddate() {
		return loanopenedcreateddate;
	}

	public void setLoanopenedcreateddate(String loanopenedcreateddate) {
		this.loanopenedcreateddate = loanopenedcreateddate;
	}

	public String getPermvillage() {
		return permvillage;
	}

	public void setPermvillage(String permvillage) {
		this.permvillage = permvillage;
	}

	public String getCommvillage() {
		return commvillage;
	}

	public void setCommvillage(String commvillage) {
		this.commvillage = commvillage;
	}

	public String getPermsubdistrict() {
		return permsubdistrict;
	}

	public void setPermsubdistrict(String permsubdistrict) {
		this.permsubdistrict = permsubdistrict;
	}

	public String getApppushstatus() {
		return apppushstatus;
	}

	public void setApppushstatus(String apppushstatus) {
		this.apppushstatus = apppushstatus;
	}

	public String getSavingaccnumber() {
		return savingaccnumber;
	}

	public void setSavingaccnumber(String savingaccnumber) {
		this.savingaccnumber = savingaccnumber;
	}

	public String getBankcustid() {
		return bankcustid;
	}

	public void setBankcustid(String bankcustid) {
		this.bankcustid = bankcustid;
	}

	public String getMclr() {
		return mclr;
	}

	public void setMclr(String mclr) {
		this.mclr = mclr;
	}

	public String getMclrMaleFemale() {
		return mclrMaleFemale;
	}

	public void setMclrMaleFemale(String mclrMaleFemale) {
		this.mclrMaleFemale = mclrMaleFemale;
	}

	public String getEkycApplicantphoto() {
		return ekycApplicantphoto;
	}

	public void setEkycApplicantphoto(String ekycApplicantphoto) {
		this.ekycApplicantphoto = ekycApplicantphoto;
	}

	public String getApplicationid() {
		return applicationid;
	}

	public void setApplicationid(String applicationid) {
		this.applicationid = applicationid;
	}

	public String getAppl1name() {
		return appl1name;
	}

	public void setAppl1name(String appl1name) {
		this.appl1name = appl1name;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getPermvillcity() {
		return permvillcity;
	}

	public void setPermvillcity(String permvillcity) {
		this.permvillcity = permvillcity;
	}

	public String getBcupdateddatetime() {
		return bcupdateddatetime;
	}

	public void setBcupdateddatetime(String bcupdateddatetime) {
		this.bcupdateddatetime = bcupdateddatetime;
	}

//	public String getUhRecommendedLoanamount() {
//		return UhRecommendedLoanamount;
//	}
//
//	public void setUhRecommendedLoanamount(String uhRecommendedLoanamount) {
//		UhRecommendedLoanamount = uhRecommendedLoanamount;
//	}

	public String getFxupdateddatetime() {
		return fxupdateddatetime;
	}

	public void setFxupdateddatetime(String fxupdateddatetime) {
		this.fxupdateddatetime = fxupdateddatetime;
	}

	public String getTeupdateddatetime() {
		return teupdateddatetime;
	}

	public void setTeupdateddatetime(String teupdateddatetime) {
		this.teupdateddatetime = teupdateddatetime;
	}

	public String getUhupdateddatetime() {
		return uhupdateddatetime;
	}

	public void setUhupdateddatetime(String uhupdateddatetime) {
		this.uhupdateddatetime = uhupdateddatetime;
	}

	public String getBankverificationstatus() {
		return bankverificationstatus;
	}

	public void setBankverificationstatus(String bankverificationstatus) {
		this.bankverificationstatus = bankverificationstatus;
	}

	public String getBankupdateddatetime() {
		return bankupdateddatetime;
	}

	public void setBankupdateddatetime(String bankupdateddatetime) {
		this.bankupdateddatetime = bankupdateddatetime;
	}

	public String getSolid() {
		return solid;
	}

	public void setSolid(String solid) {
		this.solid = solid;
	}

	public String getLoanamountsactioned() {
		return loanamountsactioned;
	}

	public void setLoanamountsactioned(String loanamountsactioned) {
		this.loanamountsactioned = loanamountsactioned;
	}

	public String getBankremarks() {
		return bankremarks;
	}

	public void setBankremarks(String bankremarks) {
		this.bankremarks = bankremarks;
	}

	public String getDobdate() {
		return dobdate;
	}

	public void setDobdate(String dobdate) {
		this.dobdate = dobdate;
	}

	public String getAppl1address() {
		return appl1address;
	}

	public void setAppl1address(String appl1address) {
		this.appl1address = appl1address;
	}

	public String getUhRecommendedNoofmonths() {
		return uhRecommendedNoofmonths;
	}

	public void setUhRecommendedNoofmonths(String uhRecommendedNoofmonths) {
		this.uhRecommendedNoofmonths = uhRecommendedNoofmonths;
	}

	public String getUhRecommendedInstalmentamount() {
		return uhRecommendedInstalmentamount;
	}

	public void setUhRecommendedInstalmentamount(String uhRecommendedInstalmentamount) {
		this.uhRecommendedInstalmentamount = uhRecommendedInstalmentamount;
	}

	public String getApplicantphotopath() {
		return applicantphotopath;
	}

	public void setApplicantphotopath(String applicantphotopath) {
		this.applicantphotopath = applicantphotopath;
	}

	@Override
	public String toString() {
		return "Loanorigination [applicationid=" + applicationid + ", bcupdateddatetime=" + bcupdateddatetime
				+ ", custtypecode=" + custtypecode + ", mobileno=" + mobileno + ", bcverificationstatus="
				+ bcverificationstatus + ", applicationno=" + applicationno + ", groupid=" + groupid + ", groupname="
				+ groupname + ", custinfo=" + custinfo + ", newcust=" + newcust + ", subkid=" + subkid + ", sbaccno="
				+ sbaccno + ", custid=" + custid + ", bcomobile=" + bcomobile + ", bcopin=" + bcopin + ", savingacinfo="
				+ savingacinfo + ", bcoid=" + bcoid + ", bconame=" + bconame + ", custtitle=" + custtitle
				+ ", custtitlecode=" + custtitlecode + ", appl1name=" + appl1name + ", familyexp=" + familyexp
				+ ", networthcode=" + networthcode + ", constitutioncode=" + constitutioncode + ", castecode="
				+ castecode + ", communitycode=" + communitycode + ", schemecode=" + schemecode
				+ ", introducerstatuscode=" + introducerstatuscode + ", annualturnovercode=" + annualturnovercode
				+ ", maritalstatutcode=" + maritalstatutcode + ", glsubheadcode=" + glsubheadcode + ", srcofincomecode="
				+ srcofincomecode + ", nomineedetails=" + nomineedetails + ", applicantaddress=" + applicantaddress
				+ ", permpincode=" + permpincode + ", electrbill=" + electrbill + ", permdistrict=" + permdistrict
				+ ", studyexp=" + studyexp + ", permstate=" + permstate + ", blockcode=" + blockcode
				+ ", insuranceprem=" + insuranceprem + ", fuelchrgs=" + fuelchrgs + ", permlandmark=" + permlandmark
				+ ", televchrgs=" + televchrgs + ", permstreetno=" + permstreetno + ", internetchrgs=" + internetchrgs
				+ ", jlggroupname=" + jlggroupname + ", jlgleader=" + jlgleader + ", familysust=" + familysust
				+ ", appl1careof=" + appl1careof + ", mobilechrgs=" + mobilechrgs + ", permhouseno=" + permhouseno
				+ ", bankremarks=" + bankremarks + ", uhremarks=" + uhremarks + ", uhverificationstatus="
				+ uhverificationstatus + ", teremarks=" + teremarks + ", teverificationstatus=" + teverificationstatus
				+ ", fxremarks=" + fxremarks + ", fxverificationstatus=" + fxverificationstatus + ", permvillcity="
				+ permvillcity + ",  fxupdateddatetime="
				+ fxupdateddatetime + ", teupdateddatetime=" + teupdateddatetime + ", uhupdateddatetime="
				+ uhupdateddatetime + ", bankverificationstatus=" + bankverificationstatus + ", bankupdateddatetime="
				+ bankupdateddatetime + ", solid=" + solid + ", dbaction=" + dbaction + ", dobdate=" + dobdate
				+ ", appl1address=" + appl1address + ", uhRecommendedNoofmonths=" + uhRecommendedNoofmonths
				+ ", uhRecommendedInstalmentamount=" + uhRecommendedInstalmentamount + ", applicantphotopath="
				+ applicantphotopath + ", panchtax=" + panchtax + ", telephone=" + telephone + ", miscexp=" + miscexp
				+ ", totalyrlyfamexp=" + totalyrlyfamexp + ", yrlysurplus=" + yrlysurplus + ", idproof=" + idproof
				+ ", idproofimage=" + idproofimage + ", avgweeklysurplus=" + avgweeklysurplus + ", addressproof="
				+ addressproof + ", addressproofimage=" + addressproofimage + ", loanamount=" + loanamount
				+ ", pmsbypmjjbycovrg=" + pmsbypmjjbycovrg + ", approachfor=" + approachfor + ", pmsbypmjjbyvalidity="
				+ pmsbypmjjbyvalidity + ", presentfinancer=" + presentfinancer + ", appl1occupation=" + appl1occupation
				+ ", repayperiod=" + repayperiod + ", apprxyrlyapp1income=" + apprxyrlyapp1income
				+ ", weeklyinstalment=" + weeklyinstalment + ", familymembsoccupation=" + familymembsoccupation
				+ ", bankmitraremarks=" + bankmitraremarks + ", yrlyothfamilymembincome=" + yrlyothfamilymembincome
				+ ", bankmanagremarks=" + bankmanagremarks + ", totalyrlyfamilyincome=" + totalyrlyfamilyincome
				+ ", bcrecomendation=" + bcrecomendation + ", bcremarks=" + bcremarks + ", newsubkid=" + newsubkid
				+ ", appl1image=" + appl1image + ", permhusfathname=" + permhusfathname + ", permreltype=" + permreltype
				+ ", permtelephone=" + permtelephone + ", commhusfathname=" + commhusfathname + ", commrelationtype="
				+ commrelationtype + ", commhouseno=" + commhouseno + ", commstreetno=" + commstreetno
				+ ", commlandmark=" + commlandmark + ", commvillcity=" + commvillcity + ", commdistrict=" + commdistrict
				+ ", commstate=" + commstate + ", commpincode=" + commpincode + ", commtelephone=" + commtelephone
				+ ", areavillagecode=" + areavillagecode + ", areablockcode=" + areablockcode + ", areadistcode="
				+ areadistcode + ", areastatecode=" + areastatecode + ", fhgtype=" + fhgtype + ", fhgname=" + fhgname
				+ ", operationmode=" + operationmode + ", sex=" + sex + ", pangirno=" + pangirno + ", annualturnover="
				+ annualturnover + ", category=" + category + ", qualification=" + qualification + ", estatement="
				+ estatement + ", houseloan=" + houseloan + ", chequebook=" + chequebook + ", vehicleloan="
				+ vehicleloan + ", mobilebanking=" + mobilebanking + ", mutualfund=" + mutualfund + ", internetbanking="
				+ internetbanking + ", lifeinsurance=" + lifeinsurance + ", creditcard=" + creditcard + ", pension="
				+ pension + ", addonothers=" + addonothers + ", crosssellothers=" + crosssellothers + ", atmrequest="
				+ atmrequest + ", smsalert=" + smsalert + ", emailid=" + emailid + ", idprooftype=" + idprooftype
				+ ", addressprooftype=" + addressprooftype + ", introducertitle=" + introducertitle
				+ ", introducername=" + introducername + ", introducercustid=" + introducercustid + ", introduceraccno="
				+ introduceraccno + ", introducerknownyear=" + introducerknownyear + ", nomineeaccnature="
				+ nomineeaccnature + ", nomineeage=" + nomineeage + ", nomineeaccno=" + nomineeaccno + ", nomineedob="
				+ nomineedob + ", nomineeaddetails=" + nomineeaddetails + ", nomineegaurdianname=" + nomineegaurdianname
				+ ", nomineename=" + nomineename + ", nomineeaddress=" + nomineeaddress + ", nomineeguardianage="
				+ nomineeguardianage + ", nomineerelation=" + nomineerelation + ", nomineeguardianadd="
				+ nomineeguardianadd + ", minornomiguartitle=" + minornomiguartitle + ", minornomiguarname="
				+ minornomiguarname + ", minornomiguarAge=" + minornomiguarAge + ", minornomiguarreltype="
				+ minornomiguarreltype + ", minornomiguarrelcode=" + minornomiguarrelcode + ", nomineecity="
				+ nomineecity + ", witness1name=" + witness1name + ", witness2name=" + witness2name + ", witness1add="
				+ witness1add + ", witness2add=" + witness2add + ", witness1place=" + witness1place + ", witness2place="
				+ witness2place + ", witness1telephone=" + witness1telephone + ", witness2telephone="
				+ witness2telephone + ", appl2name=" + appl2name + ", appl2pincode=" + appl2pincode + ", appl2fhgname="
				+ appl2fhgname + ", appl2telephone=" + appl2telephone + ", appl2careof=" + appl2careof
				+ ", appl2mobile=" + appl2mobile + ", appl2houseno=" + appl2houseno + ", appl2sex=" + appl2sex
				+ ", appl2dob=" + appl2dob + ", appl2landmark=" + appl2landmark + ", appl2occupation=" + appl2occupation
				+ ", appl2villcity=" + appl2villcity + ", appl2category=" + appl2category + ", appl2district="
				+ appl2district + ", appl2idproof=" + appl2idproof + ", appl2state=" + appl2state + ", appl2addproof="
				+ appl2addproof + ", custpin=" + custpin + ", confirmcustpin=" + confirmcustpin + ", familymemb1name="
				+ familymemb1name + ", familymemb1relation=" + familymemb1relation + ", familymemb1sex="
				+ familymemb1sex + ", familymemb1age=" + familymemb1age + ", familymemb1Occup=" + familymemb1Occup
				+ ", familymemb1qualification=" + familymemb1qualification + ", familymemb2name=" + familymemb2name
				+ ", familymemb2relation=" + familymemb2relation + ", familymemb2sex=" + familymemb2sex
				+ ", familymemb2age=" + familymemb2age + ", familymemb2occup=" + familymemb2occup
				+ ", familymemb2qualification=" + familymemb2qualification + ", familymemb3name=" + familymemb3name
				+ ", familymemb3relation=" + familymemb3relation + ", familymemb3sex=" + familymemb3sex
				+ ", familymemb3age=" + familymemb3age + ", familymemb3occup=" + familymemb3occup
				+ ", familymemb3qualification=" + familymemb3qualification + ", loanamountdue=" + loanamountdue
				+ ", livestock=" + livestock + ", agrlimpl=" + agrlimpl + ", activity=" + activity + ", assetvehicle="
				+ assetvehicle + ", loanreuired=" + loanreuired + ", assetanyother=" + assetanyother
				+ ", loanaccnomineename=" + loanaccnomineename + ", loansource=" + loansource
				+ ", loanaccnomineerelation=" + loanaccnomineerelation + ", loanpurpose=" + loanpurpose
				+ ", loanaccnomineeage=" + loanaccnomineeage + ", amountborrowed=" + amountborrowed
				+ ", loanaccnomineeresident=" + loanaccnomineeresident + ", isfpcaptured=" + isfpcaptured
				+ ", noofinstalments=" + noofinstalments + ", interestrate=" + interestrate + ", instalmentamount="
				+ instalmentamount + ", activitycode=" + activitycode + ", runningsince=" + runningsince
				+ ", yearsofexperience=" + yearsofexperience + ", savingopenedstatus=" + savingopenedstatus
				+ ", savingopenedcreateddate=" + savingopenedcreateddate + ", loanopenedstatus=" + loanopenedstatus
				+ ", loandisbursedcreateddate=" + loandisbursedcreateddate + ", loandisbursedstatus="
				+ loandisbursedstatus + ", loanopenedcreateddate=" + loanopenedcreateddate + ", permvillage="
				+ permvillage + ", commvillage=" + commvillage + ", permsubdistrict=" + permsubdistrict
				+ ", apppushstatus=" + apppushstatus
				+ ", savingaccnumber=" + savingaccnumber + ", bankcustid=" + bankcustid + ", mclr=" + mclr
				+ ", mclrMaleFemale=" + mclrMaleFemale + ", ekycApplicantphoto=" + ekycApplicantphoto + "]";
	}

}