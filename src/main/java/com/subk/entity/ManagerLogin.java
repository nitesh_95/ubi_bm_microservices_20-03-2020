package com.subk.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ManagerLogin")
public class ManagerLogin {
	
	@Id
	@Column(name = "Solid")
	private String solid;
	@Column(name = "Branchname")
	private String branchName;
	@Column(name = "Createddatetime")
	private String createddatetime;
	@Column(name = "Status")
	private String status;
	@Column(name = "Password")
	private String password;
	@Column(name = "Roid")
	private int roid;
	public String getSolid() {
		return solid;
	}
	public void setSolid(String solid) {
		this.solid = solid;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getCreateddatetime() {
		return createddatetime;
	}
	public void setCreateddatetime(String createddatetime) {
		this.createddatetime = createddatetime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public int getRoid() {
		return roid;
	}
	public void setRoid(int roid) {
		this.roid = roid;
	}
	@Override
	public String toString() {
		return "ManagerLogin [solid=" + solid + ", branchName=" + branchName + ", createddatetime=" + createddatetime
				+ ", status=" + status + ", password=" + password + ", roid=" + roid + "]";
	}
	
	
	
	

}
