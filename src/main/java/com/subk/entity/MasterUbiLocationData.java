package com.subk.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MasterUbiLocationData")
public class MasterUbiLocationData {
	@Id
	@Column(name = "Id")
	private int id;
	@Column(name = "Statedesc")
	private String statedesc;
	@Column(name = "Statecode")
	private String statecode;
	@Column(name = "Districtcode")
	private String districtcode;
	@Column(name = "Districtdesc")
	private String districtdesc;
	@Column(name = "Subdistrictcode")
	private String subdistrictcode;
	@Column(name = "Subdistrictdesc")
	private String subdistrictdesc;
	@Column(name = "Villagecode")
	private String villagecode;
	@Column(name = "Villagedesc")
	private String villagedesc;
	@Column(name = "Areastatecode")
	private String areastatecode;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStatedesc() {
		return statedesc;
	}
	public void setStatedesc(String statedesc) {
		this.statedesc = statedesc;
	}
	public String getStatecode() {
		return statecode;
	}
	public void setStatecode(String statecode) {
		this.statecode = statecode;
	}
	public String getDistrictcode() {
		return districtcode;
	}
	public void setDistrictcode(String districtcode) {
		this.districtcode = districtcode;
	}
	public String getDistrictdesc() {
		return districtdesc;
	}
	public void setDistrictdesc(String districtdesc) {
		this.districtdesc = districtdesc;
	}
	public String getSubdistrictcode() {
		return subdistrictcode;
	}
	public void setSubdistrictcode(String subdistrictcode) {
		this.subdistrictcode = subdistrictcode;
	}
	public String getSubdistrictdesc() {
		return subdistrictdesc;
	}
	public void setSubdistrictdesc(String subdistrictdesc) {
		this.subdistrictdesc = subdistrictdesc;
	}
	public String getVillagecode() {
		return villagecode;
	}
	public void setVillagecode(String villagecode) {
		this.villagecode = villagecode;
	}
	public String getVillagedesc() {
		return villagedesc;
	}
	public void setVillagedesc(String villagedesc) {
		this.villagedesc = villagedesc;
	}
	public String getAreastatecode() {
		return areastatecode;
	}
	public void setAreastatecode(String areastatecode) {
		this.areastatecode = areastatecode;
	}
	@Override
	public String toString() {
		return "MasterUbiLocationData [id=" + id + ", statedesc=" + statedesc + ", statecode=" + statecode
				+ ", districtcode=" + districtcode + ", districtdesc=" + districtdesc + ", subdistrictcode="
				+ subdistrictcode + ", subdistrictdesc=" + subdistrictdesc + ", villagecode=" + villagecode
				+ ", villagedesc=" + villagedesc + ", areastatecode=" + areastatecode + "]";
	}
	
}
