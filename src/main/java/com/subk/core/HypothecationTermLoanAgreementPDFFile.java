/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.core;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import com.subk.entity.Loanorigination;
import com.subk.util.AddressCodes;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 *
 * @author Ramya
 */
@Configuration
public class HypothecationTermLoanAgreementPDFFile extends PDFGenerationAbstract {
	@Autowired
	AddressCodes addresscodes;
	
	@Value("${MCLRL}")
	private String mclrl;  
    private String empty = " ";
    private static final Logger logger = Logger.getLogger(GenerateUndertakingPDFFile.class);
//    private static final ResourceBundle rb = ResourceBundle.getBundle("systemPath");
//    private static final ResourceBundle rb1 = ResourceBundle.getBundle("com.subk.Resources//subkResources");

    public String getPdf(Loanorigination loanData, String fileName,String pathAdd, String img, String jsPaths) {
        logger.info("************ ENTERED ************* ");

        String destPath = null;

        logger.debug("UBILoanOrigination Data: " + loanData.toString());
        logger.debug("FileName: " + fileName);

        try {
            destPath = pathAdd;
            destPath = destPath + fileName;

            //File file = new File(destPath);
            //file.getParentFile().mkdirs();
            new HypothecationTermLoanAgreementPDFFile().createPdf(destPath, loanData , img, jsPaths);
        } catch (Exception ex) {
            logger.error("Error:....!!! " + ex.getMessage());
        }

        logger.info("destPathINATION PATH:...........!!!!!!!!!!  " + destPath);
        logger.info("************ ENDED.************* ");
        return destPath;
    }

    public void createPdf(String dest, Loanorigination loanData , String img, String jsPaths) {
        logger.debug("***********Entered***********");
        final String imgPath = img;
        final String mclrVal = loanData.getMclr() != null && !loanData.getMclr().equals("") ? loanData.getMclr() : "  "; //rb1.getString("MCLRL");
        String mclrMF = null;
//        BigDecimal mclrMF = new BigDecimal("0.0");

        String gender = loanData.getSex();
        if (gender.equals("MALE")) {
            mclrMF = loanData.getMclrMaleFemale() != null && !loanData.getMclrMaleFemale().equals("") ? loanData.getMclrMaleFemale() : "";
//                    rb1.getString("MCLRL_MALE");
//                    loanData.getMclrMale();
        } else if (gender.equals("FEMALE")) {
            mclrMF = loanData.getMclrMaleFemale() != null && !loanData.getMclrMaleFemale().equals("") ? loanData.getMclrMaleFemale() : ""; //rb1.getString("MCLRL_FEMALE");
//                    loanData.getMclrFemale();
        }

        Document document = null;
        PdfWriter writer = null;

        try {
//            String appl1Address = (loanData.getAppl1address() != null && loanData.getAppl1address() != "" && !loanData.getAppl1address().equals("null") ? loanData.getAppl1address() : " ");
            document = new Document();
//            Rectangle rect= new Rectangle(36,108);
//            rect.setBorder(Rectangle.BOX);
//            rect.setBorderWidth(2);
//            document.add(rect);
            writer = PdfWriter.getInstance(document, new FileOutputStream(dest, false));
            //writer.setPageEvent(new Header());
            HeaderFooterPageEvent event = new HeaderFooterPageEvent();
            writer.setPageEvent(event);
            document.open();
            
            Font canFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
            Font canFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
            Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
            Font redFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

            Font font11b = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);

            Font font11bi = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLDITALIC);

            //File file = new File("C:\\"+loanData.getAppl1Name()+"_"+loanData.getApplicationId());
            // Get the output stream for writing PDF object
            //ServletOutputStream out = response.getOutputStream();
            //OutputStream oos = new FileOutputStream(file,true);
            //Document document = new Document();
            //PdfWriter writer = PdfWriter.getInstance(document, out);
//            document.open();
            document.addKeywords("Java, Servelet, PDF, iText");
            document.addAuthor("Raj Grover");
            document.addCreator("UBILoanOrigination");
            String applicantName = (loanData.getAppl1name() != null && loanData.getAppl1name() != "" && !loanData.getAppl1name().equals("null") ? loanData.getAppl1name() : " ");
            String applicantFhgName = (loanData.getFhgname() != null && loanData.getFhgname() != "" && !loanData.getFhgname().equals("null") ? loanData.getFhgname() : " ");
//            String applicantAddress = (loanData.getAppl1address() != null && loanData.getAppl1address() != "" && !loanData.getAppl1address().equals("null") ? loanData.getAppl1address() : " ");
            String permAddress = (loanData.getPermhouseno() != null && !loanData.getPermhouseno().equalsIgnoreCase("null") && loanData.getPermhouseno() != " " ? loanData.getPermhouseno()+ "," : " ");
            permAddress += (loanData.getPermstreetno() != null && !loanData.getPermstreetno().equalsIgnoreCase("null") && loanData.getPermstreetno() != " " ? loanData.getPermstreetno()+ "," : " ");
            permAddress += (loanData.getPermlandmark() != null && !loanData.getPermlandmark().equalsIgnoreCase("null") && loanData.getPermlandmark() != " " ? loanData.getPermlandmark()+ "," : " ");
            permAddress += (loanData.getPermvillage() != null && !loanData.getPermvillage().equalsIgnoreCase("null") && loanData.getPermvillage() != " " ? loanData.getPermvillage()+ "," : " ");
            permAddress += (loanData.getPermdistrict() != null && !loanData.getPermdistrict().equalsIgnoreCase("null") && loanData.getPermdistrict() != " " ? loanData.getPermdistrict()+ "," : " ");		
            permAddress += (loanData.getPermsubdistrict() != null && !loanData.getPermsubdistrict().equalsIgnoreCase("null") && loanData.getPermsubdistrict() != " " ? loanData.getPermsubdistrict()+ "," : " ");
            permAddress += (loanData.getPermstate() != null && !loanData.getPermstate().equalsIgnoreCase("null") && loanData.getPermstate() != " " ? loanData.getPermstate()+ "," : " ");
            permAddress += (loanData.getPermpincode() != null && !loanData.getPermpincode().equalsIgnoreCase("null") && loanData.getPermpincode() != " " ? loanData.getPermpincode()+ "," : " ");
            permAddress += (loanData.getPermtelephone() != null && !loanData.getPermtelephone().equalsIgnoreCase("null") && loanData.getPermtelephone() != " " ? loanData.getPermtelephone()+ "," : " ");
            
            String applicantAddress1=permAddress.toUpperCase();
            Image img2 = Image.getInstance(imgPath);
            img2.setAbsolutePosition(250f, 740f);
            img2.scaleAbsolute(80f, 80f);
            document.add(img2);
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("\n"));

            Paragraph pkk27 = new Paragraph("UNITED BANK OF INDIA", canFont);
            pkk27.setAlignment(Element.ALIGN_CENTER);
            //pkk26.setSpacingAfter(5);
            document.add(pkk27);

            Paragraph pkk28 = new Paragraph("AGREEMENT FOR TERM LOAN AND HYPOTHECATION", canFont);
            pkk28.setAlignment(Element.ALIGN_CENTER);
            //pkk26.setSpacingAfter(5);
            document.add(pkk28);

//            String applicantName = (loanData.getAppl1Name() != null && loanData.getAppl1Name() != "" && !loanData.getAppl1Name().equals("null") ? loanData.getAppl1Name() : " ");
//            String applicantFhgName = (loanData.getFhgName() != null && loanData.getFhgName() != "" && !loanData.getFhgName().equals("null") ? loanData.getFhgName() : " ");
//            String applicantAddress = (loanData.getAppl1Address() != null && loanData.getAppl1Address() != "" && !loanData.getAppl1Address().equals("null") ? loanData.getAppl1Address() : " ");
            String branchName = loanData.getBranchname();

            Paragraph pkk29 = new Paragraph();
            pkk29.setAlignment(Element.ALIGN_JUSTIFIED);
            pkk29.add(new Chunk("\nThis Agreement ", redFont));
            pkk29.add(new Chunk("...........................made at ", redFont));

            Chunk branchName_underline = new Chunk(branchName, redFont);
            branchName_underline.setUnderline(0.6f, -2f);
            pkk29.add(branchName_underline);

            pkk29.add(new Chunk(" the ..................... day "
                    + "of ...............................Two thousand "
                    + "and .............................Between ", redFont));

            Chunk applicantName_underline = new Chunk(applicantName, redFont1);
            applicantName_underline.setUnderline(0.6f, -2f);
            pkk29.add(applicantName_underline);

            pkk29.add(new Chunk(" S/o,W/o,D/o ", redFont));

            Chunk applicantFhgName_underline = new Chunk(applicantFhgName, redFont1);
            applicantFhgName_underline.setUnderline(0.6f, -2f);
            pkk29.add(applicantFhgName_underline);

            pkk29.add(new Chunk(" residing at address ", redFont));

            Chunk applicantAddress_underline = new Chunk(applicantAddress1, redFont1);
            applicantAddress_underline.setUnderline(0.6f, -2f);
            pkk29.add(applicantAddress_underline);

            pkk29.add(new Chunk(" referred to as the "
                    + "Borrower which expression shall, include heirs, executors, administrators and legal representatives of the One part and "
                    + "United Bank of India, a body corporate constituted under the Banking Companies (Acquisition and Transfer of "
                    + "Undertakings) Act, 1970 and having its Head Office at Kolkata, and a Branch Office amongst other places "
                    + "at ", redFont));

            pkk29.add(branchName_underline);
            pkk29.add(new Chunk(" (hereinafter referred to as The Bank which expression shall include "
                    + "its successors and assigns) of the other part.", redFont));

            pkk29.setAlignment(Element.ALIGN_JUSTIFIED);
//            pkk29.setSpacingAfter();
            document.add(pkk29);
            document.add(new Paragraph("\n"));
            String loanAmount = (loanData.getLoanamountsactioned() != null && loanData.getLoanamountsactioned() != " " && !loanData.getLoanamountsactioned().equals("null") ? loanData.getLoanamountsactioned() : "");
            String commaloanAmount = getIndianCurrencyFormat(loanAmount);

            //String loanAmount = (loanData.getLoanAmountSanctioned() != null && loanData.getLoanAmountSanctioned() != " " && !loanData.getLoanAmountSanctioned().equals("null") ? loanData.getLoanAmountSanctioned() : "");
            Paragraph pkk30 = new Paragraph();
            pkk30.setAlignment(Element.ALIGN_JUSTIFIED);
            pkk30.add(new Chunk("Where as ", redFont));
            pkk30.add(new Chunk("the Borrower has applied to the Bank for a loan to the extent of Rs ", redFont));
            Chunk loanAmount_underline = new Chunk(commaloanAmount, font11b);
            loanAmount_underline.setUnderline(0.6f, -2f);
            pkk30.add(loanAmount_underline);
            pkk30.add(new Chunk("/- for the express purpose and "
                    + "upon the basis set out in the Borrower�s Loan Application dated ", redFont));
            pkk30.add(underLine(loanData.getUhupdateddatetime() != null && !loanData.getUhupdateddatetime().equals("null")
                    && !loanData.getUhupdateddatetime().isEmpty() ? convertToDate(loanData.getUhupdateddatetime()) : empty));

            pkk30.add(new Chunk(" which the Bank agreed to do upon having ", redFont));
            pkk30.add(new Chunk("repayment thereof secured on the terms and conditions and in the manner hereinafter mentioned. ", redFont));
            document.add(pkk30);
            document.add(new Paragraph("\n"));
            Paragraph pkk31 = new Paragraph();
            pkk31.setAlignment(Element.ALIGN_JUSTIFIED);
            pkk31.add(new Chunk("NOW IN CONSIDERATION OF THE PREMISES IT IS HEREBY AGREED ", canFont1));
            pkk31.add(new Chunk(" by and between the parties as follows : ", canFont1));
            document.add(pkk31);

            String loanAmountSanctioned = (loanData.getLoanamountsactioned() != null && loanData.getLoanamountsactioned() != " " && !loanData.getLoanamountsactioned().equals("null") ? loanData.getLoanamountsactioned() : " ");
            //String loanAmountSanctioned = (loanData.getLoanAmountSanctioned() != null && loanData.getLoanAmountSanctioned() != " " && !loanData.getLoanAmountSanctioned().equals("null") ? loanData.getLoanAmountSanctioned() : " ");
            String commaloanAmountSanctioned = getIndianCurrencyFormat(loanAmountSanctioned);
            Paragraph pkk32 = new Paragraph();
             pkk32.setAlignment(Element.ALIGN_JUSTIFIED);
            pkk32.add(new Chunk("1. (a) The Borrower shall repay to the Bank at..................................the said loan of "
                    + "Rs ", redFont));
            Chunk loanSanctionedAmount_underline = new Chunk(commaloanAmountSanctioned, font11b);
            loanSanctionedAmount_underline.setUnderline(0.6f, -2f);
            pkk32.add(loanSanctionedAmount_underline);

            pkk32.add(new Chunk("/- by the installments, on the respective days and in the manner mentioned in Schedule hereto "
                    + "provided that if the amount of loan actually disbursed is less than Rs ", redFont));
            pkk32.add(loanSanctionedAmount_underline);

            pkk32.add(new Chunk("/- then the amount of installments payable as aforesaid shall be reduced pro-rata but the same shall be payable on the stipulated days "
                    + "mentioned in Schedule of repayment.", redFont));

            document.add(pkk32);
//            document.add(new Paragraph("\n"));
            Paragraph pkk33 = new Paragraph("(b) The Borrower shall so long as the said loan or any part thereof remains unpaid pay to the Bank interest thereon or on "
                    + "the balance thereof due from time to time at the rate of ", redFont);
            pkk33.setAlignment(Element.ALIGN_JUSTIFIED);
            String mclrMFFinal = ((String.valueOf(mclrMF)) != null && !((String.valueOf(mclrMF)).equals("")) && (String.valueOf(mclrMF)) != "null" ? (String.valueOf(mclrMF)) : " ");
            Chunk roi = new Chunk("  " + mclrMFFinal + "  ", redFont);
            roi.setUnderline(0.6f, -2f);
            pkk33.add(roi);
//            pkk33.add(underLine(mclrMFFinal));
            pkk33.add(" % per annum over the United "
                    + "Bank of India MCLR-Y which is at present ");
            Chunk mclrval1 = new Chunk("  " + String.valueOf(mclrVal) + "  ", redFont);
            mclrval1.setUnderline(0.6f, -2f);
            pkk33.add(mclrval1);
//            pkk33.add(underLine(String.valueOf(mclrVal)));
            pkk33.add(" % p.a., with ");
            Chunk mon = new Chunk(" monthly " , redFont);
            mon.setUnderline(0.6f, -2f);
            pkk33.add(mon);
            pkk33.add("rests for value received/with "
                    + "interest thereon at the rate of ..........");
//            pkk33.add(underLine(String.valueOf(appropriateRate)));
            pkk33.add("% p.a. with .........");
//            pkk33.add(underLine((loanData.getNoOfInstalments()!= null && !loanData.getNoOfInstalments().equals("null")
//                    && !loanData.getNoOfInstalments().isEmpty() ? loanData.getNoOfInstalments() : empty)));
            pkk33.add(" rests for value received plus ");
            //pkk33.add(underLine(String.valueOf(appropriateRate)));
            pkk33.add("........... % p.a. additional rate in the event of default by the Borrower in punctual payment of "
                    + "interest and / or of any installments of principal with quarterly/half yearly rests in 30th day of March, June, "
                    + "September and December in each year and so that on default in payment of interest as stipulated above interest "
                    + "shall be charged and payable on the footing of compound interest at the rates and in the manner mentioned "
                    + "above and all such interest (including compound and penal interest) shall be calculated and charged in "
                    + "accordance with the usual practice of the Bank and shall be hereby secured.");
            document.add(pkk33);
             document.add(new Paragraph("\n"));
            Paragraph pkk35 = new Paragraph("2. The borrower both hereby hypothecate and charge by way of first charge in favour of the Bank the "
                    + "equipment......................... kept or found at.......................................which may "
                    + "at any time hereafter during the continuance of this security be kept or found upon or in the borrowers premises/at other place, ", redFont);
                    
            pkk35.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk35);
            document.newPage();
            Paragraph pk35 = new Paragraph("situated in vill : ......................Taluka........................................ "
                    + "district................................. or in the cause of transit of wherever else the same may be or held by any party anywhere to "
                    + "the order and disposition of the Borrower and whether now belonging or which may at any time hereafter during the "
                    + "continuance of this security belong to Borrower (hereinafter for security�s save collectively referred to as Hypothecation "
                    + "of.................................................)", redFont);
            pk35.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pk35);
            document.add(new Paragraph("\n"));
            Paragraph pkk36 = new Paragraph("(OR)", redFont);
            pkk36.setAlignment(Element.ALIGN_CENTER);
            document.add(pkk36);
//            document.add(new Paragraph("\n"));
            Paragraph pkk37 = new Paragraph("3. The Borrower both hereby hypothecate and charge by way of first charge in favour of the Bank the entire live-stock "
                    + "of the Borrower including buffaloes, cows, goats, dry cattle, bulls, young calves, sheep, lambs and also horses, donkeys, "
                    + "ponies, camels, pigs and all other animals whatsoever now stabled or kept or found which may at any time hereafter during "
                    + "the continuance of this security be stabled or kept or found upon or in the Borrower�s premises, dairy farm, farm house, "
                    + "shed, Barn and /or stables situated in Village............. Taluka......................... District............................... Or at any other "
                    + "place or in the Course of transit of wherever else the same may be or held by any party any where to the order and "
                    + "disposition of the Borrower and whether now belonging or which may at any time hereafter during the continuance of this "
                    + "security belong to the Borrower (hereinafter for brevity�s sake collectively referred to as  ", redFont);
            pkk37.setAlignment(Element.ALIGN_JUSTIFIED);

// Paragraph pbranchname = new Paragraph();
            Chunk underline = new Chunk("''the Hypothecated cattle livestock'').",font11b);
            underline.setUnderline(0.6f, -2f);
            pkk37.add(underline);

            document.add(pkk37);
//            document.newPage();
            document.add(new Paragraph("\n"));
            Paragraph pkk40 = new Paragraph();
            pkk40.add(new Chunk("And also ", redFont));
            pkk40.add(new Chunk("the entire stock of cattle feed, fodder, dairy equipments such as milk pans, fat testing machines, cream "
                    + "separators, weighing/measuring equipments, dairy utensils, and all other machinery and plant and equipments as also all "
                    + "other tools, implements, utensils, applicances and things with all the spares, stores replacements and accessories "
                    + "whatsoever pertaining thereto as also any carts, tongas and all other vehicles, what ever which may be stored or lying on "
                    + "the Borrower�s said premises dairy farm, farm house, shed, or any part thereof or in any godown or anywhere else or "
                    + "which may be in the course of transit or held by any party any where to the order and disposition of the Borrower and "
                    + "whether now belonging or which may at any time hereafter during the continuance of this security belong to the Borrower "
                    + "(hereinafter for brevity�s sake collectively referred to as ", redFont));
            pkk40.setAlignment(Element.ALIGN_JUSTIFIED);

            Chunk underline1 = new Chunk("''the hypothecated callte-stock and equipment.'').", font11b);
            underline1.setUnderline(0.6f, -2f);
            pkk40.add(underline1);
            document.add(pkk40);
            document.add(new Paragraph("\n"));

            Paragraph pkk38 = new Paragraph("(OR)", redFont);
            pkk38.setAlignment(Element.ALIGN_CENTER);
            document.add(pkk38);
//             document.add(new Paragraph("\n"));
            Paragraph pkk39 = new Paragraph("4. And also the entire stock of hens, cocks, chicks, chickens, broilers fowls and other domestic birds of the "
                    + "Borrower now kept or contained or which may at any time hereafter during the continuance of the securities be kept or "
                    + "stocked upon the Borrower's premises/poultry farm/farm house situated in the Village of ........................................ "
                    + "Taluka .............................. District........................................... or any other place or in course of transit wherever else the "
                    + "same may be of held by any party any where to the order and disposition of the Borrower and whether now belonging or "
                    + "which may at any time hereafter during the continuance of this security belong to the Borrower (and all of which are "
                    + "hereafter for brevitys sake collectively referred to as ", redFont);
            pkk39.setAlignment(Element.ALIGN_JUSTIFIED);

            Chunk underline2 = new Chunk("the hypothecated poultry livestock).", font11b);
            underline2.setUnderline(0.6f, -2f);
            pkk39.add(underline2);

            document.add(pkk39);
            document.add(new Paragraph("\n"));

            Paragraph pk41 = new Paragraph();
            pk41.setAlignment(Element.ALIGN_JUSTIFIED);
            pk41.add(new Chunk("And also ", redFont));
            pk41.add(new Chunk("all the stock of poultry food of whatever kind and nature, eggs, brooders, waterers chick, feeders, layer "
                    + "feeders, egg trays and boxes, poultry crate, incubator, hatcher, egg grading equipment air conditioner, feed grinder and "
                    + "other machinery, plant and equipments whatsoever together with all the spares, store, replacement and accessories "
                    + "whatsoever pertaining there to which may be stored upon or enclosed or installed or be lying in the Borrower�s said "
                    + "premises/poultry farm/farm house of any part thereof anywhere", redFont));
               document.add(pk41);
               document.newPage();
               Paragraph pkk41 = new Paragraph();
               pkk41.setAlignment(Element.ALIGN_JUSTIFIED);
           pkk41.add(new Chunk("else or which may be in course of transit "
                    + "to ................................................. of wherever else the same may be "+"or held by any party at any time to the order and "
                    + "disposition of the borrower and whether now belonging or which may at any time hereafter during the continuance of this "
                    + "security belong to the Borrower (and all of which are hereinafter for brevity�s sake collectively referred to as ��the "
                    + "hypothecated poultry stock and equipment��) [ all of which the said hypothecated cattle live-stock and hypothecated cattle "
                    + "stock and equipment and hypothecated poultry livestock and hypothecated poultry stock and equipment are hereinafter "
                    + "referred to as ��the hypotheca�� which expression shall include all or any of them] as also the sale proceeds of the "
                    + "hypotheca as security or due repayment by the Borrower to the Bank at................................ of the said loan of Rs.", redFont));
            Chunk amount = new Chunk(" "+commaloanAmount, font11b);
            amount.setUnderline(0.6f, -2f);
            pkk41.add(amount);
//            pkk41.add(underLine(commaloanAmount));
            pkk41.add(new Chunk("/- by the installments on the days and the manner aforesaid together with all interest due and "
                    + "payable thereon (including compound interest and penal interest payable hereunder) and also for all costs charges and "
                    + "expenses (the legal costs being between the Advocate and client) incurred by the Bank for the protection, preservation, "
                    + "defence and perfection of this security and for attempted or actual realisation therof and for recovery of the moneys due "
                    + "hereunder by the Borrower to the Bank and hereby secured or expressed so to be. ", redFont));
            document.add(pkk41);

            Paragraph pkk42 = new Paragraph("(OR)", redFont);
            pkk42.setAlignment(Element.ALIGN_CENTER);
            document.add(pkk42);

            Paragraph pkk43 = new Paragraph();
            pkk43.add(new Chunk("5.And also ", redFont));
            pkk43.add(new Chunk("all the stock of spawn, fry, fingerling, fish now kept or stored or contained or which may at any time "
                    + "hereafter during the continuance of this security be kept or stored or stocked upon the Borrower�s premises / fishery tank / "
                    + "fishery farm house situated in the village of .........................................................P.S. ......................District......................or "
                    + "which may be caught from or kept or stored in any water area or at any other place or in course of transit wherever else the "
                    + "same may be or held by any party anywhere to the order and disposition of the Borrower and whether now belonging or "
                    + "which may at any time hereafter during the continuance of this security belong to the Borrower (and all of which are "
                    + "hereafter for brevity�s sake collectively referred to as��the hypothecated fish-stock��). \n\n", redFont));
                   pkk43.add(new Chunk("And also all the stock of fish fee, manure, chemical fertilizers, crafts, gears, nursery and breeding equipments, "
                    + "medicines, chemicals, weighing equipments, utensils as also all other tools, implements, applicances and things with all "
                    + "the spares, stores, replacements and accessories whatsoever pertaining thereto as also any cart, vehicle, van whatsoever "
                    + "which may be stored or lying on the Borrower�s said premises, fishery tank, fish farm, farm house, shed or any part thereof "
                    + "or in any godown or anywhere else or which may be in course of transit or held by any party anywhere to the order and "
                    + "disposition of the Borrower and whether now belonging or which may at any time hereafter during the continuance of this"
                    + "security belong to the Borrower (hereinafter for brevity�s sake collectively reffered to as ��the hypothecated fish-stock and ", redFont));
            pkk43.setAlignment(Element.ALIGN_JUSTIFIED);

            Chunk underline3 = new Chunk("��the hypothecated fish stock and equipment��).", font11b);
            underline3.setUnderline(0.6f, -2f);
            pkk43.add(underline3);

            document.add(pkk43);

            document.add(new Paragraph("\n"));
            Paragraph pkk44 = new Paragraph("6. The Borrower shall not during the continuance of this Agreement sell, dispose of, pledge, hypothecate or otherwise "
                    + "charge, encumber, or in any manner, part with the possession of the hypotheca or any part thereof nor shall the Borrower "
                    + "do or permit to be done any act whereby the security hereinbefore expressly given to the Bank shall in any way be "
                    + "prejudicially affected or whereby any distress or attachment or execution may be levied thereon by any creditor or other "
                    + "person including any government or municipal authority or body\n\n", redFont);
            pkk44.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk44);

            Paragraph pkk45 = new Paragraph("7. So long as any money remains due in respect of the said loan the Borrower shall not remove of cause or prement to "
                    + "be to remove the hypotheca from the Borrower�s permises at ........................................where the same are presently kept or "
                    + "to which (with the consent of the Bank) the same may be hereafter removed,", redFont);
                    
            pkk45.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk45);
            document.newPage();
            Paragraph pk45 = new Paragraph(" except in the manner and to the extent "
                    + "allowed by the Bank or except for effecting necessary repairs thereto. The Bank shall be entitled to put up and the "
                    + "Borrower hereby gives his consent to the Bank to put up the Bank�s name board on the hypotheca or at the place where the "
                    + "hypotheca are or may be stored, at such time and in such manner as the Bank may deem proper.\n\n", redFont);
            document.add(pk45);
            Paragraph pkk46 = new Paragraph("8. The Borrower will at time pay all taxes, assessments, dues and outgoings payable in respect of the hypotheca and "
                    + "will also keep the same in a marketable state and in thorogh working order and will not make any alteration therein "
                    + "without the previous written consent of the Bank\n\n", redFont);
            pkk46.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk46);

            Paragraph pkk47 = new Paragraph("9. The Borrower undertakes to identify the Bank against every and any kind of loss or damage by reason of damage to "
                    + "or destruction or loss of the hypotheca from any cause whatsoever or by reason of any claims by third parties.\n\n", redFont);
            pkk47.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk47);

            Paragraph pkk48 = new Paragraph("10. The Borrower shall notify the Bank at once of any change in his present address given below and also the address to "
                    + "which the hypotheca may be moved subject nevertheless to the provisions of clause 4 above, ��Provided however that as "
                    + "far as possible the Borrower shall give prior intimation to the Bank whenever Borrower intends to move the hypotheca "
                    + "from the existing premises and shall obtain the significance of the Bank to the movement of the hypotheca��.\n\n", redFont);
            pkk48.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk48);

            Paragraph pkk49 = new Paragraph("11. The Borrower hereby irrevocably empowers the Bank and any person or persons from time to time authorised by the "
                    + "Bank in that behalf and without previous notice to the Borrower to enter the said premises or any other premises "
                    + "whatsoever for the purpose of inspection or valuation or taking possession of the hypotheca pursuant to clause 10 herein "
                    + "contained or for any other purpose mentioned in the said clause and to remain on the premises so long as the Bank may "
                    + "think necessary.\n\n", redFont);
            pkk49.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk49);

            Paragraph pkk50 = new Paragraph("12. Notwithstanding anything herein contained to the contrary the whole of the said loan or the entire balance thereof "
                    + "outstanding for the time being shall at the option of the Bank become forthwith due and payable by the Borrower to the "
                    + "Bank and the security hereunder shall at the option of the Bank become enforceable immediately upon the happening of "
                    + "any of the following events, namely :\n ", redFont);
            pkk50.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk50);

            Paragraph pkk51 = new Paragraph("(a) Any installment of principal or interest or any part thereof, in respect of the said loan being unpaid for a period of 15 "
                    + "days after the respective due dates for payment thereof. ", redFont);
            pkk51.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk51);

            Paragraph pkk52 = new Paragraph("(b) Any representation or statement in the Borrower�s Loan Application being found to be materially incorrect; \n", redFont);
            pkk52.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk52);

            Paragraph pkk53 = new Paragraph("(c) The Borrower committing any breach or default in the performance or observance of any term of condition contained "
                    + "in these presents or in the said Loan Application. \n", redFont);
            pkk53.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk53);

            Paragraph pkk54 = new Paragraph("(d) Exclusion or distress or other process being enforced or levied upon or against the whole or any part of the "
                    + "Borrower�s property whether secured to the Bank or not.\n", redFont);
            pkk54.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk54);

            Paragraph pkk55 = new Paragraph("(e) The Borrower being adjudicated insolvent or taking advantage of any law for the relief of insolvent debtors or "
                    + "entering into any arrangement or composition with his creditors or committing any act of insolvency;\n", redFont);
            pkk55.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk55);

            

            //Rupesh 15 to 23
            document.newPage();
            Paragraph pkk56 = new Paragraph("(f) If the Borrower shall without the consent in writing of the Bank create or attempts or purport to create any mortgage, "
                    + "charge, pledge, hypothecation or lien or encumbrance on the hypotheca which is the subject of the Bank�s security "
                    + "hereunder or any part thereof ;\n\n", redFont);
            pkk56.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk56);
//            document.add(new Paragraph("\n"));
            Paragraph rp1 = new Paragraph(17, "13. If the Borrower makes any default in payment of any installment thereof as mentioned above or if loan or any part of such installment on the respective due dates  of payment there of as mentioned above or if any event or circumstances shall occur which shall in the opinion of the Bank be prejudicial to endanger or be likely to endanger this security to if any other event or circumstances mentioned in clause 9 above happens or  occurs, the Bank if it thinks fit shall be entitled at the risk and expense of the Borrower without any notice at any time or times after such default or event or circumstance occurs or happens to enter (and for that purpose to do any necessary act deed or thing) and remain upon any place where the hypotheca or the books of account of receiver, appoint receivers, of all or any part of the hypotheca or books of account and thereupon forthwith or at any time and from time to time but after giving not less than 48 hours notice at least to sell either by public auction or private contract or otherwise dispose of or deal with the hypotheca in such manner and upon such ultimate balance due in the said Loan account and interest due thereon upto the date of such application. And to enforce, realise, settle, compromise and deal with any rights aforesaid without being bound to exercise any of these powers or being liable for any losses in the exercise thereof and without prejudice to the Bank�s rights and remedies of suit or otherwise and notwithstanding there may be any pending suit or other proceeding. The Borower hereby also agrees to accept the Bank�s accounts of sales realisation and to pay any shortfall or deficiency thereby shown. And if the net sum realised by such sale shall be insufficient to pay the total amount secured hereunder the Bank shall be at liberty to apply any other money or moneys in the hands of the Bank standing to the credit of or belonging to the Borrower in or towards the payment of the balance and in the event of there being still a deficiency, the Borrower shall forthwith pay such deficiency. Provided that nothing herein contained shall in any manner prejudice or affect the Bank�s remedy against the Borrower personally.\n", redFont);
            rp1.setAlignment(Element.ALIGN_JUSTIFIED);
            rp1.setSpacingAfter(5);
            document.add(rp1);
             document.add(new Paragraph("\n"));
            Paragraph rp2 = new Paragraph(17, "14. The Borrower hereby declares and guarantees that the hypotheca are and shall remain the absolute and unencumbered property of the Borrower with full power of disposition thereover.\n", redFont);
            rp2.setAlignment(Element.ALIGN_JUSTIFIED);
            rp2.setSpacingAfter(5);
            document.add(rp2);
            document.add(new Paragraph("\n"));
            Paragraph rp3 = new Paragraph(17, "15. The Borrower shall furnish (and verify all statement and) information from time to time as required by the Bank and give execute and necessary documents required to give effect to this security.\n", redFont);
            rp3.setAlignment(Element.ALIGN_JUSTIFIED);
            rp3.setSpacingAfter(5);
            document.add(rp3);
             document.add(new Paragraph("\n"));
            Paragraph rp4 = new Paragraph(17, "16. The Borrower shall, whenever required by the Bank, give full particulars to the Bank of all the assets of the Borrower and of the hypotheca and shall at all times allow the Bank of the authorised agent of the Bank inspection of hypotheca and of all records of the Borrower in reference thereto and shall allow the Bank or its agents to value the same. All costs charges and expenses incurred by the Bank and incidental to such inspection and valuation shall be paid to the Bank forthwith on demand (the Bank statement being conclusive), and until payment, shall with interest at the rate aforesaid be a charge upon the hypotheca. Any such valuation shall be conclusive and binding on the borrower both in and out of court.\n", redFont);
            rp4.setAlignment(Element.ALIGN_JUSTIFIED);
            rp4.setSpacingAfter(5);
            document.add(rp4);
            
            
//             document.add(new Paragraph("\n"));
            document.newPage();
            Paragraph rp5 = new Paragraph(17, "17. The Borrower shall at all times during the continuance of this security keep and maintain such margin of security in favour of the Bank (hereinafter called ��the said margin��) as may be required by the Bank from time to time. If and often as the said margin shall fail to be maintained, then the Borrower shall forthwith (according as the Bank may require) either hypothecate to the Bank further goods or tangible movable property or goods approved by the Bank and of sufficient value to make up the deficiency or shall reduce the amount for the time being due to the Bank by a cash payment so as to maintain the said margin.\n", redFont);
            rp5.setAlignment(Element.ALIGN_JUSTIFIED);
            rp5.setSpacingAfter(5);
            document.add(rp5);
             document.add(new Paragraph("\n"));
            Paragraph rp6 = new Paragraph(17, "18. (a) The Borrower shall at its own expense insure and keep insured the hypotheca against fire and such other risks as the Bank shall from time to time require for the full market value thereof in one or more insurance offices approved by the Bank and shall deliver to the Bank the policies of insurance duly assigned to the Banks and shall keep on foot and maintain such insurances throughout the continuance of the security and deliver to the Bank the renewal receipt therefor.\n", redFont);
            rp6.setAlignment(Element.ALIGN_JUSTIFIED);
            rp6.setSpacingAfter(5);
            document.add(rp6);
             document.add(new Paragraph("\n"));
            //PAGE REPESH 2ND.
//            document.newPage();
            Paragraph rp7 = new Paragraph(17, "(b)In default of the Borrower doing so, the Bank may (but shall not be bound to) keep in good working order and condition and render marketable the hypotheca or effect or renew such insurance and may pay all taxes, assessments dues and outgoing payable by the Borrower. Any premium paid by the Bank or any other amount paid by the Bank under this clause and any costs, charges and expenses whatsoever incur by the Bank shall be repaid by the Borrower on demand forthwith AND until repayment with interest at the rate aforesaid the same shall be a charge on the hypotheca. All sums received under such insurance shall be applied in or towards liquidation of the amount for the time being due hereunder to the Bank.\n", redFont);
            rp7.setAlignment(Element.ALIGN_JUSTIFIED);
            rp7.setSpacingAfter(5);
            document.add(rp7);
             document.add(new Paragraph("\n"));
            Paragraph rp8 = new Paragraph(17, "19. The Borrower agree to accept as conclusive proof of the correctness of any sum claimed to be due from him to the Bank under this Agreement a statement of account made out from the books of the bank and signed by the Manager/Dy. Manager and / or other duly authorised officer of the Bank without the production of any other voucher, document or paper.\n", redFont);
            rp8.setAlignment(Element.ALIGN_JUSTIFIED);
            rp8.setSpacingAfter(5);
            document.add(rp8);
             document.add(new Paragraph("\n"));
            Paragraph rp9 = new Paragraph(17, "20. The Bank shall not in any way be liable or responsible for any damage or depreciation which the hypotheca or any part thereof may suffer or sustain on any account whatsoever while the same shall at any time come into possession of the Bank or any receiver appointed by the Bank.\n", redFont);
            rp9.setAlignment(Element.ALIGN_JUSTIFIED);
            rp9.setSpacingAfter(5);
            document.add(rp9);
             document.add(new Paragraph("\n"));
            Paragraph rp10 = new Paragraph(17, "21. The Borrower shall inform the Bank promptly of any notice or intimation received from any Government, semi-Government, Revenue, Municipal of local or other authorities regarding any default delay, etc. by the Borrower in payment of any dues or in the performance of the Borrower�s obligation towards them.\n", redFont);
            rp10.setAlignment(Element.ALIGN_JUSTIFIED);
            rp10.setSpacingAfter(5);
            document.add(rp10);
             document.add(new Paragraph("\n"));
            Paragraph rp11 = new Paragraph(17, "22. The Borrower shall provide to the Bank all such informations as the Bank shall from time to time at its discretion require, including information relating to the financial conditions of the Borrower.\n", redFont);
            rp11.setAlignment(Element.ALIGN_JUSTIFIED);
            rp11.setSpacingAfter(5);
            document.add(rp11);
             document.add(new Paragraph("\n"));
           
//             document.add(new Paragraph("\n"));
            document.newPage();
             Paragraph rp12 = new Paragraph(17, "23. The Borrower agree to open and / or maintain with the Bank a Saving Bank/Current Account and keep the account in sufficient funds and hereby irrevocably authorises the Bank to debit the same or any other account of the Borrower with the Bank with the amount of each installment of the loan and interest thereon payable hereunder as and when it falls due. The Bank shall also have right to set-off the Balance due in the said account or in such other account against the balance due in the said Loan Account together with all interest costs, charges and expenses and other moneys payable by the Borrower hereunder at any time after such Balance or interest of such costs, charges and expenses and other moneys has/have become payable by the Borrower under the terms hereof.\n", redFont);
            rp12.setAlignment(Element.ALIGN_JUSTIFIED);
//            rp12.setSpacingAfter(5);
            document.add(rp12);
            document.add(new Paragraph("\n"));
            Paragraph rp13 = new Paragraph(17, "24. Nothing herein shall operate to prejudice the Bank�s rights or remedies in respect of any present or future security, guarantee obligation or decree for any indebtedness or liability of the Borrower to the Bank.\n", redFont);
            rp13.setAlignment(Element.ALIGN_JUSTIFIED);
            rp13.setSpacingAfter(5);
            document.add(rp13);
             document.add(new Paragraph("\n"));
            Paragraph rp14 = new Paragraph(17, "25. The Borrower confirms that he has not borrowed any money from any other Bank or Co-Operative Society or from any other source whatsoever and whomsoever further that so long as the Borrower continues to be indebted or liable to the Bank in the said Loan Account or in any other Account or manner the Borrower shall not without the previous written consent of the Bank borrow any moneys from any other Bank of financier or from any such other source.\n", redFont);
            rp14.setAlignment(Element.ALIGN_JUSTIFIED);
            rp14.setSpacingAfter(5);
            document.add(rp14);
             document.add(new Paragraph("\n"));
            Paragraph rp15 = new Paragraph(17, "26. The Borrower hereby agrees that the Bank may hold the hypotheca and the proceed thereof not only as a security for the said loan but also as collateral security for any other moneys now due or which may at any time be due from the Borrower to the bank whether singly or jointly with another or others and that in addition to any general lien or similar right to which the Bank may be entitled by law the Bank may at any time and without notice to the Borrower combine or consolidate all or any of Borrower�s Accounts with and liabilities to the Bank and set off or transfer any sum or sums standing to the credit of any one or more of such accounts in or towards the satisfaction of any of the liabilities of the Borrower to the Bank on any other account or in any other respect whether such liabilities be actual or contingent primary or collateral and several or joint.\n", redFont);
            rp15.setAlignment(Element.ALIGN_JUSTIFIED);
            rp15.setSpacingAfter(5);
            document.add(rp15);
              document.add(new Paragraph("\n"));
            //RUPESH NEXT PAGE...
//            document.newPage();
            Paragraph rp16 = new Paragraph(17, "27. Where the Borrower is more than one individual, each one of them shall be bound and liable hereunder jointly and severally with the other or others of them and all covenants condition agreements herein contained shall be performed by them and each of them jointly and severally and any act or default by any of them shall be deemed to be an act or default by all of them. Where the Borrower is a firm such firm and all partners thereof from time to time shall be bound hereunder both joinly and severally notwithstanding any changes in the constitution or style thereof and notwithstanding that the firm shall consist of or be reduced to one individual.\n", redFont);
            rp16.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(rp16);
             document.add(new Paragraph("\n"));
            Paragraph rp17 = new Paragraph(17, "28. That the Borrower hereby irrevocable gives his consent to and agrees to the amounts due hereunder being recovered as a public demand/public moneys in terms of any legislation (now existing or hereafter to be enacted) relating to recoveries thereof and further irrevocably consents and agrees to submit to the jurisdiction of the appropriate body or authority or tribunal appointed under any such legislation or under any rules or regulations made under pursuant to such legislation. The Borrower also irrevocably undertakes to execute such further writing as may be necessary to give effect to this clause.\n", redFont);
            rp17.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(rp17);
              document.add(new Paragraph("\n"));
              document.newPage();
            Paragraph rp18 = new Paragraph(17, "29. Any notice by way of request or otherwise hereunder may be given by the Bank to the Borrower personally or may be left at the address given below or at the then or last known place of business or residence of the Borrower in the Republic of India as the case may be addressed to the Borrower or may be sent by post to the Borrower as aforesaid and if sent by post, such notice shall be deemed to have been given at the time when it would be delivered in due course of post, and in proving such notice when given by post, it shall be sufficient to prove that the envelope containing the notice was posted, and a certificate signed by the Bank�s Local Manager or Agent or Accountant that the envelope was so posted shall be conclusive. If by reason of absence of the Borrower from the place mentioned below or otherwise, any such notice to the Borrower cannot be given then the same, if inserted once as an advertisement in a newspaper circulating in the district where the Borrower was last known to reside or carry on business, shall be deemed to have been effectually given and received on the day on which such advertisement appears. Consent clause I/we hereby agree to that in case of default in repayment of loan ; the Bank is at liberty to inform RBI/CIBIL for displaying my/our name in the website or any media as deemed fit, by the Bank.", redFont);
            rp18.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(rp18);
             document.add(new Paragraph("\n"));
            Paragraph rp19 = new Paragraph(17, "In Witness whereof the Borrower has executed this agreement the day and year first above wirtten.\n", redFont);
            rp19.setSpacingAfter(5);
            document.add(rp19);

            document.add(new Paragraph("SCHEDULE OF REPAYMENT\n", canFont1));
             document.add(new Paragraph("\n"));
            Paragraph rp20 = new Paragraph("Amount of Installments                                                             Date of Payment of Installments", redFont);
            document.add(rp20);

            Paragraph rp21 = new Paragraph("Rs.", redFont);
            rp21.setSpacingAfter(5);
            document.add(rp21);

            Paragraph rp22 = new Paragraph("Rs.", redFont);
            rp22.setSpacingAfter(5);
            document.add(rp22);

            Paragraph rp23 = new Paragraph("Rs.", redFont);
            rp23.setSpacingAfter(5);
            document.add(rp23);

            Paragraph rp24 = new Paragraph("Rs.", redFont);
            rp24.setSpacingAfter(5);
            document.add(rp23);

            document.add(new Paragraph("Signed by the above named                                                                              (Signature�/s of Borrower/s)\n", redFont));
            document.add(new Paragraph("        " + loanData.getAppl1name() != null && !loanData.getAppl1name().equals("null") ? loanData.getAppl1name() : "", font11b));
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("  (Name/s of Borrower/s)\n", redFont));
//            document.add(new Paragraph("...................................................\n"
//                    + "...................................................\n"
//                    + "...................................................\n"
//                    + "..................................................."));
//            System.out.println("addressLength***************: " + permAddress1.length());
            Paragraph rp58 = new Paragraph("Address / es of the Borrower/s :  ", redFont);
            rp58.add("\n");
            String permAddress1 = (loanData.getPermhouseno() != null && !loanData.getPermhouseno().equalsIgnoreCase("null") && loanData.getPermhouseno() != " " ? loanData.getPermhouseno()+ "," : " ");
            permAddress1 += (loanData.getPermstreetno() != null && !loanData.getPermstreetno().equalsIgnoreCase("null") && loanData.getPermstreetno() != " " ? loanData.getPermstreetno()+ "," : " ");
            permAddress1 += (loanData.getPermlandmark() != null && !loanData.getPermlandmark().equalsIgnoreCase("null") && loanData.getPermlandmark() != " " ? loanData.getPermlandmark()+ "," : " ");
            permAddress1 += (loanData.getPermvillage() != null && !loanData.getPermvillage().equalsIgnoreCase("null") && loanData.getPermvillage() != " " ? loanData.getPermvillage()+ ",\n" : " ");
            permAddress1 += (loanData.getPermdistrict() != null && !loanData.getPermdistrict().equalsIgnoreCase("null") && loanData.getPermdistrict() != " " ? loanData.getPermdistrict()+ "," : " ");		
            permAddress1 += (loanData.getPermsubdistrict() != null && !loanData.getPermsubdistrict().equalsIgnoreCase("null") && loanData.getPermsubdistrict() != " " ? loanData.getPermsubdistrict()+ "," : " ");
            permAddress1 += (loanData.getPermstate() != null && !loanData.getPermstate().equalsIgnoreCase("null") && loanData.getPermstate() != " " ? loanData.getPermstate()+ "," : " ");
            permAddress1 += (loanData.getPermpincode() != null && !loanData.getPermpincode().equalsIgnoreCase("null") && loanData.getPermpincode() != " " ? loanData.getPermpincode()+ "," : " ");
            permAddress1 += (loanData.getPermtelephone() != null && !loanData.getPermtelephone().equalsIgnoreCase("null") && loanData.getPermtelephone() != " " ? loanData.getPermtelephone()+ "," : " ");
            
            
            String addValArr[] = permAddress1.split(",");
            if (permAddress1.length() > 25) {
                for (int i = 0; i < 3; i++) {

                    rp58.add(underLine(addValArr[i].toUpperCase()));
                    rp58.add(underLine(" , "));
                }

                rp58.add("\n");
                for (int i = 3; i < addValArr.length; i++) {
                    rp58.add(underLine(addValArr[i].toUpperCase()));
                    rp58.add(underLine(" , "));
                }
            }
            Chunk c = new Chunk(underLine(permAddress1.toUpperCase()));

            document.add(rp58);
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("Witness :\n"
                    + "1.\n"
                    + "2.\n", font11b));
            rp24.add(underLine(permAddress1));

            Paragraph rp25 = new Paragraph("(N.B. : Item 2, 3, 4,5, may be ticked according to nature of Hypothecation)", font11bi);
            rp25.setAlignment(Element.ALIGN_CENTER);
            document.add(rp25);

            //Page-2 start##################################################
        } catch (FileNotFoundException fnf) {
            logger.error("Error:.....!!! " + fnf.getMessage());
        } catch (DocumentException de) {
            logger.error("Error:.....!!! " + de.getMessage());
        } catch (IOException ex) {
            logger.error("Error:.....!!! " + ex.getMessage());
        }
        document.close();

        logger.debug("***********Ended createPdf()***********");
    }

    public Chunk underLine(String data) {
        Paragraph pp9 = new Paragraph();
        pp9.setAlignment(Element.ALIGN_JUSTIFIED);
        Chunk underline1 = new Chunk(data, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL));
        underline1.setUnderline(0.1f, -2f);
        pp9.add(underline1);
        return underline1;
    }

    public String getIndianCurrencyFormat(String amount) {
        StringBuilder stringBuilder = new StringBuilder();
        char amountArray[] = amount.toCharArray();
        int a = 0, b = 0;
        for (int i = amountArray.length - 1; i >= 0; i--) {
            if (a < 3) {
                stringBuilder.append(amountArray[i]);
                a++;
            } else if (b < 2) {
                if (b == 0) {
                    stringBuilder.append(",");
                    stringBuilder.append(amountArray[i]);
                    b++;
                } else {
                    stringBuilder.append(amountArray[i]);
                    b = 0;
                }
            }
        }
        return stringBuilder.reverse().toString();
    }
}
