/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.core;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.subk.entity.Loanorigination;
import com.subk.util.AddressCodes;

/**
 *
 * @author Raj.k
 */
@Configuration
public class GenerateSanctionPDFFile extends PDFGenerationAbstract {
	
	@Autowired
	AddressCodes addresscodes;

	private String empty = ".........";
	Document document = null;
	Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
	public static final Logger logger = Logger.getLogger(GenerateSanctionPDFFile.class);
//    public static final ResourceBundle rb = ResourceBundle.getBundle("systemPath");
//    private static final ResourceBundle rb1 = ResourceBundle.getBundle("com.subk.Resources//subkResources");

	public String getPdf(Loanorigination loanData, String fileName,String pathAdd, String img, String jsPaths) {
		
		logger.info("************ ENTERED ************* ");
		
		String destPath = null;

		logger.debug("UBILoanOrigination Data: " + loanData.toString());
		logger.debug("FileName: " + fileName);

		try {
			destPath = pathAdd;
			destPath = destPath + fileName;

			// File file = new File(destPath);
			// file.getParentFile().mkdirs();
			new GenerateSanctionPDFFile().createPdf(destPath, loanData , img, jsPaths);
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.info("Error:....!!! " + ex.getMessage());
		}

		logger.info("destPathINATION PATH:...........!!!!!!!!!!  " + destPath);
		logger.info("************ ENDED.************* ");
		return destPath;
	}

	public void createPdf(String dest, Loanorigination loanData , String img, String jsPaths) {
		logger.debug("***********Entered***********");
		final String imgPath = img;
		String pre = "";
		PdfWriter writer = null;

		try {
			document = new Document();
			writer = PdfWriter.getInstance(document, new FileOutputStream(dest, false));

//            document.open();
			Font canFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
			Font canFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
			Font redFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);
			Font font10b = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
			Font font11b = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
			HeaderFooterPageEvent event = new HeaderFooterPageEvent();
			writer.setPageEvent(event);
			document.open();

			document.addKeywords("Java, Servelet, PDF, iText");
			document.addAuthor("Raj Grover");
			document.addCreator("UBILoanOrigination");
			String gender1 = loanData.getSex();
//			System.out.println("gggggggggggggggggggg" + gender1);
			if (gender1.equals("MALE")) {
				pre = "Mr";
			} else if (gender1.equals("FEMALE")) {
				pre = "Ms";
			}
			Image img2 = Image.getInstance(imgPath);
			img2.setAbsolutePosition(250f, 740f);
			img2.scaleAbsolute(80f, 80f);
			document.add(img2);
			document.add(new Paragraph("\n"));
			document.add(new Paragraph("\n"));
			document.add(new Paragraph("\n"));
			Paragraph para1 = new Paragraph("", canFont);
			para1.setAlignment(Element.ALIGN_CENTER);
			Chunk head1 = new Chunk("United Bank of India");
			head1.setUnderline(1, -2f);
			para1.add(head1);
			document.add(para1);
			Paragraph para2 = new Paragraph("Head Office: 11, Hemanta basu Sarani \n Kolkata -700001" + "\n "
					+ loanData.getBranchname() + " Branch \n\n", redFont);
			para2.setAlignment(Element.ALIGN_CENTER);
			document.add(para2);
			String applName = (loanData.getAppl1name() != null && !loanData.getAppl1name().equals("null")
					&& !loanData.getAppl1name().isEmpty() ? loanData.getAppl1name() : empty);
			PdfPTable table = new PdfPTable(2);
			table.setWidthPercentage(100f);
			table.setWidths(new float[] { 3, 0.8f });
			table.addCell(getCell(pre + ". " + applName, PdfPCell.ALIGN_LEFT));
			table.addCell(getCell("Date:" + "           ", PdfPCell.ALIGN_LEFT));
			document.add(table);
			String house_name = (loanData.getPermhouseno() != null && !loanData.getPermhouseno().equals("null")
					&& !loanData.getPermhouseno().isEmpty() ? loanData.getPermhouseno() : empty);
			String street_name = (loanData.getPermstreetno() != null && !loanData.getPermstreetno().equals("null")
					&& !loanData.getPermstreetno().isEmpty() ? loanData.getPermstreetno() : empty);
			String vill_name = (loanData.getPermvillage() != null && !loanData.getPermvillage().equalsIgnoreCase("null") && loanData.getPermvillage() != " " ? loanData.getPermvillage() : " ");
			String landmark = (loanData.getPermlandmark() != null && !loanData.getPermlandmark().equals("null")
					&& !loanData.getPermlandmark().isEmpty() ? loanData.getPermlandmark() : empty);
			String dist = (loanData.getPermdistrict() != null && !loanData.getPermdistrict().equalsIgnoreCase("null") && loanData.getPermdistrict() != " " ? loanData.getPermdistrict() : " ");
			String subdist = (loanData.getPermsubdistrict() != null && !loanData.getPermsubdistrict().equalsIgnoreCase("null") && loanData.getPermsubdistrict() != " " ? loanData.getPermsubdistrict() : " ");
			String state = (loanData.getPermstate() != null && !loanData.getPermstate().equalsIgnoreCase("null") && loanData.getPermstate() != " " ? loanData.getPermstate()  : " ");
			String pin_num = (loanData.getPermpincode() != null && !loanData.getPermpincode().equals("null")
					&& !loanData.getPermpincode().isEmpty() ? loanData.getPermpincode() : empty);
			String bc_updatedate=(loanData.getBcupdateddatetime() != null && !loanData.getBcupdateddatetime().equalsIgnoreCase("null") && loanData.getBcupdateddatetime() != " " ? loanData.getBcupdateddatetime() : " ");
			String date1=convertStringToDate(bc_updatedate);
			Paragraph para3 = new Paragraph("Address: ", redFont);
			para3.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(para3);
			Paragraph par4 = new Paragraph("", redFont1);
			par4.setLeading(0, 1);
			par4.add(house_name.toUpperCase() + "," + "\n");
			par4.add(street_name.toUpperCase() + "," + "\n");
			par4.add(vill_name.toUpperCase() + "," + "\n");
			par4.add(landmark.toUpperCase() + ", " + dist.toUpperCase() + ", " +subdist.toUpperCase() + ", "  + "\n" +state.toUpperCase() + ", " + pin_num
					+ "\n\n");
			document.add(par4);
			Paragraph pa4 = new Paragraph(" ", redFont);
			pa4.add("Dear Sir/Madam,");
			pa4.setSpacingAfter(3);
			document.add(pa4);
			Paragraph p4 = new Paragraph(" ", redFont);
			p4.setAlignment(Element.ALIGN_JUSTIFIED);
			p4.add("With reference to your application dated " );
			Chunk date = new Chunk(date1);
			date.setUnderline(0.6f, -2f);
			p4.add(date);
			p4.add(" for the Term Loan under"
					+ "United Samriddhi Scheme and subsequent particulars/information submitted"
					+ "by you, we are pleased to inform you that the bank has considered your "
					+ "request favorably and has sanctioned/modified the facilities to you"
					+ "according to terms and conditions mentioned here under.");
			document.add(p4);
			document.add(new Paragraph("\n"));
			Paragraph para4 = new Paragraph("Term Loan Limit: Rs.", canFont1);
			para4.setSpacingAfter(1.3f);
			document.add(para4);
//            document.add(new Paragraph("\n"));
			String margin = marginGet(
					loanData.getLoanamountsactioned() != null && !loanData.getLoanamountsactioned().equals("null")
							&& !loanData.getLoanamountsactioned().isEmpty() ? loanData.getLoanamountsactioned()
									: empty);
			String roi = (loanData.getInterestrate() != null && !loanData.getInterestrate().equals("null")
					&& !loanData.getInterestrate().isEmpty() ? loanData.getInterestrate() : empty);
			String mclrMF = null;
			String gender = loanData.getSex();
			if (gender.equalsIgnoreCase("MALE")) {
				mclrMF = loanData.getMclrMaleFemale() != null && !loanData.getMclrMaleFemale().equals("")
						? loanData.getMclrMaleFemale()
						: ""; // rb1.getString("MCLRL_MALE");
			} else if (gender.equalsIgnoreCase("FEMALE")) {
				mclrMF = loanData.getMclrMaleFemale() != null && !loanData.getMclrMaleFemale().equals("")
						? loanData.getMclrMaleFemale()
						: ""; // rb1.getString("MCLRL_FEMALE");
			}
			String instal = (loanData.getNoofinstalments() != null && !loanData.getNoofinstalments().equals("null")
					&& !loanData.getNoofinstalments().isEmpty() ? loanData.getNoofinstalments() : empty);
			String InstalmentAmount = loanData.getInstalmentamount() != null
					&& !loanData.getInstalmentamount().equals("null") ? loanData.getInstalmentamount() : empty;
			String CommaInstalmentAmount = getIndianCurrencyFormat(InstalmentAmount);
			String amtSanctioned = (loanData.getLoanamountsactioned() != null
					&& !loanData.getLoanamountsactioned().equals("null") && !loanData.getLoanamountsactioned().isEmpty()
							? loanData.getLoanamountsactioned()
							: empty);
			
			String commaamtSanctioned = getIndianCurrencyFormat(amtSanctioned);
			Paragraph para5 = new Paragraph();
			para5.setAlignment(Element.ALIGN_JUSTIFIED);
			para5.add(new Chunk("i. Purpose: ", canFont1));
			Chunk bus_name = new Chunk("Business");
			bus_name.setUnderline(0.6f, -2f);
			para5.add(bus_name);
//            para5.setSpacingAfter(2.3f);
			para5.add("\n\n");
			document.add(para5);
			Paragraph para20 = new Paragraph();
			para20.setAlignment(Element.ALIGN_JUSTIFIED);
			para20.add(new Chunk("ii. Margin: ", canFont1));
			Chunk mar = new Chunk(margin, font11b);
			mar.setUnderline(0.6f, -2f);
			para20.add(mar);
//            para20.setSpacingAfter(2.3f);
			para20.add("\n\n");
			document.add(para20);

			Paragraph para21 = new Paragraph();
			para21.setAlignment(Element.ALIGN_JUSTIFIED);
			para21.add(new Chunk("iii. Rate of Interest: ", canFont1));
			Chunk roi1 = new Chunk(roi, font11b);
			roi1.setUnderline(0.6f, -2f);
			para21.add(roi1);
			para21.add(new Chunk(" P.A [MCLR(Y)", redFont));
			Chunk mclr = new Chunk(" " + mclrMF);
			mclr.setUnderline(0.6f, -2f);
			para21.add(mclr);
			para21.add(new Chunk("] (The rate of interest is subject to "
					+ "changes as per HO/RBI guidelines to be issued from time to time.) Reset "
					+ "Clause-Bank reserves the right to reset the interest rate at the interval "
					+ "of every one year.", redFont));
			para21.add("\n\n");
//            para21.setSpacingAfter(2.3f);
			document.add(para21);
			Paragraph para22 = new Paragraph();
			para22.setAlignment(Element.ALIGN_JUSTIFIED);
			para22.add(new Chunk("iv. Repayment: ", canFont1));
			para22.add(new Chunk("In", redFont));
			Chunk instal1 = new Chunk(" " + instal, font11b);
			instal1.setUnderline(0.6f, -2f);
			para22.add(instal1);
			para22.add(new Chunk(" EMIs of Rs", redFont));
			Chunk emi1 = new Chunk(" " + CommaInstalmentAmount, font11b);
			emi1.setUnderline(0.6f, -2f);
			para22.add(emi1);
			para22.add(new Chunk(
					"/- .starting one month after disbursement (EMI is subject to change with change in rate of interest)",
					redFont));
			para22.add("\n\n");
//            para22.setSpacingAfter(2.3f);
			document.add(para22);
			Paragraph para23 = new Paragraph();
			para23.setAlignment(Element.ALIGN_JUSTIFIED);
			para23.add(new Chunk("v. Security: ", canFont1));
//            para23.add("\n\n");
			para23.add(new Chunk(" i) Hypothecation of Rs.", redFont));
			Chunk san = new Chunk(" " + commaamtSanctioned + "/-", font11b);
			san.setUnderline(0.6f, -2f);
			para23.add(san);
			para23.add(new Chunk(" purchased out of loan\n", redFont));
//            para23.add("\n\n");
			para23.add(new Chunk(
					" ii) Exclusive hypothecation charge on plant & machinery/transport port equipment (both present and future) and other movable assets of the company/firm/individual (not financed by the bank) valued Rs………………..",
					redFont));
			para23.add("\n\n");
//            para23.setSpacingAfter(2.3f);
			document.add(para23);
			Paragraph para24 = new Paragraph();
			para24.setAlignment(Element.ALIGN_JUSTIFIED);
			para24.add(new Chunk("vi. Terms & Conditions: \n ", canFont1));
			para24.add(new Chunk(" a) Processing Charge of Rs ",redFont));
			if(Integer.parseInt(amtSanctioned)<=200000) {
			Chunk charge = new Chunk("0% + GST", font11b);
			charge.setUnderline(0.6f, -2f);
			para24.add(charge);}else {
				Chunk charge = new Chunk("1% + GST", font11b);
				charge.setUnderline(0.6f, -2f);
				para24.add(charge);
			}
			para24.add(new Chunk(" Documentation charge of Rs......."
					+ "-and supervision charge of Rs ……… /-(per inspection) etc are to be levied "
					+ "in terms of HO circular. Bank’s service charges are subject to change as"
					+ " per HO circular to be issued from time to time.\n", redFont));
//            para24.setSpacingAfter(2.3f);
			para24.add("\n");
			document.add(para24);
			Paragraph para6 = new Paragraph();
			para6.setAlignment(Element.ALIGN_JUSTIFIED);
			para6.add(new Chunk(
					"b) The disbursement will be made after execution of bank’s standard documents and creation /extension of mortgage"
							+ " and other securities and realization of other charges upfront.\n\n",
					redFont));
//            para6.setSpacingAfter(1.5f);
			document.add(para6);
//            document.add(para5);
			document.newPage();

			Paragraph para7 = new Paragraph();
			para7.setAlignment(Element.ALIGN_JUSTIFIED);
			para7.add(new Chunk("c) The charged asset comprising stock in trade & movable"
					+ " fixed assets are to be insured adequately with full value coverage against comprehensive risks with Bank’s clause and"
					+ " shall have to be renewed on expiry of the current insurance policy. The premium of such insurance policy is to be paid"
					+ " by the borrower. Borrower should have to mandatorily take coverage under ", redFont));
			para7.add(new Chunk("PMJJBY and PMSBY ", canFont1));
			para7.add(new Chunk("schemes as per their eligibility.\n\n", redFont));
			para7.setSpacingAfter(1.5f);
			document.add(para7);
			Paragraph para8 = new Paragraph();
			para8.setAlignment(Element.ALIGN_JUSTIFIED);
			para8.add(new Chunk(
					"d) Penal Interest will be levied @0.50% per quarter per default above normal rate of interest in the following cases as per extant HO guidelines;\n\n",
					redFont));
			para8.setSpacingAfter(1.5f);
			document.add(para8);
			Paragraph para9 = new Paragraph();
			para9.setAlignment(Element.ALIGN_JUSTIFIED);
			para9.add(new Chunk(
					"  i) Default in repayment of loan installment and in servicing of monthly interests.\n", redFont));
			para9.setSpacingAfter(1);
			document.add(para9);
			Paragraph para10 = new Paragraph();
			para10.setAlignment(Element.ALIGN_JUSTIFIED);
			para10.add(new Chunk("  ii) Non compliance of terms of sanction\n\n", redFont));
			para10.setSpacingAfter(1.5f);
			document.add(para10);
			Paragraph para11 = new Paragraph();
			para11.setAlignment(Element.ALIGN_JUSTIFIED);
			para11.add(new Chunk(
					"e) Bank officials/appointed agents will have free access to inspect the securities, books of accounts and other related documents, ledgers as required by the bank.\n\n",
					redFont));
			para11.setSpacingAfter(1.5f);
			document.add(para11);
			Paragraph para12 = new Paragraph();
			para12.setAlignment(Element.ALIGN_JUSTIFIED);
			para12.add(new Chunk(
					"f) Any expansion/modernization etc will be done with the prior approval of the bank.\n\n",
					redFont));
			para12.setSpacingAfter(1.5f);
			document.add(para12);
			Paragraph para13 = new Paragraph();
			para13.setAlignment(Element.ALIGN_JUSTIFIED);
			para13.add(new Chunk(
					"g) The company/ Borrower must not enter into borrowing arrangements with any other Bank/FIs/Companies without obtaining prior approval of the bank.\n\n",
					redFont));
			para13.setSpacingAfter(1.5f);
			document.add(para13);
			Paragraph para14 = new Paragraph();
			para14.setAlignment(Element.ALIGN_JUSTIFIED);
			para14.add(new Chunk(
					"h) Sale proceeds and business transactions must be routed through the banks accounts.\n\n",
					redFont));
			para14.setSpacingAfter(1);
			document.add(para14);
			Paragraph para15 = new Paragraph();
			para15.setAlignment(Element.ALIGN_JUSTIFIED);
			para15.add(new Chunk(
					"  i) Borrower will submit copies of all relevant licenses required by the unit as per law and shall ensure that all such licenses are kept renewed from time to time.\n\n",
					redFont));
			para15.setSpacingAfter(1.5f);
			document.add(para15);
			Paragraph para16 = new Paragraph();
			para16.setAlignment(Element.ALIGN_JUSTIFIED);
			para16.add(new Chunk(
					"j) Bank’s hypothecation board should be properly displayed at a prominent place of the unit both at its factory site as well as godown.\n\n",
					redFont));
			para16.setSpacingAfter(1.5f);
			document.add(para16);
			Paragraph para17 = new Paragraph();
			para17.setAlignment(Element.ALIGN_JUSTIFIED);
			para17.add(new Chunk(
					"k) The borrower/company must submit information/statement as may be required by the bank from time to time.\n\n",
					redFont));
			para17.setSpacingAfter(1.5f);
			document.add(para17);
			Paragraph para18 = new Paragraph();
			para18.setAlignment(Element.ALIGN_JUSTIFIED);
			para18.add(new Chunk(
					"l) The bank reserves the right to modify/add/remove any term and conditions in future if deemed fit.\n\n",
					redFont));
			para18.setSpacingAfter(1.5f);
			document.add(para18);
			Paragraph para19 = new Paragraph();
			para19.setAlignment(Element.ALIGN_JUSTIFIED);
			para19.add(new Chunk(
					"If the above terms and conditions are acceptable to you, please return one copy of this letter after putting your signature as token of your acceptance. All the facilities will be allowed to you on receipt of your acceptance and execution of required documents for each facility.\n\n",
					redFont));

			para19.add(new Chunk("Yours faithfully\n", redFont));

			para19.add(new Chunk("Branch Manager\n\n", redFont));

			para19.add(new Chunk(
					"I/we accept all the terms and conditions detailed above with respect to United Samriddhi Loan scheme availed by me/us.\n\n",
					redFont));

			para19.add(new Chunk("Borrower’s Signature ………………………………….\n", redFont));

			para19.add(new Chunk("Date: ………………….. ", redFont));

			document.add(para19);

		} catch (FileNotFoundException fnf) {
			fnf.printStackTrace();
			logger.info("FileNotFoundException:.....!!! " + fnf.getMessage());
		} catch (DocumentException de) {
			de.printStackTrace();
			logger.info("DocumentException:.....!!! " + de.getMessage());
		} catch (IOException ex) {
			ex.printStackTrace();
			logger.info("IOException:.....!!! " + ex.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("IOException:.....!!! " + e.getMessage());
		}
		document.close();

		logger.debug("***********Ended createPdf()***********");
	}

	private String marginGet(String margin) {
		System.out.println("marginget" + margin);
		String marginGet = "";
		if (Integer.parseInt(margin) > 100000) {
			marginGet = "15%";
		} else {
			marginGet = "........";
		}
		return marginGet;
	}

	public Chunk underLine(String data) {
		Paragraph pp9 = new Paragraph();
		pp9.setAlignment(Element.ALIGN_JUSTIFIED);
		Chunk underline1 = new Chunk(data, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL));
		underline1.setUnderline(0.1f, -2f);
		pp9.add(underline1);
		return underline1;
	}

	public String convertStringToDate(String dateString) {

// 20170602193129
		Date date = null;
		String formattedDate = null;
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddhhmmss");
		SimpleDateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");

		try {
			date = df.parse(dateString);
			formattedDate = df1.format(date);
		} catch (ParseException pe) {
			logger.error("Error: " + pe.getMessage());
		}

		return formattedDate;
	}

	public PdfPCell getCell(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text, redFont));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}
	
	public String getIndianCurrencyFormat(String amount) {
	    StringBuilder stringBuilder = new StringBuilder();
	    char amountArray[] = amount.toCharArray();
	    int a = 0, b = 0;
	    for (int i = amountArray.length - 1; i >= 0; i--) {
	        if (a < 3) {
	            stringBuilder.append(amountArray[i]);
	            a++;
	        } else if (b < 2) {
	            if (b == 0) {
	                stringBuilder.append(",");
	                stringBuilder.append(amountArray[i]);
	                b++;
	            } else {
	                stringBuilder.append(amountArray[i]);
	                b = 0;
	            }
	        }
	    }
	    return stringBuilder.reverse().toString();
	}
	public String ConverttoDate(String date) {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String date1 = "";
        try {
            Date dateString = df.parse(date);
            String pattern = "MM-dd-yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            date1 = simpleDateFormat.format(dateString);
        } catch (Exception ex) {
            logger.info("Exception Occured,,,,,,,,,,,,,,, " + ex);
        }

        //System.out.println(date);
        return date1;
    }
}
