/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.core;

import com.subk.entity.Loanorigination;

/**
 *
 * @author Raj. k
 */
public interface PDFGenerationOperations {
    public String getPdf(Loanorigination loanData, String fileName,String filepath , String img, String jsPaths);
    public void createPdf(String dest, Loanorigination loanData ,String imgPath, String jsPaths);
    public String getJsData(String functionName, String paramVal,String jsPaths);
    public String convertToDate(String dateStr);
}
