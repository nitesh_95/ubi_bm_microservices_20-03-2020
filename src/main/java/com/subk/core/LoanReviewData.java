/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.core;

import java.io.Serializable;

/**
 *
 * @author subashkumar.s
 */
public class LoanReviewData implements Serializable {
    private String applicationNo;
    private String dateOfSubmit;
    private String custName;
    private String contactNo;
    private String villageName;
    private String fxVeriStatus;
    private String fxVeriDate;
    private String teVeriStatus;
    private String teVeriDate;
    private String uhVeriStatus;
    private String uhVeriDate;
    private String bmVeriStatus;
    private String bmVeriDate;
    private String loanAmount;
    private int noOfTerm;
    private String sex;
    private String uhRecomLoanAmt;
    private String uhRecomInstalAmt;
    private int uhnoOfTerm;

    /**
     * @return the applicationNo
     */
    public String getApplicationNo() {
        return applicationNo;
    }

    /**
     * @param applicationNo the applicationNo to set
     */
    public void setApplicationNo(String applicationNo) {
        this.applicationNo = applicationNo;
    }

    /**
     * @return the dateOfSubmit
     */
    public String getDateOfSubmit() {
        return dateOfSubmit;
    }

    /**
     * @param dateOfSubmit the dateOfSubmit to set
     */
    public void setDateOfSubmit(String dateOfSubmit) {
        this.dateOfSubmit = dateOfSubmit;
    }

    /**
     * @return the custName
     */
    public String getCustName() {
        return custName;
    }

    /**
     * @param custName the custName to set
     */
    public void setCustName(String custName) {
        this.custName = custName;
    }

    /**
     * @return the contactNo
     */
    public String getContactNo() {
        return contactNo;
    }

    /**
     * @param contactNo the contactNo to set
     */
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    /**
     * @return the villageName
     */
    public String getVillageName() {
        return villageName;
    }

    /**
     * @param villageName the villageName to set
     */
    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }

    /**
     * @return the fxVeriStatus
     */
    public String getFxVeriStatus() {
        return fxVeriStatus;
    }

    /**
     * @param fxVeriStatus the fxVeriStatus to set
     */
    public void setFxVeriStatus(String fxVeriStatus) {
        this.fxVeriStatus = fxVeriStatus;
    }

    /**
     * @return the fxVeriDate
     */
    public String getFxVeriDate() {
        return fxVeriDate;
    }

    /**
     * @param fxVeriDate the fxVeriDate to set
     */
    public void setFxVeriDate(String fxVeriDate) {
        this.fxVeriDate = fxVeriDate;
    }

    /**
     * @return the teVeriStatus
     */
    public String getTeVeriStatus() {
        return teVeriStatus;
    }

    /**
     * @param teVeriStatus the teVeriStatus to set
     */
    public void setTeVeriStatus(String teVeriStatus) {
        this.teVeriStatus = teVeriStatus;
    }

    /**
     * @return the teVeriDate
     */
    public String getTeVeriDate() {
        return teVeriDate;
    }

    /**
     * @param teVeriDate the teVeriDate to set
     */
    public void setTeVeriDate(String teVeriDate) {
        this.teVeriDate = teVeriDate;
    }

    /**
     * @return the uhVeriStatus
     */
    public String getUhVeriStatus() {
        return uhVeriStatus;
    }

    /**
     * @param uhVeriStatus the uhVeriStatus to set
     */
    public void setUhVeriStatus(String uhVeriStatus) {
        this.uhVeriStatus = uhVeriStatus;
    }

    /**
     * @return the uhVeriDate
     */
    public String getUhVeriDate() {
        return uhVeriDate;
    }

    /**
     * @param uhVeriDate the uhVeriDate to set
     */
    public void setUhVeriDate(String uhVeriDate) {
        this.uhVeriDate = uhVeriDate;
    }

    /**
     * @return the bmVeriStatus
     */
    public String getBmVeriStatus() {
        return bmVeriStatus;
    }

    /**
     * @param bmVeriStatus the bmVeriStatus to set
     */
    public void setBmVeriStatus(String bmVeriStatus) {
        this.bmVeriStatus = bmVeriStatus;
    }

    /**
     * @return the bmVeriDate
     */
    public String getBmVeriDate() {
        return bmVeriDate;
    }

    /**
     * @param bmVeriDate the bmVeriDate to set
     */
    public void setBmVeriDate(String bmVeriDate) {
        this.bmVeriDate = bmVeriDate;
    }

    /**
     * @return the loanAmount
     */
    public String getLoanAmount() {
        return loanAmount;
    }

    /**
     * @param loanAmount the loanAmount to set
     */
    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    /**
     * @return the noOfTerm
     */
    public int getNoOfTerm() {
        return noOfTerm;
    }

    /**
     * @param noOfTerm the noOfTerm to set
     */
    public void setNoOfTerm(int noOfTerm) {
        this.noOfTerm = noOfTerm;
    }

    /**
     * @return the sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * @param sex the sex to set
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * @return the uhRecomLoanAmt
     */
    public String getUhRecomLoanAmt() {
        return uhRecomLoanAmt;
    }

    /**
     * @param uhRecomLoanAmt the uhRecomLoanAmt to set
     */
    public void setUhRecomLoanAmt(String uhRecomLoanAmt) {
        this.uhRecomLoanAmt = uhRecomLoanAmt;
    }

    /**
     * @return the uhRecomInstalAmt
     */
    public String getUhRecomInstalAmt() {
        return uhRecomInstalAmt;
    }

    /**
     * @param uhRecomInstalAmt the uhRecomInstalAmt to set
     */
    public void setUhRecomInstalAmt(String uhRecomInstalAmt) {
        this.uhRecomInstalAmt = uhRecomInstalAmt;
    }

    /**
     * @return the uhnoOfTerm
     */
    public int getUhnoOfTerm() {
        return uhnoOfTerm;
    }

    /**
     * @param uhnoOfTerm the uhnoOfTerm to set
     */
    public void setUhnoOfTerm(int uhnoOfTerm) {
        this.uhnoOfTerm = uhnoOfTerm;
    }
    
}
