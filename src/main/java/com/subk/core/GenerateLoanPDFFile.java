/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.core;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.subk.entity.Loanorigination;
import com.subk.util.AddressCodes;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 *
 * @author Raj. k
 */
@Configuration
public class GenerateLoanPDFFile extends PDFGenerationAbstract {
	
	@Autowired
	AddressCodes addresscodes;

    public static final Logger logger = Logger.getLogger(GenerateLoanPDFFile.class);
//    public static final ResourceBundle rb = ResourceBundle.getBundle("systemPath");
    Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

    public String getPdf(Loanorigination loanData, String fileName,String pathAdd, String img, String jsPaths) {
        logger.info("************ ENTERED ************* ");

        String destPath = null;

        logger.debug("UBILoanOrigination Data: " + loanData.toString());
        logger.debug("FileName: " + fileName);

        try {
            destPath = pathAdd;
            destPath = destPath + fileName;

            //File file = new File(destPath);
            //file.getParentFile().mkdirs();
            new GenerateLoanPDFFile().createPdf(destPath, loanData, img, jsPaths);
        } catch (Exception ex) {
            logger.error("Error:....!!! " + ex.getMessage());
        }

        logger.info("destPathINATION PATH:...........!!!!!!!!!!  " + destPath);
        logger.info("************ ENDED.************* ");
        return destPath;
    }

    public void createPdf(String dest, Loanorigination loanData , String img, String jsPaths) {
        final String imgPath = img;

        Document document = null;
        PdfWriter writer = null;

        try {
            document = new Document();
            writer = PdfWriter.getInstance(document, new FileOutputStream(dest, false));

//            document.open();

            Font canFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
            Font canFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);

            Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 9);
            Font redFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.NORMAL);

            Font font11b = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
            HeaderFooterPageEvent event = new HeaderFooterPageEvent();
            writer.setPageEvent(event);
            document.open();

            document.addKeywords("Java, Servelet, PDF, iText");
            document.addAuthor("Raj Grover");
            document.addCreator("UBILoanOrigination");

            NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));

            String ApprxyrlyApp1Income = loanData.getApprxyrlyapp1income() != null && !loanData.getApprxyrlyapp1income().equalsIgnoreCase("null") && loanData.getApprxyrlyapp1income() != " " ? loanData.getApprxyrlyapp1income() : " ";

            String resultApprxyrlyApp1IncomeComma = getIndianCurrencyFormat(ApprxyrlyApp1Income);

            String FamilyExp = loanData.getFamilyexp() != null && loanData.getFamilyexp() != " " && !loanData.getFamilyexp().equalsIgnoreCase("null") ? loanData.getFamilyexp() : " ";

            String resultFamilyExpComma = getIndianCurrencyFormat(FamilyExp);

            String YrlySurplus = loanData.getYrlysurplus() != null && !loanData.getYrlysurplus().equalsIgnoreCase("null") && loanData.getYrlysurplus() != " " ? loanData.getYrlysurplus() : " ";

            String resultYrlySurplusComma = getIndianCurrencyFormat(YrlySurplus);
            String bconame = loanData.getBconame() != null && loanData.getBconame().equalsIgnoreCase("null") && loanData.getBconame() != " " ? loanData.getBconame() : " ";
            System.out.println("bconame::::::::"+bconame);
            String LoanAmount = loanData.getLoanamount() != null && !loanData.getLoanamount().equalsIgnoreCase("null") && loanData.getLoanamount() != " " ? loanData.getLoanamount() : " ";
            String resultLoanAmountComma = getIndianCurrencyFormat(LoanAmount);
            //document.newPage();
            Image img2 = Image.getInstance(imgPath);
            img2.setAbsolutePosition(250f, 740f);
            img2.scaleAbsolute(80f, 80f);
            document.add(img2);

            document.add(new Paragraph("\n"));
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("\n"));
            Paragraph pk4 = new Paragraph("", canFont);
            pk4.setAlignment(Element.ALIGN_CENTER);
            Chunk head1 = new Chunk("APPLICATION CUM APPRAISAL FORM");
            head1.setUnderline(1, -2f);
            pk4.add(head1);
            document.add(pk4);
            Paragraph pk5 = new Paragraph("(Based on Primary Information Sheet/ Check List)", canFont);
            pk5.setAlignment(Element.ALIGN_CENTER);
            document.add(pk5);

            Paragraph pk6 = new Paragraph("(For financing individuals under tie up arrangement with M/s. Basix Sub-K)", canFont);
            pk6.setAlignment(Element.ALIGN_CENTER);
            document.add(pk6);
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("\n"));
            PdfPTable table1 = new PdfPTable(2);
            table1.setWidthPercentage(100f);
            table1.setWidths(new float[]{3, 0.5f});
            table1.addCell(getCell("For office use only:", PdfPCell.ALIGN_LEFT));
            PdfPCell cell1 = new PdfPCell(new Phrase(" \n " + "Self-attested" + "\n" + "      recent" + "\n" + "photograph to be" + "\n" + "verified by branch" + "\n" + "      Official", subFont));
            cell1.setRowspan(6);
            table1.addCell(cell1);
            table1.addCell(getCell("Repeat Borrower: Yes/No", PdfPCell.ALIGN_LEFT));
            table1.addCell(getCell("Loan A/c number: ....................", PdfPCell.ALIGN_LEFT));
            table1.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
            table1.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
            table1.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
            table1.setSpacingAfter(6f);
            document.add(table1);
            document.add(new Paragraph("\n"));
            String branchname = loanData.getBranchname() != null && !loanData.getBranchname().equalsIgnoreCase("null") && !loanData.getBranchname().equals("") ? loanData.getBranchname() : "";
            Chunk br_name = new Chunk(branchname);
            br_name.setUnderline(0.1f, -2f);
            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(100);
            table.addCell(getCell("To,", PdfPCell.ALIGN_LEFT));
            table.addCell(getCell("  ", PdfPCell.ALIGN_RIGHT));
            table.addCell(getCell("Manager", PdfPCell.ALIGN_LEFT));
//            
            table.addCell(getCell(" BC Agent Name: "+bconame, PdfPCell.ALIGN_RIGHT));
            table.addCell(getCell("United Bank of India", PdfPCell.ALIGN_LEFT));
            table.addCell(getCell("Linked to Village: .................................", PdfPCell.ALIGN_RIGHT));
            table.addCell(getCell(br_name + "  Branch", PdfPCell.ALIGN_LEFT));
            table.addCell(getCell("Under Bank Branch: ..............................", PdfPCell.ALIGN_RIGHT));
            table.setSpacingAfter(6f);
            document.add(table);
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("\n"));

            String appl1Name = (loanData.getAppl1name() != null && loanData.getAppl1name() != "" && !loanData.getAppl1name().equalsIgnoreCase("null") ? loanData.getAppl1name() : " ");
            String appl2FhgName = (loanData.getFhgname() != null && loanData.getFhgname() != "" && !loanData.getFhgname().equalsIgnoreCase("null") ? loanData.getFhgname() : " ");
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String ApplicantDob = loanData.getDobdate() != null && loanData.getDobdate() != " " && !loanData.getDobdate().equalsIgnoreCase("null") ? loanData.getDobdate() : " ";
            Date birthDate = sdf.parse(ApplicantDob);
            int age = calculateAge(birthDate);
            String permAddress = (loanData.getPermhouseno() != null && !loanData.getPermhouseno().equalsIgnoreCase("null") && loanData.getPermhouseno() != " " ? loanData.getPermhouseno()+ "," : " ");
            permAddress += (loanData.getPermstreetno() != null && !loanData.getPermstreetno().equalsIgnoreCase("null") && loanData.getPermstreetno() != " " ? loanData.getPermstreetno()+ "," : " ");
            permAddress += (loanData.getPermlandmark() != null && !loanData.getPermlandmark().equalsIgnoreCase("null") && loanData.getPermlandmark() != " " ? loanData.getPermlandmark()+ "," : " ");
            permAddress += (loanData.getPermvillage() != null && !loanData.getPermvillage().equalsIgnoreCase("null") && loanData.getPermvillage() != " " ? loanData.getPermvillage()+ "," : " ");
            permAddress += (loanData.getPermdistrict() != null && !loanData.getPermdistrict().equalsIgnoreCase("null") && loanData.getPermdistrict() != " " ? loanData.getPermdistrict()+ "," : " ");		
            permAddress += (loanData.getPermsubdistrict() != null && !loanData.getPermsubdistrict().equalsIgnoreCase("null") && loanData.getPermsubdistrict() != " " ? loanData.getPermsubdistrict()+ "," : " ");
            permAddress += (loanData.getPermstate() != null && !loanData.getPermstate().equalsIgnoreCase("null") && loanData.getPermstate() != " " ? loanData.getPermstate()+ "," : " ");
            permAddress += (loanData.getPermpincode() != null && !loanData.getPermpincode().equalsIgnoreCase("null") && loanData.getPermpincode() != " " ? loanData.getPermpincode()+ "," : " ");
            permAddress += (loanData.getPermtelephone() != null && !loanData.getPermtelephone().equalsIgnoreCase("null") && loanData.getPermtelephone() != " " ? loanData.getPermtelephone()+ "," : " ");
            String loanAmount = (loanData.getLoanamount() != null && loanData.getLoanamount() != "" && !loanData.getLoanamount().equalsIgnoreCase("null") ? loanData.getLoanamount() : " ");
            String loanPurpose = (loanData.getLoanpurpose() != null && loanData.getLoanpurpose() != "" && !loanData.getLoanpurpose().equalsIgnoreCase("null") ? loanData.getLoanpurpose() : " ");
            String loanamt = getIndianCurrencyFormat(loanAmount);
            Paragraph paragraph1 = new Paragraph("", redFont);
            paragraph1.setAlignment(Element.ALIGN_JUSTIFIED);
            paragraph1.add("I,Sri/Smt.");
            String n = addingSpace(appl1Name, 80);
            Chunk name = new Chunk(" " + n,redFont1);
            name.setUnderline(0.6f, -2f);
            paragraph1.add(name);
            paragraph1.add("S/o/W/o/D/o");
            String n2 = addingSpace(appl2FhgName, 55);
            Chunk relname = new Chunk(" " + n2);
            relname.setUnderline(0.6f, -2f);
            paragraph1.add(relname);
            paragraph1.add("aged");
            Chunk year = new Chunk("  " + age + "          ",font11b);
            year.setUnderline(0.6f, -2f);
            paragraph1.add(year);
            paragraph1.add("resident of");
            String n4 = addingSpace(permAddress, 60);
            Chunk resident = new Chunk(" " + n4);
            resident.setUnderline(0.6f, -2f);
            paragraph1.add(resident);
            paragraph1.add("village would like to apply for a loan of Rs.");
            String n5 = addingSpace(loanamt+"/-", 15);
            Chunk money = new Chunk("  " + n5,font11b);
            money.setUnderline(0.6f, -2f);
            paragraph1.add(money);
            paragraph1.add("for the purpose of (specify)");
            String n6 = addingSpace(loanPurpose, 40);
            Chunk purpose = new Chunk(" " + n6);
            purpose.setUnderline(0.6f, -2f);
            paragraph1.add(purpose);
            document.add(paragraph1);
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("I furnish the following particulars:", canFont1));
            String appl1Occupations = null;
            String appl1Occupation = (loanData.getAppl1occupation() != null && loanData.getAppl1occupation() != "" && !loanData.getAppl1occupation().equalsIgnoreCase("null") ? loanData.getAppl1occupation() : " ");
            String permAddress1 = (loanData.getPermhouseno() != null && !loanData.getPermhouseno().equalsIgnoreCase("null") && loanData.getPermhouseno() != " " ? loanData.getPermhouseno()+ "," : " ");
            permAddress1 += (loanData.getPermstreetno() != null && !loanData.getPermstreetno().equalsIgnoreCase("null") && loanData.getPermstreetno() != " " ? loanData.getPermstreetno()+ "," : " ");
            permAddress1 += (loanData.getPermlandmark() != null && !loanData.getPermlandmark().equalsIgnoreCase("null") && loanData.getPermlandmark() != " " ? loanData.getPermlandmark()+ "," : " ");
            permAddress1 += (loanData.getPermvillage() != null && !loanData.getPermvillage().equalsIgnoreCase("null") && loanData.getPermvillage() != " " ? loanData.getPermvillage()+ "," : " ");
            permAddress1 += (loanData.getPermdistrict() != null && !loanData.getPermdistrict().equalsIgnoreCase("null") && loanData.getPermdistrict() != " " ? loanData.getPermdistrict()+ "," : " ");		
            permAddress1 += (loanData.getPermsubdistrict() != null && !loanData.getPermsubdistrict().equalsIgnoreCase("null") && loanData.getPermsubdistrict() != " " ? loanData.getPermsubdistrict()+ "," : " ");
            permAddress1 += (loanData.getPermstate() != null && !loanData.getPermstate().equalsIgnoreCase("null") && loanData.getPermstate() != " " ? loanData.getPermstate()+ "," : " ");
            permAddress1 += (loanData.getPermpincode() != null && !loanData.getPermpincode().equalsIgnoreCase("null") && loanData.getPermpincode() != " " ? loanData.getPermpincode()+ "," : " ");
            permAddress1 += (loanData.getPermtelephone() != null && !loanData.getPermtelephone().equalsIgnoreCase("null") && loanData.getPermtelephone() != " " ? loanData.getPermtelephone()+ "," : " ");
            String appl1Category = (loanData.getCategory() != null && loanData.getCategory() != "" && !loanData.getCategory().equalsIgnoreCase("null") ? loanData.getCategory() : " ");
//            String appl1Address = (loanData.getAppl1address() != null && loanData.getAppl1address() != "" && !loanData.getAppl1address().equalsIgnoreCase("null") ? loanData.getAppl1address() : " ");
            Paragraph paragraph5 = new Paragraph("", redFont);
            paragraph5.add("1. My main occupation is: ");
            String n7 = addingSpace(appl1Occupation.substring(0,1).toUpperCase()+appl1Occupation.substring(1), 50);
            Chunk occu = new Chunk(" " + n7);
            occu.setUnderline(0.6f, -2f);
            paragraph5.add(occu);
            document.add(paragraph5);
            Paragraph paragraph6 = new Paragraph("", redFont);
            paragraph6.add("2. I belong to: ");
            String n8 = addingSpace(appl1Category.substring(0,1).toUpperCase()+appl1Category.substring(1).toLowerCase(), 50);
            Chunk occu1 = new Chunk(" " + n8,redFont1);
            occu1.setUnderline(0.6f, -2f);
            paragraph6.add(occu1);
            document.add(paragraph6);
            Paragraph paragraph7 = new Paragraph("", redFont);
            paragraph7.add("3. Address: ");
            String n9 = addingSpace(permAddress1.toUpperCase(), 50);
            Chunk app_add = new Chunk(" " + n9,redFont1);
            app_add.setUnderline(0.6f, -2f);
            paragraph7.add(app_add);
            document.add(paragraph7);
            document.add(new Paragraph("4. SB a/c no._________________ Name of the bank_________________ Branch__________", redFont));
            document.add(new Paragraph("\n"));

            Paragraph paragraph8 = new Paragraph("5. Particulars of movable property owned by the family", canFont1);
            document.add(paragraph8);

            String amount1 = (loanData.getLivestock() != null && loanData.getLivestock().trim().length() != 0 ? loanData.getLivestock() : "    ");
            String amt1 = getIndianCurrencyFormat(amount1);
            String amount2 = (loanData.getAgrlimpl() != null && loanData.getAgrlimpl().trim().length() != 0 ? loanData.getAgrlimpl() : "    ");
            String amt2 = getIndianCurrencyFormat(amount2);
            String amount3 = (loanData.getAssetvehicle() != null && loanData.getAssetvehicle().trim().length() != 0 ? loanData.getAssetvehicle() : "     ");
            String amt3 = getIndianCurrencyFormat(amount3);
            String amount4 = (loanData.getAssetanyother() != null && loanData.getAssetanyother().trim().length() != 0 ? loanData.getAssetanyother() : "    ");
            String amt4 = getIndianCurrencyFormat(amount4);
            PdfPTable tablek3 = new PdfPTable(3);
            tablek3.setWidthPercentage(100);
            tablek3.setTotalWidth(288);
            tablek3.setWidths(new float[]{0.3f, 2, 1});
            document.add(new Paragraph("\n"));
            PdfPCell ck3 = new PdfPCell(new Phrase("S.No.", canFont1));
//             ck3.setBorder(PdfPCell.NO_BORDER);
            ck3.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek3.addCell(ck3);
            ck3 = new PdfPCell(new Phrase("Type of Asset Description", canFont1));
            ck3.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek3.addCell(ck3);
            ck3 = new PdfPCell(new Phrase("Approximate Market Value", canFont1));
            ck3.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek3.addCell(ck3);

            ck3 = new PdfPCell(new Phrase("1", redFont));
            ck3.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek3.addCell(ck3);
            tablek3.addCell(new Phrase("Livestock", redFont));
            tablek3.addCell(new Phrase("Rs. " + amt1 + "/-", redFont));

            ck3 = new PdfPCell(new Phrase("2", redFont));
            ck3.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek3.addCell(ck3);
            tablek3.addCell(new Phrase("Agricultural implements", redFont));
            tablek3.addCell(new Phrase("Rs. " + amt2 + "/-", redFont));

            ck3 = new PdfPCell(new Phrase("3", redFont));
            ck3.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek3.addCell(ck3);
            tablek3.addCell(new Phrase("Vehicles", redFont));
            tablek3.addCell(new Phrase("Rs. " + amt3 + "/-", redFont));

            ck3 = new PdfPCell(new Phrase("4", redFont));
            ck3.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek3.addCell(ck3);
            tablek3.addCell(new Phrase("Any Other", redFont));
            tablek3.addCell(new Phrase("Rs. " + amt4 + "/-", redFont));
            document.add(tablek3);
//            document.add(new Paragraph("\n\n\n\n\n"));
//            Paragraph footer1 = new Paragraph("Page(1 of 3)", redFont);
//            footer1.setAlignment(Element.ALIGN_RIGHT);
//            document.add(footer1);
            document.newPage();
            document.add(new Paragraph("\n"));

            Paragraph pk8 = new Paragraph("6. Family details", canFont1);
            pk8.setAlignment(Element.ALIGN_LEFT);
            document.add(pk8);

            PdfPTable tablek4 = new PdfPTable(7);
            tablek4.setWidthPercentage(100f);
            //  tablek3.setTotalWidth(288);
            //tablek3.setWidths(new float[]{2, 4, 5, 3, 2, 5, 6});//original
            tablek4.setWidths(new float[]{2, 5.2f, 4, 2.8f, 1.8f, 5, 5});
            document.add(new Paragraph("\n"));
            //table.setHorizontalAlignment(Element.ALIGN_CENTER);
            PdfPCell ck4 = new PdfPCell(new Phrase("S. No.", canFont1));
            ck4.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek4.addCell(ck4);
            ck4 = new PdfPCell(new Phrase("Name", canFont1));
            ck4.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek4.addCell(ck4);
            ck4 = new PdfPCell(new Phrase("Relationship", canFont1));
            ck4.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek4.addCell(ck4);
            ck4 = new PdfPCell(new Phrase("Sex", canFont1));
            ck4.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek4.addCell(ck4);
            ck4 = new PdfPCell(new Phrase("Age", canFont1));
            ck4.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek4.addCell(ck4);
            ck4 = new PdfPCell(new Phrase("Occupation", canFont1));
            ck4.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek4.addCell(ck4);
            ck4 = new PdfPCell(new Phrase("Educational Status", canFont1));
            ck4.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek4.addCell(ck4);

            String FamilyMemb1Relation = (loanData.getFamilymemb1relation() != null && !loanData.getFamilymemb1relation().equalsIgnoreCase("null") && loanData.getFamilymemb1relation() != "" && !loanData.getFamilymemb1relation().equalsIgnoreCase("select") ? loanData.getFamilymemb1relation() : " ");//getJsData("getRelation", loanData.getFamilymemb1relation(), jsPaths)
            String FamilyMemb1Occup = (loanData.getFamilymemb1Occup() != null && !loanData.getFamilymemb1Occup().equalsIgnoreCase("null") && loanData.getFamilymemb1Occup() != "" && !loanData.getFamilymemb1Occup().equalsIgnoreCase("select") ? loanData.getFamilymemb1Occup() : " ");//getJsData("getOccupation", loanData.getFamilymemb1Occup(), jsPaths)
            String FamilyMemb1Name = (loanData.getFamilymemb1name() != null && !loanData.getFamilymemb1name().equalsIgnoreCase("null") && loanData.getFamilymemb1name() != " " ? loanData.getFamilymemb1name() : " ");
            String FamilyMemb1age = (loanData.getFamilymemb1age() != null && !loanData.getFamilymemb1age().equalsIgnoreCase("null") && loanData.getFamilymemb1age() != "" && !loanData.getFamilymemb1age().equalsIgnoreCase("select") ? loanData.getFamilymemb1age() : "");
            String FamilyMemb1qualif = (loanData.getFamilymemb1qualification() != null && !loanData.getFamilymemb1qualification().equalsIgnoreCase("null") && loanData.getFamilymemb1qualification() != "" && !loanData.getFamilymemb1qualification().equalsIgnoreCase("select") ? loanData.getFamilymemb1qualification() : "");
            if (FamilyMemb1Name != null && !FamilyMemb1Name.equalsIgnoreCase("null") && !FamilyMemb1Name.equals("")) {
                ck4 = new PdfPCell(new Phrase("1", redFont));
                ck4.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablek4.addCell(ck4);
                tablek4.addCell(new Phrase(FamilyMemb1Name.toLowerCase(), redFont));
                tablek4.addCell(new Phrase(FamilyMemb1Relation.toLowerCase(), redFont));
                tablek4.addCell(new Phrase(loanData.getFamilymemb1sex() != null && !loanData.getFamilymemb1sex().equalsIgnoreCase("null") && loanData.getFamilymemb1sex() != "" ? loanData.getFamilymemb1sex().toLowerCase() : "", redFont));
                tablek4.addCell(new Phrase(FamilyMemb1age, redFont));
                tablek4.addCell(new Phrase(FamilyMemb1Occup.toLowerCase(), redFont));
                tablek4.addCell(new Phrase(FamilyMemb1qualif.toLowerCase(), redFont));
            } else {
                tablek4.addCell(new Phrase("", redFont));
                tablek4.addCell(new Phrase(" ", redFont));
                tablek4.addCell(new Phrase(" ", redFont));
                tablek4.addCell(new Phrase(" ", redFont));
                tablek4.addCell(new Phrase(" ", redFont));
                tablek4.addCell(new Phrase(" ", redFont));
                tablek4.addCell(new Phrase(" ", redFont));
            }
            String FamilyMemb2Relation = (loanData.getFamilymemb2relation() != null && !loanData.getFamilymemb2relation().equalsIgnoreCase("null") && loanData.getFamilymemb2relation() != "" && !loanData.getFamilymemb2relation().equalsIgnoreCase("select") ? loanData.getFamilymemb2relation() : " ");//getJsData("getRelation", loanData.getFamilymemb2relation(), jsPaths)
            String FamilyMemb2Occup = (loanData.getFamilymemb2occup() != null && !loanData.getFamilymemb2occup().equalsIgnoreCase("null") && loanData.getFamilymemb2occup() != "" && !loanData.getFamilymemb2occup().equalsIgnoreCase("select") ? loanData.getFamilymemb2occup() : " ");// getJsData("getOccupation", loanData.getFamilymemb2occup(), jsPaths)
            String familyMem2Sex = (loanData.getFamilymemb2sex() != null && !loanData.getFamilymemb2sex().equalsIgnoreCase("null") && loanData.getFamilymemb2sex() != "" ? loanData.getFamilymemb2sex() : " ");
            String FamilyMemb2Name = (loanData.getFamilymemb2name() != null && !loanData.getFamilymemb2name().equalsIgnoreCase("null") && loanData.getFamilymemb2name() != "" ? loanData.getFamilymemb2name() : " ");
            String FamilyMemb2Age = (loanData.getFamilymemb2age() != null && !loanData.getFamilymemb2age().equalsIgnoreCase("null") && loanData.getFamilymemb2age() != "" && !loanData.getFamilymemb2age().equalsIgnoreCase("select") ? loanData.getFamilymemb2age() : "");
            String FamilyMemb2Qualification = (loanData.getFamilymemb2qualification() != null && !loanData.getFamilymemb2qualification().equalsIgnoreCase("null") && loanData.getFamilymemb2qualification() != "" && !loanData.getFamilymemb2qualification().equalsIgnoreCase("select") ? loanData.getFamilymemb2qualification() : "");
            if (FamilyMemb2Name != null && !FamilyMemb2Name.equalsIgnoreCase("null") && !FamilyMemb2Name.equals("")) {
                ck4 = new PdfPCell(new Phrase("2", redFont));
                ck4.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablek4.addCell(ck4);
                tablek4.addCell(new Phrase(FamilyMemb2Name.toLowerCase(), redFont));
                tablek4.addCell(new Phrase(FamilyMemb2Relation.toLowerCase(), redFont));
                tablek4.addCell(new Phrase(familyMem2Sex.toLowerCase(), redFont));
                tablek4.addCell(new Phrase((FamilyMemb2Age), redFont));
                tablek4.addCell(new Phrase(FamilyMemb2Occup.toLowerCase(), redFont));
                tablek4.addCell(new Phrase((FamilyMemb2Qualification.toLowerCase()), redFont));
            } else {
                tablek4.addCell(new Phrase(" ", redFont));
                tablek4.addCell(new Phrase(" ", redFont));
                tablek4.addCell(new Phrase(" ", redFont));
                tablek4.addCell(new Phrase(" ", redFont));
                tablek4.addCell(new Phrase(" ", redFont));
                tablek4.addCell(new Phrase(" ", redFont));
                tablek4.addCell(new Phrase(" ", redFont));
            }
            String FamilyMemb3Name = (loanData.getFamilymemb3name() != null && !loanData.getFamilymemb3name().equalsIgnoreCase("null") && loanData.getFamilymemb3name() != "" ? loanData.getFamilymemb3name() : " ");
            String FamilyMemb3Occup = (loanData.getFamilymemb3occup() != null && !loanData.getFamilymemb3occup().equalsIgnoreCase("null") && loanData.getFamilymemb3occup() != "" && !loanData.getFamilymemb3occup().equalsIgnoreCase("select") ? loanData.getFamilymemb3occup() : " ");//getJsData("getOccupation", loanData.getFamilymemb3occup(), jsPaths) 
            String FamilyMemb3Relation = (loanData.getFamilymemb3relation() != null && !loanData.getFamilymemb3relation().equalsIgnoreCase("null") && loanData.getFamilymemb3relation() != "" && !loanData.getFamilymemb3relation().equalsIgnoreCase("select") ? loanData.getFamilymemb3relation() : " ");//getJsData("getRelation", loanData.getFamilymemb3relation(), jsPaths)
            String FamilyMemb3Sex = (loanData.getFamilymemb3sex() != null && !loanData.getFamilymemb3sex().equalsIgnoreCase("null") && loanData.getFamilymemb3sex() != "" ? loanData.getFamilymemb3sex() : "");
            String FamilyMemb3Age = (loanData.getFamilymemb3age() != null && !loanData.getFamilymemb3age().equalsIgnoreCase("null") && loanData.getFamilymemb3age() != "" && !loanData.getFamilymemb3age().equalsIgnoreCase("select") ? loanData.getFamilymemb3age() : "");
            String FamilyMemb3Qualification = (loanData.getFamilymemb3qualification() != null && !loanData.getFamilymemb3qualification().equalsIgnoreCase("null") && loanData.getFamilymemb3qualification() != "" && !loanData.getFamilymemb3qualification().equalsIgnoreCase("select") ? loanData.getFamilymemb3qualification() : "");
            if (FamilyMemb3Name != null && !FamilyMemb3Name.equalsIgnoreCase("null") && !FamilyMemb3Name.equals("")) {
                ck4 = new PdfPCell(new Phrase("3", redFont));
                ck4.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablek4.addCell(ck4);
                tablek4.addCell(new Phrase(FamilyMemb3Name.toLowerCase(), redFont));
                tablek4.addCell(new Phrase(FamilyMemb3Relation.toLowerCase(), redFont));
                tablek4.addCell(new Phrase(FamilyMemb3Sex.toLowerCase(), redFont));
                tablek4.addCell(new Phrase(FamilyMemb3Age, redFont));
                tablek4.addCell(new Phrase(FamilyMemb3Occup.toLowerCase(), redFont));
                tablek4.addCell(new Phrase(FamilyMemb3Qualification.toLowerCase(), redFont));
            } else {
                tablek4.addCell(new Phrase("", redFont));
                tablek4.addCell(new Phrase(" ", redFont));
                tablek4.addCell(new Phrase(" ", redFont));
                tablek4.addCell(new Phrase(" ", redFont));
                tablek4.addCell(new Phrase(" ", redFont));
                tablek4.addCell(new Phrase(" ", redFont));
                tablek4.addCell(new Phrase(" ", redFont));
            }
            document.add(tablek4);
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("7. Present earning & expenditure of applicant or other members of family:\n", canFont1));
            String applIncome = "";
            if (loanData.getApprxyrlyapp1income() != null && !loanData.getApprxyrlyapp1income().equalsIgnoreCase("null")) {
                applIncome = "Rs. " + resultApprxyrlyApp1IncomeComma + "/-";
            }
            String annualExp = "";
            if (loanData.getFamilyexp() != null && loanData.getFamilyexp().trim().length() != 0) {
                annualExp = "Rs. " + resultFamilyExpComma + "/-";
            }
            String annualSurplus = "";
            if (loanData.getYrlysurplus() != null && loanData.getYrlysurplus().trim().length() != 0) {
                annualSurplus = "Rs. " + resultYrlySurplusComma + "/-";
            }
            PdfPTable tablek5 = new PdfPTable(3);
            tablek5.setWidthPercentage(100f);
            tablek5.setTotalWidth(288);
            tablek5.setWidths(new float[]{0.25f, 2, 1});
            document.add(new Paragraph("\n"));
            PdfPCell ck5 = new PdfPCell(new Phrase("S.No.", canFont1));
            ck5.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek5.addCell(ck5);
            ck5 = new PdfPCell(new Phrase("Parameter", canFont1));
            ck5.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek5.addCell(ck5);
            ck5 = new PdfPCell(new Phrase("Amount", canFont1));
            ck5.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek5.addCell(ck5);
            ck5 = new PdfPCell(new Phrase("1", redFont));
            ck5.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek5.addCell(ck5);
            tablek5.addCell(new Phrase("Annual Income ", redFont));
            tablek5.addCell(new Phrase(applIncome, redFont));

            ck5 = new PdfPCell(new Phrase("2", redFont));
            ck5.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek5.addCell(ck5);
            tablek5.addCell(new Phrase("Annual Expenditure ", redFont));
            tablek5.addCell(new Phrase(annualExp, redFont));

            ck5 = new PdfPCell(new Phrase("3", redFont));
            ck5.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek5.addCell(ck5);
            tablek5.addCell(new Phrase("Annual Surplus ", redFont));
            tablek5.addCell(new Phrase(annualSurplus, redFont));
            document.add(tablek5);
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("8. Loan, if already availed\n", canFont1));
            String loanSource = "";
            logger.info(("Source:********************** " + loanSource));

            if (loanData.getLoansource() != null && loanData.getLoansource().trim().length() != 0) {
                loanSource = loanData.getLoansource();
            }

            String amtBorrowed = "";
            if (loanData.getAmountborrowed() != null && loanData.getAmountborrowed().trim().length() != 0) {
                amtBorrowed = ("Rs. " + getIndianCurrencyFormat(loanData.getAmountborrowed()) + "/-");
            }
            String loanAmtDue = " ";
            if (loanData.getLoanamountdue() != null && loanData.getLoanamountdue().trim().length() != 0) {
                loanAmtDue = ("Rs. " + getIndianCurrencyFormat(loanData.getLoanamountdue()) + "/-");
            }
            PdfPTable tablek6 = new PdfPTable(3);
            tablek6.setWidthPercentage(100f);
            tablek6.setTotalWidth(288);
            tablek6.setWidths(new float[]{0.25f, 2, 1});
            document.add(new Paragraph("\n"));
            PdfPCell ck6 = new PdfPCell(new Phrase("S.No.", canFont1));
            ck6.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek6.addCell(ck6);
            ck6 = new PdfPCell(new Phrase("Parameter", canFont1));
            ck6.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek6.addCell(ck6);
            ck6 = new PdfPCell(new Phrase("Amount", canFont1));
            ck6.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek6.addCell(ck6);
            ck6 = new PdfPCell(new Phrase("1", redFont));
            ck6.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek6.addCell(ck6);
            tablek6.addCell(new Phrase("Source ", redFont));
            tablek6.addCell(new Phrase(loanSource, redFont));

            ck6 = new PdfPCell(new Phrase("2", redFont));
            ck6.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek6.addCell(ck6);
            tablek6.addCell(new Phrase("Purpose ", redFont));
            tablek6.addCell(new Phrase((loanData.getLoanpurpose() != null && !loanData.getLoanpurpose().equalsIgnoreCase("null") ? loanData.getLoanpurpose() : " "), redFont));

            ck6 = new PdfPCell(new Phrase("3", redFont));
            ck6.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek6.addCell(ck6);
            tablek6.addCell(new Phrase("Amount Borrowed ", redFont));
            tablek6.addCell(new Phrase(amtBorrowed, redFont));

            ck6 = new PdfPCell(new Phrase("4", redFont));
            ck6.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek6.addCell(ck6);
            tablek6.addCell(new Phrase("Amount Due ", redFont));
            tablek6.addCell(new Phrase(loanAmtDue, redFont));
            document.add(tablek6);
            String activity =  (loanData.getActivity() != null && loanData.getActivity().equalsIgnoreCase("null") && loanData.getActivity() != " " ? loanData.getActivity() : " ");
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("9. Financial Assistance sought for\n", canFont1));
            document.add(new Paragraph("Activity: "+ activity+"\n", redFont));
            document.add(new Paragraph("Loan required: ", redFont));
            document.add(new Paragraph((loanData.getLoanamount() != null && !loanData.getLoanamount().equalsIgnoreCase("null") ? "Rs. " + resultLoanAmountComma + "/-" : " ") + "\n",font11b));
             //document.add(new Paragraph("\n"));
            document.add(new Paragraph("\n"));
            Paragraph pk10 = new Paragraph("Nomination:", canFont1);
            document.add(pk10);
            String loanAccNomineeRelation = (loanData.getLoanaccnomineerelation() != null && loanData.getLoanaccnomineerelation() != "" && !loanData.getLoanaccnomineerelation().equalsIgnoreCase("null") ? loanData.getLoanaccnomineerelation() : " ");//getJsData("getRelation", loanData.getLoanaccnomineerelation(), jsPaths)
            String loanAccNomineeName = (loanData.getLoanaccnomineename() != null && loanData.getLoanaccnomineename().equalsIgnoreCase("null") && loanData.getLoanaccnomineename() != " " ? loanData.getLoanaccnomineename() : " ");
            String loanAccNomineeAge = (loanData.getLoanaccnomineeage() != null && loanData.getLoanaccnomineeage() != " " && !loanData.getLoanaccnomineeage().equalsIgnoreCase("null") ? loanData.getLoanaccnomineeage() : " ");
            String loanAccNomineeResident = (loanData.getLoanaccnomineeresident() != null && loanData.getLoanaccnomineeresident() != " " && !loanData.getLoanaccnomineeresident().equalsIgnoreCase("null") ? loanData.getLoanaccnomineeresident() : " ");
            document.add(new Paragraph("In case of my death/permanent disability, my nominee" + loanAccNomineeName + " who is, my " + loanAccNomineeRelation + " (relationship with customer), aged " + loanAccNomineeAge + " years, resident of " + loanAccNomineeResident + " , shall be entitled to receive and settle any claims from/ to Bank on my behalf. \n\n", redFont));
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("\n"));
            PdfPTable table6 = new PdfPTable(2);
            table6.setWidthPercentage(100f);
            table6.setWidths(new float[]{3, 2});
            table6.addCell(getCell("Place:", PdfPCell.ALIGN_LEFT));
            table6.addCell(getCell("Applicant’s Signature", PdfPCell.ALIGN_RIGHT));
            table6.addCell(getCell("Date:", PdfPCell.ALIGN_LEFT));
            table6.addCell(getCell(" ", PdfPCell.ALIGN_RIGHT));
            document.add(table6);
//            document.add(new Paragraph("                                                                                                                                        Applicant’s Signature\n"
//                    + "Place:\n"
//                    + "Date:\n\n",redFont));
            Chunk chunk2 = new Chunk(loanAccNomineeName);
            chunk2.setUnderline(1.5f, 3.5f);

            Chunk chunk3 = new Chunk(loanAccNomineeRelation);
            chunk3.setUnderline(1.5f, 3.5f);
            Chunk chunk4 = new Chunk(loanAccNomineeAge);
            chunk4.setUnderline(1.5f, 3.5f);
            Chunk chunk5 = new Chunk(loanAccNomineeResident);
            chunk5.setUnderline(1.5f, 3.5f);
//            document.add(new Paragraph("\n\n\n\n\n\n\n"));
//            Paragraph footer2 = new Paragraph("Page(2 of 3)", redFont);
//            footer2.setAlignment(Element.ALIGN_RIGHT);
//            document.add(footer2);
            String familyincome = (loanData.getTotalyrlyfamilyincome() != null && loanData.getTotalyrlyfamilyincome().equalsIgnoreCase("null") && loanData.getTotalyrlyfamilyincome() != " " ? loanData.getTotalyrlyfamilyincome() : " ");
            String bccomments = (loanData.getBcremarks() != null && loanData.getBcremarks().equalsIgnoreCase("null") && loanData.getBcremarks() != " " ? loanData.getBcremarks() : " ");
            System.out.println("bccomments:::::::::::::"+bccomments);
            System.out.println("activity::::::::::::::"+activity);
            System.out.println("income:::::::::::::::"+familyincome);
            document.newPage();
            document.add(new Paragraph("\n"));
            Paragraph pk11 = new Paragraph("Due Diligence at Basix CRO Level (as per Primary Information Sheet/ Check List): \n", canFont1);
            document.add(pk11);

            PdfPTable tablek7 = new PdfPTable(3);
            tablek7.setWidthPercentage(100);
            tablek7.setTotalWidth(288);
            tablek7.setWidths(new float[]{0.3f, 2, 1});
            document.add(new Paragraph("\n"));
            PdfPCell ck7 = new PdfPCell(new Phrase("S.No.", canFont1));
            ck7.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek7.addCell(ck7);
            ck7 = new PdfPCell(new Phrase("Parameters", canFont1));
            ck7.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek7.addCell(ck7);
            ck7 = new PdfPCell(new Phrase("Remarks", canFont1));
            ck7.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek7.addCell(ck7);

            ck7 = new PdfPCell(new Phrase("1", redFont));
            ck7.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek7.addCell(ck7);
            tablek7.addCell(new Phrase("Comments of Basix CRO on-", redFont));
            tablek7.addCell(new Phrase(bccomments, redFont));

            ck7 = new PdfPCell(new Phrase("2", redFont));
            ck7.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek7.addCell(ck7);
            tablek7.addCell(new Phrase("Activity of Applicant", redFont));
            tablek7.addCell(new Phrase(activity, redFont));

            ck7 = new PdfPCell(new Phrase("3", redFont));
            ck7.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek7.addCell(ck7);
            tablek7.addCell(new Phrase("Family Income", redFont));
            tablek7.addCell(new Phrase(familyincome+"/-", redFont));

            ck7 = new PdfPCell(new Phrase("4", redFont));
            ck7.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek7.addCell(ck7);
            tablek7.addCell(new Phrase("Reputation at Locality", redFont));
            tablek7.addCell(new Phrase((loanData.getUhremarks() != null && !loanData.getUhremarks().equalsIgnoreCase("null") ? loanData.getUhremarks() : " "), redFont));

            ck7 = new PdfPCell(new Phrase("5", redFont));
            ck7.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek7.addCell(ck7);
            tablek7.addCell(new Phrase("Credit worthiness of Applicant", redFont));
            tablek7.addCell(new Phrase((loanData.getUhremarks() != null && !loanData.getUhremarks().equalsIgnoreCase("null") ? loanData.getUhremarks() : " "), redFont));

            ck7 = new PdfPCell(new Phrase("6", redFont));
            ck7.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek7.addCell(ck7);
            tablek7.addCell(new Phrase("Credit record/ information of applicant", redFont));
            tablek7.addCell(new Phrase(" ", redFont));

            ck7 = new PdfPCell(new Phrase("7", redFont));
            ck7.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablek7.addCell(ck7);
            tablek7.addCell(new Phrase("Credit record/ information of applicant\n(As per Highmark/ Experian/ Equifax)", redFont));
            tablek7.addCell(new Phrase(" ", redFont));
            document.add(tablek7);

//            Paragraph pk12 = new Paragraph("Comments of Basix CRO on-\n", redFont);
//            document.add(pk12);
//            Paragraph pk13 = new Paragraph("Activity of Applicant:\n", redFont);
//            document.add(pk13);
//            Paragraph pk14 = new Paragraph("Family income:\n", redFont);
//            document.add(pk14);
//
//            Paragraph pk15 = new Paragraph("Reputation at Locality:  ",redFont);
//            Chunk chunk13 = new Chunk((loanData.getUhRemarks() != null && !loanData.getUhRemarks().equalsIgnoreCase("null") ? loanData.getUhRemarks() : " "),redFont);
//            pk15.add((" "+chunk13 +"\n"));
//            document.add(pk15);
//
//            Paragraph pk16 = new Paragraph("Credit worthiness of Applicant:  ", redFont);
//             Chunk chunk14 = new Chunk((loanData.getUhRemarks() != null && !loanData.getUhRemarks().equalsIgnoreCase("null") ? loanData.getUhRemarks() : " "),redFont);
//            pk16.add((" "+chunk14 +"\n"));
//            document.add(pk16);
//            Paragraph pk17 = new Paragraph("Credit record/ information of applicant:\n", redFont);
//            document.add(pk17);
//            Paragraph pk18 = new Paragraph("(As per Highmark/ Experian/ Equifax)\n", redFont);
//            document.add(pk18);
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("Credibility Check done with _____________________________________________________\n"
                    + "Sub-K CSR may attach separate sheet along with this form for furnishing comments in detail,\n"
                    + "if necessary."
                    + "\n"
                    + "\n"
                    + "Please refer physical ‘Form 2’ for detailed appraisal and cash flow data as performed by Sub-K representative.\n"
                    + "\n"
                    + "\n"
                    + "Signature of Sub-K CSR \n"
                    + "Device ID No.\n"
                    + "\n"
                    + "Village:", redFont));
            Paragraph pk19 = new Paragraph("Report of BC Agent is verified \n", canFont1);
            document.add(pk19);
            Paragraph pk20 = new Paragraph("Area Manager/ District Manager of Basix Sub-K\n\n", canFont1);
            document.add(pk20);
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("Signature"
                    + "\n"
                    + "Place:"
                    + "\n"
                    + "Date:", redFont));
//            document.add(new Paragraph("\n\n\n\n\n\n\n\n\n"));
//            Paragraph footer3 = new Paragraph("pages(3 of 3)", redFont);
//            footer3.setAlignment(Element.ALIGN_RIGHT);
//            document.add(footer3);
        } catch (FileNotFoundException fnf) {
            logger.error("Error:.....!!! " + fnf.getMessage());
        } catch (DocumentException de) {
            logger.error("Error:.....!!! " + de.getMessage());
        } catch (IOException ex) {
            logger.error("Error:.....!!! " + ex.getMessage());
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(GenerateLoanPDFFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        document.close();
    }

    public Chunk underLine(String data) {
        Paragraph pp9 = new Paragraph();
        pp9.setAlignment(Element.ALIGN_JUSTIFIED);
        Chunk underline1 = new Chunk(data, new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL));
        underline1.setUnderline(0.1f, -2f);
        pp9.add(underline1);
        return underline1;
    }

    public PdfPCell getCell(String text, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text, redFont));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }

    private static int calculateAge(Date birthDate) {
        int years = 0;
        int months = 0;
        int days = 0;

        //create calendar object for birth day
        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(birthDate.getTime());

        //create calendar object for current day
        long currentTime = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);

        //Get difference between years
        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        int currMonth = now.get(Calendar.MONTH) + 1;
        int birthMonth = birthDay.get(Calendar.MONTH) + 1;

        //Get difference between months
        months = currMonth - birthMonth;

        //if month difference is in negative then reduce years by one
        //and calculate the number of months.
        if (months < 0) {
            years--;
            months = 12 - birthMonth + currMonth;
            if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                months--;
            }
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            years--;
            months = 11;
        }

        //Calculate the days
        if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE)) {
            days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
        } else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            int today = now.get(Calendar.DAY_OF_MONTH);
            now.add(Calendar.MONTH, -1);
            days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
        } else {
            days = 0;
            if (months == 12) {
                years++;
                months = 0;
            }
        }
        //Create new Age object
        return (years);
    }

    public PdfPCell get1Cell(String text, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text, redFont));
        cell.setPadding(1);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }

    public String addingSpace(String text, int space) {
        String name = text;
        int len = name.length();
        int s = space - len;
        for (int i = 0; i < s; i++) {
            name = name + " ";
        }
        return name;
    }

    public String getIndianCurrencyFormat(String amount) {
        StringBuilder stringBuilder = new StringBuilder();
        char amountArray[] = amount.toCharArray();
        int a = 0, b = 0;
        for (int i = amountArray.length - 1; i >= 0; i--) {
            if (a < 3) {
                stringBuilder.append(amountArray[i]);
                a++;
            } else if (b < 2) {
                if (b == 0) {
                    stringBuilder.append(",");
                    stringBuilder.append(amountArray[i]);
                    b++;
                } else {
                    stringBuilder.append(amountArray[i]);
                    b = 0;
                }
            }
        }
        return stringBuilder.reverse().toString();
    }

}
