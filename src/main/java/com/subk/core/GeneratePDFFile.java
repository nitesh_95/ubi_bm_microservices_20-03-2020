/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.subk.core;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.subk.entity.Loanorigination;
import com.subk.util.AddressCodes;

/**
 *
 * @author raj
 */
@Configuration
public class GeneratePDFFile extends PDFGenerationAbstract {
	
	@Autowired
	AddressCodes addresscodes;

	public static final Logger logger = Logger.getLogger(GeneratePDFFile.class);
//    public static final ResourceBundle rb = ResourceBundle.getBundle("systemPath");

	public String getPdf(Loanorigination loanData, String fileName, String pathAdd, String img, String jsPaths) {
		logger.info("************ ENTERED ************* ");
		String destPath = null;
		logger.info("File path:::::::::::::::::: " + pathAdd);
		logger.debug("UBILoanOrigination Data: " + loanData.toString());
		logger.debug("FileName: " + fileName);

		try {
			destPath = pathAdd;
			destPath = destPath + fileName;
			new GeneratePDFFile().createPdf(destPath, loanData, img, jsPaths);
		} catch (Exception ex) {
			logger.error("Error:....!!! " + ex.getMessage());
		}

		logger.info("DESTINATION PATH:...........!!!!!!!!!!  " + destPath);
		logger.info("************ ENDED.************* ");
		return destPath;
	}

	public void createPdf(String dest, Loanorigination loanData, String imgData, String jsPaths) {
		logger.debug("***********Entered***********");
		final String imgPath = imgData;
		System.out.println("imgData::::::::" + imgData);

		Document document = null;
		PdfWriter writer = null;

		try {
			document = new Document();
			writer = PdfWriter.getInstance(document, new FileOutputStream(dest, false));
			

			Font canFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
			Font canFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 13, Font.BOLD);
			Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
			
			Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);
			Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLDITALIC);
			Font canFont2 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
			HeaderFooterPageEvent event = new HeaderFooterPageEvent();
			
			writer.setPageEvent(event);
			document.open();
			document.addKeywords("Java, Servelet, PDF, iText");
			document.addAuthor("Raj Grover");
			document.addCreator("UBILoanOrigination");

			Image img1 = Image.getInstance(imgPath);
			System.out.println(imgPath);
			img1.setAbsolutePosition(250f, 740f);
			img1.scaleAbsolute(80f, 80f);
			document.add(img1);

			document.add(new Paragraph("\n\n\n"));
			Paragraph pr1 = new Paragraph("SAVINGS BANK ACCOUNT FORM", canFont);
			pr1.setAlignment(Element.ALIGN_CENTER);
			pr1.add(new Paragraph(" "));
			document.add(pr1);

			PdfPTable tableR1 = new PdfPTable(17);
			tableR1.setTotalWidth(240f);
			tableR1.setWidths(new float[] { 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });
			tableR1.setHorizontalAlignment(Element.ALIGN_LEFT);
			PdfPCell cr1 = new PdfPCell(new Phrase("For Bank Use Only", canFont));
			cr1.setColspan(17);
			tableR1.addCell(cr1);

			cr1 = new PdfPCell(new Phrase("Name & Code of the Branch", canFont));
			cr1.setColspan(17);
			tableR1.addCell(cr1);

			tableR1.addCell(new Paragraph("Cust ID", canFont));
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");

			tableR1.addCell(new Paragraph("A/C No.", canFont));
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			tableR1.addCell("");
			if (loanData.getApplicantphotopath() != null && !loanData.getApplicantphotopath().equalsIgnoreCase("")) {
				Image img11 = Image.getInstance(loanData.getApplicantphotopath());
				img11.setAbsolutePosition(470f, 650f);
//              img11.scaleAbsolute(60f, 60f);
				img11.scaleToFit(60, 70);
				document.add(img11);
			} else {
				document.add(new Paragraph(""));
			}
			document.add(tableR1);
			Paragraph pr2 = new Paragraph("[FOR SMALL ACCOUNT]\n", canFont);
			document.add(pr2);
			String nameVal = (loanData.getAppl1name() != null && !loanData.getAppl1name().equalsIgnoreCase("null")
					? loanData.getAppl1name()
					: " ");
			String fatherNameVal = (loanData.getFhgname() != null && !loanData.getWitness1add().equalsIgnoreCase("null")
					? loanData.getFhgname()
					: " ");
			String residenceAddrsVal = (loanData.getAppl1address() != null
					&& !loanData.getAppl1address().equalsIgnoreCase("null") ? loanData.getAppl1address() : " ");
			String careOfVall = (loanData.getFhgname() != null && !loanData.getFhgname().equalsIgnoreCase("null")
					? loanData.getFhgname()
					: " ");
			String houseNo_nameVal = (loanData.getPermhouseno() != null
					&& !loanData.getPermhouseno().equalsIgnoreCase("null") ? loanData.getPermhouseno() : " ");
			String streetNo_nameVal = (loanData.getPermstreetno() != null
					&& !loanData.getPermstreetno().equalsIgnoreCase("null") ? loanData.getPermstreetno() : " ");
			String landmarkVal = (loanData.getPermlandmark() != null
					&& !loanData.getPermlandmark().equalsIgnoreCase("null") ? loanData.getPermlandmark() : " ");
//            String villageOrCityVals =  null;
			String villageOrCityVal = (loanData.getPermvillage() != null
					&& !loanData.getPermvillage().equalsIgnoreCase("null") && loanData.getPermvillage() != " "
					?loanData.getPermvillage() : " ");
			// logger.info("DistrictVal:.......!!! "+loanData.getPermDistrict());
			String districtVal = (loanData.getPermdistrict() != null
					&& !loanData.getPermdistrict().equalsIgnoreCase("null") && loanData.getPermdistrict() != " "
							? loanData.getPermdistrict() : " ");
			String stateVal = (loanData.getPermstate() != null && !loanData.getPermstate().equalsIgnoreCase("null")
					&& loanData.getPermstate() != " " ? loanData.getPermstate() : " ");
			String pincodeVal = (loanData.getPermpincode() != null
					&& !loanData.getPermpincode().equalsIgnoreCase("null") ? loanData.getPermpincode() : " ");
			String telephoneOrLandlineVal = (loanData.getPermtelephone() != null
					&& !loanData.getPermtelephone().equalsIgnoreCase("null") ? loanData.getPermtelephone() : " ");
			String mobileVal = (loanData.getMobileno() != null && !loanData.getMobileno().equalsIgnoreCase("null")
					? loanData.getMobileno()
					: " ");
			String sex = (loanData.getSex() != null && !loanData.getSex().equalsIgnoreCase("null")
					? loanData.getSex()
					: " ");
			String sexVal = sex.substring(0, 1).toUpperCase() + sex.substring(1).toLowerCase();
			System.out.println("sex:::::::::::::::::::"+sexVal);
			String dobVal = (loanData.getDobdate() != null && !loanData.getDobdate().equalsIgnoreCase("null")
					? loanData.getDobdate()
					: " ");
			String occupation = (loanData.getAppl1occupation() != null
					&& !loanData.getAppl1occupation().equalsIgnoreCase("null")
							? loanData.getAppl1occupation() : " ");
			String occupationVal = occupation.substring(0, 1).toUpperCase() + occupation.substring(1);
			String category = (loanData.getCategory() != null && !loanData.getCategory().equalsIgnoreCase("null")
					? loanData.getCategory() : " ");
			String categoryVal = category.substring(0,1).toUpperCase()+category.substring(1).toLowerCase();
			String KYCVal = (loanData.getIdprooftype() != null && !loanData.getIdprooftype().equalsIgnoreCase("null")
					? loanData.getIdprooftype().substring(0,1).toUpperCase()+loanData.getIdprooftype().substring(1).toLowerCase() : " ")
					+ ", "
					+ (loanData.getAddressprooftype() != null
							&& !loanData.getAddressprooftype().equalsIgnoreCase("null")
									? loanData.getAddressprooftype().substring(0,1).toUpperCase()+loanData.getAddressprooftype().substring(1).toLowerCase() : " ");
			String KYCIdProofVal = (loanData.getIdprooftype() != null
					&& !loanData.getIdprooftype().equalsIgnoreCase("null")
							? loanData.getIdprooftype() : " ");
			String KYCAddrsProofVal = (loanData.getAddressprooftype() != null
					&& !loanData.getAddressprooftype().equalsIgnoreCase("null")
							? loanData.getAddressprooftype() : " ");
//            String nominationRequiredVal;
			String requestForATMCardval = (loanData.getAtmrequest() != null
					&& !loanData.getAtmrequest().equalsIgnoreCase("null") ? loanData.getAtmrequest() : " ");
			String smsAlertVal = (loanData.getSmsalert() != null && !loanData.getSmsalert().equalsIgnoreCase("null")
					? loanData.getSmsalert()
					: " ");

			// **********Start**************
//Static blocks are added and code is changed inside loops and also font size changed inside cells
			Paragraph pr101 = new Paragraph("Full Name in (Mr/Ms): ", redFont);
			pr101.setSpacingAfter(5f);
			document.add(pr101);
			PdfPTable tableR101 = new PdfPTable(20);
//
//            //for (int i = 0; i < houseNo_nameVal.length(); i++) {
			for (int i = 0; i < 20; i++) {
				PdfPCell ck = new PdfPCell();

//                //c.setFixedHeight(15f);
//                //c.setMinimumHeight(5f);
				if (i < nameVal.length()) {

					ck.addElement(new Phrase(("" + nameVal.charAt(i)).toUpperCase(),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));

					ck.setHorizontalAlignment(Element.ALIGN_CENTER);

				}

//                //c.setHeight(15f);
				ck.setFixedHeight(22f);
				tableR101.addCell(ck);
			}
			tableR101.setWidthPercentage(70f);// 70f
			tableR101.setHorizontalAlignment(Element.ALIGN_LEFT);
			document.add(tableR101);

			Paragraph pr102 = new Paragraph("Father/Husband/Guardian Name: ",
					FontFactory.getFont(FontFactory.TIMES_ROMAN, 11));
			pr102.setSpacingAfter(5f);
			document.add(pr102);
			PdfPTable tableR102 = new PdfPTable(20);
//            //for (int i = 0; i < houseNo_nameVal.length(); i++) {
			for (int i = 0; i < 20; i++) {
				PdfPCell c = new PdfPCell();
				c.setFixedHeight(22f);
				if (i < fatherNameVal.length()) {
					c.addElement(new Phrase(("" + fatherNameVal.charAt(i)).toUpperCase(),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
					c.setHorizontalAlignment(Element.ALIGN_CENTER);

				}
				tableR102.addCell(c);
			}
			tableR102.setWidthPercentage(70f);
			tableR102.setHorizontalAlignment(Element.ALIGN_LEFT);
			document.add(tableR102);

			Paragraph pr103 = new Paragraph("Residence Address:  ", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11));
			pr103.setSpacingAfter(5f);
			document.add(pr103);
			PdfPTable tableR7 = null;

			if (residenceAddrsVal.length() >= 20) {
//                //  logger.info("Table created......................");
				tableR7 = new PdfPTable(32);
				tableR7.setHorizontalAlignment(Element.ALIGN_CENTER);
				for (int i = 0; i < 20; i++) {
					PdfPCell c = new PdfPCell(new Phrase(("" + residenceAddrsVal.charAt(i)).toUpperCase(),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
					c.setHorizontalAlignment(Element.ALIGN_CENTER);
					tableR7.addCell(c);
				}
				tableR7.setWidthPercentage(100f);
				tableR7.setHorizontalAlignment(Element.ALIGN_LEFT);
				tableR7.addCell(new Paragraph("\n"));

//                // for (int j = 20; j < residenceAddrsVal.length(); j++) {
				for (int j = 20; j < 40; j++) {
					PdfPCell c = new PdfPCell(new Phrase(("" + residenceAddrsVal.charAt(j)).toUpperCase(),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
					c.setHorizontalAlignment(Element.ALIGN_CENTER);
					tableR7.addCell(c);
				}
//
////                    tableR70.setWidthPercentage(100f);
////                    tableR70.setHorizontalAlignment(Element.ALIGN_LEFT);
				document.add(tableR7);
//
//                //tableR70.addCell(""+residenceAddrsVal.charAt(i)+"  "); 
			} else {
				tableR7 = new PdfPTable(residenceAddrsVal.length());
				for (int i = 0; i < residenceAddrsVal.length(); i++) {
					tableR7.addCell(("" + residenceAddrsVal.charAt(i)).toUpperCase());
				}
				tableR7.setWidthPercentage(70f);
				tableR7.setHorizontalAlignment(Element.ALIGN_CENTER);
				document.add(tableR7);
			}

			Paragraph pr19 = new Paragraph("C/o: ", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11));
			pr19.setSpacingAfter(5f);
			document.add(pr19);
			PdfPTable tableR8 = new PdfPTable(20);
//            // for (int i = 0; i < careOfVall.length(); i++) {
			for (int i = 0; i < 20; i++) {
				PdfPCell c = new PdfPCell();
				if (i < careOfVall.length()) {
					c.addElement(new Phrase(("" + careOfVall.charAt(i)).toUpperCase(),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
					c.setHorizontalAlignment(Element.ALIGN_CENTER);

				}
				tableR8.addCell(c);
			}
			tableR8.setWidthPercentage(70f);
			tableR8.setHorizontalAlignment(Element.ALIGN_LEFT);
			document.add(tableR8);

			Paragraph pr20 = new Paragraph("House No. and Name: ", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11));
			pr20.setSpacingAfter(5f);
			document.add(pr20);
			PdfPTable tableR9 = new PdfPTable(20);
//            //for (int i = 0; i < houseNo_nameVal.length(); i++) {
			for (int i = 0; i < 20; i++) {
				PdfPCell c = new PdfPCell();
				if (i < houseNo_nameVal.length()) {
					c.addElement(new Phrase(("" + houseNo_nameVal.charAt(i)).toUpperCase(),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
					c.setHorizontalAlignment(Element.ALIGN_CENTER);

				}
				tableR9.addCell(c);
			}
			tableR9.setWidthPercentage(70f);
			tableR9.setHorizontalAlignment(Element.ALIGN_LEFT);
			document.add(tableR9);

			Paragraph pr21 = new Paragraph("Street No.and name: ", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11));
			pr21.setSpacingAfter(5f);
			document.add(pr21);
			PdfPTable tableR10 = new PdfPTable(20);
//            //for (int i = 0; i < streetNo_nameVal.length(); i++) {
			for (int i = 0; i < 20; i++) {
				PdfPCell c = new PdfPCell();
				if (i < streetNo_nameVal.length()) {
					c.addElement(new Phrase(("" + streetNo_nameVal.charAt(i)).toUpperCase(),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
					c.setHorizontalAlignment(Element.ALIGN_CENTER);

				}
				tableR10.addCell(c);
			}
			tableR10.setWidthPercentage(70f);
			tableR10.setHorizontalAlignment(Element.ALIGN_LEFT);
			document.add(tableR10);

			Paragraph pr22 = new Paragraph("Landmark: ", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11));
			pr22.setSpacingAfter(5f);
			document.add(pr22);
			PdfPTable tableR11 = new PdfPTable(20);
//            // for (int i = 0; i < landmarkVal.length(); i++) {
			for (int i = 0; i < 20; i++) {
				PdfPCell c = new PdfPCell();
				if (i < landmarkVal.length()) {
					c.addElement(new Phrase(("" + landmarkVal.charAt(i)).toUpperCase(),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
					c.setHorizontalAlignment(Element.ALIGN_CENTER);

				}
				tableR11.addCell(c);
			}
			tableR11.setWidthPercentage(70f);
			tableR11.setHorizontalAlignment(Element.ALIGN_LEFT);
			document.add(tableR11);

			Paragraph pr26 = new Paragraph("Village/City/District  ",
					FontFactory.getFont(FontFactory.TIMES_ROMAN, 11));
//            //+ "State :" + stateVal, redFont);
			pr26.setSpacingAfter(5f);
			document.add(pr26);
			PdfPTable tableR26 = new PdfPTable(20);
//            // for (int i = 0; i < landmarkVal.length(); i++) {
			for (int i = 0; i < 20; i++) {
				PdfPCell c = new PdfPCell();
				if (i < districtVal.length()) {
					c.addElement(new Phrase(("" + districtVal.charAt(i)).toUpperCase(),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
					c.setHorizontalAlignment(Element.ALIGN_CENTER);

				}
				tableR26.addCell(c);
			}
			tableR26.setWidthPercentage(70f);
			tableR26.setHorizontalAlignment(Element.ALIGN_LEFT);
			document.add(tableR26);

			Paragraph pr100 = new Paragraph("State: ", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11));
			pr100.setSpacingAfter(5f);
			document.add(pr100);
			PdfPTable tableR100 = new PdfPTable(20);
//            // for (int i = 0; i < landmarkVal.length(); i++) {
			for (int i = 0; i < 20; i++) {
				PdfPCell c = new PdfPCell();
				if (i < stateVal.length()) {
					c.addElement(new Phrase(("" + stateVal.charAt(i)).toUpperCase(),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
					c.setHorizontalAlignment(Element.ALIGN_CENTER);

				}
				tableR100.addCell(c);
			}
			tableR100.setWidthPercentage(70f);
			tableR100.setHorizontalAlignment(Element.ALIGN_LEFT);
			document.add(tableR100);

			Paragraph pr23 = new Paragraph("Pincode: ", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11));
			pr23.setSpacingAfter(5f);
			document.add(pr23);
			PdfPTable tableR12 = new PdfPTable(pincodeVal.length());
			for (int i = 0; i < pincodeVal.length(); i++) {
				PdfPCell c = new PdfPCell(new Phrase(("" + pincodeVal.charAt(i)).toUpperCase(),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
				c.setHorizontalAlignment(Element.ALIGN_CENTER);
				tableR12.addCell(c);
			}
			tableR12.setWidthPercentage(25f);
			tableR12.setHorizontalAlignment(Element.ALIGN_LEFT);
			document.add(tableR12);

			Paragraph pr24 = new Paragraph("Telephone/Landline:", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11));
			pr24.setSpacingAfter(5f);
			document.add(pr24);
			PdfPTable tableR13 = new PdfPTable(telephoneOrLandlineVal.length());
			for (int i = 0; i < telephoneOrLandlineVal.length(); i++) {
				PdfPCell c = new PdfPCell(new Phrase(("" + telephoneOrLandlineVal.charAt(i)).toUpperCase(),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
				c.setHorizontalAlignment(Element.ALIGN_CENTER);
				tableR13.addCell(c);
			}
			tableR13.setWidthPercentage(30f);
			tableR13.setHorizontalAlignment(Element.ALIGN_LEFT);
			document.add(tableR13);

			Paragraph pr25 = new Paragraph("Mobile: ", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11));
			pr25.setSpacingAfter(5f);
			document.add(pr25);
			PdfPTable tableR14 = new PdfPTable(mobileVal.length());
			for (int i = 0; i < mobileVal.length(); i++) {
				PdfPCell c = new PdfPCell(new Phrase(("" + mobileVal.charAt(i)).toUpperCase(),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
				c.setHorizontalAlignment(Element.ALIGN_CENTER);
				tableR14.addCell(c);
			}
			tableR14.setWidthPercentage(30f);
			tableR14.setHorizontalAlignment(Element.ALIGN_LEFT);
			document.add(tableR14);

			Paragraph pr27 = new Paragraph("Sex: ", redFont);
			Chunk gender1 = new Chunk(sexVal);
			gender1.setUnderline(1, -2f);
			pr27.add(gender1);
			pr27.add("\nDate of Birth: ");
			Chunk dob = new Chunk(dobVal);
			dob.setUnderline(1, -2f);
			pr27.add(dob);
			pr27.add("\na)Occupation: ");
			Chunk occu = new Chunk(occupationVal);
			occu.setUnderline(1, -2f);
			pr27.add(occu);
			pr27.add("          b)Category: ");
			Chunk cata = new Chunk(categoryVal);
			cata.setUnderline(1, -2f);
			pr27.add(cata);
			pr27.add("\nKYC Documents Provided: ");
			Chunk kyc = new Chunk(KYCVal);
			kyc.setUnderline(1, -2f);
			pr27.add(kyc);
			document.add(pr27);
			document.newPage();
			Paragraph p27 = new Paragraph("", redFont);
			Chunk sideh = new Chunk("Nomination Required:");
			sideh.setUnderline(1, -2f);
			p27.add(sideh);
			p27.add("\nRequest for Atm Debit card : ");
			Chunk atemreq = new Chunk(requestForATMCardval);
			atemreq.setUnderline(1, -2f);
			p27.add(atemreq);
			p27.add("\nSMS alert                             : ");
			Chunk sms = new Chunk(smsAlertVal);
			sms.setUnderline(1, -2f);
			p27.add(sms);
			document.add(p27);
//                    + "\nDate of Birth: " + dobVal
//                    + "\na)Occupation: " + occupationVal
//                    + "          b)Category: " + categoryVal
//                    + "\nKYC Documents Provided: " + KYCVal
//                    + "\nNomination Required: "
//                    + "\nRequest for Atm Debit card: " + "'" + requestForATMCardval + "'"
//                    + "\nSMS alert: " + "'" + smsAlertVal + "'",
//                    redFont);

			String introducerNameval = (loanData.getIntroducername() != null
					&& !loanData.getIntroducername().equalsIgnoreCase("null") ? loanData.getIntroducername() : " ");
			String custIDVal = (loanData.getIntroducercustid() != null
					&& !loanData.getIntroducercustid().equalsIgnoreCase("null") ? loanData.getIntroducercustid() : " ");
			String accountNoVal = (loanData.getIntroduceraccno() != null
					&& !loanData.getIntroduceraccno().equalsIgnoreCase("null") ? loanData.getIntroduceraccno() : " ");
			String introducerKnowyear = (loanData.getIntroducerknownyear() != null
					&& !loanData.getIntroducerknownyear().equalsIgnoreCase("null") ? loanData.getIntroducerknownyear()
							: " ");
			String modeOfOperationval = (loanData.getOperationmode() != null
					&& !loanData.getOperationmode().equalsIgnoreCase("null")
							? loanData.getOperationmode() : " ");
			String PanGirNoVal = (loanData.getPangirno() != null && !loanData.getPangirno().equalsIgnoreCase("null")
					? loanData.getPangirno()
					: " ");
			String incomePerAnnum = (loanData.getApprxyrlyapp1income() != null
					&& !loanData.getApprxyrlyapp1income().equalsIgnoreCase("null") ? loanData.getApprxyrlyapp1income()
							: " ");
			String educationVal = (loanData.getQualification() != null
					&& !loanData.getQualification().equalsIgnoreCase("null") ? loanData.getQualification() : " ");
			String eStatementVal = (loanData.getEstatement() != null
					&& !loanData.getEstatement().equalsIgnoreCase("null") ? loanData.getEstatement() : "N");
			String chequeBookVal = (loanData.getChequebook() != null
					&& !loanData.getChequebook().equalsIgnoreCase("null") ? loanData.getChequebook() : "N ");
			String mobilebankingVal = (loanData.getMobilebanking() != null
					&& !loanData.getMobilebanking().equalsIgnoreCase("null") ? loanData.getMobilebanking() : " N");
			String internetBankingVal = (loanData.getInternetbanking() != null
					&& !loanData.getInternetbanking().equalsIgnoreCase("null") ? loanData.getInternetbanking() : "N");
			String creditCardVal = (loanData.getCreditcard() != null
					&& !loanData.getCreditcard().equalsIgnoreCase("null") ? loanData.getCreditcard() : "N");
			String addOnOthersVal = (loanData.getAddonothers() != null
					&& !loanData.getAddonothers().equalsIgnoreCase("null") ? loanData.getAddonothers() : "N");
			String houseLoanVal = (loanData.getHouseloan() != null && !loanData.getHouseloan().equalsIgnoreCase("null")
					? loanData.getHouseloan()
					: "N");
			String vehicleLoan = (loanData.getVehicleloan() != null
					&& !loanData.getVehicleloan().equalsIgnoreCase("null") ? loanData.getVehicleloan() : "N");
			String mutualFundVal = (loanData.getMutualfund() != null
					&& !loanData.getMutualfund().equalsIgnoreCase("null") ? loanData.getMutualfund() : "N");
			String insuranceVal = (loanData.getLifeinsurance() != null
					&& !loanData.getLifeinsurance().equalsIgnoreCase("null") ? loanData.getLifeinsurance() : "N");
			String pensionVal = (loanData.getPension() != null && !loanData.getPension().equalsIgnoreCase("null")
					? loanData.getPension()
					: "N");
			String addOnOthersVal2 = (loanData.getCrosssellothers() != null
					&& !loanData.getCrosssellothers().equalsIgnoreCase("null") ? loanData.getCrosssellothers() : "N");
			String natureOfAccntVal = (loanData.getNomineeaccnature() != null
					&& !loanData.getNomineeaccnature().equalsIgnoreCase("null") ? loanData.getNomineeaccnature() : " ");
			String accntNoVal = (loanData.getNomineeaccno() != null
					&& !loanData.getNomineeaccno().equalsIgnoreCase("null") ? loanData.getNomineeaccno() : " ");
			String additionaldtlsVal = (loanData.getNomineeaddetails() != null
					&& !loanData.getNomineeaddetails().equalsIgnoreCase("null") ? loanData.getNomineeaddetails() : " ");
			String nomineeNameVal = (loanData.getNomineename() != null
					&& !loanData.getNomineename().equalsIgnoreCase("null") ? loanData.getNomineename() : " ");
			String nomineeAddrsVal = (loanData.getNomineeaddress() != null
					&& !loanData.getNomineeaddress().equalsIgnoreCase("null") ? loanData.getNomineeaddress() : " ");
			String nomineeRelationVal = (loanData.getNomineerelation() != null
					&& !loanData.getNomineerelation().equalsIgnoreCase("null")
							? loanData.getNomineerelation() : " ");
			String nomineeAgeVal = (loanData.getNomineeage() != null
					&& !loanData.getNomineeage().equalsIgnoreCase("null") ? loanData.getNomineeage() : " ");
			String nomineeDOBVal = (loanData.getNomineedob() != null
					&& !loanData.getNomineedob().equalsIgnoreCase("null") ? loanData.getNomineedob() : " ");
			String witness1NameVal = (loanData.getWitness1name() != null
					&& !loanData.getWitness1name().equalsIgnoreCase("null") ? loanData.getWitness1name() : " ");
			String witness1AddrsVal = (loanData.getWitness1add() != null
					&& !loanData.getWitness1add().equalsIgnoreCase("null") ? loanData.getWitness1add() : " ");
			String witness1PlaceVal = (loanData.getWitness1place() != null
					&& !loanData.getWitness1place().equalsIgnoreCase("null") ? loanData.getWitness1place() : " ");
			String witness1phoneVal = (loanData.getWitness1telephone() != null
					&& !loanData.getWitness1telephone().equalsIgnoreCase("null") ? loanData.getWitness1telephone()
							: " ");

			String witness2NameVal = (loanData.getWitness2name() != null
					&& !loanData.getWitness2name().equalsIgnoreCase("null") ? loanData.getWitness2name() : " ");
			String witness2AddrsVal = (loanData.getWitness2add() != null
					&& !loanData.getWitness2add().equalsIgnoreCase("null") ? loanData.getWitness2add() : " ");
			String witness2PlaceVal = (loanData.getWitness2place() != null
					&& !loanData.getWitness2place().equalsIgnoreCase("null") ? loanData.getWitness2place() : " ");
			String witness2phoneVal = (loanData.getWitness2telephone() != null
					&& !loanData.getWitness2telephone().equalsIgnoreCase("null") ? loanData.getWitness2telephone()
							: " ");
			String email = (loanData.getEmailid() != null && !loanData.getEmailid().equalsIgnoreCase("null")
					? loanData.getEmailid()
					: " ");
			// To get thumb impression box correctly just added on \n after years/months
			Paragraph pr4 = new Paragraph("", redFont);
			Chunk sideh1 = new Chunk("Introduction[if applicable] : ");
			sideh1.setUnderline(1, -2f);
			pr4.add(sideh1);
			pr4.add("\nName of the Introducer        : ");
			Chunk intronam = new Chunk(introducerNameval);
			intronam.setUnderline(1, -2f);
			pr4.add(intronam);
			pr4.add("\nCustomer Id                         : ");
			Chunk cusid = new Chunk(custIDVal);
			cusid.setUnderline(1, -2f);
			pr4.add(cusid);
			pr4.add("\nAccount No.                         : ");
			Chunk accnum = new Chunk(accountNoVal);
			accnum.setUnderline(1, -2f);
			pr4.add(accnum);
			document.add(pr4);

			Paragraph paragraph1 = new Paragraph("", redFont);
			paragraph1.setAlignment(Element.ALIGN_JUSTIFIED);
			paragraph1.add("\nI know Shri/Smt ");
			String n = addingSpace(nameVal, 40);
			Chunk name = new Chunk(" " + n, redFont);
			name.setUnderline(0.6f, -2f);
			paragraph1.add(name);
			paragraph1.add(" for the past ");
			String year = addingSpace(introducerKnowyear, 4);
			Chunk year1 = new Chunk(" " + year, redFont);
			year1.setUnderline(0.6f, -2f);
			paragraph1.add(year1);
			paragraph1.add(" Years/months.He/she is residing at the address given above.Date ");
			paragraph1.add(
					"Signature of the introducer___________________(Name, SS No & Signature of the verifying branch official) "
							+ "Please open a Savings Bank account in the name of Mr./Ms.");
			String n1 = addingSpace(nameVal, 40);
			Chunk name1 = new Chunk(" " + n1, redFont);
			name1.setUnderline(0.6f, -2f);
			paragraph1.add(name1);
			paragraph1.add(
					" (first/sole applicant) and Mr./Ms. ___________________ (second Applicant)**. The Saving Bank rules and "
							+ "regulations including those relating to small account have been explained to me/us and I/we agree to "
							+ "abide by the same.An additional photograph of sole/each applicant is attached."
							+ "\n\nDate:__________ \nPlace:__________\n                                         ");
			
			Chunk sign = new Chunk("   Signature/Thumb Impression of first/sole Applicant                          Signature/Thumb Impression of Second applicant\n  _______________________", subFont);
			paragraph1.add(sign);
			document.add(paragraph1);

////            		+ "Introduction[if applicable]: \n" + "Name of the introducer :" + introducerNameval
////                    + "\nCustomer id  :" + custIDVal + "\nAccount no. :" + accountNoVal
////                    + "\nI know Shri/Smt " + nameVal + " for the past " + introducerKnowyear
//                    + " Years/months.\n He/she is residing at the address given above.\n"
////                    + "Date: ___________ Signature of the introducer_________________________________________(Name, SS No & Signature of the verifying branch official) "
////                    + "Please open a Savings Bank account in the name of Mr./Ms. ________________________(first/sole "
////                    + "applicant)and Mr./Ms. _______________________(second Applicant)**. The Saving Bank rules and "
////                    + "regulations including those relating to Small Account have been explained to me/us and I/we agree to "
//                    + "abide by the same.An additional photograph of sole/each applicant is attached. \n\nDate:__________ \nPlace:______________"
//                    + "\n                                         Signature/Thumb Impression of first/sole Applicant              Signature/Thumb\n Impression of Second applicant\n\n  _______________________" //                    + "\n                                                 Signature of Business Correspondent/Facilitator_______________________"
//                    //                    //                    + "\n                                   Name, SS No & Signature of the verifying Branch official_______________________\n\n"
//                    //                    //                            + "*DDm-Drop down Menu \n\n**The Joint Account Holder(i.e.second applicant)shall fill up a supplementary form. \n\n"
//                    ,
//                    redFont);

//
			PdfPTable tableR6;
			tableR6 = new PdfPTable(11);
			tableR6.setWidthPercentage(50f);
			tableR6.setWidths(
					new float[] { 0.08f, 0.01f, 0.01f, 0.01f, 0.01f, 0.010f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f });
			tableR6.setHorizontalAlignment(Element.ALIGN_RIGHT);

			tableR6.addCell(new Paragraph("Name & No of BC/BF", redFont));
			tableR6.addCell("");
			tableR6.addCell("");
			tableR6.addCell("");
			tableR6.addCell("");
			tableR6.addCell("");
			tableR6.addCell("");
			tableR6.addCell("");
			tableR6.addCell("");
			tableR6.addCell("");
			tableR6.addCell("");

			document.add(tableR6);

			Paragraph pr18 = new Paragraph(
					"                                                 Signature of Business Correspondent/Facilitator________________________\n\n",
					redFont);// + "\n Name, SS No & Signature of the verifying Branch
								// official_____________________________\n\n"
//                    //+ "*DDm-Drop down Menu \n**The Joint Account Holder(i.e.second applicant)shall fill up a supplementary form. \n\n"
//                    ,
//                    redFont);
			document.add(pr18);
//
			PdfPTable tableR15 = new PdfPTable(1);
//            //tableR15.setTotalWidth(240f);
			tableR15.setWidthPercentage(100f);
//            //tableR15.setWidths(new float[]{1});
			tableR15.setHorizontalAlignment(Element.ALIGN_RIGHT);
//
			PdfPCell cr15 = new PdfPCell(new Phrase(
					"Name, SS No & Signature of the verifying \n" + "Branch official______________________\n\n"));
//            //cr15.setColspan(1);
			cr15.setIndent(280f);
			tableR15.addCell(cr15);
			document.add(tableR15);

			Paragraph pr28 = new Paragraph(
					"*DDm-Drop down Menu \n**The Joint Account Holder (i.e.second applicant) shall fill up a supplementary form. \n\n",
					redFont);
			document.add(pr28);

			PdfContentByte cbr1 = writer.getDirectContent();
			cbr1.saveState();
//            //cb.setColorStroke(Color.BLACK);
//            //cbr1.rectangle(160, 550, 200, 50);
			cbr1.rectangle(170, 480, 200, 40);
			cbr1.stroke();
			cbr1.restoreState();
//
			PdfContentByte cbr2 = writer.getDirectContent();
			cbr2.saveState();
//            //cb.setColorStroke(Color.BLACK);
			cbr2.rectangle(400, 480, 150, 40);
			cbr2.stroke();
			cbr2.restoreState();
////            
//
			Paragraph pr5 = new Paragraph("SAVING BANK ACCOUNT FORM-ADDITIONAL INFORMATION\n", canFont);
			pr5.setAlignment(Element.ALIGN_CENTER);
//            //pr5.add(new Paragraph(" "));
			document.add(pr5);
			Paragraph pr6 = new Paragraph("1. Mode of Operation                  : ", redFont);
			Chunk mode = new Chunk(modeOfOperationval.substring(0,1).toUpperCase()+modeOfOperationval.substring(1).toLowerCase());
			mode.setUnderline(0.6f, -2f);
			pr6.add(mode);
			pr6.add("\n2. PAN /GIR NO./FORM 60/61 : ");
			Chunk pan = new Chunk(PanGirNoVal);
			pan.setUnderline(0.6f, -2f);
			pr6.add(pan);
			pr6.add("\n3. Income per annum                  : ");
			Chunk income = new Chunk(incomePerAnnum);
			income.setUnderline(0.6f, -2f);
			pr6.add(income);
			pr6.add("\n4. Educational Qualification       : ");
			Chunk edu = new Chunk(educationVal);
			edu.setUnderline(0.6f, -2f);
			pr6.add(edu);
			pr6.add("\n5. Email ID                                 : ");
			Chunk ema = new Chunk(email);
			ema.setUnderline(0.6f, -2f);
			pr6.add(ema);
			pr6.add("\n6. KYC Document : \n    Identification Proof : ");
			Chunk iden = new Chunk(KYCIdProofVal.substring(0,1).toUpperCase()+KYCIdProofVal.substring(1).toLowerCase());
			iden.setUnderline(0.6f, -2f);
			pr6.add(iden);
			pr6.add(", Address Proof : ");
			Chunk add = new Chunk(KYCAddrsProofVal.substring(0,1).toUpperCase()+KYCAddrsProofVal.substring(1).toLowerCase());
			add.setUnderline(0.6f, -2f);
			pr6.add(add);
			document.add(pr6);

//            + modeOfOperationval
//                    + "\n2. PAN /GIR NO./FORM 60/61 :" + PanGirNoVal
//                    + "\n3. Income per annum : " + incomePerAnnum
//                    + "\n4. Educational Qualification : " + educationVal
//                    + "\n5. Email ID : "
//                    + "\n6. KYC Document : "
//                    + "\n    Identification Proof : " + KYCIdProofVal + ", Address Proof : " + KYCAddrsProofVal, redFont);
//            document.add(pr6);
//
			document.newPage();
//
			Paragraph pr7 = new Paragraph("7.Request for add on : \n", redFont);
			pr7.add(new Paragraph(" "));
			document.add(pr7);

			PdfPTable tableR2 = new PdfPTable(3);
			tableR2.setWidthPercentage(100f);
			tableR2.setWidths(new float[] { 1, 3, 3 });
			tableR2.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell tableR2_c1 = new PdfPCell(new Phrase("Sr. No."));
			tableR2_c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			tableR2.addCell(tableR2_c1);

			tableR2_c1 = new PdfPCell(new Phrase("Product"));
			tableR2_c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			tableR2.addCell(tableR2_c1);

			tableR2_c1 = new PdfPCell(new Phrase(""));
			tableR2_c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			tableR2.addCell(tableR2_c1);
			tableR2.setHeaderRows(1);

			tableR2.addCell("1");
			tableR2.addCell("e-statement of Account");
			tableR2.addCell(eStatementVal);

			tableR2.addCell("2");
			tableR2.addCell("Cheque Book");
			tableR2.addCell(chequeBookVal);

			tableR2.addCell("3");
			tableR2.addCell("Mobile Banking");
			tableR2.addCell(mobilebankingVal);

			tableR2.addCell("4");
			tableR2.addCell("Internet Banking");
			tableR2.addCell(internetBankingVal);

			tableR2.addCell("5");
			tableR2.addCell("Credit Card ");
			tableR2.addCell(creditCardVal);

			tableR2.addCell("6");
			tableR2.addCell("Others ");
			tableR2.addCell(addOnOthersVal);

			document.add(tableR2);

			Paragraph pr8 = new Paragraph("8.Additional Information for Cross selling : \n\n"
					+ "______________________________________________________________\n\n"
					+ "I would like to also avail: ", redFont);
			pr8.add(new Paragraph(" "));
			document.add(pr8);

			PdfPTable tableR3 = new PdfPTable(3);
			tableR3.setWidthPercentage(100f);
			tableR3.setWidths(new float[] { 1, 3, 3 });
			tableR3.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell tableR3_c1 = new PdfPCell(new Phrase("Sr. No."));
			tableR3_c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			tableR3.addCell(tableR3_c1);

			tableR3_c1 = new PdfPCell(new Phrase("Product"));
			tableR3_c1.setHorizontalAlignment(Element.ALIGN_CENTER);

			tableR3.addCell(tableR3_c1);

			tableR3_c1 = new PdfPCell(new Phrase(""));
			tableR3_c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			tableR3.addCell(tableR3_c1);
			tableR3.setHeaderRows(1);

			tableR3.addCell("1");
			tableR3.addCell("Housing Loan");
			tableR3.addCell(houseLoanVal);

			tableR3.addCell("2");
			tableR3.addCell("Vehicle Loan");
			tableR3.addCell(vehicleLoan);

			tableR3.addCell("3");
			tableR3.addCell("Mutual Fund");
			tableR3.addCell(mutualFundVal);

			tableR3.addCell("4");
			tableR3.addCell("Life/General INsurance");
			tableR3.addCell(insuranceVal);

			tableR3.addCell("5");
			tableR3.addCell("Pension");
			tableR3.addCell(pensionVal);

			tableR3.addCell("6");
			tableR3.addCell("Others");
			tableR3.addCell(addOnOthersVal2);

			document.add(tableR3);

			Paragraph pr9 = new Paragraph(
					"I/we understand that a booklet on the Banking Codes & Standards Board of India"
							+ "Code(BCSBI) posted on your website shall be provided to me on demand.\n",
					redFont);
			pr9.add(new Paragraph(" "));
			document.add(pr9);

			Paragraph pr16 = new Paragraph("Terms & Conditions:\n", canFont2);

			document.add(pr16);

			Paragraph pr17 = new Paragraph(
					"I/we confirm having received, read and understood (a) the accounts rules and "
							+ "hereby agree to be bound by the terms& conditions outlined in these rules which "
							+ "governs the account(s) which I/we am/are opening/will open and (b) amendments to "
							+ "the rules made from time to time and those relating to various services availed by "
							+ "me/us when displayed by the Bank on its notice board or on its website and those "
							+ "relating to various services offered by the Bank including but not limited to debit card, "
							+ "credit card, internet banking, mobile banking and other facilities listed in this form. "
							+ "The usage of these facilities is governed by the terms and conditions stipulated by "
							+ "the Bank from time to time.",
					redFont);

			document.add(pr17);

			Paragraph pr10 = new Paragraph("\nDate  : ___________" + "\nPlace : ___________\n"
					+ "                                          ", redFont);
			Chunk sign1= new Chunk("Signature/Thumb Impression of first/soleApplicant            Signature/Thumb Impression of second Applicant"
					+ "\n\n",subFont);
			pr10.add(sign1);
			Chunk sign2= new Chunk("Branch office..............", redFont);
			pr10.add(sign2);
			pr10.add(new Paragraph(" "));
			document.add(pr10);
//
//            //MUST BE ADDED THESE 2 PARAS
//changed some values
			PdfContentByte cbr3 = writer.getDirectContent();
			cbr3.saveState();
			cbr3.rectangle(170, 190, 180, 40);// 180 in 2nd//180 in 3rd//4th for reducing height//2nd one if we increase
												// it will go down
			cbr3.stroke();
			cbr3.restoreState();

			PdfContentByte cbr4 = writer.getDirectContent();
			cbr4.saveState();
			cbr4.rectangle(370, 190, 150, 40);// 180 in 2nd//150 in 3rd
			cbr4.stroke();
			cbr4.restoreState();

			document.newPage();
			String permAddress = (loanData.getPermhouseno() != null && !loanData.getPermhouseno().equalsIgnoreCase("null") && loanData.getPermhouseno() != " " ? loanData.getPermhouseno()+ "," : " ");
            permAddress += (loanData.getPermstreetno() != null && !loanData.getPermstreetno().equalsIgnoreCase("null") && loanData.getPermstreetno() != " " ? loanData.getPermstreetno()+ "," : " ");
            permAddress += (loanData.getPermlandmark() != null && !loanData.getPermlandmark().equalsIgnoreCase("null") && loanData.getPermlandmark() != " " ? loanData.getPermlandmark()+ "," : " ");
            permAddress += (loanData.getPermvillage() != null && !loanData.getPermvillage().equalsIgnoreCase("null") && loanData.getPermvillage() != " " ? loanData.getPermvillage()+ "," : " ");
            permAddress += (loanData.getPermdistrict() != null && !loanData.getPermdistrict().equalsIgnoreCase("null") && loanData.getPermdistrict() != " " ? loanData.getPermdistrict()+ "," : " ");		
            permAddress += (loanData.getPermsubdistrict() != null && !loanData.getPermsubdistrict().equalsIgnoreCase("null") && loanData.getPermsubdistrict() != " " ? loanData.getPermsubdistrict()+ "," : " ");
            permAddress += (loanData.getPermstate() != null && !loanData.getPermstate().equalsIgnoreCase("null") && loanData.getPermstate() != " " ? loanData.getPermstate()+ "," : " ");
            permAddress += (loanData.getPermpincode() != null && !loanData.getPermpincode().equalsIgnoreCase("null") && loanData.getPermpincode() != " " ? loanData.getPermpincode()+ "," : " ");
            permAddress += (loanData.getPermtelephone() != null && !loanData.getPermtelephone().equalsIgnoreCase("null") && loanData.getPermtelephone() != " " ? loanData.getPermtelephone()+ "," : " ");
//			String permAddress = (loanData.getPermhouseno() != null
//					&& !loanData.getPermhouseno().equalsIgnoreCase("null") && loanData.getPermhouseno() != " "
//							? loanData.getPermhouseno() + ","
//							: " ");
//			permAddress += (loanData.getPermstreetno() != null && !loanData.getPermstreetno().equalsIgnoreCase("null")
//					&& loanData.getPermstreetno() != " " ? loanData.getPermstreetno() + "," : " ");
//			permAddress += (loanData.getPermlandmark() != null && !loanData.getPermlandmark().equalsIgnoreCase("null")
//					&& loanData.getPermlandmark() != " " ? loanData.getPermlandmark() + "," : " ");
//			permAddress += (loanData.getPermvillage() != null && !loanData.getPermvillage().equalsIgnoreCase("null")
//					&& loanData.getPermvillage() != " " ? addresscodes.getVillage(loanData.getPermstate(), loanData.getPermdistrict(), loanData.getPermsubdistrict(), loanData.getPermvillage()) + "," : " ");
//			permAddress += (loanData.getPermdistrict() != null && !loanData.getPermdistrict().equalsIgnoreCase("null")
//					&& loanData.getPermdistrict() != " " ? addresscodes.getdistrict(loanData.getPermstate(), loanData.getPermdistrict()) + "," : " ");
//			permAddress += (loanData.getPermsubdistrict() != null
//					&& !loanData.getPermsubdistrict().equalsIgnoreCase("null") && loanData.getPermsubdistrict() != " "
//							?addresscodes.getSubDistrict(loanData.getPermstate(), loanData.getPermdistrict(),loanData.getPermsubdistrict()) + ","
//							: " ");
//			permAddress += (loanData.getPermstate() != null && !loanData.getPermstate().equalsIgnoreCase("null")
//					&& loanData.getPermstate() != " " ? addresscodes.getState(loanData.getPermstate()) + "," : " ");
//			permAddress += (loanData.getPermpincode() != null && !loanData.getPermpincode().equalsIgnoreCase("null")
//					&& loanData.getPermpincode() != " " ? loanData.getPermpincode() + "," : " ");
//			permAddress += (loanData.getPermtelephone() != null && !loanData.getPermtelephone().equalsIgnoreCase("null")
//					&& loanData.getPermtelephone() != " " ? loanData.getPermtelephone() + "," : " ");

//            String applicantAddress = (loanData.getApplicantaddress() != null && loanData.getApplicantaddress() != "" && !loanData.getApplicantaddress().equalsIgnoreCase("null") ? loanData.getApplicantaddress() : " ");
			Paragraph pr11 = new Paragraph("FORM DA-1 NOMINATION\n", canFont);
			pr11.setAlignment(Element.ALIGN_CENTER);
			document.add(pr11);

			Paragraph pr12 = new Paragraph(
					"Nomination under Section 45 ZA of Banking Regulation Act, 1949 and Rule 2(1) of the Banking Companies (Nomination) Rules"
							+ "1985 in respect of Bank Deposits,",
					redFont);
			document.add(pr12);
			Paragraph paragraph2 = new Paragraph("", redFont);
			paragraph2.setAlignment(Element.ALIGN_JUSTIFIED);
			paragraph2.add("I/ We ( Name(s))");
			String na = addingSpace(nameVal, 80);
			Chunk appname = new Chunk(" " + na);
			appname.setUnderline(0.6f, -2f);
			paragraph2.add(appname);
			paragraph2.add("R/o");
			String add1 = addingSpace(permAddress, 80);
			Chunk addr = new Chunk(" " + add1);
			addr.setUnderline(0.6f, -2f);
			paragraph2.add(addr);
			paragraph2.add(
					"nominate the following person to whom in the event of my/our/ minor’s death, the amount of deposit in the account may be"
							+ "returned by Swabhiman Bank, Branch Office_____________\n");
			paragraph2.setSpacingAfter(10);
			document.add(paragraph2);

//                    + "\nI/ We ( Name(s)) ____________________________________ R/o ________________________________"
//                    + "\nnominate the following person to whom in the event of my/our/ minor’s death, the amount of deposit in the account may be"
//                    + "returned by Swabhiman Bank, Branch Office________________________"
//            pr12.setAlignment(Element.ALIGN_CENTER);
//            pr12.add(new Paragraph(" "));

//
//            //Changed values for column widths and also changed font size for cells in tables and also added \n to get thumb impression box correctly
			PdfPTable tableR4 = new PdfPTable(8);
			tableR4.setWidthPercentage(100f);
//            // tableR4.setWidths(new float[]{1.3f, 2.3f, 1.5f, 2, 2f, 2, 0.7f, 1.8f});
			tableR4.setWidths(new float[] { 2, 2, 2, 4, 5, 3, 1, 3 });
			tableR4.setHorizontalAlignment(Element.ALIGN_LEFT);
//
			PdfPCell tableR4_c1 = new PdfPCell(new Phrase("Deposit", canFont));
			tableR4_c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			tableR4_c1.setColspan(3);

			tableR4.addCell(tableR4_c1);

			tableR4_c1 = new PdfPCell(new Phrase("Nominee", canFont));
			tableR4_c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			tableR4_c1.setColspan(5);
			tableR4.addCell(tableR4_c1);

			tableR4.addCell(new Phrase("Nature of account", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
			tableR4.addCell(new Phrase("Account no", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
			tableR4.addCell(new Phrase("Additional details", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
			tableR4.addCell(new Phrase("Name", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
			tableR4.addCell(new Phrase("Address", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
			tableR4.addCell(
					new Phrase("Relationship with depositor", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
			tableR4.addCell(new Phrase("Age", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
			tableR4.addCell(new Phrase("Date of Birth", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));

			tableR4.addCell(new Phrase(natureOfAccntVal, FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
			tableR4.addCell(new Phrase(accntNoVal, FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
			tableR4.addCell(new Phrase(additionaldtlsVal, FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
			tableR4.addCell(new Phrase(nomineeNameVal, FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
			tableR4.addCell(new Phrase(nomineeAddrsVal, FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
			tableR4.addCell(
					new Phrase(nomineeRelationVal.toLowerCase() + "\n\n\n\n\n", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
			tableR4.addCell(new Phrase(nomineeAgeVal, FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
			tableR4.addCell(new Phrase(nomineeDOBVal, FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
//
//            //changed some values
			document.add(tableR4);
			PdfContentByte cbr50 = writer.getDirectContent();
			cbr50.saveState();
			cbr50.rectangle(350, 450, 180, 40);// 180 in 2nd//150 in 3rd
			cbr50.stroke();
			cbr50.restoreState();

			Paragraph pr13 = new Paragraph("", redFont);
			pr13.setAlignment(Element.ALIGN_JUSTIFIED);
			pr13.add("*As the nominee is minor on this date,I/we appoint Mr/Ms");
			String nom = addingSpace(nomineeNameVal, 40);
			Chunk nomname = new Chunk(" " + nom);
			nomname.setUnderline(0.6f, -2f);
			pr13.add(nomname);
			pr13.add("age");
			String ag = addingSpace(nomineeAgeVal, 4);
			Chunk age1 = new Chunk(" " + ag);
			age1.setUnderline(0.6f, -2f);
			pr13.add(age1);
			pr13.add("address");
			String nadd1 = addingSpace(nomineeAddrsVal, 50);
			Chunk nomadd1 = new Chunk(" " + nadd1);
			nomadd1.setUnderline(0.6f, -2f);
			pr13.add(nomadd1);
			pr13.add(
					"to receive the amount of the deposit on behalf of the nominee in the event of my/our/minor’s death during the minority of the nominee."
							+ "\nDate : ____________\nPlace: _____________________ "
							+ "\nWhere the deposit is made in the name of minor, the nomination is to be signed by natural/legal guardian of the minor to act on behalf of the minor. "
							+ "\n*Strike out if nominee is not a minor");
			document.add(pr13);
//            _____________________________ Age_______"
//                    + "\nAddress________________________________________________________________________________"
//                    + "\nto receive the amount of the deposit on behalf of the nominee in the event of my/our/minor’s death during the minority of the nominee."
//                    + "\nDate : ____________"
//                    + "\nPlace: _____________________                                        Signature(s) / Thumb impression(s) of  depositors"
//                    + "\nWhere the deposit is made in the name of minor, the nomination is to be signed by natural/legal guardian of the minor to act on behalf of the minor. "
//                    + "\n*Strike out if nominee is not a minor's", redFont);
//            document.add(pr13);

			Paragraph pr15 = new Paragraph("WITNESS", canFont);
			pr15.setAlignment(Element.ALIGN_CENTER);
			pr15.add(new Paragraph(" "));
			document.add(pr15);

			PdfPTable tableR5 = new PdfPTable(2);
			tableR5.setWidthPercentage(100f);
			tableR5.setWidths(new float[] { 1, 1 });
			tableR5.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell tableR5_c1 = new PdfPCell(new Phrase("Name & Signature of the first Witness", canFont));
			tableR5_c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			tableR5.addCell(tableR5_c1);

			tableR5_c1 = new PdfPCell(new Phrase("Name & Signature of the second Witness", canFont));
			tableR5_c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			tableR5.addCell(tableR5_c1);

			tableR5.addCell(new Phrase("Name       : " + witness1NameVal + "\nSignature : _________________" + "\nAddress   : "
					+ witness1AddrsVal + "\nPlace      : " + witness1PlaceVal + "\nDate       : ____________\n" + "\nTelephone No: "
					+ witness1phoneVal + "\n", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));

			tableR5.addCell(new Phrase("Name       : " + witness2NameVal + "\nSignature : _________________" + "\nAddress    : "
					+ witness2AddrsVal + "\nPlace         : " + witness2PlaceVal + "\nDate          : ____________\n" + "\nTelephone No: "
					+ witness2phoneVal + "\n", FontFactory.getFont(FontFactory.TIMES_ROMAN, 11)));
			document.add(tableR5);

			Paragraph pr14 = new Paragraph(
					"\nThumb impression(s) shall be attested by two witnesses, otherwise it shall be attested by one witness........\n ........................................................................................................................................................................\nNOMINATION REGISTERED"
							+ "\nThe above mentioned nomination is registered at serial no ____________________________________ in respect of (Type of Account.) _________________ Deposit Account No._________________________."
							+ "\n\nDate : _____________"
							+ "\nPlace : ____________                           For(Authorised Official) SS.No_________________",
					redFont);
			document.add(pr14);

		} catch (FileNotFoundException fnf) {
			logger.error("Error:.....!!! " + fnf.getMessage());
		} catch (DocumentException de) {
			logger.error("Error:.....!!! " + de.getMessage());
		} catch (IOException ex) {
			logger.error("Error:.....!!! " + ex.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		document.close();
		logger.debug("***********Ended createPdf()***********");
	}

	public String ConverttoDate(String date) {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String date1 = "";
		try {
			Date dateString = df.parse(date);
			String pattern = "MM-dd-yyyy";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			date1 = simpleDateFormat.format(dateString);
		} catch (Exception ex) {
			logger.info("Exception Occured,,,,,,,,,,,,,,, " + ex);
		}

		// System.out.println(date);
		return date1;
	}

	public String convertStringToDate(String dateString) {
		Date date = null;
		String formatteddate = null;
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		try {
			date = df.parse(dateString);
			formatteddate = (df.format(date));
		} catch (Exception ex) {
			System.out.println(ex);
		}
		return formatteddate;
	}

	public String addingSpace(String text, int space) {
		String name = text;
		int len = name.length();
		int s = space - len;
		for (int i = 0; i < s; i++) {
			name = name + " ";
		}
		return name;
	}

}
