/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.core;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;

/**
 *
 * @author root
 */
@Configuration
public abstract class PDFGenerationAbstract implements PDFGenerationOperations {

	
    public static final Logger logger = Logger.getLogger(PDFGenerationAbstract.class);
//    public static final ResourceBundle rb = ResourceBundle.getBundle("systemPath");

    public String getJsData(String functionName, String paramVal,String jsPaths) {
        logger.info("FunctionName: " + functionName + " , ParamValue: " + paramVal);
        String jsPath = jsPaths;
        System.out.println("Jspath is defined............."+ jsPath);

        Object result = null;
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("JavaScript");

        if (!(engine instanceof Invocable)) {
            logger.info("Invoking methods is not supported.");
        }

        Invocable inv = (Invocable) engine;
        try {
            engine.eval(Files.newBufferedReader(Paths.get(jsPaths), StandardCharsets.UTF_8));
            result = inv.invokeFunction(functionName, paramVal);
            logger.debug("JSDataResult.." + result);
        } catch (ScriptException se) {
            logger.error("Error: " + se.getMessage());
        } catch (NoSuchMethodException nsme) {
            logger.error("Error: " + nsme.getMessage());
        } catch (IOException io) {
            logger.error("Error: " + io.getMessage());
        }
        return result.toString();
    }

    public Chunk underLine(String data) {
        Paragraph pp9 = new Paragraph();
        pp9.setAlignment(Element.ALIGN_JUSTIFIED);
        Chunk underline1 = new Chunk(data, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL));
        underline1.setUnderline(0.1f, -2f);
        pp9.add(underline1);
        return underline1;
    }

    public String convertToDate(String dateStr) {
        System.out.println("datettttttttttttttttttttttttttt " + dateStr);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = sdf.parse(dateStr);

            System.out.println(sdf1.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf1.format(date);
    }

    public String returnOnlyDate(String dateStr) {
        System.out.println("datettttttttttttttttttttttttttt " + dateStr);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        int day = 0;
         String currentday = null;
        try {
            date = sdf.parse(dateStr);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            day = cal.get(Calendar.DAY_OF_MONTH);
             currentday = String.valueOf(day);
            System.out.println("dateasgfaslgljhhkj  " + day);
            System.out.println(sdf1.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return currentday;
    }

    public String returnMonthandYear(String dateStr) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
         String monthyear = null;
        Date date = null;
        int day = 0;
        try {
            date = sdf.parse(dateStr);
            SimpleDateFormat dateFormatter = new SimpleDateFormat("MMMM  yyyy");
             monthyear = dateFormatter.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return monthyear;
    }
//    public String getIndianCurrencyFormat(String amount) {
//    StringBuilder stringBuilder = new StringBuilder();
//    char amountArray[] = amount.toCharArray();
//    int a = 0, b = 0;
//    for (int i = amountArray.length - 1; i >= 0; i--) {
//        if (a < 3) {
//            stringBuilder.append(amountArray[i]);
//            a++;
//        } else if (b < 2) {
//            if (b == 0) {
//                stringBuilder.append(",");
//                stringBuilder.append(amountArray[i]);
//                b++;
//            } else {
//                stringBuilder.append(amountArray[i]);
//                b = 0;
//            }
//        }
//    }
//    return stringBuilder.reverse().toString();
//}
}
