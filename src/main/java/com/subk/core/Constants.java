package com.subk.core;

/**
 *
 * @author subashkumar.s
 */
public class Constants {
    
    public static final String ACTIVE = "Active";
    public static final String INACTIVE = "Inactive";
    
    public static final String PENDING_FOR_REVIEW = "pendingForReview";
    public static final String APPLICATION_DETAILS = "aplicationDetails";
    public static final String LOAN_RETURNED = "returnedLoans";
    public static final String LOAN_REJECTED = "rejectedLoans";
    public static final String LOAN_APPROVED = "approvedLoans";
    
    public static final String UPDATE_VERIFICATION_STATUS = "updateVerificationStatus";
    
    
    public static final String[] PENDING_HEADINGS = {"Chk Box","Appl No","Date Of Submision","Customer Name","Contact No","Village Name","Subk recommended  Loan Amount","FX Verified Date","TE Verified Date","UH Verified Date","BM Verification Status","BM Verified Date"};
    public static final String[] APPROVED_HEADINGS = {"Chk Box","Appl No","Date Of Submision","Customer Name","Contact No","Village Name","Subk recommended  Loan Amount","Loan amount Sanctioned","Date Of Approved"};//"Sanction Letter",
    public static final String[] RETURNED_HEADINGS = {"S.No","Appl No","Date Of Submision","Customer Name","Contact No","Village Name","Subk recommended  Loan Amount","Date Of Return","Returned By","Remarks"};
    public static final String[] REJECTED_HEADINGS = {"S.No","Appl No","Date Of Submision","Customer Name","Contact No","Village Name","Subk recommended  Loan Amount","DATE OF Reject","Rejected By","Remarks"};
    
//    public static final double INTEREST_RATE = 14.00;
}
