/*
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.core;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.subk.entity.Loanorigination;
import com.subk.util.AddressCodes;

@Service
public class GeneratePrimaryAppPDF extends PDFGenerationAbstract {

	
	
	

    public static final Logger logger = Logger.getLogger(GeneratePrimaryAppPDF.class);
//    public static final ResourceBundle rb = ResourceBundle.getBundle("application.properties");

    public String getPdf(Loanorigination loanData, String fileName,String pathAdd, String img, String jsPaths) {
        logger.info("************ ENTERED ************* ");

        String destPath = null;

        logger.debug("UBILoanOrigination Data: " + loanData.toString());
        logger.debug("FileName: " + fileName);

        try {
            destPath = pathAdd;
            destPath = destPath + fileName;

            //File file = new File(destPath);
            //file.getParentFile().mkdirs();
            new GeneratePrimaryAppPDF().createPdf(destPath, loanData , img, jsPaths);
        } catch (Exception ex) {
            logger.error("Error:....!!! " + ex.getMessage());
        }

        logger.info("destPathINATION PATH:...........!!!!!!!!!!  " + destPath);
        logger.info("************ ENDED.************* ");
        return destPath;
    }

    public void createPdf(String dest, Loanorigination loanData, String img, String jsPaths) {
        logger.debug("***********Entered***********");
        final String imgPath =img;
        AddressCodes addresscodes = new AddressCodes();
        Document document = null;
        PdfWriter writer = null;

        try {
            document = new Document();
            writer = PdfWriter.getInstance(document, new FileOutputStream(dest, false));

//            document.open();

            Font canFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
            Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
            Font redFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);
            Font canFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
            Font font11b = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
            //File file = new File("C:\\"+loanData.getAppl1Name()+"_"+loanData.getApplicationId());
            // Get the output stream for writing PDF object
            //ServletOutputStream out = response.getOutputStream();
            //OutputStream oos = new FileOutputStream(file,true);

            //Document document = new Document();
            //PdfWriter writer = PdfWriter.getInstance(document, out);
            HeaderFooterPageEvent event = new HeaderFooterPageEvent();
            writer.setPageEvent(event);
            document.open();

            document.addKeywords("Java, Servelet, PDF, iText");
            document.addAuthor("Raj Grover");
            document.addCreator("UBILoanOrigination");

            Image img2 = Image.getInstance(imgPath);
            img2.setAbsolutePosition(250f, 740f);
            img2.scaleAbsolute(80f, 80f);
            document.add(img2);
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("\n"));

            Paragraph p1 = new Paragraph("Primary Information Sheet/Check List for individual loan under\n"
                    + "\"United Samriddhi Scheme\" through facilitation by M/s. Basix Sub-K\n\n", canFont);
            p1.setAlignment(Element.ALIGN_CENTER);
            document.add(p1);

            Paragraph p2 = new Paragraph("United Bank of India"+"\n", redFont);
            p2.add(loanData.getBranchname() + " Branch");
            p2.setAlignment(Element.ALIGN_CENTER);
            document.add(p2);

            document.add(Chunk.SPACETABBING);
            System.out.println(loanData.toString());
            String businessAddrs = (loanData.getCommhouseno() != null && !loanData.getCommhouseno().equalsIgnoreCase("null") && loanData.getCommhouseno() != " " ? loanData.getCommhouseno()+ "," : " ");
            businessAddrs += (loanData.getCommstreetno() != null && !loanData.getCommstreetno().equalsIgnoreCase("null") && loanData.getCommstreetno() != " " ? loanData.getCommstreetno()+"," : " ");
            businessAddrs += (loanData.getCommlandmark() != null && !loanData.getCommlandmark().equalsIgnoreCase("null") && loanData.getCommlandmark() != " " ? loanData.getCommlandmark()+ "," : " ");
            businessAddrs += (loanData.getCommvillcity() != null && !loanData.getCommvillcity().equalsIgnoreCase("null") && loanData.getCommvillcity() != " " ? loanData.getCommvillage()+ "," : " ");
            businessAddrs += (loanData.getCommdistrict() != null && !loanData.getCommdistrict().equalsIgnoreCase("null") && loanData.getCommdistrict() != " " ? loanData.getCommdistrict()+ "," : " ");
            businessAddrs += (loanData.getCommstate() != null && !loanData.getCommstate().equalsIgnoreCase("null") && loanData.getCommstate() != " " ? loanData.getCommstate()+ "," : " ");
            businessAddrs += (loanData.getCommpincode() != null && !loanData.getCommpincode().equalsIgnoreCase("null") && loanData.getCommpincode() != " " ? loanData.getCommpincode()+ "," : " ");
            businessAddrs += (loanData.getCommtelephone() != null && !loanData.getCommtelephone().equalsIgnoreCase("null") && loanData.getCommtelephone() != " " ? loanData.getCommtelephone() : " ");

            String permAddress = (loanData.getPermhouseno() != null && !loanData.getPermhouseno().equalsIgnoreCase("null") && loanData.getPermhouseno() != " " ? loanData.getPermhouseno()+ "," : " ");
            permAddress += (loanData.getPermstreetno() != null && !loanData.getPermstreetno().equalsIgnoreCase("null") && loanData.getPermstreetno() != " " ? loanData.getPermstreetno()+ "," : " ");
            permAddress += (loanData.getPermlandmark() != null && !loanData.getPermlandmark().equalsIgnoreCase("null") && loanData.getPermlandmark() != " " ? loanData.getPermlandmark()+ "," : " ");
            permAddress += (loanData.getPermvillage() != null && !loanData.getPermvillage().equalsIgnoreCase("null") && loanData.getPermvillage() != " " ? loanData.getPermvillage()+ "," : " ");
            permAddress += (loanData.getPermdistrict() != null && !loanData.getPermdistrict().equalsIgnoreCase("null") && loanData.getPermdistrict() != " " ? loanData.getPermdistrict()+ "," : " ");		
            permAddress += (loanData.getPermsubdistrict() != null && !loanData.getPermsubdistrict().equalsIgnoreCase("null") && loanData.getPermsubdistrict() != " " ? loanData.getPermsubdistrict()+ "," : " ");
            permAddress += (loanData.getPermstate() != null && !loanData.getPermstate().equalsIgnoreCase("null") && loanData.getPermstate() != " " ? loanData.getPermstate()+ "," : " ");
            permAddress += (loanData.getPermpincode() != null && !loanData.getPermpincode().equalsIgnoreCase("null") && loanData.getPermpincode() != " " ? loanData.getPermpincode()+ "," : " ");
            permAddress += (loanData.getPermtelephone() != null && !loanData.getPermtelephone().equalsIgnoreCase("null") && loanData.getPermtelephone() != " " ? loanData.getPermtelephone()+ "," : " ");
            
            PdfPTable table = new PdfPTable(3);
            table.setWidthPercentage(100f);
            // table.setTotalWidth(288);
            table.setWidths(new float[]{1, 5, 4});
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            //document.add(table);

            //Changed font size for each cell in the table
            PdfPCell c1 = new PdfPCell(new Phrase("S.No.", canFont1));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            //  c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Item", canFont1));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);

            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Information", canFont1));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.setHeaderRows(1);

            c1 = new PdfPCell(new Phrase("1", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Name of the Applicant", redFont));
            table.addCell(new Phrase(loanData.getAppl1name() != null && !loanData.getAppl1name().equalsIgnoreCase("null") && loanData.getAppl1name() != " " ? loanData.getAppl1name() : " ", redFont));

            c1 = new PdfPCell(new Phrase("2", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Name of the JLG/ Group Leader & other members (wherever applicable)", redFont));
            table.addCell(new Phrase(loanData.getJlgleader() != null && loanData.getJlgleader() != "" && !loanData.getJlgleader().equalsIgnoreCase("null") ? loanData.getJlgleader() : " ", redFont));

            c1 = new PdfPCell(new Phrase("3", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Address of the Applicant", redFont));
            table.addCell(new Phrase(permAddress, redFont1));

            c1 = new PdfPCell(new Phrase("4", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Business Address", redFont));
            table.addCell(new Phrase(businessAddrs, redFont1));
            // table.addCell(loanData.getAppl1Address()!=null ?loanData.getAppl1Address():" " );

            c1 = new PdfPCell(new Phrase("5", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Contact No. (Mandatory)", redFont));
            table.addCell(new Phrase(loanData.getMobileno() != null && loanData.getMobileno() != " " && !loanData.getMobileno().equalsIgnoreCase("null") ? loanData.getMobileno() : " ", redFont));

            c1 = new PdfPCell(new Phrase("6", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Details of Nominee", redFont));
            table.addCell(new Phrase(loanData.getNomineename() != null && !loanData.getNomineename().equalsIgnoreCase("null") && loanData.getNomineename() != " " ? loanData.getNomineename() : " ", redFont));

            c1 = new PdfPCell(new Phrase("7", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Documents relied upon for identity & residence verification", redFont));
            table.addCell(new Phrase(loanData.getIdprooftype() != null && loanData.getIdprooftype() != "" && !loanData.getIdprooftype().equalsIgnoreCase("null") ?loanData.getIdprooftype() : " ", redFont1)); //loanData.getIdprooftype()//

           c1 = new PdfPCell(new Phrase("8", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Coverage under PMSBY/ PMJJBY/APY", redFont));
            table.addCell(new Phrase(loanData.getPmsbypmjjbycovrg() != null && !loanData.getPmsbypmjjbycovrg().equalsIgnoreCase("null") && loanData.getPmsbypmjjbycovrg() != " " ? loanData.getPmsbypmjjbycovrg() : " ", redFont));

            c1 = new PdfPCell(new Phrase("9", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Validity period of PMSBY/PMJJBY/APY", redFont));
            table.addCell(new Phrase((loanData.getPmsbypmjjbyvalidity() != null && !loanData.getPmsbypmjjbyvalidity().equalsIgnoreCase("null") && loanData.getPmsbypmjjbyvalidity() != " " ? loanData.getPmsbypmjjbyvalidity() : " "), redFont));

            c1 = new PdfPCell(new Phrase("10", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Activity/ Occupation of the Applicant", redFont));
            table.addCell(new Phrase(loanData.getAppl1occupation() != null && loanData.getAppl1occupation() != " " && !loanData.getAppl1occupation().equalsIgnoreCase("null") ? loanData.getAppl1occupation().toUpperCase() : " ", redFont1));//loanData.getAppl1occupation() 

            c1 = new PdfPCell(new Phrase("11", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Activity/ Occupation of other family member/(s), if any", redFont));
            table.addCell(new Phrase(loanData.getFamilymembsoccupation() != null && loanData.getFamilymembsoccupation() != " " && !loanData.getFamilymembsoccupation().equalsIgnoreCase("null") ?loanData.getFamilymembsoccupation().toUpperCase(): " ", redFont1));//loanData.getFamilymembsoccupation()

           c1 = new PdfPCell(new Phrase("12", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Running since how many years ", redFont));
            table.addCell(new Phrase(loanData.getRunningsince() != null && loanData.getRunningsince() != " " && !loanData.getRunningsince().equalsIgnoreCase("null") ? loanData.getRunningsince() : " ", redFont));

            c1 = new PdfPCell(new Phrase("13", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Total number of years of experience in business ", redFont));
            table.addCell(new Phrase(loanData.getYearsofexperience() != null && loanData.getYearsofexperience() != " " && !loanData.getYearsofexperience().equalsIgnoreCase("null") ? loanData.getYearsofexperience() : " ", redFont));
            System.out.println(loanData.toString());
            NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
            String ApprxyrlyApp1Income = loanData.getApprxyrlyapp1income() != null && !loanData.getApprxyrlyapp1income().equalsIgnoreCase("null") && loanData.getApprxyrlyapp1income() != " " ? loanData.getApprxyrlyapp1income() : " ";
            String resultApprxyrlyApp1IncomeComma = getIndianCurrencyFormat(ApprxyrlyApp1Income);
            String FamilyExp = loanData.getFamilyexp() != null && loanData.getFamilyexp() != " " && !loanData.getFamilyexp().equalsIgnoreCase("null") ? loanData.getFamilyexp() : " ";
            String resultFamilyExpComma = getIndianCurrencyFormat(FamilyExp);
            String YrlySurplus = loanData.getYrlysurplus() != null && !loanData.getYrlysurplus().equalsIgnoreCase("null") && loanData.getYrlysurplus() != " " ? loanData.getYrlysurplus() : " ";
            String resultYrlySurplusComma = getIndianCurrencyFormat(YrlySurplus);
            String LoanAmount = loanData.getLoanamount() != null && !loanData.getLoanamount().equalsIgnoreCase("null") && loanData.getLoanamount() != " " ? loanData.getLoanamount() : " ";
            String resultLoanAmountComma = getIndianCurrencyFormat(LoanAmount);

           c1 = new PdfPCell(new Phrase("14", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Annual Income as per the applicant's initial pre-appraisal inputs(Household and Business)", redFont));
            table.addCell(new Phrase("Rs." + resultApprxyrlyApp1IncomeComma + "/-", redFont));

           c1 = new PdfPCell(new Phrase("15", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Annual Expenditure as per the applicant's initial pre-appraisal inputs(Household and Business)", redFont));
            table.addCell(new Phrase("Rs." + resultFamilyExpComma + "/-", redFont));

            c1 = new PdfPCell(new Phrase("16", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Annual Surplus as per the applicant's initial pre-appraisal inputs(Household and Business)", redFont));
            table.addCell(new Phrase("Rs." + resultYrlySurplusComma + "/-", redFont));

            c1 = new PdfPCell(new Phrase("17", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Amount of Loan applied for", redFont));
            table.addCell(new Phrase("Rs." + resultLoanAmountComma + "/-", redFont));

            c1 = new PdfPCell(new Phrase("18", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("First Time Loan/ Repeat Loan/Take over", redFont));
            table.addCell(new Phrase(loanData.getApproachfor() != null && !loanData.getApproachfor().equalsIgnoreCase("null") && loanData.getApproachfor() != " " ? loanData.getApproachfor() : " ", redFont1));

            c1 = new PdfPCell(new Phrase("19", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("If Take over loan, present financier, status of loan account and outstanding", redFont));
            table.addCell(new Phrase((loanData.getPresentfinancer() != null) && !loanData.getPresentfinancer().equalsIgnoreCase("null") && loanData.getPresentfinancer() != " " ? loanData.getPresentfinancer() : " ", redFont));

            c1 = new PdfPCell(new Phrase("20", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Proposed Repayment period", redFont));
            table.addCell(new Phrase(loanData.getRepayperiod() != null && !loanData.getRepayperiod().equalsIgnoreCase("null") && loanData.getRepayperiod() != " " ? loanData.getRepayperiod() : " ", redFont));

            String WeeklyInstalment = loanData.getWeeklyinstalment() != null && !loanData.getWeeklyinstalment().equalsIgnoreCase("null") && loanData.getWeeklyinstalment() != " " ? loanData.getWeeklyinstalment() : " ";
            String resultWeeklyInstalmentComma = getIndianCurrencyFormat(WeeklyInstalment);
            c1 = new PdfPCell(new Phrase("21", redFont));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.addCell(new Phrase("Equated Monthly Instalment", redFont));
            table.addCell(new Phrase("Rs." + resultWeeklyInstalmentComma + "/-", redFont));
            document.add(table);
            table.completeRow();
            //creating new page
            document.newPage();
//            
            //Changed font size for each cell in the table
            PdfPTable table_1 = new PdfPTable(3);
            table_1.setWidthPercentage(100f);
            //table_1.setTotalWidth(288);
            table_1.setWidths(new float[]{1, 3, 3});
            table_1.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell c2 = new PdfPCell(new Phrase("SI", canFont1));
            //c2.setHorizontalAlignment(Element.ALIGN_CENTER);
            c2.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_1.addCell(c2);

            PdfPCell c21 = new PdfPCell(new Phrase("Item", canFont1));
            c21.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_1.addCell(c21);

            PdfPCell c22 = new PdfPCell(new Phrase("Branch findings",canFont1));
            c22.setHorizontalAlignment(Element.ALIGN_CENTER);

            table_1.addCell(c22);
            table_1.setHeaderRows(1);
            PdfPCell c3 = new PdfPCell(new Phrase("22.Remarks/ Observation of Basix CRO/ BCA on Eligibility/ Creditworthiness of applicant :\n\n\n\n\n\n\n Please refer physical ‘Form 2’ for detailed appraisal and cash flow data as performed by Sub-K representative\n\n\n\n\n                             ...................................................\n"
                    + "\n                     Signature of CRO/ BCA with Device ID No.\n\n Date:", redFont));
            c3.setColspan(3);
            //c3.setPaddingBottom(100f);
            table_1.addCell(c3);

            PdfPCell c4 = new PdfPCell(new Phrase("23. Remarks & Observation of Branch Official with particular reference to KYC compliance and findings on creditworthiness", redFont));
            c4.setColspan(3);
            table_1.addCell(c4);

            PdfPCell c5 = new PdfPCell(new Phrase("\n\n\n\n\n\n\n\n\n\n\nSignature of Officer/ Bank Manager\n\n"
                    + "Date:",redFont));
            c5.setColspan(3);
            table_1.addCell(c5);

            document.add(table_1);

            document.newPage();
            //last page is added in the middle of 2nd page
            Paragraph pk21 = new Paragraph("PRE-DISBURSEMENT APPRAISAL OF APPLICATION FORM", canFont);
            pk21.setAlignment(Element.ALIGN_CENTER);
            pk21.add(new Paragraph(" "));
            document.add(pk21);

            Paragraph p22 = new Paragraph("Information furnished by the applicant as above has been checked and identity/ residence/"
                    + "activity etc. has been verified with the help of Basix CRO/ BCA. Based on the study of the information "
                    + "furnished in loan application as well as in the Primary Information Sheet/ Check List: \n",redFont);
            p22.add("A loan of Rs.");
            Chunk loanamt=new Chunk(resultLoanAmountComma,font11b);
            loanamt.setUnderline(0.6f, -2f);
            p22.add(loanamt);
             p22.add("/- is recommended for sanction.\n\n");
             document.add(p22);
            Paragraph pk22 = new Paragraph("Signature of Officer\n", canFont1);
            document.add(pk22);

            String sanctionedamount = loanData.getLoanamountsactioned() != null && !loanData.getLoanamountsactioned().equalsIgnoreCase("null") && loanData.getLoanamountsactioned() != " " ? loanData.getLoanamountsactioned() : " ";
            String sanctioned = getIndianCurrencyFormat(sanctionedamount);
            String interestrate = loanData.getInterestrate() != null && !loanData.getInterestrate().equalsIgnoreCase("null") && loanData.getInterestrate() != " " ? loanData.getInterestrate() : " ";
            String noofinstal = loanData.getNoofinstalments() != null && !loanData.getNoofinstalments().equalsIgnoreCase("null") && loanData.getNoofinstalments() != " " ? loanData.getNoofinstalments() : " ";
            String instalamount = loanData.getInstalmentamount() != null && !loanData.getInstalmentamount().equalsIgnoreCase("null") && loanData.getInstalmentamount() != " " ? loanData.getInstalmentamount() : " ";
            String instalments = getIndianCurrencyFormat(instalamount);
            Paragraph pk24 = new Paragraph("As per above recommendation, loan of Rs     ", redFont);
            Chunk sanloan=new Chunk(sanctioned+"/-",font11b);
            sanloan.setUnderline(0.6f, -2f);
            pk24.add(sanloan);
            pk24.add("   is sanctioned.\n");
            pk24.add("Interest Rate : ");
            Chunk intrate=new Chunk(interestrate+"\n",font11b);
            intrate.setUnderline(0.6f, -2f);
            pk24.add(intrate);
            pk24.add("Security       :");
            Chunk sec=new Chunk("Hypothecation of Assets\n",redFont);
            sec.setUnderline(0.6f, -2f);
            pk24.add(sec);
            pk24.add("Repayment Schedule   : ");
            Chunk instal=new Chunk(noofinstal,font11b);
            instal.setUnderline(0.6f, -2f);
            pk24.add(instal);
            pk24.add(" months\n");
            pk24.add("Amount of Instalment :  ");
            Chunk instalamt=new Chunk(instalments+"/-",font11b);
            instalamt.setUnderline(0.6f, -2f);
            pk24.add(instalamt);
            pk24.add(" p.m\n\n");
            document.add(pk24);
//            document.add(new Paragraph("As per above recommendation, loan of Rs     " + loanData.getLoanamountsactioned() + "   is sanctioned.",redFont));
//            document.add(new Paragraph("Interest Rate : " +  "__________________________" + "\n",redFont));/*loanData.getInterestRate()*/ 
//            document.add(new Paragraph("Security       : Hypothecation of Assets\n",redFont));
//            document.add(new Paragraph("Repayment Schedule   : " + loanData.getNoofinstalments()+  " months\n",redFont));
//            document.add(new Paragraph("Amount of Instalment :  " + loanData.getInstalmentamount()+ " p.m\n\n",redFont));

            Paragraph pk23 = new Paragraph("Signature of Branch Manager\n\n", canFont1);
            document.add(pk23);

            document.add(new Paragraph("Place:\n"
                    + "Date:"));

        } catch (FileNotFoundException fnf) {
            logger.error("Error:.....!!! " + fnf.getMessage());
        } catch (DocumentException de) {
            logger.error("Error:.....!!! " + de.getMessage());
        } catch (IOException ex) {
            logger.error("Error:.....!!! " + ex.getMessage());
        }catch(Exception e) {
        	e.printStackTrace();
        }
        document.close();

        logger.debug("***********Ended createPdf()***********");
    }

    public String getIndianCurrencyFormat(String amount) {
        StringBuilder stringBuilder = new StringBuilder();
        char amountArray[] = amount.toCharArray();
        int a = 0, b = 0;
        for (int i = amountArray.length - 1; i >= 0; i--) {
            if (a < 3) {
                stringBuilder.append(amountArray[i]);
                a++;
            } else if (b < 2) {
                if (b == 0) {
                    stringBuilder.append(",");
                    stringBuilder.append(amountArray[i]);
                    b++;
                } else {
                    stringBuilder.append(amountArray[i]);
                    b = 0;
                }
            }
        }
        return stringBuilder.reverse().toString();
    }
}
