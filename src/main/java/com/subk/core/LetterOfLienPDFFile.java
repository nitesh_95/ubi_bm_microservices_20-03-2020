/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.core;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;
import com.subk.entity.Loanorigination;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 *
 * @author Ramya
 */
@Configuration
public class LetterOfLienPDFFile extends PDFGenerationAbstract {

	@Value("${MCLRL}")
	private String mclrl;  
	private String empty = " ";
	private static final Logger logger = Logger.getLogger(GenerateUndertakingPDFFile.class);
//	private static final ResourceBundle rb = ResourceBundle.getBundle("systemPath");
//	private static final ResourceBundle rb1 = ResourceBundle.getBundle("com.subk.Resources//subkResources");

	public String getPdf(Loanorigination loanData, String fileName,String pathAdd, String img, String jsPaths) {
		logger.info("************ ENTERED ************* ");

		String destPath = null;

		logger.debug("UBILoanOrigination Data: " + loanData.toString());
		logger.debug("FileName: " + fileName);

		try {
			destPath = pathAdd;
			destPath = destPath + fileName;
			new LetterOfLienPDFFile().createPdf(destPath, loanData , img, jsPaths);
		} catch (Exception ex) {
			logger.error("Error:....!!! " + ex.getMessage());
		}

		logger.info("destPathINATION PATH:...........!!!!!!!!!!  " + destPath);
		logger.info("************ ENDED.************* ");
		return destPath;
	}

	public void createPdf(String dest, Loanorigination loanData, String img, String jsPaths) {
		logger.debug("***********Entered***********");
		final String imgPath = img;
 	final String mclrVal = mclrl;

		BigDecimal mclrMF = new BigDecimal("0.0");

		String gender = loanData.getSex();
		if (gender.equals("MALE")) {
			if (loanData.getMclrMaleFemale() != null) {
				Double dd = new Double(loanData.getMclrMaleFemale());
				mclrMF = BigDecimal.valueOf(dd);
			}

		} else if (gender.equals("FEMALE")) {
			if (loanData.getMclrMaleFemale() != null) {
				Double dd = new Double(loanData.getMclrMaleFemale());
				mclrMF = BigDecimal.valueOf(dd);
			}
		}

		Document document = null;
		PdfWriter writer = null;

		try {
			String appl1Address = (loanData.getAppl1address() != null && loanData.getAppl1address() != ""
					&& !loanData.getAppl1address().equals("null") ? loanData.getAppl1address() : " ");
			document = new Document();
			writer = PdfWriter.getInstance(document, new FileOutputStream(dest, false));
			HeaderFooterPageEvent event = new HeaderFooterPageEvent();
			writer.setPageEvent(event);
			document.open();

			Font canFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
			Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
			document.addKeywords("Java, Servelet, PDF, iText");
			document.addAuthor("Raj Grover");
			document.addCreator("UBILoanOrigination");

			Image img2 = Image.getInstance(imgPath);
			img2.setAbsolutePosition(250f, 740f);
			img2.scaleAbsolute(80f, 80f);
			document.add(img2);
			document.add(new Paragraph("\n"));
			document.add(new Paragraph("\n"));
			document.add(new Paragraph("\n"));

			Paragraph sp19 = new Paragraph("LETTER OF LIEN\n", canFont);
			sp19.setAlignment(Element.ALIGN_CENTER);
			document.add(sp19);

			Paragraph sp20 = new Paragraph("Date ......................\n", redFont);
			sp20.setAlignment(Element.ALIGN_RIGHT);
			document.add(sp20);

			Paragraph sp21 = new Paragraph("To,\n", redFont);
			sp21.setAlignment(Element.ALIGN_LEFT);
			document.add(sp21);

			Paragraph sp22 = new Paragraph("UNITED BANK OF INDIA\n", redFont);
			sp22.add(loanData.getBranchname());
			sp22.add(" Branch\n");
			sp22.add("_____________________\n\n");
			sp22.setAlignment(Element.ALIGN_LEFT);
			document.add(sp22);

			Paragraph sp23 = new Paragraph("Dear Sir,\n\n", redFont);
			sp23.setAlignment(Element.ALIGN_LEFT);
			document.add(sp23);

			Paragraph sp24 = new Paragraph();
			sp24.setAlignment(Element.ALIGN_JUSTIFIED);
			sp24.add(new Chunk(
					"In consideration of the advances already made and/or agreed to be made by you from time to time at your ",
					redFont));
			sp24.add(new Chunk(
					"absolute discretion to me or to ................................ at my request and also other advances or credit ",
					redFont));
			sp24.add(new Chunk(
					" or accommodation that you may make form time to time to me or to anybody else at my/our request, I/We hereby give you a lien on all share stocks debentures ",
					redFont));
			sp24.add(new Chunk(
					"stocks securities and cash belonging to me/us and which are now or may hereafter be held by you or be in your custody ",
					redFont));
			sp24.add(new Chunk(
					" on my account  (hereinafter referred to us the said securities) for the general outstanding balance",
					redFont));
			sp24.add(new Chunk(
					" of any and every loan, current, overdraft, cash credit or other account or accounts whatsoever with you and in respect where of ",
					redFont));
			sp24.add(new Chunk(
					"I are or may become liable to you as borrower(s) guarantor(s) or otherwise and whether solely or jointly ",
					redFont));
			sp24.add(new Chunk(
					"with others including Stamp duties, ordinary bank charges, commission, costs charges and expenses incidental to the ",
					redFont));
			sp24.add(new Chunk(
					"said account and/or this security and interest at your usual rate or rates on similar account or accounts or at such other rate ",
					redFont));
			sp24.add(new Chunk(
					"as may be fixed by you form time to time in respect of the said account or accounts and/or  I authorise you on ",
					redFont));
			sp24.add(new Chunk(
					"non-payment by me to you on demand of the aforesaid advances or if whenever the value of the said securities held by you ",
					redFont));
			sp24.add(new Chunk(
					"or in  your custody does not exceed my indebtedness by ............................ percent or is not made up to that ",
					redFont));
			sp24.add(new Chunk(
					"amount on demand to realise in such manner as you may think fit all or any part of the said securities without further notice ",
					redFont));
			sp24.add(new Chunk(
					"to me and you shall be entitled to attach to the said securities whatsoever stamps if any as may be required for making them ",
					redFont));
			sp24.add(new Chunk(
					"valid in law or for effecting such realisation and do apply the proceeds after deduction of the costs and expenses incurred by you ",
					redFont));
			sp24.add(new Chunk(
					"in that behalf towards satisfactions of my indebtedness to you crediting my account or accounts with the balance if any and in ",
					redFont));
			sp24.add(new Chunk(
					"case of any deficit in the amount recovered out of the said securities and your dues from me / I shall remain ",
					redFont));
			sp24.add(new Chunk(
					"liable to make good the same and I authorise you at my our expense to collect and receive and give receipts for and recover ",
					redFont));
			sp24.add(new Chunk(
					"I undertake to execute and do all assurances and acts which may be necessary to effectuate this security and such realisation and pay all stamp ",
					redFont));
			sp24.add(new Chunk(
					"duties incidental thereto and I further agree and declare that this security shall both as to my past and future indebtedness ",
					redFont));
			sp24.add(new Chunk(
					"ensure for the benefit of any concern with which you may be incorporated by absorption, amalgamation or otherwise and also for the benefit ",
					redFont));
			sp24.add(new Chunk(
					"of your assigns and I further declare that you will be at liberty to re-pledge or create any ",
					redFont));
			sp24.add(new Chunk(
					"other security over all or any part of the said securities. This security will be a continuing security notwithstanding any ",
					redFont));
			sp24.add(new Chunk(
					"partial payments or any fluctuations of accounts other existence of a credit balance on any account at any time and I declare that all present ",
					redFont));
			sp24.add(new Chunk(
					"securities are my absolute property at my sole disposal and free from any charge or encumbrance and that all future securities ",
					redFont));
			sp24.add(new Chunk(
					"hereunder shall be likewise my unencumbered disposable property and if and whenever the securities are held for my liability ",
					redFont));
			document.add(sp24);
			document.newPage();
			Paragraph p24 = new Paragraph();
			p24.setAlignment(Element.ALIGN_JUSTIFIED);
			p24.add(new Chunk("for any third party obligation to you, you shall be ", redFont));
			p24.add(new Chunk(
					"free without reference to me to deal with the principal debtor and any securities obligations or decrees and generally to act as if I were primarily liable and the provisions of your / General ",
					redFont));
			p24.add(new Chunk(
					"Guarantee Form in use for the time being to the full extent applicable hereto shall be deemed incorporated herein and any notice by way of request demand or otherwise hereunder may be given by you to me or any of us ",
					redFont));
			p24.add(new Chunk(
					"personally or may be left at the then or last known place of business or residence in India of me or any of us addressed to me or any of us or ",
					redFont));
			p24.add(new Chunk(
					"any be sent by post to me or any of us addressed as aforesaid and if sent by post it shall be deemed to have been given at ",
					redFont));
			p24.add(new Chunk(
					"the time when it would be delivered in due course of post and in proving such notice when given by post it shall be sufficient to prove that ",
					redFont));
			p24.add(new Chunk(
					"the envelope containing the notice was posted and a certificate signed by your local Agent that the envelope was so posted shall be conclusive.  ",
					redFont));
			p24.add(new Chunk(
					"If by reason of absence from India or otherwise I or any of us cannot be given any such notice the same if inserted ",
					redFont));
			p24.add(new Chunk(
					"once as an advertisement in a newspaper circulating in the District where the last known address is situated shall be deemed to have been effectually ",
					redFont));
			p24.add(new Chunk("given and received on the day on which such advertisement appears.", redFont));

			document.add(p24);
			String applicantName = (loanData.getAppl1name() != null && loanData.getAppl1name() != ""
					&& !loanData.getAppl1name().equals("null") ? loanData.getAppl1name() : " ");
			Paragraph sp25 = new Paragraph("\nYours faithfully,\n" + applicantName, redFont);
			sp25.setAlignment(Element.ALIGN_RIGHT);
			document.add(sp25);


		} catch (FileNotFoundException fnf) {
			logger.error("Error:.....!!! " + fnf.getMessage());
		} catch (DocumentException de) {
			logger.error("Error:.....!!! " + de.getMessage());
		} catch (IOException ex) {
			logger.error("Error:.....!!! " + ex.getMessage());
		}
		document.close();

		logger.debug("***********Ended createPdf()***********");
	}

	public Chunk underLine(String data) {
		Paragraph pp9 = new Paragraph();
		pp9.setAlignment(Element.ALIGN_JUSTIFIED);
		Chunk underline1 = new Chunk(data, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL));
		underline1.setUnderline(0.1f, -2f);
		pp9.add(underline1);
		return underline1;
	}

}
