/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.core;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.subk.entity.Loanorigination;
/**
 *
 * @author abhilash.d
 */
public class savingsSandhyaDelete extends PDFGenerationAbstract{


    public static final Logger logger = Logger.getLogger(savingsSandhyaDelete.class);
//    public static final ResourceBundle rb = ResourceBundle.getBundle("systemPath");

    public String getPdf(Loanorigination loanData, String fileName,String pathAdd, String img, String jsPaths) {
        logger.info("************ ENTERED ************* ");

        String destPath = null;

        logger.debug("UBILoanOrigination Data: " + loanData.toString());
        logger.debug("FileName: " + fileName);

        try {
            destPath = pathAdd;
            destPath = destPath + fileName;

            //File file = new File(destPath);
            //file.getParentFile().mkdirs();
            new savingsSandhyaDelete().createPdf(destPath, loanData,img, jsPaths);
        } catch (Exception ex) {
            logger.error("Error:....!!! " + ex.getMessage());
        }

        logger.info("DESTINATION PATH:...........!!!!!!!!!!  " + destPath);
        logger.info("************ ENDED.************* ");
        return destPath;
    }
    public void createPdf(String dest, Loanorigination loanData , String img, String jsPaths) {
        logger.debug("***********Entered***********");
        final String imgPath = img;

        Document document = null;
        PdfWriter writer = null;

        try {
            document = new Document();
            writer = PdfWriter.getInstance(document, new FileOutputStream(dest, false));

            document.open();

            Font canFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
            Font canFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 13, Font.BOLD);
            Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
            Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 12);
            Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLDITALIC);
            Font canFont2 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
            //UNDERLINE 

            //File file = new File("C:\\"+loanData.getAppl1Name()+"_"+loanData.getApplicationId());
            // Get the output stream for writing PDF object
            //ServletOutputStream out = response.getOutputStream();
            //OutputStream oos = new FileOutputStream(file,true);
            //Document document = new Document();
            //PdfWriter writer = PdfWriter.getInstance(document, out);
            document.open();

            document.addKeywords("Java, Servelet, PDF, iText");
            document.addAuthor("Raj Grover");
            document.addCreator("UBILoanOrigination");

            Image img1 = Image.getInstance(imgPath);
            img1.setAbsolutePosition(250f, 740f);
            img1.scaleAbsolute(80f, 80f);
            document.add(img1);

            document.add(new Paragraph("\n\n\n"));
            Paragraph pr1 = new Paragraph("SAVING BANK ACCOUNT FORM", canFont);
            pr1.setAlignment(Element.ALIGN_CENTER);
            pr1.add(new Paragraph(" "));
            document.add(pr1);

           PdfPTable tableR1 = new PdfPTable(17);
            tableR1.setTotalWidth(240f);
            tableR1.setWidths(new float[]{3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1});
            tableR1.setHorizontalAlignment(Element.ALIGN_LEFT);
            PdfPCell cr1 = new PdfPCell(new Phrase("For Bank Use Only", canFont));
            cr1.setColspan(17);
            tableR1.addCell(cr1);

            cr1 = new PdfPCell(new Phrase("Name & Code of the Branch", canFont));
            cr1.setColspan(17);
            tableR1.addCell(cr1);

            tableR1.addCell(new Paragraph("Cust ID", canFont));
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");

            tableR1.addCell(new Paragraph("A/C No.", canFont));
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            tableR1.addCell("");
            if(loanData.getApplicantphotopath()!=null && !loanData.getApplicantphotopath().equalsIgnoreCase("")){
            Image img11 = Image.getInstance(loanData.getApplicantphotopath());
            img11.setAbsolutePosition(470f, 650f);
//            img11.scaleAbsolute(60f, 60f);
            img11.scaleToFit(60, 70);
             document.add(img11); 
            }else {
                 document.add(new Paragraph("")); 
            }
           
            document.add(tableR1);
          

        } catch (FileNotFoundException fnf) {
            logger.error("Error:.....!!! " + fnf.getMessage());
        } catch (DocumentException de) {
            logger.error("Error:.....!!! " + de.getMessage());
        } catch (IOException ex) {
            logger.error("Error:.....!!! " + ex.getMessage());
        }
        document.close();

        logger.debug("***********Ended createPdf()***********");
    }
    
}
