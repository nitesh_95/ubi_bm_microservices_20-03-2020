package com.subk.core;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.subk.entity.Loanorigination;

@Configuration
public class ConsentClauseBorrowerUndertakingPDFFIle extends PDFGenerationAbstract {

	@Value("${MCLRL}")
	private String mclrl; 
	private String empty = " ";
	private static final Logger logger = Logger.getLogger(GenerateUndertakingPDFFile.class);

//	private static final ResourceBundle rb1 = ResourceBundle.getBundle("application.properties");

	public String getPdf(Loanorigination loanData, String fileName,String pathAdd, String img, String jsPaths) {
		logger.info("************ ENTERED ************* ");

		String destPath = null;

		logger.debug("UBILoanOrigination Data: " + loanData.toString());
		logger.debug("FileName: " + fileName);

		try {
			destPath = pathAdd;
			destPath = destPath + fileName;
			new ConsentClauseBorrowerUndertakingPDFFIle().createPdf(destPath, loanData, img, jsPaths);
		} catch (Exception ex) {
			logger.error("Error:....!!! " + ex.getMessage());
		}

		logger.info("destPathINATION PATH:...........!!!!!!!!!!  " + destPath);
		logger.info("************ ENDED.************* ");
		return destPath;
	}

	public void createPdf(String dest, Loanorigination loanData,String img, String jsPaths) {
		logger.debug("***********Entered***********");
		final String imgPath = img;
	final String mclrVal = mclrl;

		BigDecimal mclrMF = new BigDecimal("0.0");

		String gender = loanData.getSex();
		if (gender.equals("MALE")) {
			if (loanData.getMclrMaleFemale() != null) {
				Double dd = new Double(loanData.getMclrMaleFemale());
				mclrMF = BigDecimal.valueOf(dd);
			}

		} else if (gender.equals("FEMALE")) {
			if (loanData.getMclrMaleFemale() != null) {
				Double dd = new Double(loanData.getMclrMaleFemale());
				mclrMF = BigDecimal.valueOf(dd);
			}
		}

		Document document = null;
		PdfWriter writer = null;

		try {
			String appl1Address = (loanData.getAppl1address() != null && loanData.getAppl1address() != ""
					&& !loanData.getAppl1address().equals("null") ? loanData.getAppl1address() : " ");
			document = new Document();
			writer = PdfWriter.getInstance(document, new FileOutputStream(dest, false));
			HeaderFooterPageEvent event = new HeaderFooterPageEvent();
			writer.setPageEvent(event);
			document.open();
			Font canFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
			Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
			document.addKeywords("Java, Servelet, PDF, iText");
			document.addAuthor("Raj Grover");
			document.addCreator("UBILoanOrigination");

			Image img2 = Image.getInstance(imgPath);
			img2.setAbsolutePosition(250f, 740f);
			img2.scaleAbsolute(80f, 80f);
			document.add(img2);
			document.add(new Paragraph("\n"));
			document.add(new Paragraph("\n"));
			document.add(new Paragraph("\n"));

			Paragraph sp9 = new Paragraph(
					"Text of the consent clause to be taken from the Borrowers along with loan documents\n\n", canFont);
			sp9.setAlignment(Element.ALIGN_CENTER);
			document.add(sp9);

			Paragraph sp10 = new Paragraph(
					"I, understand that as a pre-condition relating to grant of the loan/advances/other non-fund based credit facilities to me, United Bank of India, requires my consent for the disclosure by the Bank of information and data relating to me, of the credit facility availed of/to be availed, by me, obligations assumed/to be assumed by me, in relation thereto and default, if any, committed by me, in discharge thereof.\n\n1. Accordingly, I, hereby agree and give consent for the disclosure by United Bank of India of all or any such ;\n"
							+ "\n" + "(a) Information and data relating to me ;\n" + "\n"
							+ "(b) The information or data relating to any credit facility availed of/to be availed, by me; and\n"
							+ "\n"
							+ "(c) Default, if any, committed by me, in discharge of my such obligation as United Bank of India may deem appropriate and necessary, to disclose and furnish to Credit Information Bureau (India) Ltd. and any other agency authorized in this behalf by RBI.\n"
							+ "\n"
							+ "2. I, declare that the information and data furnished by me to United Bank of India are true and correct.\n"
							+ "\n" + "3. I, undertake that\n" + "\n"
							+ "(a) The Credit Information Bureau (India) Ltd. and any other agency so authorized may use, process the said information and data disclosed by the bank in the manner as deemed fit by them; and\n"
							+ "\n"
							+ "(b) The Credit Information Bureau (India) Ltd. and any other agency so authorized may furnish for consideration, the processed information and data of products thereof prepared by them, to bank/financial institutions and other credit grantors of registered users, as may be specified by the Reserve Bank in this behalf.\n\n\n\n",
					redFont);
			sp10.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(sp10);

			Paragraph sp11 = new Paragraph(underLine(
					loanData.getAppl1name() != null && !loanData.getAppl1name().equals("null") ? loanData.getAppl1name()
							: " ")
					+ "\n" + "Signature(s) of Borrower(s)\n", redFont);
			sp11.setAlignment(Element.ALIGN_RIGHT);
			document.add(sp11);

			// Page-2 start##################################################

		} catch (FileNotFoundException fnf) {
			logger.error("Error:.....!!! " + fnf.getMessage());
		} catch (DocumentException de) {
			logger.error("Error:.....!!! " + de.getMessage());
		} catch (IOException ex) {
			logger.error("Error:.....!!! " + ex.getMessage());
		}
		document.close();

		logger.debug("***********Ended createPdf()***********");
	}

	public Chunk underLine(String data) {
		Paragraph pp9 = new Paragraph();
		pp9.setAlignment(Element.ALIGN_JUSTIFIED);
		Chunk underline1 = new Chunk(data, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL));
		underline1.setUnderline(0.1f, -2f);
		pp9.add(underline1);
		return underline1;
	}

}
