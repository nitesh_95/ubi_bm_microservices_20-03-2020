package com.subk.core;

import java.util.HashMap;

public class StaffServices {
    private static StaffServices instance;
    public static final String UBI_GROUP_REGISTRATION = "111";
    public static final String UBI_LOAN_ACC_OPENIG = "112";
    public static final String PENDING_FOR_REVIEW = "113";
    public static final String PENDING_AT_SUBORDINATE= "114";
    public static final String PENDING_AT_SUPERIOR = "115";
    public static final String APPROVED_LOANS = "116";
    public static final String REJECTED_LOANS = "117";
    public static final String RETURNED_LOANS = "118";
    public static final String SEARCH = "119";

    public static final String[] STAFF_SERVICES = {
        UBI_GROUP_REGISTRATION,UBI_LOAN_ACC_OPENIG,PENDING_FOR_REVIEW,PENDING_AT_SUBORDINATE,
        PENDING_AT_SUPERIOR,APPROVED_LOANS,REJECTED_LOANS,RETURNED_LOANS,SEARCH,
    };

    public static final String SERVICE_DOWN = "SERVICE_DOWN";
    public static final String ENABLED = "ENABLED";
    public static final String DISABLED = "DISABLED";

    private HashMap<String, String> serviceMap;
    private HashMap<String, String> serviceNames;
    private HashMap<String, String> serviceDownInfo;
    private HashMap<String, String> serviceLinks;

    private StaffServices() {
        serviceMap = new HashMap<String, String>();
        serviceDownInfo = new HashMap<String, String>();
        serviceNames = new HashMap<String, String>();
        serviceLinks = new HashMap<String, String>();
        
        serviceNames.put(UBI_GROUP_REGISTRATION, "UBI Group Registration");
        serviceNames.put(UBI_LOAN_ACC_OPENIG, "UBI Loan Origination");
        serviceNames.put(PENDING_FOR_REVIEW, "Pending for Review");
        serviceNames.put(PENDING_AT_SUBORDINATE, "Pending at Subordinate");
        serviceNames.put(PENDING_AT_SUPERIOR, "Pending at Superior");
        serviceNames.put(APPROVED_LOANS, "Approved Loans");
        serviceNames.put(REJECTED_LOANS, "Rejected Loans");
        serviceNames.put(RETURNED_LOANS, "Returned Loans");
        serviceNames.put(SEARCH, "Search");
        
        serviceLinks.put(UBI_GROUP_REGISTRATION, "./ubiGroupRegistration.jsp");
        serviceLinks.put(UBI_LOAN_ACC_OPENIG, "./ubiLoanAccOpening.jsp");
        serviceLinks.put(PENDING_FOR_REVIEW, "./pendingForReview");
        serviceLinks.put(PENDING_AT_SUBORDINATE, "./pendingatSubordinate");
        serviceLinks.put(PENDING_AT_SUPERIOR, "./pendingatSuperior");
        serviceLinks.put(APPROVED_LOANS, "./approvedLoan");
        serviceLinks.put(REJECTED_LOANS, "./rejectedLoan");
        serviceLinks.put(RETURNED_LOANS, "./returnedLoan");
        serviceLinks.put(SEARCH, "./search.jsp");
    }
    public static StaffServices getInstance() {
        if (instance == null)
            instance = new StaffServices();
        return instance;
    }

    public void setServices(String staffServices) {
        // 001|1|#002|0|serivce down#
        System.out.println("staffServices.length(): "+staffServices.length());
	if (staffServices.length() == 0)
            return;
	String[] services = staffServices.split("#");
	for (String service : services) {
            String[] temp = service.split("\\|");
            String status = temp[1].equals("1") ? ENABLED : SERVICE_DOWN;
            serviceMap.put(temp[0], status);
            if (status.equals(SERVICE_DOWN)) {
                serviceDownInfo.put(temp[0], temp[2]);
            }
	}
    }

    public String getServiceNameById(String id) {
        return serviceNames.get(id);
    }
    public String getServiceLinkById(String id) {
        return serviceLinks.get(id);
    }
    public boolean isServiceDisabled(String service) {
        if (serviceMap.get(service) != null && serviceMap.get(service).equals(DISABLED))
            return true;
        else
            return false;
    }
    public boolean isServiceEnabled(String service) {
        if (serviceMap.get(service) != null && serviceMap.get(service).equals(ENABLED))
            return true;
        else
            return false;
    }
    public boolean isServiceDown(String service) {
        if (serviceMap.get(service) != null && serviceMap.get(service).equals(SERVICE_DOWN))
            return true;
        else
            return false;
    }

    public String getServiceDownMessage(String service) {
        return serviceDownInfo.get(service);
    }
    public String[] getServices() {
        return STAFF_SERVICES;
    }
    public String[] getAllServices() {
        String[] defaultServices = {"BOD","EOD","CRO Monthly Plan","Transaction History", "BCO Details","Staff PIN Change", "Preferences", "About" };
        String[] listItems = new String[STAFF_SERVICES.length + defaultServices.length];
        String[] temp = new String[STAFF_SERVICES.length + defaultServices.length];

        for (int j = 0; j < STAFF_SERVICES.length; j++) {
            listItems[j] = getServiceNameById(STAFF_SERVICES[j]);
        }

        // System.arraycopy(STAFF_SERVICES, 0, listItems, 0,
        // STAFF_SERVICES.length);
        System.arraycopy(defaultServices, 0, listItems, STAFF_SERVICES.length,defaultServices.length);

        for (int i = 0; i < listItems.length; i++) {
            temp[i] = (i + 1) + ". " + listItems[i];
        }
        return temp;
    }
}
