/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.core;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.subk.entity.Loanorigination;
import com.subk.util.AddressCodes;

/**
 *
 * @author Ramya
 */
@Configuration
public class DemandPromissoryNotePDFFIle extends PDFGenerationAbstract {
	@Autowired
	AddressCodes addresscodes;
	
	@Value("${MCLRL}")
	private String mclrl; 
	private String empty = " ";
	private static final Logger logger = Logger.getLogger(GenerateUndertakingPDFFile.class);
//	private static final ResourceBundle rb = ResourceBundle.getBundle("systemPath");
//	private static final ResourceBundle rb1 = ResourceBundle.getBundle("com.subk.Resources//subkResources");
	Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

	public String getPdf(Loanorigination loanData, String fileName,String pathAdd, String img, String jsPaths) {
		logger.info("************ ENTERED ************* ");

		String destPath = null;

		logger.debug("UBILoanOrigination Data: " + loanData.toString());
		logger.debug("FileName: " + fileName);

		try {
			destPath =pathAdd;
			destPath = destPath + fileName;

			// File file = new File(destPath);
			// file.getParentFile().mkdirs();
			new DemandPromissoryNotePDFFIle().createPdf(destPath, loanData ,img, jsPaths);
		} catch (Exception ex) {
			logger.error("Get pdf Exception:....!!! " + ex.getMessage());
		}

		logger.info("destPathINATION PATH:...........!!!!!!!!!!  " + destPath);
		logger.info("************ ENDED.************* ");
		return destPath;
	}

	public void createPdf(String dest, Loanorigination loanData, String img, String jsPaths) {
		logger.debug("***********Entered***********");
		final String imgPath = img;
		final String mclrVal = loanData.getMclr() != null && !loanData.getMclr().equals("") ? loanData.getMclr() : "";// rb1.getString("MCLRL");
		String mclrMF = null;
		 int totalinterest = 0;;
//        BigDecimal mclrMF = new BigDecimal("0.0");

		String gender = loanData.getSex();
		if (gender.equals("MALE")) {
			mclrMF = loanData.getMclrMaleFemale() != null && !loanData.getMclrMaleFemale().equals("")
					? loanData.getMclrMaleFemale()
					: ""; // rb1.getString("MCLRL_MALE");
//                    loanData.getMclrMale();
		} else if (gender.equals("FEMALE")) {
			mclrMF = loanData.getMclrMaleFemale() != null && !loanData.getMclrMaleFemale().equals("")
					? loanData.getMclrMaleFemale()
					: "";// rb1.getString("MCLRL_FEMALE");

//                    loanData.getMclrFemale();
		}

		Document document = null;
		PdfWriter writer = null;

		try {
			String appl1Address = (loanData.getAppl1address() != null && loanData.getAppl1address() != ""
					&& !loanData.getAppl1address().equals("null") ? loanData.getAppl1address() : " ");
			document = new Document();
			writer = PdfWriter.getInstance(document, new FileOutputStream(dest, false));
			// writer.setPageEvent(new Header());
			HeaderFooterPageEvent event = new HeaderFooterPageEvent();
			writer.setPageEvent(event);
			document.open();

			Font canFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
			Font font11b = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
			// File file = new
			// File("C:\\"+loanData.getAppl1Name()+"_"+loanData.getApplicationId());
			// Get the output stream for writing PDF object
			// ServletOutputStream out = response.getOutputStream();
			// OutputStream oos = new FileOutputStream(file,true);
			// Document document = new Document();
			// PdfWriter writer = PdfWriter.getInstance(document, out);
//            document.open();
			document.addKeywords("Java, Servelet, PDF, iText");
			document.addAuthor("Raj Grover");
			document.addCreator("UBILoanOrigination");

			Image img2 = Image.getInstance(imgPath);
			img2.setAbsolutePosition(250f, 740f);
			img2.scaleAbsolute(80f, 80f);
			document.add(img2);
			document.add(new Paragraph("\n"));
			document.add(new Paragraph("\n"));
			document.add(new Paragraph("\n"));

			Paragraph sp16 = new Paragraph("\nDemand Promissory Note\n\n\n", canFont);
			sp16.setAlignment(Element.ALIGN_CENTER);
			document.add(sp16);
			System.out.println("looaaaaaa"+loanData.getLoanamountsactioned());
			String loanAmountSanctioned = loanData.getLoanamountsactioned() != null && !loanData.getLoanamountsactioned().equals("null")
                    && !loanData.getLoanamountsactioned().isEmpty() ? loanData.getLoanamountsactioned() : empty;
//			String loanAmountSanctioned = loanData.getLoanamountsactioned() != null
//					&& !loanData.getLoanamountsactioned().equals("null") && !loanData.getLoanamountsactioned().isEmpty()
//							? loanData.getLoanamountsactioned()
//							: empty;
			String rupees = NumberToWord1(loanAmountSanctioned);
			String commaloanAmountSanctioned = getIndianCurrencyFormat(loanAmountSanctioned);
//            double result = Double.parseDouble(loanAmountSanctioned);
//
//String resultmoney=String.format("%,.2f",result);

//			Paragraph sp17 = new Paragraph(
//					"Place : ..................................                                                                                                Rs: ",
//					redFont);
			String interest=(loanData.getInterestrate() != null && loanData.getInterestrate() != ""
					&& !loanData.getInterestrate().equals("null") ? loanData.getInterestrate() : " ");
			Paragraph sp17 = new Paragraph("Place : ..................................                                                                                                Rs: ",redFont);
			Chunk amm = new Chunk(commaloanAmountSanctioned, font11b);
			sp17.add(amm);
			sp17.add("/-" + "\n" + "Date : ...................................\n\n\n"
					+ "On demand I / We jointly and severally promise to pay UNITED BANK OF INDIA or order at ");
			Chunk brname = new Chunk("Kolkata", redFont);
			brname.setUnderline(0.6f, -2f);
			sp17.add(brname);
//			sp17.add(" or");
//			Chunk br_name = new Chunk("Kolkata", redFont);
//			br_name.setUnderline(0.6f, -2f);
//			sp17.add(br_name);
//            sp17.add(underLine(loanData.getBranchName()));
			sp17.add(" the sum of Rs. ");
			Chunk amount = new Chunk(commaloanAmountSanctioned, font11b);
			amount.setUnderline(0.6f, -2f);
			sp17.add(amount);
//            sp17.add(underLine(commaloanAmountSanctioned));
			sp17.add("/- Loan amount in words: (");
			Chunk rupee = new Chunk(rupees, redFont);
			rupee.setUnderline(0.6f, -2f);
			sp17.add(rupee);
//            sp17.add(underLine(rupees));
			sp17.add(" Rupees only) with interest thereon at the rate of ");
			String mclrMFFinal = ((String.valueOf(mclrMF)) != null && !((String.valueOf(mclrMF)).equals(""))
					&& (String.valueOf(mclrMF)) != "null" ? (String.valueOf(mclrMF)) : " ");
			Chunk mclr = new Chunk(mclrMFFinal, font11b);
			mclr.setUnderline(0.6f, -2f);
			sp17.add(mclr);
//            sp17.add(underLine(mclrMFFinal));

			sp17.add(" percent per annum below / at / over the United Bank of India MCLR-Y which is at present ");

			// logger.info("**************mclr_fixed: " + Float.parseFloat(mclrVal));
			// logger.info("**************mclr: " + mclrMFFinal);
//            BigDecimal appropriateRate = new BigDecimal(mclrVal);
//            appropriateRate = appropriateRate.add(mclrMF);
//
//            System.out.println("appropriateRate****************************: " + appropriateRate);
			Chunk mclr_val = new Chunk(String.valueOf(mclrVal), font11b);
			mclr_val.setUnderline(0.6f, -2f);
			sp17.add(mclr_val);
//            sp17.add(underLine(String.valueOf(mclrVal)));
			sp17.add(" % per annum with ");
			Chunk mnth = new Chunk("monthly", redFont);
			mnth.setUnderline(0.6f, -2f);
			sp17.add(mnth);
			sp17.add(" rests for value received / with interest thereon at the rate of ");
			System.out.println("total:::::::"+totalinterest);
			Chunk totint = new Chunk(interest, font11b);
			totint.setUnderline(0.6f, -2f);
			sp17.add(totint);
			sp17.add("% annum with ....................");
			sp17.add(" rests for value received.\n\n\n");
			sp17.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(sp17);
			document.add(new Paragraph("\n"));
			document.add(new Paragraph("\n"));
			String appl_name = (loanData.getAppl1name() != null && !loanData.getAppl1name().equals("null")
					&& !loanData.getAppl1name().isEmpty() ? loanData.getAppl1name() : empty);
			String hou_num = (loanData.getPermhouseno() != null && !loanData.getPermhouseno().equals("null")
					&& !loanData.getPermhouseno().isEmpty() ? loanData.getPermhouseno() : empty);
			String street_no = (loanData.getPermstreetno() != null && !loanData.getPermstreetno().equals("null")
					&& !loanData.getPermstreetno().isEmpty() ? loanData.getPermstreetno() : empty);
			String vill_name = (loanData.getPermvillage() != null && !loanData.getPermvillage().equalsIgnoreCase("null") && loanData.getPermvillage() != " " ? loanData.getPermvillage() : empty);
			String landmark = (loanData.getPermlandmark() != null && !loanData.getPermlandmark().equals("null")
					&& !loanData.getPermlandmark().isEmpty() ? loanData.getPermlandmark() : empty);
			String dis = (loanData.getPermdistrict() != null && !loanData.getPermdistrict().equalsIgnoreCase("null") && loanData.getPermdistrict() != " " ? loanData.getPermdistrict() : empty);
			String subdis = (loanData.getPermsubdistrict() != null && !loanData.getPermsubdistrict().equalsIgnoreCase("null") && loanData.getPermsubdistrict() != " " ? loanData.getPermsubdistrict() : empty);
			String state = (loanData.getPermstate() != null && !loanData.getPermstate().equalsIgnoreCase("null") && loanData.getPermstate() != " " ? loanData.getPermstate() : " ");
			PdfPTable table1 = new PdfPTable(2);
			table1.setWidthPercentage(100f);
			table1.setWidths(new float[] { 3, 0.3f });
			table1.addCell(getCell("Address of executant(s):\n", PdfPCell.ALIGN_LEFT));
			PdfPCell cell1 = new PdfPCell(new Phrase(" \n " + "\n" + "\n" + "\n", redFont));
			cell1.setRowspan(4);
			table1.addCell(cell1);
			table1.addCell(getCell(appl_name, PdfPCell.ALIGN_LEFT));
			table1.addCell(getCell(hou_num.toUpperCase(), PdfPCell.ALIGN_LEFT));
			table1.addCell(getCell(street_no.toUpperCase() + "," 
					+ landmark.toUpperCase() + ","+ "\n" +vill_name.toUpperCase() + "," + subdis + "," + "\n" +dis.toUpperCase() + "," + state, PdfPCell.ALIGN_LEFT));
			table1.setSpacingAfter(6f);
			document.add(table1);

		} catch (FileNotFoundException fnf) {
			logger.error("FileNotFoundException:.....!!! " + fnf.getMessage());
		} catch (DocumentException de) {
			logger.error("DocumentException:.....!!! " + de.getMessage());
		} catch (IOException ex) {
			logger.error("IOException:.....!!! " + ex.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception::::::::::::::::::::!!!!!" + e.getMessage());
		}
		document.close();

		logger.debug("***********Ended createPdf()***********");
	}

	public PdfPCell getCell(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text, redFont));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}

	public Chunk underLine(String data) {
		Paragraph pp9 = new Paragraph();
		pp9.setAlignment(Element.ALIGN_JUSTIFIED);
		Chunk underline1 = new Chunk(data, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL));
		underline1.setUnderline(0.1f, -2f);
		pp9.add(underline1);
		return underline1;
	}

	private static String NumberToWord1(String number) {
		String twodigitword = "";
		String word = "";
		String[] HTLC = { "", "Hundred", "Thousand", "Lakh", "Crore" }; // H-hundread , T-Thousand, ..
		int split[] = { 0, 2, 3, 5, 7, 9 };
		String[] temp = new String[split.length];
		boolean addzero = true;
		int len1 = number.length();
		if (len1 > split[split.length - 1]) {
			System.out.println("Error. Maximum Allowed digits " + split[split.length - 1]);
			System.exit(0);
		}
		for (int l = 1; l < split.length; l++) {
			if (number.length() == split[l]) {
				addzero = false;
			}
		}
		if (addzero == true) {
			number = "0" + number;
		}
		int len = number.length();
		int j = 0;
		// spliting & putting numbers in temp array.
		while (split[j] < len) {
			int beg = len - split[j + 1];
			int end = beg + split[j + 1] - split[j];
			temp[j] = number.substring(beg, end);
			j = j + 1;
		}

		for (int k = 0; k < j; k++) {
			twodigitword = ConvertOnesTwos(temp[k]);
			if (k >= 1) {
				if (twodigitword.trim().length() != 0) {
					word = twodigitword + " " + HTLC[k] + " " + word;
				}
			} else {
				word = twodigitword;
			}
		}
		return (word);
	}

	private static String ConvertOnesTwos(String t) {
		final String[] ones = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten",
				"Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
		final String[] tens = { "", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty",
				"Ninety" };

		String word = "";
		int num = Integer.parseInt(t);
		if (num % 10 == 0) {
			word = tens[num / 10] + " " + word;
		} else if (num < 20) {
			word = ones[num] + " " + word;
		} else {
			word = tens[(num - (num % 10)) / 10] + word;
			word = word + " " + ones[num % 10];
		}
		return word;
	}
	
	
	public String getIndianCurrencyFormat(String amount) {
	    StringBuilder stringBuilder = new StringBuilder();
	    char amountArray[] = amount.toCharArray();
	    int a = 0, b = 0;
	    for (int i = amountArray.length - 1; i >= 0; i--) {
	        if (a < 3) {
	            stringBuilder.append(amountArray[i]);
	            a++;
	        } else if (b < 2) {
	            if (b == 0) {
	                stringBuilder.append(",");
	                stringBuilder.append(amountArray[i]);
	                b++;
	            } else {
	                stringBuilder.append(amountArray[i]);
	                b = 0;
	            }
	        }
	    }
	    return stringBuilder.reverse().toString();
	}

}
