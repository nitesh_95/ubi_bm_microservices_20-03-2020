package com.subk.core;

import java.io.File;

import org.springframework.context.annotation.Configuration;

@Configuration
public class GetAppraisalDocumentsLuc {

	public String getFXAppraisalPDF(String applicationid, String solid, String applicantImagePath) {
		String rootdirectory = applicantImagePath;
		System.out.println("RootDirectory: " + rootdirectory);
		String filePath = null;
		File soliddir = new File(rootdirectory + File.separator + solid);
		System.out.println("Dir:*****: " + soliddir);
		if (soliddir.exists()) {
			System.out.println("inside if.........."+applicationid);
			File applidir = new File(soliddir + File.separator + applicationid);
			if (applidir.exists()) {
				System.out.println("appliDir path:*****: " + soliddir);
				File appliFxappraisalpdf = new File(applidir + File.separator + "LUCpdf.pdf");
				if (appliFxappraisalpdf.exists()) {
					System.out.println("appliFxappraisal:*******: " + appliFxappraisalpdf);
					filePath = rootdirectory + File.separator + solid + File.separator + applicationid + File.separator
							+ "LUCpdf.pdf";
				} else {
					System.out.println("Else block.................");
					filePath = null;
					return filePath;
				}
			} else {
				System.out.println("inside else............");
				filePath = null;
				return filePath;
			}
		} else {
			System.out.println("inside else............");
			filePath = null;
			return filePath;
		}
		return filePath;
	}

	public String getFXAppraisalPDFs(String applicationid, String solid, String applicantImagePath) {
		String rootdirectory = applicantImagePath;
		System.out.println("RootDirectory: " + rootdirectory);
		String filePath = null;
		File soliddir = new File(rootdirectory + File.separator + solid);
		System.out.println("Dir:*****: " + soliddir);
		if (soliddir.exists()) {
			System.out.println("inside if..........");
			File applidir = new File(soliddir + File.separator + applicationid);
			if (applidir.exists()) {
				System.out.println("appliDir path:*****: " + soliddir);
				File appliFxappraisalpdf = new File(applidir + File.separator + "CibilReportUpload.pdf");
				if (appliFxappraisalpdf.exists()) {
					System.out.println("appliFxappraisal:*******: " + appliFxappraisalpdf);
					filePath = rootdirectory + File.separator + solid + File.separator + applicationid + File.separator
							+ "CibilReportUpload.pdf";
				} else {
					System.out.println("Else block.................");
					filePath = null;
					return filePath;
				}
			} else {
				System.out.println("inside else............");
				filePath = null;
				return filePath;
			}
		} else {
			System.out.println("inside else............");
			filePath = null;
			return filePath;
		}
		return filePath;
	}

	public String getFXAppraisalzip(String applicationid, String solid, String pathAdd) {
		String rootdirectory = pathAdd;
		System.out.println("RootDirectory: " + rootdirectory);
		String filePath = null;
		File soliddir = new File(rootdirectory + File.separator + solid);
		System.out.println("Dir:*****: " + soliddir);
		if (soliddir.exists()) {
			System.out.println("inside if..........");
			File applidir = new File(soliddir + File.separator + applicationid);
			if (applidir.exists()) {
				System.out.println("appliDir path:*****: " + soliddir);
				File appliFxappraisalpdf = new File(applidir + File.separator + "ApplicantImages.zip");
				if (appliFxappraisalpdf.exists()) {
					filePath = rootdirectory + File.separator + solid + File.separator + applicationid + File.separator
							+ "ApplicantImages.zip";
				} else {
					System.out.println("Else block.................");
					filePath = null;
					return filePath;
				}
			} else {
				System.out.println("inside else............");
				filePath = null;
				return filePath;
			}
		} else {
			System.out.println("inside else............");
			filePath = null;
			return filePath;
		}
		return filePath;
	}

public String getFXAppraisalImages(String applicationid, String solid, String applicantImagePath) {
	String rootdirectory = applicantImagePath;
	System.out.println("RootDirectory: " + rootdirectory);
	String filePath = null;
	File soliddir = new File(rootdirectory + File.separator + solid);
	System.out.println("Dir:*****: " + soliddir);
	if (soliddir.exists()) {
		System.out.println("inside if..........");
		File applidir = new File(soliddir + File.separator + applicationid);
		if (applidir.exists()) {
			System.out.println("appliDir path:*****: " + soliddir);
			File appliFxappraisalpdf = new File(applidir + File.separator + "ApplicantPhoto.jpg");
			if (appliFxappraisalpdf.exists()) {
				System.out.println("appliFxappraisal:*******: " + appliFxappraisalpdf);
				filePath = rootdirectory + File.separator + solid + File.separator + applicationid + File.separator
						+ "ApplicantPhoto.jpg";
			} else {
				System.out.println("Else block.................");
				filePath = null;
				return filePath;
			}
		} else {
			System.out.println("inside else............");
			filePath = null;
			return filePath;
		}
	} else {
		System.out.println("inside else............");
		filePath = null;
		return filePath;
	}
	return filePath;
}

}