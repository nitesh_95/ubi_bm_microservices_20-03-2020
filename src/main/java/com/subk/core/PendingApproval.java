/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.core;

/**
 *
 * @author subashkumar.s
 */
public class PendingApproval {
    private String sno;
    private String applicationNo;
    private String dtOfSubmission;
    private String customerName;
    private String contactNo;
    private String villageName;
    private String fxVerificationStatus;
    private String fxVerifiedDate;
    private String teVerificationStatus;
    private String teVerifiedDate;
    private String uhVerificationStatus;
    private String uhVerifiedDate;
    private String bmVerificationStatus;

    /**
     * @return the sno
     */
    public String getSno() {
        return sno;
    }

    /**
     * @param sno the sno to set
     */
    public void setSno(String sno) {
        this.sno = sno;
    }

    /**
     * @return the applicationNo
     */
    public String getApplicationNo() {
        return applicationNo;
    }

    /**
     * @param applicationNo the applicationNo to set
     */
    public void setApplicationNo(String applicationNo) {
        this.applicationNo = applicationNo;
    }

    /**
     * @return the dtOfSubmission
     */
    public String getDtOfSubmission() {
        return dtOfSubmission;
    }

    /**
     * @param dtOfSubmission the dtOfSubmission to set
     */
    public void setDtOfSubmission(String dtOfSubmission) {
        this.dtOfSubmission = dtOfSubmission;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the contactNo
     */
    public String getContactNo() {
        return contactNo;
    }

    /**
     * @param contactNo the contactNo to set
     */
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    /**
     * @return the villageName
     */
    public String getVillageName() {
        return villageName;
    }

    /**
     * @param villageName the villageName to set
     */
    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }

    /**
     * @return the fxVerificationStatus
     */
    public String getFxVerificationStatus() {
        return fxVerificationStatus;
    }

    /**
     * @param fxVerificationStatus the fxVerificationStatus to set
     */
    public void setFxVerificationStatus(String fxVerificationStatus) {
        this.fxVerificationStatus = fxVerificationStatus;
    }

    /**
     * @return the fxVerifiedDate
     */
    public String getFxVerifiedDate() {
        return fxVerifiedDate;
    }

    /**
     * @param fxVerifiedDate the fxVerifiedDate to set
     */
    public void setFxVerifiedDate(String fxVerifiedDate) {
        this.fxVerifiedDate = fxVerifiedDate;
    }

    /**
     * @return the teVerificationStatus
     */
    public String getTeVerificationStatus() {
        return teVerificationStatus;
    }

    /**
     * @param teVerificationStatus the teVerificationStatus to set
     */
    public void setTeVerificationStatus(String teVerificationStatus) {
        this.teVerificationStatus = teVerificationStatus;
    }

    /**
     * @return the teVerifiedDate
     */
    public String getTeVerifiedDate() {
        return teVerifiedDate;
    }

    /**
     * @param teVerifiedDate the teVerifiedDate to set
     */
    public void setTeVerifiedDate(String teVerifiedDate) {
        this.teVerifiedDate = teVerifiedDate;
    }

    /**
     * @return the uhVerificationStatus
     */
    public String getUhVerificationStatus() {
        return uhVerificationStatus;
    }

    /**
     * @param uhVerificationStatus the uhVerificationStatus to set
     */
    public void setUhVerificationStatus(String uhVerificationStatus) {
        this.uhVerificationStatus = uhVerificationStatus;
    }

    /**
     * @return the uhVerifiedDate
     */
    public String getUhVerifiedDate() {
        return uhVerifiedDate;
    }

    /**
     * @param uhVerifiedDate the uhVerifiedDate to set
     */
    public void setUhVerifiedDate(String uhVerifiedDate) {
        this.uhVerifiedDate = uhVerifiedDate;
    }

    /**
     * @return the bmVerificationStatus
     */
    public String getBmVerificationStatus() {
        return bmVerificationStatus;
    }

    /**
     * @param bmVerificationStatus the bmVerificationStatus to set
     */
    public void setBmVerificationStatus(String bmVerificationStatus) {
        this.bmVerificationStatus = bmVerificationStatus;
    }
    
}
