/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.core;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.subk.entity.Loanorigination;
import com.subk.util.AddressCodes;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 *
 * @author Ramya
 */
@Configuration
public class HypothecationAgreementPDFFile extends PDFGenerationAbstract {
	@Autowired
	AddressCodes addresscodes;

	@Value("${MCLRL}")
	private String mclrl;  
    private String empty = " ";
    private static final Logger logger = Logger.getLogger(GenerateUndertakingPDFFile.class);
//    private static final ResourceBundle rb = ResourceBundle.getBundle("systemPath");
//    private static final ResourceBundle rb1 = ResourceBundle.getBundle("com.subk.Resources//subkResources");

    public String getPdf(Loanorigination loanData, String fileName,String pathAdd, String img, String jsPaths) {
        logger.info("************ ENTERED ************* ");

        String destPath = null;

        logger.debug("UBILoanOrigination Data: " + loanData.toString());
        logger.debug("FileName: " + fileName);

        try {
            destPath = pathAdd;
            destPath = destPath + fileName;

            //File file = new File(destPath);
            //file.getParentFile().mkdirs();
            new HypothecationAgreementPDFFile().createPdf(destPath, loanData, img, jsPaths);
        } catch (Exception ex) {
            logger.error("Error:....!!! " + ex.getMessage());
        }

        logger.info("destPathINATION PATH:...........!!!!!!!!!!  " + destPath);
        logger.info("************ ENDED.************* ");
        return destPath;
    }

    public void createPdf(String dest, Loanorigination loanData , String img, String jsPaths) {
        logger.debug("***********Entered***********");
        final String imgPath = img;
        final String mclrVal = loanData.getMclr() != null && !loanData.getMclr().equals("") ? loanData.getMclr() : "";//rb1.getString("MCLRL");

        String mclrMF = null;
//        BigDecimal mclrMF = new BigDecimal("0.0");

        String gender = loanData.getSex();
        if (gender.equals("MALE")) {
            mclrMF = loanData.getMclrMaleFemale() != null && !loanData.getMclrMaleFemale().equals("") ? loanData.getMclrMaleFemale() : ""; //rb1.getString("MCLRL_MALE");
//                    loanData.getMclrMale();
        } else if (gender.equals("FEMALE")) {
            mclrMF = loanData.getMclrMaleFemale() != null && !loanData.getMclrMaleFemale().equals("") ? loanData.getMclrMaleFemale() : ""; //rb1.getString("MCLRL_FEMALE");

//                    loanData.getMclrFemale();
        }

        Document document = null;
        PdfWriter writer = null;

        try {
            String appl1Address = (loanData.getAppl1address() != null && loanData.getAppl1address() != "" && !loanData.getAppl1address().equals("null") ? loanData.getAppl1address() : " ");
            document = new Document();
            writer = PdfWriter.getInstance(document, new FileOutputStream(dest, false));
            //writer.setPageEvent(new Header());
            HeaderFooterPageEvent event = new HeaderFooterPageEvent();
            writer.setPageEvent(event);
            document.open();

            Font canFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
            Font canFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
            Font canFont2 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
            Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
            Font redFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);
            Font font11b = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
            //File file = new File("C:\\"+loanData.getAppl1Name()+"_"+loanData.getApplicationId());
            // Get the output stream for writing PDF object
            //ServletOutputStream out = response.getOutputStream();
            //OutputStream oos = new FileOutputStream(file,true);
            //Document document = new Document();
            //PdfWriter writer = PdfWriter.getInstance(document, out);
//            document.open();
            document.addKeywords("Java, Servelet, PDF, iText");
            document.addAuthor("Raj Grover");
            document.addCreator("UBILoanOrigination");

            Image img2 = Image.getInstance(imgPath);
            img2.setAbsolutePosition(250f, 740f);
            img2.scaleAbsolute(80f, 80f);
            document.add(img2);
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("\n"));

            String applicantName = (loanData.getAppl1name() != null && loanData.getAppl1name() != "" && !loanData.getAppl1name().equals("null") ? loanData.getAppl1name() : " ");
            String applicantFhgName = (loanData.getFhgname() != null && loanData.getFhgname() != "" && !loanData.getFhgname().equals("null") ? loanData.getFhgname() : " ");
//            String applicantAddress = (loanData.getAppl1address() != null && loanData.getAppl1address() != "" && !loanData.getAppl1address().equals("null") ? loanData.getAppl1address() : " ");
            String permAddress = (loanData.getPermhouseno() != null && !loanData.getPermhouseno().equalsIgnoreCase("null") && loanData.getPermhouseno() != " " ? loanData.getPermhouseno()+ "," : " ");
            permAddress += (loanData.getPermstreetno() != null && !loanData.getPermstreetno().equalsIgnoreCase("null") && loanData.getPermstreetno() != " " ? loanData.getPermstreetno()+ "," : " ");
            permAddress += (loanData.getPermlandmark() != null && !loanData.getPermlandmark().equalsIgnoreCase("null") && loanData.getPermlandmark() != " " ? loanData.getPermlandmark()+ "," : " ");
            permAddress += (loanData.getPermvillage() != null && !loanData.getPermvillage().equalsIgnoreCase("null") && loanData.getPermvillage() != " " ? loanData.getPermvillage()+ "," : " ");
            permAddress += (loanData.getPermdistrict() != null && !loanData.getPermdistrict().equalsIgnoreCase("null") && loanData.getPermdistrict() != " " ? loanData.getPermdistrict()+ "," : " ");		
            permAddress += (loanData.getPermsubdistrict() != null && !loanData.getPermsubdistrict().equalsIgnoreCase("null") && loanData.getPermsubdistrict() != " " ? loanData.getPermsubdistrict()+ "," : " ");
            permAddress += (loanData.getPermstate() != null && !loanData.getPermstate().equalsIgnoreCase("null") && loanData.getPermstate() != " " ? loanData.getPermstate()+ "," : " ");
            permAddress += (loanData.getPermpincode() != null && !loanData.getPermpincode().equalsIgnoreCase("null") && loanData.getPermpincode() != " " ? loanData.getPermpincode()+ "," : " ");
            permAddress += (loanData.getPermtelephone() != null && !loanData.getPermtelephone().equalsIgnoreCase("null") && loanData.getPermtelephone() != " " ? loanData.getPermtelephone()+ "," : " ");
            
            
            Paragraph sp26 = new Paragraph("HYPOTHECATION AGREEMENT\n", canFont);
            sp26.setAlignment(Element.ALIGN_CENTER);
            document.add(sp26);
            Paragraph sp27 = new Paragraph("(Goods, Book debts, Plant & Machineries)\n\n", canFont1);
            sp27.setAlignment(Element.ALIGN_CENTER);
            document.add(sp27);

            String sanctionedLoanAmt = loanData.getLoanamountsactioned() != null && !loanData.getLoanamountsactioned().equals("null") && loanData.getLoanamountsactioned() != " " ? loanData.getLoanamountsactioned() : "";
            String commasanctionedLoanAmt = getIndianCurrencyFormat(sanctionedLoanAmt);

            
            //String sanctionedLoanAmt = loanData.getLoanAmountSanctioned() != null && !loanData.getLoanAmountSanctioned().equals("null") && loanData.getLoanAmountSanctioned() != " " ? loanData.getLoanAmountSanctioned() : "";
            Paragraph sp28 = new Paragraph("",redFont);
            Chunk b_name = new Chunk("UNITED BANK OF INDIA ", canFont2);
            sp28.add(b_name);
//            sp28.add(new Chunk("UNITED BANK OF INDIA ", canFont2));
            sp28.add(new Chunk("(hereinafter called the bank) having at the request of ", redFont));
            Chunk app_name = new Chunk(applicantName,redFont1);
            app_name.setUnderline(0.6f, -2f);
            sp28.add(app_name);
            
            sp28.add(new Chunk(" S/o,W/o,D/o ",redFont1));
            Chunk rel_name = new Chunk(applicantFhgName ,redFont1);
            rel_name.setUnderline(0.6f, -2f);
            sp28.add(rel_name);      
            sp28.add(new Chunk(" residing at address ",redFont));
            Chunk addr = new Chunk(permAddress.toUpperCase(),redFont1);
            addr.setUnderline(0.6f, -2f);
            sp28.add(addr);  
            sp28.add(new Chunk(" (hereinafter called the borrower) agreed to grant accommodation to the borrower by way of Overdraft/Cash Credit account up to a limit of Rs. ", redFont));
            Chunk loanamt = new Chunk(commasanctionedLoanAmt,font11b);
            loanamt.setUnderline(0.6f, -2f);
            sp28.add(loanamt);  
//            sp28.add(new Chunk(underLine(commasanctionedLoanAmt)));
            sp28.add(new Chunk("/- on the security hereof and pursuant thereto the bank has called upon the borrower to execute the deed of Hypothecation being these presents in favour of the bank which the borrower has agreed to do in the manner hereinafter appearing.\n\n", redFont));
            sp28.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(sp28);

            PdfPTable tablekk1 = new PdfPTable(2);
            tablekk1.setWidthPercentage(100f);
            tablekk1.setWidths(new float[]{0.7f, 15});
            tablekk1.setHorizontalAlignment(Element.ALIGN_LEFT);

            PdfPCell cs1 = new PdfPCell();
            Paragraph ps1 = new Paragraph("1.", redFont);
            cs1.addElement(ps1);
            cs1.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs1);

            PdfPCell cs2 = new PdfPCell();
            Paragraph ps2 = new Paragraph("That the whole of the Borrower�s stock consisting of stocks of .................. whether raw or in process of manufacture and all products goods and movable property of any kind which now or hereafter from time to time during this security shall be brought into stored or be in or about the borrowers premises or godowns at " + ""
                    + ".................................................."
                    + "(hereinafter called the said goods) or wherever else the same may be (including any goods in course of transit) with the benefit of all rights relating thereto on sale thereof or otherwise shall remain hypothecated to the bank and its assigns by way of first charge as security for the due repayment to the bank on demand of the balance of accounts at any time on the cash credit account and future indebtedness and liabilities of the borrower to the bank of any kind in any manner whether solely or jointly primary collateral accrued or accruing with all relative interest charges costs (as between attorney and client) and expenses.\n", redFont);
            ps2.setAlignment(Element.ALIGN_JUSTIFIED);
            cs2.addElement(ps2);
            cs2.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs2);

            PdfPCell cs3 = new PdfPCell();
            Paragraph ps3 = new Paragraph("2.", redFont);
            cs3.addElement(ps3);
            cs3.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs3);

            PdfPCell cs4 = new PdfPCell();
            Paragraph ps4 = new Paragraph("That all the borrowers present and future book debts outstanding money receivable claims bills contracts engagements securities rights and movable assets (except property effectively otherwise hypothecated, charged or mortgaged to the bank) shall be hypothecated to the bank and it assigns by way of first charge as security for the payment to the bank on demand of the balance of accounts at any time and for the payment and discharge of all the borrower�s present and future indebtedness and liabilities to the bank of any kind in any manner whether solely or jointly primary or collateral accrued or accruing with all relative interest charges costs (as between attorney and client) and expenses.\n", redFont);
            ps4.setAlignment(Element.ALIGN_JUSTIFIED);
            cs4.addElement(ps4);
            cs4.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs4);

            PdfPCell cs5 = new PdfPCell();
            Paragraph ps5 = new Paragraph("3.", redFont);
            cs5.addElement(ps5);
            cs5.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs5);

            PdfPCell cs6 = new PdfPCell();
            Paragraph ps6 = new Paragraph("The borrower hereby hypothecates by way of first charge in favour of the bank and its assigns all plant, machinery, engines, boilers, appliances, tools and implements described in general terms in the schedule hereto (hereinafter referred to as �the plant & machinery�) which are now or shall hereafter at any time or from time to time during the continuance of the security hereby created be brought in or stored of to be in or about the borrowers premises or godowns at...... or wherever else the same may be (including plant & machinery in course of transit) as security for the due repayment to the bank on demand of the balance of accounts at any time on the said Overdraft/Cash Credit A/c or otherwise and for the payment of all present and future indebtedness and liabilities of the borrower to the bank of any ", redFont);
            ps6.setAlignment(Element.ALIGN_JUSTIFIED);
            cs6.addElement(ps6);
            cs6.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs6);
            document.add(tablekk1);
            document.newPage();
            PdfPTable tablekk2 = new PdfPTable(2);
            tablekk2.setWidthPercentage(100f);
            tablekk2.setWidths(new float[]{0.7f, 15});
            tablekk2.setHorizontalAlignment(Element.ALIGN_LEFT);
            
             PdfPCell c5 = new PdfPCell();
            Paragraph p5 = new Paragraph("  ", redFont);
            c5.addElement(p5);
            c5.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(c5);

            PdfPCell c6 = new PdfPCell();
            Paragraph p6 = new Paragraph("kind in any manner whether solely or jointly primary or collateral accrued or accruing with all relative interest charges costs (as between attorney and client) and expenses.\n", redFont);
            p6.setAlignment(Element.ALIGN_JUSTIFIED);
            c6.addElement(p6);
            c6.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(c6);
            
            PdfPCell cs7 = new PdfPCell();
            Paragraph ps7 = new Paragraph("4.", redFont);
            cs7.addElement(ps7);
            cs7.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs7);

            PdfPCell cs8 = new PdfPCell();
            Paragraph ps8 = new Paragraph("That interest at the rate of ", redFont);
            System.out.println("hkdhii"+mclrMF);
            ps8.add(underLine(String.valueOf(mclrMF)));
            ps8.add(" per cent per annum over the United Bank of Indias MCLR-Y which is at present ");
            ps8.add(underLine(String.valueOf(mclrVal)));
            ps8.add(" % per annum with");
            Chunk mn=new Chunk(" monthly",redFont);
            mn.setUnderline(0.6f, -2f);
            ps8.add(mn);
//            ps8.add(underLine(" monthly"));
            ps8.add(" rests and/or at the rate of......... ");
            ps8.add("% per annum with .............  ");
            ps8.add(" rests at such other rate as may be communicated to the Borrower by the Bank from time to time shall be calculated on the daily balance of accounts and charged in accordance with the practice of the Bank. Provided however, the bank shall at any time and from time to time be entitled to change the rate and terms of interest and may thereafter charge interest at such rate and on such terms as the bank may specify and this agreement shall be constituted as if such revised rate and terms of interest were incorporated herein provided further however, the bank shall also be entitled to charge at its discretion such enhance rate of interest of the entire outstanding or on a portion thereof as the bank may fix for any irregularity and for such period as the irregularity continues or for such time as the bank deem if necessary regard being on to the nature of irregularity and charging of such enhanced rate of interest shall be without prejudice to the other rights and remedies of the bank.\n");
            ps8.setAlignment(Element.ALIGN_JUSTIFIED);
            cs8.addElement(ps8);
            cs8.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs8);

            //Page-7 start ##########################################################
            PdfPCell cs9 = new PdfPCell();
            Paragraph ps9 = new Paragraph("5.", redFont);
            cs9.addElement(ps9);
            cs9.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs9);

            PdfPCell cs10 = new PdfPCell();
            Paragraph ps10 = new Paragraph("That the Bank will from time to time advise the Borrower of the margin of value to be maintained by the Borrower in favour of the Bank and that the Borrower shall at all times maintain the said goods of a sufficient quantity and value (as estimated by the Bank) so as to provide the margin advised by the Bank from time to time and shall forthwith whenever necessary provide further goods (approved by the Bank) to restore such margin or pay the Bank equivalent in cash.\n", redFont);
            ps10.setAlignment(Element.ALIGN_JUSTIFIED);
            cs10.addElement(ps10);
            cs10.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs10);

            PdfPCell cs11 = new PdfPCell();
            Paragraph ps11 = new Paragraph("6.", redFont);
            cs11.addElement(ps11);
            cs11.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs11);

            PdfPCell cs12 = new PdfPCell();
            Paragraph ps12 = new Paragraph("That the Bank shall not be required to make or continue advances otherwise than at the Banks discretion and in no circumstances to an amount exceeding with interest thereon the sum of Rs ", redFont);
            Chunk a=new Chunk(commasanctionedLoanAmt,font11b);
            a.setUnderline(0.6f, -2f);
            ps12.add(a);
            ps12.add(new Chunk("/- being the limit mentioned as aforesaid or a sum equal to the value of the goods to be maintained by the Borrower hereunder less the margin of value which the Borrower is required to maintain as provided in Clause 5 hereof whichever may be the less.\n", redFont));
            ps12.setAlignment(Element.ALIGN_JUSTIFIED);
            cs12.addElement(ps12);
            cs12.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs12);

            PdfPCell cs13 = new PdfPCell();
            Paragraph ps13 = new Paragraph("7.", redFont);
            cs13.addElement(ps13);
            cs13.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs13);

            PdfPCell cs14 = new PdfPCell();
            Paragraph ps14 = new Paragraph("That the goods/plant & machineries or such portion thereof as the bank may require shall be kept and maintained by the Borrower at his risk and expense in good condition and fully insured against loss or damage by fire/pilferage, accident or from any cause whatsoever as may be required by the Bank with an Insurance Company of repute to be approved of by the Bank in the joint names of the Bank and the Borrower and the Insurance Policies shall be transferred and delivered to the Bank. The Borrower will be liable to make good any loss occasioned by the theft wastage, damage, deterioration, or any other cause whatsoever. The plant and machinery shall at all times remain at the risk of the Borrower and he shall be liable for all losses and damages thereto howsoever caused. The Borrower shall duly and punctually pay all premium for such policies of Insurance and shall produce from time to time to the Bank all receipts of such premium paid on such policies of Insurance.\n", redFont);
            ps14.setAlignment(Element.ALIGN_JUSTIFIED);
            cs14.addElement(ps14);
            cs14.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs14);

            PdfPCell cs15 = new PdfPCell();
            Paragraph ps15 = new Paragraph("8.", redFont);
            cs15.addElement(ps15);
            cs15.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs15);

            PdfPCell cs16 = new PdfPCell();
            Paragraph ps16 = new Paragraph("The goods, plant and machinery and each and every one of them shall be kept in possession of the Borrower at the said premises or godowns or at such other address as the Bank may by writing authorize free from distress, execution or other legal process, whatsoever and the Borrower shall on no occasion resell, assign or deal with nor shall the Borrower part with possession thereof or any portion thereof.\n", redFont);
            ps16.setAlignment(Element.ALIGN_JUSTIFIED);
            cs16.addElement(ps16);
            cs16.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs16);

            document.add(tablekk2);
            
            document.newPage();
            PdfPTable tablekk3 = new PdfPTable(2);
            tablekk3.setWidthPercentage(100f);
            tablekk3.setWidths(new float[]{0.7f, 15});
            tablekk3.setHorizontalAlignment(Element.ALIGN_LEFT);
            
             PdfPCell c17 = new PdfPCell();
            Paragraph p17 = new Paragraph("9.", redFont);
            c17.addElement(p17);
            c17.setBorder(PdfPCell.NO_BORDER);
            tablekk3.addCell(c17);

            PdfPCell c18 = new PdfPCell();
            Paragraph p18 = new Paragraph("All rents, rates, taxes, impositions and other outgoings payable by the Borrower in respect of the said premises or godowns where the goods, plant and machinery may be kept/located shall be duly and punctually paid and discharged by the Borrower and the current receipts thereof shall be produced to the Bank by the Borrower on demand.\n", redFont);
            p18.setAlignment(Element.ALIGN_JUSTIFIED);
            c18.addElement(p18);
            c18.setBorder(PdfPCell.NO_BORDER);
            tablekk3.addCell(c18);
                    
            PdfPCell cs19 = new PdfPCell();
            Paragraph ps19 = new Paragraph("10.", redFont);
            cs19.addElement(ps19);
            cs19.setBorder(PdfPCell.NO_BORDER);
            tablekk3.addCell(cs19);

            PdfPCell cs20 = new PdfPCell();
            Paragraph ps20 = new Paragraph("The goods, plant and machinery shall be used for the purpose of the Borrower�s business of and for no other purpose whatsoever save with the previous consent of the Bank in writing.\n", redFont);
            ps20.setAlignment(Element.ALIGN_JUSTIFIED);
            cs20.addElement(ps20);
            cs20.setBorder(PdfPCell.NO_BORDER);
            tablekk3.addCell(cs20);

            PdfPCell cs21 = new PdfPCell();
            Paragraph ps21 = new Paragraph("11.", redFont);
            cs21.addElement(ps21);
            cs21.setBorder(PdfPCell.NO_BORDER);
            tablekk3.addCell(cs21);

            PdfPCell cs22 = new PdfPCell();
            Paragraph ps22 = new Paragraph("The Borrower hereby expressly warrants that the said premises or godowns in which the goods, plant and machinery are to be located are free from any mortgage encumbrances lis_pendense other charge whatsoever except to the Bank.\n", redFont);
            ps22.setAlignment(Element.ALIGN_JUSTIFIED);
            cs22.addElement(ps22);
            cs22.setBorder(PdfPCell.NO_BORDER);
            tablekk3.addCell(cs22);

            //Page-8 start ##########################################################
            PdfPCell cs23 = new PdfPCell();
            Paragraph ps23 = new Paragraph("12.", redFont);
            cs23.addElement(ps23);
            cs23.setBorder(PdfPCell.NO_BORDER);
            tablekk3.addCell(cs23);

            PdfPCell cs24 = new PdfPCell();
            Paragraph ps24 = new Paragraph("That the Bank its Agents and Nominees shall be entitled at all times as if absolute owners and without notice to the Borrower but at the Borrower�s risk and expenses and if so required as Agent for and in the name of the Borrower to enter any place where the said goods/plant & machineries may be and inspect value insure and/or take charge and/or possession of all or any part of the said goods/ plant & machineries and/or affix Bank�s Sign Board at its discretion at such place or places as deemed fit by the Bank and on any default of the Borrower in payment of any money hereby secured or the performance of any obligation to the Bank hereunder or the occurrence of any circumstances in the opinion of the Bank endangering this security to recover receive appoint receivers or remove and/or sell by public auction or private contract or otherwise to take possession dispose of or deal with all or any part of the said goods/plant & machineries and to enforce, realize, settle, compromise and deal with any rights aforesaid without being bound to exercise any of these powers or liable for any losses in the exercise thereof and without prejudice to the Bank�s rights and remedies of suit or otherwise and notwithstanding there may be any pending suit or other proceeding the Borrower undertaking to transfer and deliver to the Bank all relative contracts, securities, bazaar chits, bills, notes, hundies and documents and agreeing to accept the bank�s accounts of sale (including the costs thereof) and realization and to pay any shortfall or deficiency thereby shown. Provided that subject to these powers of the Bank and so long as the same is not exercised the Borrower may in the ordinary course of business sell and realize any such goods on the express terms of payment or delivery to the Bank of the proceeds or documents thereof immediately on receipt thereof. If any plant and machinery or any portion thereof be lost or damaged all moneys received or receivable in respect of such Insurance shall be received by the Bank which may apply the same in making good the damage done or in replacing the said plant and machinery by other articles of similar description and such substituted articles shall become subject to the provisions of these presents in the same manner as the plant and machinery for which they have been substituted. The Bank may insure if it so desires apply the money in whole or in part in payment of the amounts payable by the Borrower to the Bank whether it has fallen due or not.\n", redFont);
            ps24.setAlignment(Element.ALIGN_JUSTIFIED);
            cs24.addElement(ps24);
            cs24.setBorder(PdfPCell.NO_BORDER);
            tablekk3.addCell(cs24);

            PdfPCell cs25 = new PdfPCell();
            Paragraph ps25 = new Paragraph("13.", redFont);
            cs25.addElement(ps25);
            cs25.setBorder(PdfPCell.NO_BORDER);
            tablekk3.addCell(cs25);

            PdfPCell cs26 = new PdfPCell();
            Paragraph ps26 = new Paragraph("That all the said goods/plant & machineries and all sales realizations and insurance proceeds thereof and all documents under this security shall be held as the Bank�s exclusive property specifically appropriated to this security and the Borrower will not create or suffer any mortgage, charge, lien or encumbrance affecting the same or any part thereof nor do or allow anything that may prejudice this security.\n", redFont);
            ps26.setAlignment(Element.ALIGN_JUSTIFIED);
            cs26.addElement(ps26);
            cs26.setBorder(PdfPCell.NO_BORDER);
            tablekk3.addCell(cs26);
            document.add(tablekk3);
            document.newPage();
            PdfPTable tablekk4 = new PdfPTable(2);
            tablekk4.setWidthPercentage(100f);
            tablekk4.setWidths(new float[]{0.7f, 15});
            tablekk4.setHorizontalAlignment(Element.ALIGN_LEFT);
            PdfPCell cs27 = new PdfPCell();
            Paragraph ps27 = new Paragraph("14.", redFont);
            cs27.addElement(ps27);
            cs27.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(cs27);

            PdfPCell cs28 = new PdfPCell();
            Paragraph ps28 = new Paragraph("That the Borrower will furnish to the Bank a statement at the end of each month or for such other period as may be advised by the Bank to the Borrower giving details of the said goods as also the cost and market value thereof and if so required by the Bank the price or manufacturing cost of the said goods and verify all statements, reports, returns, certificates and information execute all documents and do all thing which Bank may require to give effect hereto and the Borrower authorizes the Bank its Agents and nominees as Attorney for and in the name of the Borrower to do whatever the Borrower may be required to do hereunder.\n", redFont);
            ps28.setAlignment(Element.ALIGN_JUSTIFIED);
            cs28.addElement(ps28);
            cs28.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(cs28);

            PdfPCell cs29 = new PdfPCell();
            Paragraph ps29 = new Paragraph("15.", redFont);
            cs29.addElement(ps29);
            cs29.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(cs29);

            PdfPCell cs30 = new PdfPCell();
            Paragraph ps30 = new Paragraph("That the security will continue notwithstanding the existence of a credit balance on any account at any time or any partial payments or any fluctuation of account.\n", redFont);
            ps30.setAlignment(Element.ALIGN_JUSTIFIED);
            cs30.addElement(ps30);
            cs30.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(cs30);

            PdfPCell cs31 = new PdfPCell();
            Paragraph ps31 = new Paragraph("16.", redFont);
            cs31.addElement(ps31);
            cs31.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(cs31);

            PdfPCell cs32 = new PdfPCell();
            Paragraph ps32 = new Paragraph("That nothing herein shall operate to prejudice the Bank�s rights or remedies in respect of any present or future security guarantee, obligation or decree for any indebtedness or liability of the Borrower to the Bank.\n", redFont);
            ps32.setAlignment(Element.ALIGN_JUSTIFIED);
            cs32.addElement(ps32);
            cs32.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(cs32);

            PdfPCell cs33 = new PdfPCell();
            Paragraph ps33 = new Paragraph("17.", redFont);
            cs33.addElement(ps33);
            cs33.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(cs33);

            PdfPCell cs34 = new PdfPCell();
            Paragraph ps34 = new Paragraph("That it is declared that all the said goods and all present debts and assets aforesaid are the absolute property of the Borrower at the sole disposal of the Borrower and that the same is free from any prior charge or encumbrance and that all future goods/debts & assets hereunder shall be likewise the unencumbered disposable property of the Borrower. The borrower may in the ordinary course of business deal with the said assets on the express understanding that the same and all proceeds thereof are at all times held for the Bank as its exclusive property specifically and identifiably appropriated to the security and that the Borrower shall not create or suffer any mortgage charge lien or encumbrance to affect the same or any part thereof nor do or allow anything that may prejudice this security.\n", redFont);
            ps34.setAlignment(Element.ALIGN_JUSTIFIED);
            cs34.addElement(ps34);
            cs34.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(cs34);

            //document.add(tablekk1);
            //document.newPage();
            //from 9 to 14
            //PdfPTable tablekk1 = new PdfPTable(2);
            // table.setTotalWidth(288);
            //tablekk1.setWidthPercentage(100f);
            //tablekk1.setWidths(new float[]{1, 15});
            //tablekk1.setHorizontalAlignment(Element.ALIGN_LEFT);
            PdfPCell ck0 = new PdfPCell();
            Paragraph pk0 = new Paragraph(" ", redFont);
            ck0.addElement(pk0);
            ck0.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(ck0);

            PdfPCell ck00 = new PdfPCell();
            Paragraph pk00 = new Paragraph("Its exclusive property specifically and identifiably appropriated to the security and that the Borrower shall not create or suffer any mortgage charge lien or encumbrance to affect the same or any part thereof nor do or allow anything that may prejudice this security.", redFont);
            pk00.setAlignment(Element.ALIGN_JUSTIFIED);
            ck00.addElement(pk00);
            ck00.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(ck00);

            PdfPCell ck1 = new PdfPCell();
            Paragraph pk1 = new Paragraph("18.", redFont);
            ck1.addElement(pk1);
            ck1.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(ck1);

            PdfPCell ck2 = new PdfPCell();
            Paragraph pk2 = new Paragraph("That if the Borrower be more than one individual all shall be bound hereby jointly and "
                    + "severally and if the Borrower shall be a Firm, such Firm and all members from time to time "
                    + "thereof shall be bound hereby notwithstanding any changes in the constitution or style "
                    + "thereof and whether the firm shall consider of or be reduced to one individual.", redFont);
            pk2.setAlignment(Element.ALIGN_JUSTIFIED);
            ck2.addElement(pk2);
            ck2.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(ck2);

            PdfPCell ck3 = new PdfPCell();
            Paragraph pk3 = new Paragraph("19.", redFont);
            ck3.addElement(pk3);
            ck3.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(ck3);

            PdfPCell ck4 = new PdfPCell();
            Paragraph pk4 = new Paragraph("That if and whenever this security shall be held by the Bank for the Borrower�s liability for any third party obligations to the Bank shall be free without reference to the Borrower to deal with the principal debtor and any securities obligation or decrees and generally to act as if the Borrower were primarily liable and the provisions of the Bank's General Guarantee Form in use for the time being to the full extent applicable hereto shall be deemed incorporated herein.", redFont);
            pk4.setAlignment(Element.ALIGN_JUSTIFIED);
            ck4.addElement(pk4);
            ck4.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(ck4);

            PdfPCell ck5 = new PdfPCell();
            Paragraph pk5 = new Paragraph("20.", redFont);
            //pk5.setAlignment(Element.ALIGN_JUSTIFIED);
            ck5.addElement(pk5);
            ck5.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(ck5);

            PdfPCell ck6 = new PdfPCell();
            Paragraph pk6 = new Paragraph("The Borrower shall not so long as any money remains due and payable to the Bank under "
                    + "the provisions hereof create or purport or attempt or agree to create or concur in the creation "
                    + "of any mortgage or charge or encumbrance on the plant and machinery or on any part "
                    + "thereof/ goods/ debts & assets other than to the Bank.", redFont);
            pk6.setAlignment(Element.ALIGN_JUSTIFIED);
            ck6.addElement(pk6);
            ck6.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(ck6);

            PdfPCell ck7 = new PdfPCell();
            Paragraph pk7 = new Paragraph("21.", redFont);
            ck7.addElement(pk7);
            ck7.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(ck7);

            PdfPCell ck8 = new PdfPCell();
            Paragraph pk8 = new Paragraph("The Borrower hereby appoints the Bank and its agents to be the attorneys jointly and severally "
                    + "of the Borrower and in the name and on behalf of the Borrower to execute and do such "
                    + "documents and things as the Borrower may be required to execute and do hereunder.", redFont);
            pk8.setAlignment(Element.ALIGN_JUSTIFIED);
            ck8.addElement(pk8);
            ck8.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(ck8);
            document.add(tablekk4);
            document.newPage();
            PdfPTable tablekk5 = new PdfPTable(2);
            tablekk5.setWidthPercentage(100f);
            tablekk5.setWidths(new float[]{0.7f, 15});
            tablekk5.setHorizontalAlignment(Element.ALIGN_LEFT);
            PdfPCell ck9 = new PdfPCell();
            Paragraph pk9 = new Paragraph("22.", redFont);
            ck9.addElement(pk9);
            ck9.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(ck9);

            PdfPCell ck10 = new PdfPCell();
            Paragraph pk10 = new Paragraph("The Borrower hereby expressly warrants that the said premises or godowns in which the plant "
                    + "and machinery are to be located are free from any mortgage encumbrances lispendens other "
                    + "charge whatsoever except to the Bank.", redFont);
            pk10.setAlignment(Element.ALIGN_JUSTIFIED);
            ck10.addElement(pk10);
            ck10.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(ck10);

            PdfPCell ck11 = new PdfPCell();
            Paragraph pk11 = new Paragraph("23.", redFont);
            ck11.addElement(pk11);
            ck11.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(ck11);

            PdfPCell ck12 = new PdfPCell();
            Paragraph pk12 = new Paragraph("It is agreed and declared by and between the Borrower and the Bank as follows:", redFont);
            pk12.setAlignment(Element.ALIGN_JUSTIFIED);
            ck12.addElement(pk12);
            ck12.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(ck12);

            PdfPCell ck13 = new PdfPCell();
            Paragraph pk13 = new Paragraph("a)", redFont);
            ck13.addElement(pk13);
            ck13.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(ck13);

            PdfPCell ck14 = new PdfPCell();
            Paragraph pk14 = new Paragraph("The Borrower hereby declares that the Borrower is the absolute and sole beneficial owner of "
                    + "and is seized and possessed of or otherwise entitled to the goods/ plant and machinery/ debt "
                    + "& assets and that the same are and shall be free from all encumbrances, liens, attachments, "
                    + "mortgages, charges, lispendens or otherwise whatsoever.", redFont);
            pk14.setAlignment(Element.ALIGN_JUSTIFIED);
            ck14.addElement(pk14);
            ck14.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(ck14);

            PdfPCell ck15 = new PdfPCell();
            Paragraph pk15 = new Paragraph("b)", redFont);
            ck15.addElement(pk15);
            ck15.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(ck15);

            PdfPCell ck16 = new PdfPCell();
            Paragraph pk16 = new Paragraph("The Borrower shall permit the Bank and its servants, agents and all persons authorized by "
                    + "the Bank either alone or with workmen and others from time to time and at all reasonable "
                    + "times to enter upon the said premises or godowns or elsewhere and/ or inspect the "
                    + "goods/ plant and machinery and if upon such inspection it appears to the Bank that the "
                    + "plant and machinery or any of them or any part thereof requires repairs or replacements the "
                    + "Bank shall give notice thereof to the Borrower calling upon the Borrower to repair or replace "
                    + "the same.", redFont);
            pk16.setAlignment(Element.ALIGN_JUSTIFIED);
            ck16.addElement(pk16);
            ck16.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(ck16);

            PdfPCell ck17 = new PdfPCell();
            Paragraph pk17 = new Paragraph("c)", redFont);
            ck17.addElement(pk17);
            ck17.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(ck17);

            PdfPCell ck18 = new PdfPCell();
            Paragraph pk18 = new Paragraph("If default shall be made by the Borrower in keeping the goods/plant and machinery in good "
                    + "condition and serviceable order and as aforesaid or so insured as aforesaid or in repairing or "
                    + "replacing the plant and machinery as aforesaid it shall be lawful for the Bank but not "
                    + "obligatory on the Bank to keep the goods/ plant and machinery in good condition and "
                    + "serviceable order and condition and insured and to replace or repair the same and all sums "
                    + "expended by the Bank for the above purposes or any of them together with interest thereon "
                    + "at the rate aforesaid shall until repayment be a charge on the goods/plant and machinery "
                    + "and form part of the debt hereby secured and carry interest at the rate aforesaid.", redFont);
            pk18.setAlignment(Element.ALIGN_JUSTIFIED);
            ck18.addElement(pk18);
            ck18.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(ck18);

            PdfPCell ck19 = new PdfPCell();
            Paragraph pk19 = new Paragraph("d)", redFont);
            ck19.addElement(pk19);
            ck19.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(ck19);

            PdfPCell ck20 = new PdfPCell();
            Paragraph pk20 = new Paragraph("The Borrower shall not without written consent of the Bank first had and obtained remove "
                    + "the plant and machinery or any part thereof from the place where they are now at present "
                    + "and in case of such removal shall replace the same by equipment of equivalent nature and "
                    + "value PROVIDED THAT in the event of the Bank agreeing that any such part of the "
                    + "equipment so removed as aforesaid is redundant or has become worn out or obsolete and "
                    + "need not be replaced the same may be sold and the sale proceeds applied towards the "
                    + "satisfaction or payment of the Bank�s dues hereunder.", redFont);
            pk20.setAlignment(Element.ALIGN_JUSTIFIED);
            ck20.addElement(pk20);
            ck20.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(ck20);

            PdfPCell ck21 = new PdfPCell();
            Paragraph pk21 = new Paragraph("e)", redFont);
            ck21.addElement(pk21);
            ck21.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(ck21);

            PdfPCell ck22 = new PdfPCell();
            Paragraph pk22 = new Paragraph("In the event of the goods/ plant and machinery depreciating in the value to such an extent that in the opinion of the Bank further security to its satisfaction should be given the "
                    + "Borrower shall from time to time and at all times at its own cost furnish such security as "
                    + "may be considered reasonable and adequate by the Bank provided that the Bank�s decision "
                    + "on the nature and extent of the depreciation shall be final and conclusively binding on the "
                    + "Borrower.", redFont);
            pk22.setAlignment(Element.ALIGN_JUSTIFIED);
            ck22.addElement(pk22);
            ck22.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(ck22);
            document.add(tablekk5);
            document.newPage();
            PdfPTable tablekk6 = new PdfPTable(2);
            tablekk6.setWidthPercentage(100f);
            tablekk6.setWidths(new float[]{0.7f, 15});
            tablekk6.setHorizontalAlignment(Element.ALIGN_LEFT);
            
            PdfPCell ck23 = new PdfPCell();
            Paragraph pk23 = new Paragraph("f)", redFont);
            ck23.addElement(pk23);
            ck23.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(ck23);

            PdfPCell ck24 = new PdfPCell();
            Paragraph pk24 = new Paragraph("On any default of the Borrower in payment of any money hereby secured or the "
                    + "performance of any obligation to the Bank hereunder or the occurrence of any circumstances "
                    + "in the opinion of the Bank endangering this security the Bank shall be entitled at the "
                    + "Borrower�s risk and expenses as attorney for and in the name of the Borrower or otherwise to "
                    + "take possession to recover receive appoint receivers of any debts or assets under this "
                    + "security give notices and reminds to debtors and third parties liable therefor use for recover, "
                    + "receive and give receipts for the same and sell or realize by public auction or private contract "
                    + "or otherwise of all or any part of such goods/ plant and machinery debts or assets and to "
                    + "enforce, realize settle, compromise submit to arbitration or and deal with any rights aforesaid "
                    + "without being bound to exercise any of these powers or liable for any losses in the exercise "
                    + "thereof and without prejudice to the Bank�s rights and remedies of suit or otherwise and "
                    + "notwithstanding there may be any pending suit or other proceeding the Borrower "
                    + "undertaking to transfer and deliver to the Bank all relative documents, papers, contracts, "
                    + "securities, bazaar chits bills, notes, hundies and documents and agreeing to accept the "
                    + "Bank�s accounts of sale (including the costs thereof) and realization and to pay any short fall "
                    + "or deficiency thereby shown", redFont);
            pk24.setAlignment(Element.ALIGN_JUSTIFIED);
            ck24.addElement(pk24);
            ck24.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(ck24);

            PdfPCell ck25 = new PdfPCell();
            Paragraph pk25 = new Paragraph("g)", redFont);
            ck25.addElement(pk25);
            ck25.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(ck25);

            PdfPCell ck26 = new PdfPCell();
            Paragraph pk26 = new Paragraph("Upon any such sale as aforesaid the receipt of the Bank for the purchase money shall "
                    + "effectually discharge the purchaser or purchasers there from and from being concerned to "
                    + "see to the application thereof or being answerable for the loss or misapplication thereof.", redFont);
            pk26.setAlignment(Element.ALIGN_JUSTIFIED);
            ck26.addElement(pk26);
            ck26.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(ck26);

            PdfPCell ck27 = new PdfPCell();
            Paragraph pk27 = new Paragraph("h)", redFont);
            ck27.addElement(pk27);
            ck27.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(ck27);

            PdfPCell ck28 = new PdfPCell();
            Paragraph pk28 = new Paragraph("The Bank or any Receiver appointed by the Bank shall not be answerable or accountable "
                    + "for any involuntary losses which may happen in or about the exercise or execution of any of "
                    + "powers which may be vested in the Bank or the Receiver by virtue of these present.", redFont);
            pk28.setAlignment(Element.ALIGN_JUSTIFIED);
            ck28.addElement(pk28);
            ck28.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(ck28);

            PdfPCell ck29 = new PdfPCell();
            Paragraph pk29 = new Paragraph("i)", redFont);
            ck29.addElement(pk29);
            ck29.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(ck29);

            PdfPCell ck30 = new PdfPCell();
            Paragraph pk30 = new Paragraph("The Borrower shall pay all costs charges and expenses as between attorney and client in "
                    + "anywise incurred or paid by the Bank incidental to or in connection with these presents or "
                    + "this security including expenses incurred by the Bank or the Receiver or the servants and "
                    + "agents of the Bank and/ or the Receiver towards inspections of the goods/plant and "
                    + "machinery/book debts & assets and incurred as well for the assertion or defence of the "
                    + "rights of the Bank as for the protection of the security of the plant and machinery and for the "
                    + "demand realization recovery of the said principal sum interest and other money payable to "
                    + "the Bank and the same shall on demand be paid by the Borrower to the Bank with interest "
                    + "thereon at the rate aforesaid from the time of the same having been so incurred and until "
                    + "such payment the same shall be a charge upon the goods/plant and machinery/book debts "
                    + "& assets.", redFont);
            pk30.setAlignment(Element.ALIGN_JUSTIFIED);
            ck30.addElement(pk30);
            ck30.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(ck30);

            PdfPCell ck31 = new PdfPCell();
            Paragraph pk31 = new Paragraph("j)", redFont);
            ck31.addElement(pk31);
            ck31.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(ck31);

            PdfPCell ck32 = new PdfPCell();
            Paragraph pk32 = new Paragraph("The properties comprised in the said recited Agreement shall be discharged from all "
                    + "rights of release and shall be released by the Bank only on payment by the Borrower to the "
                    + "Bank of the principal amount interest and other money secured by the said recited "
                    + "Agreement or by these presents. ", redFont);
            pk32.setAlignment(Element.ALIGN_JUSTIFIED);
            ck32.addElement(pk32);
            ck32.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(ck32);

            PdfPCell ck33 = new PdfPCell();
            Paragraph pk33 = new Paragraph("k)", redFont);
            ck33.addElement(pk33);
            ck33.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(ck33);

            PdfPCell ck34 = new PdfPCell();
            Paragraph pk34 = new Paragraph("Nothing contained in these presents or in any enactment shall affect right of the Bank to "
                    + "enforce separately payment of the principal amount interest and other money secured by the "
                    + "said recited Agreement.", redFont);
            pk34.setAlignment(Element.ALIGN_JUSTIFIED);
            ck34.addElement(pk34);
            ck34.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(ck34);

            PdfPCell ck35 = new PdfPCell();
            Paragraph pk35 = new Paragraph("l)", redFont);
            ck35.addElement(pk35);
            ck35.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(ck35);

            PdfPCell ck36 = new PdfPCell();
            Paragraph pk36 = new Paragraph("Any notice hereunder may be given by the Bank to the Borrower by leaving the same at "
                    + "the last known place of business of the Borrower in India or by sending the same by post to "
                    + "the Borrower addressed as aforesaid and if sent by post the notice shall be deemed to have "
                    + "been given at the time ", redFont);
            pk36.setAlignment(Element.ALIGN_JUSTIFIED);
            ck36.addElement(pk36);
            ck36.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(ck36);
            document.add(tablekk6);
            document.newPage();
            PdfPTable tablekk7 = new PdfPTable(2);
            tablekk7.setWidthPercentage(100f);
            tablekk7.setWidths(new float[]{0.7f, 15});
            tablekk7.setHorizontalAlignment(Element.ALIGN_LEFT);
            
            PdfPCell c35 = new PdfPCell();
            Paragraph p35 = new Paragraph("  ", redFont);
            c35.addElement(p35);
            c35.setBorder(PdfPCell.NO_BORDER);
            tablekk7.addCell(c35);

            PdfPCell c36 = new PdfPCell();
            Paragraph p36 = new Paragraph("when it would be delivered in due course of post and in proving such "
                    + "notice when given by post it shall be sufficient to prove that the envelope containing the "
                    + "notice was posted and a certificate signed by any officer of the Bank that the envelope was so "
                    + "posted will be conclusive.", redFont);
            p36.setAlignment(Element.ALIGN_JUSTIFIED);
            c36.addElement(p36);
            c36.setBorder(PdfPCell.NO_BORDER);
            tablekk7.addCell(c36);
            
            
            PdfPCell ck37 = new PdfPCell();
            Paragraph pk37 = new Paragraph("m)", redFont);
            ck37.addElement(pk37);
            ck37.setBorder(PdfPCell.NO_BORDER);
            tablekk7.addCell(ck37);

            PdfPCell ck38 = new PdfPCell();
            Paragraph pk38 = new Paragraph("The Borrower hereby agrees that if any default is committed in the repayment of the loan "
                    + "and advances or in the repayment of interest thereon or any of the agreed installment of the "
                    + "loan on due date the bank and/ or the Reserve Bank of India will have an unqualified right to "
                    + "directors/partners/proprietors as defaulter in such manner and through such medium as "
                    + "the Bank or Reserve Bank of India in their absolute discretion may think fit.", redFont);
            pk38.setAlignment(Element.ALIGN_JUSTIFIED);
            ck38.addElement(pk38);
            ck38.setBorder(PdfPCell.NO_BORDER);
            tablekk7.addCell(ck38);

            PdfPCell ck39 = new PdfPCell();
            Paragraph pk39 = new Paragraph("n)", redFont);
            ck39.addElement(pk39);
            ck39.setBorder(PdfPCell.NO_BORDER);
            tablekk7.addCell(ck39);

            PdfPCell ck40 = new PdfPCell();
            Paragraph pk40 = new Paragraph("These presents shall remain in full force and effect so long as all the sums under the said "
                    + "credit facility including interests and other money due and payable hereunder and under "
                    + "the said facility agreement are not fully paid.\n ", redFont);
            pk40.setAlignment(Element.ALIGN_JUSTIFIED);
            ck40.addElement(pk40);
            ck40.setBorder(PdfPCell.NO_BORDER);
            tablekk7.addCell(ck40);

            PdfPCell ck41 = new PdfPCell();
            Paragraph pk41 = new Paragraph("o)", redFont);
            ck41.addElement(pk41);
            ck41.setBorder(PdfPCell.NO_BORDER);
            tablekk7.addCell(ck41);

            PdfPCell ck42 = new PdfPCell();
            Paragraph pk42 = new Paragraph("Nothing herein contained shall prejudice or affect any general or special lien to which the "
                    + "bank are or may be law or otherwise be entitled to or any rights or remedies of the bank in "
                    + "respect of any present or future security, guarantee, obligation or decree for any other "
                    + "indebtedness or liability of the borrower to the bank or shall preclude the bank from "
                    + "enforcing or having recourse to the security under these presents without enforcing or "
                    + "having recourse in the first instance to any other security held by the bank from the "
                    + "borrower and the bank shall be entitled to sue on any such securities without being bound "
                    + "to sue on all such securities. ", redFont);
            pk42.setAlignment(Element.ALIGN_JUSTIFIED);
            ck42.addElement(pk42);
            ck42.setBorder(PdfPCell.NO_BORDER);
            tablekk7.addCell(ck42);

            PdfPCell ck43 = new PdfPCell();
            Paragraph pk43 = new Paragraph("p)", redFont);
            ck43.addElement(pk43);
            ck43.setBorder(PdfPCell.NO_BORDER);
            tablekk7.addCell(ck43);

            PdfPCell ck44 = new PdfPCell();
            Paragraph pk44 = new Paragraph("This security shall not be prejudiced by any collateral or other security now or hereafter "
                    + "held or to the held by the bank for any money hereby secured or by any release, exchange or "
                    + "variation of the security, and "
                    + "the bank may give time for payment to or make "
                    + "any other arrangement with any surety or cosignatory without prejudice to the borrower�s "
                    + "liability hereunder and all money received by the bank from the borrower or any person "
                    + "liable to pay the same may be applied by the bank to may account to which the same may be "
                    + "applicable.\n", redFont);
            pk44.setAlignment(Element.ALIGN_JUSTIFIED);
            ck44.addElement(pk44);
            ck44.setBorder(PdfPCell.NO_BORDER);
            tablekk7.addCell(ck44);

            document.add(tablekk7);
            document.add(new Paragraph("\n"));
//                        document.add(new Paragraph("\n"));
//                        document.add(new Paragraph("\n"));

            Paragraph pkk24 = new Paragraph("THE SCHEDULE ABOVE REFERRED TO: ", canFont1);
            pkk24.setAlignment(Element.ALIGN_LEFT);
            pkk24.setSpacingAfter(15);
            document.add(pkk24);

//             document.add(new Paragraph("\n"));
//             document.add(new Paragraph("\n"));
            Paragraph pkk25 = new Paragraph("Dated this              day of                 20", redFont);
            pkk25.setAlignment(Element.ALIGN_LEFT);
            pkk25.setSpacingAfter(15);
            document.add(pkk25);

//             document.add(new Paragraph("\n"));
//             document.add(new Paragraph("\n"));
// String applicantName = (loanData.getAppl1Name() != null && loanData.getAppl1Name() != "" && !loanData.getAppl1Name().equals("null") ? loanData.getAppl1Name() : " ");
            Paragraph pkk26 = new Paragraph(applicantName + "\n" + "Signature of the borrower", redFont);
            pkk26.setAlignment(Element.ALIGN_RIGHT);
            pkk26.setSpacingAfter(15);
            document.add(pkk26);

            //Page-2 start##################################################
        } catch (FileNotFoundException fnf) {
            logger.error("Error:.....!!! " + fnf.getMessage());
        } catch (DocumentException de) {
            logger.error("Error:.....!!! " + de.getMessage());
        } catch (IOException ex) {
            logger.error("Error:.....!!! " + ex.getMessage());
        }
        document.close();

        logger.debug("***********Ended createPdf()***********");
    }

    public Chunk underLine(String data) {
        Paragraph pp9 = new Paragraph();
        pp9.setAlignment(Element.ALIGN_JUSTIFIED);
        Chunk underline1 = new Chunk(data, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL));
        underline1.setUnderline(0.1f, -2f);
        pp9.add(underline1);
        return underline1;
    }

    public String getIndianCurrencyFormat(String amount) {
        StringBuilder stringBuilder = new StringBuilder();
        char amountArray[] = amount.toCharArray();
        int a = 0, b = 0;
        for (int i = amountArray.length - 1; i >= 0; i--) {
            if (a < 3) {
                stringBuilder.append(amountArray[i]);
                a++;
            } else if (b < 2) {
                if (b == 0) {
                    stringBuilder.append(",");
                    stringBuilder.append(amountArray[i]);
                    b++;
                } else {
                    stringBuilder.append(amountArray[i]);
                    b = 0;
                }
            }
        }
        return stringBuilder.reverse().toString();
    }
}
