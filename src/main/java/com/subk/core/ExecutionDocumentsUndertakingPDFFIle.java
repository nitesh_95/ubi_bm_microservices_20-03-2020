/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.core;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.subk.entity.Loanorigination;
import com.subk.util.AddressCodes;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 *
 * @author Ramya
 */
@Configuration
public class ExecutionDocumentsUndertakingPDFFIle extends PDFGenerationAbstract {
	
	@Autowired
	AddressCodes addresscodes;
	
	@Value("${MCLRL}")
	private String mclrl; 

    private String empty = " ";
    private static final Logger logger = Logger.getLogger(GenerateUndertakingPDFFile.class);
//    private static final ResourceBundle rb = ResourceBundle.getBundle("systemPath");
//    private static final ResourceBundle rb1 = ResourceBundle.getBundle("com.subk.Resources//subkResources");
    Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

    public String getPdf(Loanorigination loanData, String fileName,String pathAdd, String img, String jsPaths) {
        logger.info("************ ENTERED ************* ");

        String destPath = null;

        logger.debug("UBILoanOrigination Data: " + loanData.toString());
        logger.debug("FileName: " + fileName);

        try {
            destPath = pathAdd;
            destPath = destPath + fileName;

            //File file = new File(destPath);
            //file.getParentFile().mkdirs();
            new ExecutionDocumentsUndertakingPDFFIle().createPdf(destPath, loanData, img, jsPaths);
        } catch (Exception ex) {
            logger.error("Error:....!!! " + ex.getMessage());
        }

        logger.info("destPathINATION PATH:...........!!!!!!!!!!  " + destPath);
        logger.info("************ ENDED.************* ");
        return destPath;
    }

    public void createPdf(String dest, Loanorigination loanData, String img, String jsPaths) {
        logger.debug("***********Entered***********");
        final String imgPath = img;
     final String mclrVal = mclrl;
         String pre= "";
     	BigDecimal mclrMF = new BigDecimal("0.0");

		String gender = loanData.getSex();
		if (gender.equals("MALE")) {
			if (loanData.getMclrMaleFemale() != null) {
				Double dd = new Double(loanData.getMclrMaleFemale());
				mclrMF = BigDecimal.valueOf(dd);
			}

		} else if (gender.equals("FEMALE")) {
			if (loanData.getMclrMaleFemale() != null) {
				Double dd = new Double(loanData.getMclrMaleFemale());
				mclrMF = BigDecimal.valueOf(dd);
			}
		}
         if (gender.equals("MALE")) {
            pre="Mr";
        } else if (gender.equals("FEMALE")) {
            pre="Ms";
        }


        Document document = null;
        PdfWriter writer = null;

        try {
            String appl1Address = (loanData.getAppl1address() != null && loanData.getAppl1address() != "" && !loanData.getAppl1address().equals("null") ? loanData.getAppl1address() : " ");
            document = new Document();
            writer = PdfWriter.getInstance(document, new FileOutputStream(dest, false));
            //writer.setPageEvent(new Header());
            HeaderFooterPageEvent event = new HeaderFooterPageEvent();
            writer.setPageEvent(event);
            document.open();

            Font canFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
            Font canFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
            Font redFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);
            //File file = new File("C:\\"+loanData.getAppl1Name()+"_"+loanData.getApplicationId());
            // Get the output stream for writing PDF object
            //ServletOutputStream out = response.getOutputStream();
            //OutputStream oos = new FileOutputStream(file,true);
            //Document document = new Document();
            //PdfWriter writer = PdfWriter.getInstance(document, out);
//            document.open();
            document.addKeywords("Java, Servelet, PDF, iText");
            document.addAuthor("Raj Grover");
            document.addCreator("UBILoanOrigination");

            Image img2 = Image.getInstance(imgPath);
            img2.setAbsolutePosition(250f, 760f);
            img2.scaleAbsolute(80f, 80f);
            document.add(img2);
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("\n"));
//            document.add(new Paragraph("\n"));
            Paragraph sp11 = new Paragraph("Declaration on execution of documents\n", canFont);
            sp11.setAlignment(Element.ALIGN_CENTER);
            document.add(sp11);
            document.add(new Paragraph("\n"));
            PdfPTable table1 = new PdfPTable(2);
            table1.setWidthPercentage(100f);
            table1.setWidths(new float[]{3, 1});
            table1.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
            table1.addCell(getCell("Place: ....................", PdfPCell.ALIGN_LEFT));
            table1.addCell(getCell("Manager", PdfPCell.ALIGN_LEFT));
            table1.addCell(getCell("Date: .....................", PdfPCell.ALIGN_LEFT));
            table1.addCell(getCell("United Bank of India", PdfPCell.ALIGN_LEFT));
            table1.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
            table1.addCell(getCell(loanData.getBranchname() +" Branch", PdfPCell.ALIGN_LEFT));
            table1.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
            table1.setSpacingAfter(6f);
            document.add(table1);

//            Paragraph sp12 = new Paragraph("Place: ....................\n"
//                    + "Date: .....................\n\n"
//                    + "Manager\n"
//                    + "United Bank of India\n" + loanData.getBranchName() + " Branch\n\n", redFont);
//
//            document.add(sp12);
            String applicantName = (loanData.getAppl1name() != null && loanData.getAppl1name() != "" && !loanData.getAppl1name().equals("null") ? loanData.getAppl1name() : " ");
            String applicantFhgName = (loanData.getFhgname() != null && loanData.getFhgname() != "" && !loanData.getFhgname().equals("null") ? loanData.getFhgname() : " ");
//            String applicantAddress = (loanData.getAppl1address() != null && loanData.getAppl1address() != "" && !loanData.getAppl1address().equals("null") ? loanData.getAppl1address() : " ");
            String permAddress = (loanData.getPermhouseno() != null && !loanData.getPermhouseno().equalsIgnoreCase("null") && loanData.getPermhouseno() != " " ? loanData.getPermhouseno()+ "," : " ");
            permAddress += (loanData.getPermstreetno() != null && !loanData.getPermstreetno().equalsIgnoreCase("null") && loanData.getPermstreetno() != " " ? loanData.getPermstreetno()+ "," : " ");
            permAddress += (loanData.getPermlandmark() != null && !loanData.getPermlandmark().equalsIgnoreCase("null") && loanData.getPermlandmark() != " " ? loanData.getPermlandmark()+ "," : " ");
            permAddress += (loanData.getPermvillage() != null && !loanData.getPermvillage().equalsIgnoreCase("null") && loanData.getPermvillage() != " " ? loanData.getPermvillage()+ "," : " ");
            permAddress += (loanData.getPermdistrict() != null && !loanData.getPermdistrict().equalsIgnoreCase("null") && loanData.getPermdistrict() != " " ? loanData.getPermdistrict()+ "," : " ");		
            permAddress += (loanData.getPermsubdistrict() != null && !loanData.getPermsubdistrict().equalsIgnoreCase("null") && loanData.getPermsubdistrict() != " " ? loanData.getPermsubdistrict()+ "," : " ");
            permAddress += (loanData.getPermstate() != null && !loanData.getPermstate().equalsIgnoreCase("null") && loanData.getPermstate() != " " ? loanData.getPermstate()+ "," : " ");
            permAddress += (loanData.getPermpincode() != null && !loanData.getPermpincode().equalsIgnoreCase("null") && loanData.getPermpincode() != " " ? loanData.getPermpincode()+ "," : " ");
            permAddress += (loanData.getPermtelephone() != null && !loanData.getPermtelephone().equalsIgnoreCase("null") && loanData.getPermtelephone() != " " ? loanData.getPermtelephone()+ "," : " ");
            
            Paragraph sp14 = new Paragraph("This is to place on record that I ", redFont);
            Chunk appl_name = new Chunk(applicantName, redFont1);
            appl_name.setUnderline(0.6f, -2f);
            sp14.add(appl_name);
            sp14.add(" S/o,W/o,D/o ");
            Chunk rel_name = new Chunk(applicantFhgName, redFont);
            rel_name.setUnderline(0.6f, -2f);
            sp14.add(rel_name);
            sp14.add(" residing at address ");
            Chunk addres = new Chunk(permAddress.toUpperCase(), redFont1);
            addres.setUnderline(0.6f, -2f);
            sp14.add(addres);
            sp14.add(new Chunk(" (Mention name of Father/Husband, registered office wherever applicable, Age, official/business address) "
                    + "have executed the under noted documents on __________________ being present in the branch premises of United Bank of India " + loanData.getBranchname() + " Branch / or at ______________________________"
                    + "(Mention the name of the place)"
                    + "________________________ being explained to me / us in connection with my/our _______________ Account.\n \nI authorize " +pre+"............................"
                    + "of .......................................................... (address) to fill in the blank spaces of documents executed by me/us and the same have been filled in under my/our instructions and I/we have understood fully the contents and purpose of the said documents and the necessity of execution of such documents.\n"
                    + "Documents executed\n"
                    + "1.\n"
                    + "2.\n"
                    + "3.\n"
                    + "4.\n"
                    + "5.\n"
                    + "6.\n"
                    + "7.\n"
                    + "8.\n"
                    + "9.\n"
                    + "10.\n"
                    + "\n"
                    + "Signature of witness\n"
                    + "Full Name: \n" + "Address:\n"
                    + "Date: ", redFont));
            sp14.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(sp14);

            Paragraph k1 = new Paragraph();
            k1.add(new Chunk("Signature of the executant\n", redFont));
            k1.add(new Chunk("Full Name: " + applicantName + "\n", redFont1));
            k1.add(new Chunk("Address: " + permAddress.toUpperCase() + "\n\n", redFont1));
            k1.setAlignment(Element.ALIGN_RIGHT);
            document.add(k1);

            Paragraph sp15 = new Paragraph("(Witness should be taken, for Illiterate person and person putting vernacular signature and the witness should invariably be an employee of the Bank)\n\n", canFont1);
            sp15.setAlignment(Element.ALIGN_LEFT);
            document.add(sp15);

            //Page-2 start##################################################
        } catch (FileNotFoundException fnf) {
            logger.error("Error:.....!!! " + fnf.getMessage());
        } catch (DocumentException de) {
            logger.error("Error:.....!!! " + de.getMessage());
        } catch (IOException ex) {
            logger.error("Error:.....!!! " + ex.getMessage());
        }
        document.close();

        logger.debug("***********Ended createPdf()***********");
    }

    public Chunk underLine(String data) {
        Paragraph pp9 = new Paragraph();
        pp9.setAlignment(Element.ALIGN_JUSTIFIED);
        Chunk underline1 = new Chunk(data, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL));
        underline1.setUnderline(0.1f, -2f);
        pp9.add(underline1);
        return underline1;
    }

    public PdfPCell getCell(String text, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text, redFont));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }

}
