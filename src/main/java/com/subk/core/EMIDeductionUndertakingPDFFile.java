/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.core;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;
import com.subk.entity.Loanorigination;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Ramya
 */
//1st Page in Undertaking form
@Configuration
public class EMIDeductionUndertakingPDFFile extends PDFGenerationAbstract {
	
    private String empty = " ";
    private static final Logger logger = Logger.getLogger(GenerateUndertakingPDFFile.class);
//    private static final ResourceBundle rb = ResourceBundle.getBundle("systemPath");
//    private static final ResourceBundle rb1 = ResourceBundle.getBundle("com.subk.Resources//subkResources");

    public String getPdf(Loanorigination loanData, String fileName,String pathAdd, String img, String jsPaths) {
        logger.info("************ ENTERED ************* ");

        String destPath = null;

        logger.debug("UBILoanOrigination Data: " + loanData.toString());
        logger.debug("FileName: " + fileName);

        try {
            destPath = pathAdd;
            destPath = destPath + fileName;

            //File file = new File(destPath);
            //file.getParentFile().mkdirs();
            new EMIDeductionUndertakingPDFFile().createPdf(destPath, loanData,img, jsPaths);
        } catch (Exception ex) {
            logger.error("Error:....!!! " + ex.getMessage());
        }

        logger.info("destPathINATION PATH:...........!!!!!!!!!!  " + destPath);
        logger.info("************ ENDED.************* ");
        return destPath;
    }

    public void createPdf(String dest, Loanorigination loanData , String img, String jsPaths) {
        logger.debug("***********Entered***********");
        final String imgPath = img;
        final String mclrVal = loanData.getMclr() != null && !loanData.getMclr().equals("") ? loanData.getMclr() : "";//rb1.getString("MCLRL");

        BigDecimal mclrMF = new BigDecimal("0.0");
//
//        String gender = loanData.getSex();
//        if (gender.equals("MALE")) {
//            mclrMF = loanData.getMclr_male_female()!=null&&!loanData.getMclr_male_female().equals("")?loanData.getMclr_male_female():"";
//        } else if (gender.equals("FEMALE")) {
//            mclrMF = loanData.getMclr_male_female()!=null&&!loanData.getMclr_male_female().equals("")?loanData.getMclr_male_female():"";
//        }

        Document document = null;
        PdfWriter writer = null;

        try {
            String appl1Address = (loanData.getAppl1address() != null && loanData.getAppl1address() != "" && !loanData.getAppl1address().equals("null") ? loanData.getAppl1address() : " ");
            document = new Document();
            writer = PdfWriter.getInstance(document, new FileOutputStream(dest, false));
            //writer.setPageEvent(new Header());
            HeaderFooterPageEvent event = new HeaderFooterPageEvent();
            writer.setPageEvent(event);
            document.open();

            Font canFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
            Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
            Font canFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);

           
            document.addKeywords("Java, Servelet, PDF, iText");
            document.addAuthor("Raj Grover");
            document.addCreator("UBILoanOrigination");
            Image img2 = Image.getInstance(imgPath);
            img2.setAbsolutePosition(250f, 740f);
            img2.scaleAbsolute(80f, 80f);
            document.add(img2);
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("\n"));
            Paragraph para1 = new Paragraph("", canFont);
            para1.setAlignment(Element.ALIGN_CENTER);
            Chunk head1 = new Chunk("Letter of undertaking to deduct monthly installment");
            head1.setUnderline(1, -2f);
            para1.add(head1);
            document.add(para1);
            document.add(new Paragraph("\n\n"));
            Paragraph para2 = new Paragraph("The Manager\n" + "United Bank of India", redFont);
            para2.setAlignment(Element.ALIGN_LEFT);
            document.add(para2);
            String branchname = loanData.getBranchname() != null && !loanData.getBranchname().equalsIgnoreCase("null") ? loanData.getBranchname() : "";
            Chunk br_name = new Chunk(branchname);
            br_name.setUnderline(0.6f, -2f);
            Paragraph para3 = new Paragraph("", redFont);
            para3.setAlignment(Element.ALIGN_LEFT);
            para3.add(br_name);
            para3.add(" Branch\n\n");
            para3.add("Dear Sir/Madam,");
            document.add(para3);
            document.add(new Paragraph("\n"));
            Paragraph para4 = new Paragraph("Re: Authority to deduct monthly installment.", canFont1);
            document.add(para4);
            document.add(new Paragraph("\n"));
            String instalmentAmt = (loanData.getInstalmentamount() != null && !loanData.getInstalmentamount().equals("null") ? loanData.getInstalmentamount() : " ");
            String commainstalmentAmt = getIndianCurrencyFormat(instalmentAmt);
            Paragraph para5 = new Paragraph("On the strength of this letter, I authorize you to deduct Rs.", redFont);
            para5.setAlignment(Element.ALIGN_JUSTIFIED);
            Chunk rupees = new Chunk(" " + commainstalmentAmt+"/-",canFont1);
            rupees.setUnderline(0.6f, -2f);
            para5.add(rupees);
            para5.add(" (Rupees ");
            Chunk rupees1 = new Chunk(" " + NumberToWord.convert(Long.parseLong(instalmentAmt)));
            rupees1.setUnderline(0.6f, -2f);
            para5.add(rupees1);
            para5.add(" only) every month from my (SB / CD) account number _____________________with your branch till the loan is fully liquidated.\n\n");
            document.add(para5);
            Paragraph para6 = new Paragraph("I do further authorize you to deduct EMI with higher amount in the event Bank�s MCLR-Y is revised upwards. I affirm that this authority would not be changed or revoked till the said loan account is fully liquidated.\n\n\n", redFont);
            para6.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(para6);
            Paragraph para7 = new Paragraph("Place: _________________\n"
                    + "Date:  _________________\n\n\n\n", redFont);
            para7.setAlignment(Element.ALIGN_LEFT);
            document.add(para7);

            Paragraph para8 = new Paragraph("Yours faithfully,\n\n\n", redFont);
            para8.setAlignment(Element.ALIGN_LEFT);
            document.add(para8);

            Paragraph para9 = new Paragraph(underLine(loanData.getAppl1name() != null && !loanData.getAppl1name().equals("null") ? loanData.getAppl1name() : " ") + "\n"
                    + "(Full signature of the borrower)\n\n\n\n", redFont);
            para9.setAlignment(Element.ALIGN_LEFT);
            document.add(para9);

        } catch (FileNotFoundException fnf) {
            logger.error("Error:.....!!! " + fnf.getMessage());
        } catch (DocumentException de) {
            logger.error("Error:.....!!! " + de.getMessage());
        } catch (IOException ex) {
            logger.error("Error:.....!!! " + ex.getMessage());
        }
        document.close();

        logger.debug("***********Ended createPdf()***********");
    }
    
    public String getIndianCurrencyFormat(String amount) {
        StringBuilder stringBuilder = new StringBuilder();
        char amountArray[] = amount.toCharArray();
        int a = 0, b = 0;
        for (int i = amountArray.length - 1; i >= 0; i--) {
            if (a < 3) {
                stringBuilder.append(amountArray[i]);
                a++;
            } else if (b < 2) {
                if (b == 0) {
                    stringBuilder.append(",");
                    stringBuilder.append(amountArray[i]);
                    b++;
                } else {
                    stringBuilder.append(amountArray[i]);
                    b = 0;
                }
            }
        }
        return stringBuilder.reverse().toString();
    }

}
