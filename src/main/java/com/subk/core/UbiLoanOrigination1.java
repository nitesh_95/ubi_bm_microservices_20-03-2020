package com.subk.core;

import java.io.InputStream;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author subashkumar.s
 */
public class UbiLoanOrigination1 {
    private String dbAction;
    private String applicationNo;
    private String groupId;
    private String groupName;
    private String custInfo;
    private String newCust;
    private String subkId;
    private String sbAccNo;
    private String custId;
    private String BcoMobile;
    private String BcoPin;
    private String savingACInfo;
    private String bcoId;
    private String bcoName;
        
    //step2
    private String custTitle;
    private String custTitleCode;
    private String appl1Name;
    private String custTypeDesc;
    private String custTypeCode;
    private String familyExp;
    private String jlgGroupName;
    private String jlgLeader;
    private String familySust;
    private String appl1Careof;
    private String mobileChrgs;
    private String permHouseNo;
    private String internetChrgs;
    private String permStreetNo;
    private String televChrgs;
    private String permLandMark;
    private String fuelChrgs;
    private String permVillCity;
    private String electrBill;
    private String permDistrict;
    private String studyExp;
    private String permState;
    private String blockCode;
    private String insurancePrem;
    private String permPinCode;
    private String applicantAddress;
    private String nomineeDetails;
    private String srcOfIncomeDesc;
    private String srcOfIncomeCode;
    private String networthDesc;
    private String networthCode;
    private String annualTurnoverDesc;
    private String annualTurnoverCode;
    private String maritalStatutDesc;
    private String maritalStatutCode;
    private String glSubheadDesc;
    private String glSubheadCode;
    private String constitutionDesc;
    private String constitutionCode;
    private String casteDesc;
    private String casteCode;
    private String communityDesc;
    private String communityCode;
    private String schemeDesc;
    private String schemeCode;
    private String introducerStatusDesc;
    private String introducerStatusCode;
    
    private String appl1Address;
    private String panchTax;
    private String telephone;
    private String miscExp;
    private String mobileNo;
    private String yrlyExp;
    private String yrlySurplus;
    private String idProof;
    private String idProofFile;
    private String avgWeeklySurplus;
    private String addressProof;
    private String addressProofFile;
    private String loanAmount;
    private String pmsbyPmjjbyCovrg;
    private String approachFor;
    private String pmsbyPmjjbyValidity;
    private String presentFinancer;
    private String appl1Occupation;
    private String repayPeriod;
    private String yrlyIncome;
    private String weeklyInstalment;
    private String familyOccupation;
    private String bankMitraRemarks;
    private String yrlyOthFamilyMembIncome;
    private String bankManagRemarks;
    private String yrlyFamilyIncome;
        
    //step3
    private String bcRecomendation;
    private String bcRemarks;
        
    //step4
    private String permHusFathName;
    private String permRelType;
    private String permTelephone;
    private String commHusFathName;
    private String commRelationType;
    private String commHouseNo;
    private String commStreetNo;
    private String commLandMark;
    private String commVillCity;
    private String commDistrict;
    private String commState;
    private String commPinCode;
    private String commTelephone;
    private String areaVillageCode;
    private String areaBlockCode;
    private String areaDistCode;
    private String areaStateCode;
    private String fhgName;
    private String operationMode;
    private String sex;
    private String panGirNo;
    private String dobDate;
    private String annualIncome;
    private String annualTurnover;
    private String category;
    private String qualification;
    private String eStatement;
    private String houseLoan;
    private String chequeBook;
    private String vehicleLoan;
    private String mobileBanking;
    private String mutualFund;
    private String internetBanking;
    private String lifeInsurance;
    private String creditCard;
    private String pension;
    private String addOnOthers;
    private String crossSellOthers;
    private String atmRequest;
    private String smsAlert;
    private String emailId;
    private String idProofType;
    private String addressProofType;
            
    //step5
    private String introducerTitle;
    private String introducerName;
    private String introducerCustId;
    private String introducerAccNo;
    private String introducerKnownYear;
        
    //step6
    private String nomineeAccNature;
    private String nomineeAge;
    private String nomineeAccNo;
    private String nomineeDob;
    private String nomineeAdDetails;
    private String nomineeGaurdianName;
    private String nomineeName;
    private String nomineeAddress;
    private String nomineeGuardianAge;
    private String nomineeRelation;
    private String nomineeGuardianAdd;
    private String minorNomiGuarTitle;
    private String minorNomiGuarName;
    private String minorNomiGuarAge;
    private String minorNomiGuarRelType;
    private String minorNomiGuarRelCode;
    
    //step7
    private String witness1Name;
    private String witness2Name;
    private String witness1Add;
    private String witness2Add;
    private String witness1Place;
    private String witness2Place;
//    private String witness1Date;
//    private String witness2Date;
    private String witness1Telephone;
    private String witness2Telephone;
     
    //step8
    private String appl2Name;
    private String appl2Pincode;
    private String appl2FhgName;
    private String appl2Telephone;
    private String appl2Careof;
    private String appl2Mobile;
    private String appl2HouseNo;
    private String appl2Sex;
    private String appl2StreetNo;
    private String appl2Dob;
    private String appl2Landmark;
    private String appl2Occupation;
    private String appl2VillCity;
    private String appl2Category;
    private String appl2District;
    private String appl2IdProof;
    private String appl2State;
    private String appl2AddProof;
     
    //step-9
    private String rightThumb;
    private String rightIndex;
    private String rightMiddle;
    private String rightRing;
    private String rightSmall;
    private String leftThumb;
    private String leftIndex;
    private String leftMiddle;
    private String leftRing;
    private String leftSmall;
    private String custPin;
    private String confirmCustPin;
    
    //step10
    private String familyMemb1Name;
    private String familyMemb1Relation;
    private String familyMemb1Sex;
    private String familyMemb1Age;
    private String familyMemb1Occup;
    private String familyMemb1Qualification;
    private String familyMemb2Name;
    private String familyMemb2Relation;
    private String familyMemb2Sex;
    private String familyMemb2Age;
    private String familyMemb2Occup;
    private String familyMemb2Qualification;
    private String familyMemb3Name;
    private String familyMemb3Relation;
    private String familyMemb3Sex;
    private String familyMemb3Age;
    private String familyMemb3Occup;
    private String familyMemb3Qualification;
    private String loanAmountDue;
    private String liveStock;
    private String agrlImpl;
    private String activity;
    private String assetVehicle;
    private String loanReuired;
    private String assetAnyOther;
    private String loanAccNomineeName;
    private String loanSource;
    private String loanAccNomineeRelation;
    private String loanPurpose;
    private String loanAccNomineeAge;
    private String amountBorrowed;
    private String loanAccNomineeResident;
    private String isFPCaptured;
    InputStream inputStream = null;
    
    public UbiLoanOrigination1(){
    
    }
    public UbiLoanOrigination1(HttpServletRequest request){
        
        dbAction = request.getParameter("DB_ACTION");
        if(dbAction.equals("RESUBMIT")){
            applicationNo = request.getParameter("APPLNO");
            System.out.println("APPLICATION NO: "+applicationNo);
        }
//       groupId = request.getParameter("GROUP_ID");
//       groupName = request.getParameter("GROUP_NAME");
       custInfo = request.getParameter("CUST_INFO");
       System.out.println("CUST_INFO: "+custInfo);
       if(custInfo.equalsIgnoreCase("newCust"))
          newCust = request.getParameter("NEW_CUST");
       else if(custInfo.equalsIgnoreCase("subkId"))
          subkId = request.getParameter("SUBK_ID");
       else if(custInfo.equalsIgnoreCase("sbAccNo")){
          sbAccNo = request.getParameter("SBACC_NO");
          custId = request.getParameter("CUST_ID");
       }
       savingACInfo = request.getParameter("SAVING_ACCINFO");
       System.out.println("SAVING_ACCINFO: "+savingACInfo);
       
       System.out.println("NEW_CUST: "+custInfo);
       System.out.println("SUBK_ID: "+subkId);
       System.out.println("SBACC_NO: "+sbAccNo);
       System.out.println("CUST_ID: "+custId);
       
       BcoMobile = request.getParameter("BCO_MOBILE");
       System.out.println("BCO_MOBILE: "+BcoMobile);
       BcoPin = request.getParameter("BCO_PIN");
       System.out.println("BCO_PIN: "+BcoPin);
        
        //step2
       
       custTitle = request.getParameter("CUST_TITLE");
       System.out.println("CUST_TITLE: "+custTitle);
       custTitleCode = request.getParameter("CUST_TITLE_CODE");
       System.out.println("CUST_TITLE_CODE: "+custTitleCode);
       appl1Name = request.getParameter("APPLICANT1_NAME");
       System.out.println("APPLICANT1_NAME: "+appl1Name);
       custTypeDesc = request.getParameter("CUST_TYPE");
       System.out.println("CUST_TYPE: "+custTypeDesc);
       custTypeCode = request.getParameter("Cust_Type_CODE");
       System.out.println("Cust_Type_CODE: "+custTypeCode);
       familyExp = request.getParameter("FAMILY_EXP");
       System.out.println("FAMILY_EXP: "+familyExp);
       jlgGroupName = request.getParameter("JLG_NAME");
       System.out.println("JLG_NAME: "+jlgGroupName);
       jlgLeader = request.getParameter("JLG_LEADER");
       System.out.println("JLG_LEADER: "+jlgLeader);
       familySust = request.getParameter("FAMILY_SUST");
       System.out.println("FAMILY_SUST: "+familySust);
       appl1Careof = request.getParameter("APPLICANT1_CAREOF");
       System.out.println("APPLICANT1_CAREOF: "+appl1Careof);
       mobileChrgs = request.getParameter("MOBILE_CHRGS");
       System.out.println("MOBILE_CHRGS: "+mobileChrgs);
       permHouseNo = request.getParameter("PERM_HOUSE_NO");
       System.out.println("PERM_HOUSE_NO: "+permHouseNo);
       internetChrgs = request.getParameter("INTERNET_CHRGS");
       System.out.println("INTERNET_CHRGS: "+internetChrgs);
       permStreetNo = request.getParameter("PERM_STREET_NO");
       System.out.println("PERM_STREET_NO: "+permStreetNo);
       televChrgs = request.getParameter("TELV_CHAGS");
       System.out.println("TELV_CHAGS: "+televChrgs);
       permLandMark = request.getParameter("PERM_LAND_MARK");
       System.out.println("PERM_LAND_MARK: "+permLandMark);
       fuelChrgs = request.getParameter("FUEL_CHRGS");
       System.out.println("FUEL_CHRGS: "+fuelChrgs);
       permVillCity = request.getParameter("PERM_VILL_CITY");
       System.out.println("PERM_VILL_CITY: "+permVillCity);
       electrBill = request.getParameter("ELECTR_BILL");
       System.out.println("ELECTR_BILL: "+electrBill);
       permDistrict = request.getParameter("PERM_DISTRICT");
       System.out.println("PERM_DISTRICT: "+permDistrict);
       studyExp = request.getParameter("STUDY_EXP");
       System.out.println("STUDY_EXP: "+studyExp);
       permState = request.getParameter("PERM_STATE");
       System.out.println("PERM_STATE: "+permState);
       blockCode = request.getParameter("BLOCK_CODE");
       System.out.println("BLOCK_CODE: "+blockCode);
       insurancePrem = request.getParameter("INSURANCE_PREM");
       System.out.println("INSURANCE_PREM: "+insurancePrem);
       permPinCode = request.getParameter("PERM_PINCODE");
       System.out.println("PERM_PINCODE: "+permPinCode);
       panchTax = request.getParameter("PANCH_TAX");
       System.out.println("PANCH_TAX: "+panchTax);
       telephone = request.getParameter("TELEPHONE");
       System.out.println("TELEPHONE: "+telephone);
       commHouseNo = request.getParameter("COMM_HOUSE_NO");
       System.out.println("COMM_HOUSE_NO: "+commHouseNo);
       commStreetNo = request.getParameter("COMM_STREET_NO");
       System.out.println("COMM_STREET_NO: "+commStreetNo);
       commLandMark = request.getParameter("COMM_LAND_MARK");
       System.out.println("COMM_LAND_MARK: "+commLandMark);
       commVillCity = request.getParameter("COMM_VILL_CITY");
       System.out.println("COMM_VILL_CITY: "+commVillCity);
       commDistrict = request.getParameter("COMM_DISTRICT");
       System.out.println("COMM_DISTRICT: "+commDistrict);
       commState = request.getParameter("COMM_STATE");
       System.out.println("COMM_STATE: "+commState);
       commPinCode = request.getParameter("COMM_PINCODE");
       System.out.println("COMM_PINCODE: "+commPinCode);
       commTelephone = request.getParameter("COMM_TELEPHONE");
       System.out.println("COMM_TELEPHONE: "+commTelephone);
       miscExp = request.getParameter("MISCL_EXP");
       System.out.println("MISCL_EXP: "+miscExp);
       mobileNo = request.getParameter("MOBILE_NO");
       System.out.println("MOBILE_NO: "+mobileNo);
       yrlyExp = request.getParameter("YRLY_EXP");
       System.out.println("YRLY_EXP: "+yrlyExp);
       nomineeDetails = request.getParameter("NOMINEE_DETAILS");
       System.out.println("NOMINEE_DETAILS: "+nomineeDetails);
       yrlySurplus = request.getParameter("YRLY_SURPLUS");
       System.out.println("YRLY_SURPLUS: "+yrlySurplus);
       avgWeeklySurplus = request.getParameter("AVG_WEEKLY_SURPLUS");
       System.out.println("AVG_WEEKLY_SURPLUS: "+avgWeeklySurplus);
       idProofType = request.getParameter("ID_PROOF_TYPE");
       System.out.println("ID_PROOF_TYPE: "+idProofType);
       addressProofType = request.getParameter("ADDRESS_PROOF_TYPE");
       System.out.println("ADDRESS_PROOF_TYPE: "+addressProofType);
       loanAmount = request.getParameter("LOAN_AMOUNT");
       System.out.println("LOAN_AMOUNT: "+loanAmount);
       pmsbyPmjjbyCovrg = request.getParameter("PMSBY_PMJJBY_COVRG");
       System.out.println("PMSBY_PMJJBY_COVRG: "+pmsbyPmjjbyCovrg);
       approachFor = request.getParameter("CUST_APPROCH");
       System.out.println("CUST_APPROCH FOR: "+approachFor);
       pmsbyPmjjbyValidity = request.getParameter("PMSBY_PMJJBY_VALIDITY");
       System.out.println("PMSBY_PMJJBY_VALIDITY: "+pmsbyPmjjbyValidity);
       presentFinancer = request.getParameter("PRSNT_FINCR");
       System.out.println("PRSNT_FINCR: "+presentFinancer);
       appl1Occupation = request.getParameter("APPLICANT1_OCCUPATION");
       System.out.println("APPLICANT1_OCCUPATION: "+appl1Occupation);
       repayPeriod = request.getParameter("PROPOSED_REPAY_PERIOD");
       System.out.println("PROPOSED_REPAY_PERIOD: "+repayPeriod);
       yrlyIncome = request.getParameter("YRLY_INCOME");
       System.out.println("YRLY_INCOME: "+yrlyIncome);
       weeklyInstalment = request.getParameter("WEEKLY_INSTALMENT");
       System.out.println("WEEKLY_INSTALMENT: "+weeklyInstalment);
       familyOccupation = request.getParameter("FAMILY_OCCUPATION");
       System.out.println("FAMILY_OCCUPATION: "+familyOccupation);
       bankMitraRemarks = request.getParameter("BANKMITRA_REMARKS");
       System.out.println("BANKMITRA_REMARKS: "+bankMitraRemarks);
       yrlyOthFamilyMembIncome = request.getParameter("YRLY_OTHER_FAMILY_MEMB_INCOME");
       System.out.println("YRLY_OTHER_FAMILY_MEMB_INCOME: "+yrlyOthFamilyMembIncome);
       bankManagRemarks = request.getParameter("BANKMANAGER_REMARKS");
       System.out.println("BANKMANAGER_REMARKS: "+bankManagRemarks);
       yrlyFamilyIncome = request.getParameter("YRLY_FAMILY_INCOME");
       System.out.println("YRLY_FAMILY_INCOME: "+yrlyFamilyIncome);
       
       srcOfIncomeDesc = request.getParameter("SRC_OF_INCOME");
        System.out.println("SRC_OF_INCOME: "+srcOfIncomeDesc);
        srcOfIncomeCode = request.getParameter("SRCofIncome_CODE");
        System.out.println("SRCofIncome_CODE: "+srcOfIncomeCode);
        networthDesc = request.getParameter("NETWORTH");
        System.out.println("NETWORTH: "+networthDesc);
        networthCode = request.getParameter("Networth_CODE");
        System.out.println("Networth_CODE: "+networthCode);
        annualTurnoverDesc = request.getParameter("ANNUAL_TURNOVER");
        System.out.println("ANNUAL_TURNOVER: "+annualTurnoverDesc);
        annualTurnoverCode = request.getParameter("AnnualTurnover_CODE");
        System.out.println("AnnualTurnover_CODE: "+annualTurnoverCode);
        
        //step3
       bcRecomendation = request.getParameter("BC_RECOMENDATION");
       System.out.println("BC_RECOMENDATION: "+bcRecomendation);
       bcRemarks = request.getParameter("BC_REMARKS");
       System.out.println("BC_REMARKS: "+bcRemarks);
        
        //step4
       permHusFathName = request.getParameter("PERM__HUSBAND_FATHER_NAME");
       permRelType = request.getParameter("PERM__RELATION_TYPE");
       permTelephone = request.getParameter("PERM_TELEPHONE");
       commHusFathName = request.getParameter("COMM_HUSBAND_FATHER_NAME");
       commRelationType = request.getParameter("COMM_TELEPHONE");
       
       fhgName = request.getParameter("FATHER_HUSBAND_GUARDIAN_NAME");
       System.out.println("FATHER_HUSBAND_GUARDIAN_NAME: "+fhgName);
       operationMode = request.getParameter("OPERATION_MODE");
       System.out.println("OPERATION_MODE: "+operationMode);
       sex = request.getParameter("SEX");
       System.out.println("SEX: "+sex);
       panGirNo = request.getParameter("PAN_GIR_NO");
       System.out.println("PAN_GIR_NO: "+panGirNo);
       dobDate = request.getParameter("DOB_DATE");
       System.out.println("DOB_DATE: "+dobDate);
       annualIncome = request.getParameter("ANNUAL_INCOME");
       System.out.println("ANNUAL_INCOME: "+annualIncome);
       annualTurnover = request.getParameter("ANNUAL_TURNOVER");
       System.out.println("ANNUAL_TURNOVER: "+annualTurnover);
       category = request.getParameter("CATEGORY");
       System.out.println("CATEGORY: "+category);
       qualification = request.getParameter("QUALIFICATION");
       System.out.println("QUALIFICATION: "+qualification);
       eStatement = request.getParameter("E_STATEMENT");
       System.out.println("E_STATEMENT: "+eStatement);
       houseLoan = request.getParameter("HOUSE_LOAN");
       System.out.println("HOUSE_LOAN: "+houseLoan);
       chequeBook = request.getParameter("CHEQUE_BOOK");
       System.out.println("CHEQUE_BOOK: "+chequeBook);
       vehicleLoan = request.getParameter("VEHICLE_LOAN");
       System.out.println("VEHICLE_LOAN: "+vehicleLoan);
       mobileBanking = request.getParameter("MOBILE_BANKING");
       System.out.println("MOBILE_BANKING: "+mobileBanking);
       mutualFund = request.getParameter("MUTUAL_FUND");
       System.out.println("MUTUAL_FUND: "+mutualFund);
       internetBanking = request.getParameter("INTERNET_BANKING");
       System.out.println("INTERNET_BANKING: "+internetBanking);
       lifeInsurance = request.getParameter("LIFE_INSURANCE");
       System.out.println("LIFE_INSURANCE: "+lifeInsurance);
       creditCard = request.getParameter("CREDIT_CARD");
       System.out.println("CREDIT_CARD: "+creditCard);
       pension = request.getParameter("PENSION");
       System.out.println("PENSION: "+pension);
       addOnOthers = request.getParameter("ADD_ON_OTHERS");
       System.out.println("ADD_ON_OTHERS: "+addOnOthers);
       crossSellOthers = request.getParameter("CROSS_SELL_OTHERS");
       System.out.println("CROSS_SELL_OTHERS: "+crossSellOthers);
       atmRequest = request.getParameter("ATM_REQUEST");
       System.out.println("ATM_REQUEST: "+atmRequest);
       smsAlert = request.getParameter("SMS_ALERT");
       System.out.println("SMS_ALERT: "+smsAlert);
       emailId = request.getParameter("EMAIL_ID");
       System.out.println("EMAIL_ID: "+emailId);
       idProof = request.getParameter("ID_PROOF");
       System.out.println("ID_PROOF: "+idProof);
       addressProof = request.getParameter("ADDRESS_PROOF");
       System.out.println("ADDRESS_PROOF: "+addressProof);
       
       idProofFile = request.getParameter("ID_PROOF_FILE");
       addressProofFile = request.getParameter("ADDRESS_PROOF_FILE");
       
       areaVillageCode = request.getParameter("AREA_VILLAGE_CODE");
       System.out.println("AREA_VILLAGE_CODE: "+areaVillageCode);
       areaBlockCode = request.getParameter("AREA_BLOCK_CODE");
       System.out.println("AREA_BLOCK_CODE: "+areaBlockCode);
       areaDistCode = request.getParameter("AREA_DIST_CODE");
       System.out.println("AREA_DIST_CODE: "+areaDistCode);
       areaStateCode = request.getParameter("AREA_STATE_CODE");
       System.out.println("AREA_STATE_CODE: "+areaStateCode);
       
       
       maritalStatutDesc = request.getParameter("MARITAL_STATUS_TYPE");
        System.out.println("MARITAL_STATUS: "+maritalStatutDesc);
        maritalStatutCode = request.getParameter("Marital_Status");
        System.out.println("Marital_Status_CODE: "+maritalStatutCode);
        glSubheadDesc = request.getParameter("GL_SUBHEAD");
        System.out.println("GL_SUBHEAD: "+glSubheadDesc);
        glSubheadCode = request.getParameter("GL_Subhead_CODE");
        System.out.println("GL_Subhead_CODE: "+glSubheadCode);
        constitutionDesc = request.getParameter("CONSTITUTION");
        System.out.println("CONSTITUTION: "+constitutionDesc);
        constitutionCode = request.getParameter("Constitution_CODE");
        System.out.println("Constitution_CODE: "+constitutionCode);
        casteDesc = request.getParameter("CASTE");
        System.out.println("CASTE: "+casteDesc);
        casteCode = request.getParameter("Caste_CODE");
        System.out.println("Caste_CODE: "+casteCode);
        communityDesc = request.getParameter("COMMUNITY");
        System.out.println("COMMUNITY: "+communityDesc);
        communityCode = request.getParameter("Community_CODE");
        System.out.println("Community_CODE: "+communityCode);
        schemeDesc = request.getParameter("SCHEME");
        System.out.println("SCHEME: "+schemeDesc);
        schemeCode = request.getParameter("Scheme_CODE");
        System.out.println("Scheme_CODE: "+schemeCode);
        introducerStatusDesc = request.getParameter("INTRODUCER");
        System.out.println("INTRODUCER: "+introducerStatusDesc);
        
        
        //step5
       introducerTitle = request.getParameter("INTRODUCER_TITLE");
       System.out.println("INTRODUCER_TITLE: "+introducerTitle);
       introducerName = request.getParameter("INTRODUCER_NAME");
       System.out.println("INTRODUCER_NAME: "+introducerName);
       introducerCustId = request.getParameter("INTRODUCER_CUST_ID");
       System.out.println("INTRODUCER_CUST_ID: "+introducerCustId);
       introducerAccNo = request.getParameter("INTRODUCER_ACC_NO");
       System.out.println("INTRODUCER_ACC_NO: "+introducerAccNo);
       
       introducerKnownYear = request.getParameter("KNOWN_YEAR");
       System.out.println("KNOWN_YEAR: "+introducerKnownYear);
       introducerStatusCode = request.getParameter("Introducer_Status_CODE");
       System.out.println("Introducer_Status_CODE: "+introducerStatusCode);
        
        //step6
       nomineeAccNature = request.getParameter("NOMINEE_ACC_NATURE");
       System.out.println("NOMINEE_ACC_NATURE: "+nomineeAccNature);
       nomineeAge = request.getParameter("NOMINEE_AGE");
       System.out.println("NOMINEE_AGE: "+nomineeAge);
       nomineeAccNo = request.getParameter("NOMINEE_ACC_NO");
       System.out.println("NOMINEE_ACC_NO: "+nomineeAccNo);
       nomineeDob = request.getParameter("NOMINEE_DOB");
       System.out.println("NOMINEE_DOB: "+nomineeDob);
       nomineeAdDetails = request.getParameter("NOMINEE_ADDIT_DETAILS");
       System.out.println("NOMINEE_ADDIT_DETAILS: "+nomineeAdDetails);
       nomineeGaurdianName = request.getParameter("NOMINEE_GAURDIAN_NAME");
       System.out.println("NOMINEE_GAURDIAN_NAME: "+nomineeGaurdianName);
       nomineeName = request.getParameter("NOMINEE_NAME");
       System.out.println("NOMINEE_NAME: "+nomineeName);
       nomineeAddress = request.getParameter("NOMINEE_ADDRESS");
       System.out.println("NOMINEE_ADDRESS: "+nomineeAddress);
       nomineeGuardianAge = request.getParameter("NOMINEE_GAURDIAN_AGE");
       System.out.println("NOMINEE_GAURDIAN_AGE: "+nomineeGuardianAge);
       nomineeRelation = request.getParameter("NOMINEE_RELATION");
       System.out.println("NOMINEE_RELATION: "+nomineeRelation);
       nomineeGuardianAdd = request.getParameter("NOMINEE_GAURDIAN_ADDRESS");
       System.out.println("NOMINEE_GAURDIAN_ADDRESS: "+nomineeGuardianAdd);
       
       minorNomiGuarTitle = request.getParameter("MINOR_NOMI_GAUR_TITLE");
       System.out.println("MINOR_NOMI_GAUR_TITLE: "+minorNomiGuarTitle);
       minorNomiGuarName = request.getParameter("MINOR_NOMI_GAUR_NAME");
       System.out.println("MINOR_NOMI_GAUR_NAME: "+minorNomiGuarName);
       minorNomiGuarAge  = request.getParameter("MINOR_NOMI_GAUR_AGE");
       System.out.println("MINOR_NOMI_GAUR_AGE: "+minorNomiGuarAge);
       minorNomiGuarRelType = request.getParameter("MINOR_NOMI_GAUR_REL_TYPE");
       System.out.println("MINOR_NOMI_GAUR_REL_TYPE: "+minorNomiGuarRelType);
       minorNomiGuarRelCode = request.getParameter("MINOR_NOMI_GAUR_REL_CODE");
       System.out.println("MINOR_NOMI_GAUR_REL_CODE: "+minorNomiGuarRelCode);
       
        //step7
       witness1Name = request.getParameter("WITNESS1_NAME");
       System.out.println("WITNESS1_NAME: "+witness1Name);
       witness2Name = request.getParameter("WITNESS2_NAME");
       System.out.println("WITNESS2_NAME: "+witness2Name);
       witness1Add = request.getParameter("WITNESS1_ADDRESS");
       System.out.println("WITNESS1_ADDRESS: "+witness1Add);
       witness2Add = request.getParameter("WITNESS2_ADDRESS");
       System.out.println("WITNESS2_ADDRESS: "+witness2Add);
       witness1Place = request.getParameter("WITNESS1_PLACE");
       System.out.println("WITNESS1_PLACE: "+witness1Place);
       witness2Place = request.getParameter("WITNESS2_PLACE");
       System.out.println("WITNESS2_PLACE: "+witness2Place);
//       witness1Date = request.getParameter("WITNESS1_DATE");
//       witness2Date = request.getParameter("WITNESS2_DATE");
       witness1Telephone = request.getParameter("WITNESS1_TELEPHONE");
       System.out.println("WITNESS1_TELEPHONE: "+witness1Telephone);
       witness2Telephone = request.getParameter("WITNESS2_TELEPHONE");
       System.out.println("WITNESS2_TELEPHONE: "+witness2Telephone);
        
        //step8
       appl2Name = request.getParameter("APPLICANT2_NAME");
       System.out.println("APPLICANT2_NAME: "+appl2Name);
       appl2Pincode = request.getParameter("APPLICANT2_PINCODE");
       System.out.println("APPLICANT2_PINCODE: "+appl2Pincode);
       appl2FhgName = request.getParameter("APPLICANT2_FATHER_HUS_GUAR_NAME");
       System.out.println("APPLICANT2_FATHER_HUS_GUAR_NAME: "+appl2FhgName);
       appl2Telephone = request.getParameter("APPLICANT2_TELEPHONE");
       System.out.println("APPLICANT2_TELEPHONE: "+appl2Telephone);
       appl2Careof = request.getParameter("APPLICANT2_CAREOF");
       System.out.println("APPLICANT2_CAREOF: "+appl2Careof);
       appl2Mobile = request.getParameter("APPLICANT2_MOBILE");
       System.out.println("APPLICANT2_MOBILE: "+appl2Mobile);
       appl2HouseNo = request.getParameter("APPLICANT2_HOUSE_NO");
       System.out.println("APPLICANT2_HOUSE_NO: "+appl2HouseNo);
       appl2Sex = request.getParameter("APPLICANT2_SEX");
       System.out.println("APPLICANT2_SEX: "+appl2Sex);
       appl2StreetNo = request.getParameter("APPLICANT2_STREET_NO");
       System.out.println("APPLICANT2_STREET_NO: "+appl2StreetNo);
       appl2Dob = request.getParameter("APPLICANT2_DOB");
       System.out.println("APPLICANT2_DOB: "+appl2Dob);
       appl2Landmark = request.getParameter("APPLICANT2_LANDMARK");
       System.out.println("APPLICANT2_LANDMARK: "+appl2Landmark);
       appl2Occupation = request.getParameter("APPLICANT2_OCCUPATION");
       System.out.println("APPLICANT2_OCCUPATION: "+appl2Occupation);
       appl2VillCity = request.getParameter("APPLICANT2_VILL_CITY");
       System.out.println("APPLICANT2_VILL_CITY: "+appl2VillCity);
       appl2Category = request.getParameter("APPLICANT2_CATEGORY");
       System.out.println("APPLICANT2_CATEGORY: "+appl2Category);
       appl2District = request.getParameter("APPLICANT2_DISTRICT");
       System.out.println("APPLICANT2_DISTRICT: "+appl2District);
       appl2IdProof = request.getParameter("APPLICANT2_ID_PROOF");
       System.out.println("APPLICANT2_ID_PROOF: "+appl2IdProof);
       appl2State = request.getParameter("APPLICANT2_STATE");
       System.out.println("APPLICANT2_STATE: "+appl2State);
       appl2AddProof = request.getParameter("APPLICANT2_ADDRESS_PROOF");
       System.out.println("APPLICANT2_ADDRESS_PROOF: "+appl2AddProof);
        
       //step9
       rightThumb = request.getParameter("RIGHT_THUMB");
       System.out.println("RIGHT_THUMB: "+rightThumb);
       rightIndex = request.getParameter("RIGHT_INDEX");
       System.out.println("RIGHT_INDEX: "+rightIndex);
       rightMiddle = request.getParameter("RIGHT_MIDDLE");
       System.out.println("RIGHT_MIDDLE: "+rightMiddle);
       rightRing = request.getParameter("RIGHT_RING");
       System.out.println("RIGHT_RING: "+rightRing);
       rightSmall = request.getParameter("RIGHT_SMALL");
       System.out.println("RIGHT_SMALL: "+rightSmall);
       leftThumb = request.getParameter("LEFT_THUMB");
       System.out.println("LEFT_THUMB: "+rightThumb);
       leftIndex = request.getParameter("LEFT_INDEX");
       System.out.println("LEFT_INDEX: "+rightIndex);
       leftMiddle = request.getParameter("LEFT_MIDDLE");
       System.out.println("LEFT_MIDDLE: "+rightMiddle);
       leftRing = request.getParameter("LEFT_RING");
       System.out.println("LEFT_RING: "+rightRing);
       leftSmall = request.getParameter("LEFT_SMALL");
       System.out.println("LEFT_SMALL: "+rightSmall);
       
       custPin = request.getParameter("CUST_PIN");
       System.out.println("CUST_PIN: "+custPin);
       confirmCustPin = request.getParameter("CONFIRM_CUST_PIN");
       System.out.println("CONFIRM_CUST_PIN: "+confirmCustPin);
       
        //step10
       familyMemb1Name = request.getParameter("FAMILY_MEMB1_NAME");
       System.out.println("FAMILY_MEMB1_NAME: "+familyMemb1Name);
       familyMemb1Relation = request.getParameter("FAMILY_MEMB1_RETATION");
       System.out.println("FAMILY_MEMB1_RETATION: "+familyMemb1Relation);
       familyMemb1Sex = request.getParameter("FAMILY_MEMB1_SEX");
       System.out.println("FAMILY_MEMB1_SEX: "+familyMemb1Sex);
       familyMemb1Age = request.getParameter("FAMILY_MEMB1_AGE");
       System.out.println("FAMILY_MEMB1_AGE: "+familyMemb1Age);
       familyMemb1Occup = request.getParameter("FAMILY_MEMB1_OCCUPATION");
       System.out.println("FAMILY_MEMB1_OCCUPATION: "+familyMemb1Occup);
       familyMemb1Qualification = request.getParameter("FAMILY_MEMB1_QUALIFICATION");
       System.out.println("FAMILY_MEMB1_QUALIFICATION: "+familyMemb1Qualification);
       familyMemb2Name = request.getParameter("FAMILY_MEMB2_NAME");
       System.out.println("FAMILY_MEMB2_NAME: "+familyMemb2Name);
       familyMemb2Relation = request.getParameter("FAMILY_MEMB2_RETATION");
       System.out.println("FAMILY_MEMB2_RETATION: "+familyMemb2Relation);
       familyMemb2Sex = request.getParameter("FAMILY_MEMB2_SEX");
       System.out.println("FAMILY_MEMB2_SEX: "+familyMemb2Sex);
       familyMemb2Age = request.getParameter("FAMILY_MEMB2_AGE");
       System.out.println("FAMILY_MEMB2_AGE: "+familyMemb2Age);
       familyMemb2Occup = request.getParameter("FAMILY_MEMB2_OCCUPATION");
       System.out.println("FAMILY_MEMB2_OCCUPATION: "+familyMemb2Occup);
       familyMemb2Qualification = request.getParameter("FAMILY_MEMB2_QUALIFICATION");
       System.out.println("FAMILY_MEMB2_QUALIFICATION: "+familyMemb2Qualification);
       familyMemb3Name = request.getParameter("FAMILY_MEMB3_NAME");
       System.out.println("FAMILY_MEMB3_NAME: "+familyMemb3Name);
       familyMemb3Relation = request.getParameter("FAMILY_MEMB3_RETATION");
       System.out.println("FAMILY_MEMB3_RETATION: "+familyMemb3Relation);
       familyMemb3Sex = request.getParameter("FAMILY_MEMB3_SEX");
       System.out.println("FAMILY_MEMB3_SEX: "+familyMemb3Sex);
       familyMemb3Age = request.getParameter("FAMILY_MEMB3_AGE");
       System.out.println("FAMILY_MEMB3_AGE: "+familyMemb3Age);
       familyMemb3Occup = request.getParameter("FAMILY_MEMB3_OCCUPATION");
       System.out.println("FAMILY_MEMB3_OCCUPATION: "+familyMemb3Occup);
       familyMemb3Qualification = request.getParameter("FAMILY_MEMB3_QUALIFICATION");
       System.out.println("FAMILY_MEMB3_QUALIFICATION: "+familyMemb3Qualification);
       loanAmountDue = request.getParameter("LOAN_AMOUNT_DUE");
       System.out.println("LOAN_AMOUNT_DUE: "+loanAmountDue);
       liveStock = request.getParameter("ASSET_LIVESTOCK");
       System.out.println("ASSET_LIVESTOCK: "+liveStock);
       agrlImpl = request.getParameter("ASSET_AGRL_IMPL");
       System.out.println("ASSET_AGRL_IMPL: "+agrlImpl);
       activity = request.getParameter("ACTIVITY");
       System.out.println("ACTIVITY: "+activity);
       assetVehicle = request.getParameter("ASSET_VEHICLE");
       System.out.println("ASSET_VEHICLE: "+assetVehicle);
       loanReuired = request.getParameter("LOAN_REUIRED");
       System.out.println("LOAN_REUIRED: "+loanReuired);
       assetAnyOther = request.getParameter("ASSET_ANYOTHER");
       System.out.println("ASSET_ANYOTHER: "+assetAnyOther);
       loanAccNomineeName = request.getParameter("LOANACC_NOMINEE_NAME");
       System.out.println("LOANACC_NOMINEE_NAME: "+loanAccNomineeName);
       loanSource = request.getParameter("LOAN_SOURCE");
       System.out.println("LOAN_SOURCE: "+loanSource);
       loanAccNomineeRelation = request.getParameter("LOANACC_NOMINEE_RELATION");
       System.out.println("LOANACC_NOMINEE_RELATION: "+loanAccNomineeRelation);
       loanPurpose = request.getParameter("LOAN_PURPOSE");
       System.out.println("LOAN_PURPOSE: "+loanPurpose);
       loanAccNomineeAge = request.getParameter("LOANACC_NOMINEE_AGE");
       System.out.println("LOANACC_NOMINEE_AGE: "+loanAccNomineeAge);
       amountBorrowed = request.getParameter("AMOUNT_BORROWED");
       System.out.println("AMOUNT_BORROWED: "+amountBorrowed);
       loanAccNomineeResident = request.getParameter("LOANACC_NOMINEE_RESIDENT");
       System.out.println("LOANACC_NOMINEE_RESIDENT: "+loanAccNomineeResident);
//       try{
//           for(Part part : request.getParts()) {
//                String fileName = extractFileName(part);
//                part.write(fileName);
//           }
//       }catch(Exception e){}
        
    }
//    private String extractFileName(Part part) {
//        String contentDisp = part.getHeader("content-disposition");
//        String[] items = contentDisp.split(";");
//        for (String s : items) {
//            if (s.trim().startsWith("filename")) {
//                return s.substring(s.indexOf("=") + 2, s.length()-1);
//            }
//        }
//        return "";
//    }
    
    
    public void setLoanParameters(HashMap map){
        savingACInfo= (String)map.get("SavingACInfo");
        BcoMobile= (String)map.get("BcoMobile");
        BcoPin= (String)map.get("BcoPin");
        custId= (String)map.get("Customer_ID");
        appl1Name= (String)map.get("Cus_Name");
        mobileNo= (String)map.get("Cus_MobileNo");
        bcoId= (String)map.get("BCO_ID");
        permState= (String)map.get("State");
        permDistrict= (String)map.get("District");
        permVillCity= (String)map.get("Village");
        permVillCity= (String)map.get("City");
        permPinCode= (String)map.get("Pincode");
        sex= (String)map.get("Gender");
        dobDate= (String)map.get("Date_Of_Birth");
        appl1Occupation= (String)map.get("Customer_Occupation");
        annualIncome= (String)map.get("Annual_Income");
        category= (String)map.get("Customer_Category");
        loanPurpose= (String)map.get("Purpose_Of_Opening_Account");
        operationMode= (String)map.get("AccountOperationType");
        isFPCaptured = (String)map.get("Is_FP_Captured");
        telephone= (String)map.get("Contact_NO");
        
//        Age= (String)map.get("Age");
        srcOfIncomeCode = (String)map.get("SRCofIncome_CODE");
//        relation_CODE= (String)map.get("Relation_CODE");
        networthCode = (String)map.get("Networth_CODE");
        maritalStatutCode = (String)map.get("Marital_Status");
        glSubheadCode = (String)map.get("Gl_Subhead_CODE");
        constitutionCode = (String)map.get("Constitution_CODE");
        setAreaVillageCode((String)map.get("Area_Village_Code"));
        areaBlockCode= (String)map.get("Area_Block_Code");
        areaDistCode= (String)map.get("Area_Dist_CODE");
        areaStateCode= (String)map.get("Area_State_CODE");
        casteCode= (String)map.get("Caste_CODE");
        communityCode= (String)map.get("Community_CODE");
//        Cust_Pin= (String)map.get("Cust_Pin");
        schemeCode= (String)map.get("Scheme_CODE");
        introducerStatusCode= (String)map.get("Introducer_Status_CODE");
        subkId= (String)map.get("SubKId");
        custTitleCode= (String)map.get("Cust_Title_CODE");
//        Cust_Type_CODE= (String)map.get("Cust_Type_CODE");
        fhgName= (String)map.get("FatherHusbandGuardianName");
        appl1Occupation= (String)map.get("Occupation_CODE");
        commVillCity= (String)map.get("Comm_City_CODE");
        commState= (String)map.get("Comm_State_CODE");
        commPinCode= (String)map.get("Comm_Pincode");
        permVillCity= (String)map.get("Permanent_City_CODE");
        permState= (String)map.get("Permanent_State_CODE");
        permPinCode= (String)map.get("Permanent_Pincode");
        blockCode= (String)map.get("Block_Code");
        telephone= (String)map.get("TelephoneNo");
        idProofType= (String)map.get("KYC_IDProofCode");
        idProof= (String)map.get("IDProofValue");
        idProofFile= (String)map.get("IDProofImage");
        addressProofType= (String)map.get("KYC_AddProof_CODE");
        addressProof= (String)map.get("AddressProofValue");
        addressProofFile= (String)map.get("AddressProofImage");
        annualIncome= (String)map.get("AnnualIncome_CODE");
        annualTurnover= (String)map.get("AnnualTurnover_CODE");
        operationMode= (String)map.get("Operation_CODE");
        introducerTitle= (String)map.get("Introducer_Title_CODE");
        introducerName= (String)map.get("IntroducerName");
        introducerCustId= (String)map.get("IntroducerCustomerId");
        introducerAccNo= (String)map.get("IntroducerAcNo");
        nomineeName= (String)map.get("NomineeName");
        nomineeGaurdianName= (String)map.get("Nominee_FatherHusbandName");
        nomineeRelation= (String)map.get("Nominee_Relation_CODE");
        nomineeDob= (String)map.get("NomineeDOB");
        nomineeAccNature= (String)map.get("NomineeAcType");
        nomineeAccNo= (String)map.get("NomineeAcNo");
        nomineeAdDetails= (String)map.get("NomineeAdditionalDetails");
        nomineeAge= (String)map.get("NomineeAge");
        minorNomiGuarName= (String)map.get("NomineeNomineeName");
        nomineeGuardianAge= (String)map.get("NomineeNomineeAge");
        nomineeGuardianAdd = (String)map.get("NomineeNomineeAddress");
        panGirNo= (String)map.get("PANGIRNo");
        emailId= (String)map.get("EmailId");
        atmRequest= (String)map.get("RequestforATM");
        smsAlert= (String)map.get("RequestforSMSAlert");
        eStatement= (String)map.get("RequestforStatement");
        chequeBook= (String)map.get("RequestforChequeBook");
        mobileBanking= (String)map.get("RequestforMobileBanking");
        internetBanking= (String)map.get("RequestforInternetBanking");
        creditCard= (String)map.get("RequestforCreditCard");
        addOnOthers= (String)map.get("RequestforOtherAddon");
        houseLoan= (String)map.get("RequestforHousingLoan");
        vehicleLoan= (String)map.get("RequestforVehicleLoan");
        mutualFund= (String)map.get("RequestforMutualFund");
        lifeInsurance= (String)map.get("RequestforInsurance");
        pension= (String)map.get("RequestforPension");
        crossSellOthers= (String)map.get("RequestforOtherproduct");
        witness1Name= (String)map.get("Witness1Name");
        witness1Add= (String)map.get("Witness1Address");
        witness1Place= (String)map.get("Witness1Place");
        witness1Telephone= (String)map.get("Witness1TelNo");
        witness2Name= (String)map.get("Witness2Name");
        witness2Add= (String)map.get("Witness2Address");
        witness2Place= (String)map.get("Witness2Place");
        witness2Telephone= (String)map.get("Witness2TelNo");
        appl2FhgName= (String)map.get("FatherHusbandGuardianName2");
        appl2Occupation= (String)map.get("Occupation_CODE2");
        appl2HouseNo= (String)map.get("HNoName2");
        appl2StreetNo= (String)map.get("StreetNoName2");
        appl2VillCity= (String)map.get("VillageCity2");
        appl2District= (String)map.get("District2");
        appl2State= (String)map.get("State2");
        appl2Pincode= (String)map.get("Pincode2");
        appl2Telephone= (String)map.get("TelephoneNo2");
        appl2Mobile= (String)map.get("MobileNo2");
        appl2Sex= (String)map.get("Sex2");
        appl2Dob= (String)map.get("DOB2");
        appl2IdProof= (String)map.get("IDProofType2");
        appl2AddProof= (String)map.get("AddressProofType2");
        appl1Name= (String)map.get("ApplicantName");
        groupName= (String)map.get("GroupName");
        jlgLeader= (String)map.get("GroupLeaderName");
        appl1Careof= (String)map.get("CO");
//        HNoName= (String)map.get("HNoName");
//        SteetNoName= (String)map.get("SteetNoName");
//        VillageCity= (String)map.get("VillageCity");
        mobileNo= (String)map.get("MobileNo");
        sex= (String)map.get("Sex");
        dobDate= (String)map.get("DOB");
//        Occupation= (String)map.get("Occupation");
        bcoName= (String)map.get("BCO_NAME");
        idProof= (String)map.get("IdProof");
        addressProof= (String)map.get("AddressProof");
        pmsbyPmjjbyCovrg= (String)map.get("CoverageUnderPMSBYorPMJJBY");
        pmsbyPmjjbyValidity= (String)map.get("ValidityPeriodPMSBYorPMJJBY");
        annualIncome= (String)map.get("ApplicantYearlyIncome");
        familyOccupation= (String)map.get("FamilyMembersOccupation");
        yrlyFamilyIncome= (String)map.get("FamilyMembersYearlyIncome");
        familySust= (String)map.get("FamilySustenance");
        mobileChrgs= (String)map.get("MobileCharges");
        internetChrgs= (String)map.get("InternetCharges");
        televChrgs= (String)map.get("TelevisionCharges");
        fuelChrgs= (String)map.get("FuelCharges");
        electrBill= (String)map.get("ElectricityBill");
        studyExp= (String)map.get("StudyExpenses");
        panchTax= (String)map.get("MunicipalityTax");
        miscExp= (String)map.get("MiscellaneousExpenses");
        yrlySurplus= (String)map.get("YearlySurplus");
        avgWeeklySurplus= (String)map.get("AvgWeeklySurplus");
        loanAmount= (String)map.get("LoanAmountApplied");
        approachFor= (String)map.get("IsFirstTimeLoanorTakeover");
        presentFinancer= (String)map.get("PresentFinancierforTakeoverLoan");
        repayPeriod= (String)map.get("ProposedRepaymentPeriodforTakeoverLoan");
        weeklyInstalment= (String)map.get("EquatedWeeklyInstalment");
        bcRemarks= (String)map.get("BCRemarks");
        bankManagRemarks= (String)map.get("BankOfficialRemarks");
        bcRecomendation= (String)map.get("BCRecommendation");
        liveStock= (String)map.get("LiveStock");
        agrlImpl= (String)map.get("AgrlImplements");
        assetVehicle= (String)map.get("Vehicles");
        assetAnyOther= (String)map.get("OtherAssets");
        loanSource= (String)map.get("LoanAvailedSource");
        loanPurpose= (String)map.get("LoanAvailedPurpose");
        amountBorrowed= (String)map.get("LoanAvailedAmountBorrowed");
        loanAmountDue= (String)map.get("LoanAvailedAmountDue");
        familyMemb1Name= (String)map.get("F1Name");
        familyMemb1Relation= (String)map.get("F1Relationship");
        familyMemb1Sex= (String)map.get("F1Sex");
        familyMemb1Age= (String)map.get("F1Age");
        familyMemb1Occup= (String)map.get("F1Occupation");
        familyMemb1Qualification= (String)map.get("F1Education");
        familyMemb2Name= (String)map.get("F2Name");
        familyMemb2Relation= (String)map.get("F2Relationship");
        familyMemb2Sex= (String)map.get("F2Sex");
        familyMemb2Age= (String)map.get("F2Age");
        familyMemb2Occup= (String)map.get("F2Occupation");
        familyMemb2Qualification= (String)map.get("F2Education");
        familyMemb3Name= (String)map.get("F3Name");
        familyMemb3Relation= (String)map.get("F3Relationship");
        familyMemb3Sex= (String)map.get("F3Sex");
        familyMemb3Age= (String)map.get("F3Age");
        familyMemb3Qualification= (String)map.get("F3Education");
        nomineeRelation= (String)map.get("NomineeRelationship");
        activity= (String)map.get("FinancialAssistanceSoughtforActivity");
        loanReuired= (String)map.get("FinancialAssistanceSoughtforLoanRequired");
        bcRemarks= (String)map.get("BcRemarks");
        
        
        setPermHusFathName((String)map.get("PermHusbandFatherName"));
        setPermRelType((String)map.get("PermRelationType"));
        setPermTelephone((String)map.get("PermTelephone"));
        setCommHusFathName((String)map.get("CommHusbandFatherName"));
        setCommRelationType((String)map.get("CommRelationType"));
       commTelephone = (String)map.get("CommTelephone");

        
    }

    /**
     * @return the groupId
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * @param groupId the groupId to set
     */
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the subkId
     */
    public String getSubkId() {
        return subkId;
    }

    /**
     * @param subkId the subkId to set
     */
    public void setSubkId(String subkId) {
        this.subkId = subkId;
    }

    /**
     * @return the sbAccNo
     */
    public String getSbAccNo() {
        return sbAccNo;
    }

    /**
     * @param sbAccNo the sbAccNo to set
     */
    public void setSbAccNo(String sbAccNo) {
        this.sbAccNo = sbAccNo;
    }

    /**
     * @return the appl1Name
     */
    public String getAppl1Name() {
        return appl1Name;
    }

    /**
     * @param appl1Name the appl1Name to set
     */
    public void setAppl1Name(String appl1Name) {
        this.appl1Name = appl1Name;
    }

    /**
     * @return the familyExp
     */
    public String getFamilyExp() {
        return familyExp;
    }

    /**
     * @param familyExp the familyExp to set
     */
    public void setFamilyExp(String familyExp) {
        this.familyExp = familyExp;
    }

    /**
     * @return the familySust
     */
    public String getFamilySust() {
        return familySust;
    }

    /**
     * @param familySust the familySust to set
     */
    public void setFamilySust(String familySust) {
        this.familySust = familySust;
    }

    /**
     * @return the appl1Careof
     */
    public String getAppl1Careof() {
        return appl1Careof;
    }

    /**
     * @param appl1Careof the appl1Careof to set
     */
    public void setAppl1Careof(String appl1Careof) {
        this.appl1Careof = appl1Careof;
    }

    /**
     * @return the mobileChrgs
     */
    public String getMobileChrgs() {
        return mobileChrgs;
    }

    /**
     * @param mobileChrgs the mobileChrgs to set
     */
    public void setMobileChrgs(String mobileChrgs) {
        this.mobileChrgs = mobileChrgs;
    }

    /**
     * @return the internetChrgs
     */
    public String getInternetChrgs() {
        return internetChrgs;
    }

    /**
     * @param internetChrgs the internetChrgs to set
     */
    public void setInternetChrgs(String internetChrgs) {
        this.internetChrgs = internetChrgs;
    }

    /**
     * @return the televChrgs
     */
    public String getTelevChrgs() {
        return televChrgs;
    }

    /**
     * @param televChrgs the televChrgs to set
     */
    public void setTelevChrgs(String televChrgs) {
        this.televChrgs = televChrgs;
    }

    /**
     * @return the fuelChrgs
     */
    public String getFuelChrgs() {
        return fuelChrgs;
    }

    /**
     * @param fuelChrgs the fuelChrgs to set
     */
    public void setFuelChrgs(String fuelChrgs) {
        this.fuelChrgs = fuelChrgs;
    }

    /**
     * @return the electrBill
     */
    public String getElectrBill() {
        return electrBill;
    }

    /**
     * @param electrBill the electrBill to set
     */
    public void setElectrBill(String electrBill) {
        this.electrBill = electrBill;
    }

    /**
     * @return the studyExp
     */
    public String getStudyExp() {
        return studyExp;
    }

    /**
     * @param studyExp the studyExp to set
     */
    public void setStudyExp(String studyExp) {
        this.studyExp = studyExp;
    }

    /**
     * @return the insurancePrem
     */
    public String getInsurancePrem() {
        return insurancePrem;
    }

    /**
     * @param insurancePrem the insurancePrem to set
     */
    public void setInsurancePrem(String insurancePrem) {
        this.insurancePrem = insurancePrem;
    }

    /**
     * @return the panchTax
     */
    public String getPanchTax() {
        return panchTax;
    }

    /**
     * @param panchTax the panchTax to set
     */
    public void setPanchTax(String panchTax) {
        this.panchTax = panchTax;
    }

    /**
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * @param telephone the telephone to set
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * @return the miscExp
     */
    public String getMiscExp() {
        return miscExp;
    }

    /**
     * @param miscExp the miscExp to set
     */
    public void setMiscExp(String miscExp) {
        this.miscExp = miscExp;
    }

    /**
     * @return the mobileNo
     */
    public String getMobileNo() {
        return mobileNo;
    }

    /**
     * @param mobileNo the mobileNo to set
     */
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    /**
     * @return the yrlyExp
     */
    public String getYrlyExp() {
        return yrlyExp;
    }

    /**
     * @param yrlyExp the yrlyExp to set
     */
    public void setYrlyExp(String yrlyExp) {
        this.yrlyExp = yrlyExp;
    }

    /**
     * @return the yrlySurplus
     */
    public String getYrlySurplus() {
        return yrlySurplus;
    }

    /**
     * @param yrlySurplus the yrlySurplus to set
     */
    public void setYrlySurplus(String yrlySurplus) {
        this.yrlySurplus = yrlySurplus;
    }

    /**
     * @return the idProof
     */
    public String getIdProof() {
        return idProof;
    }

    /**
     * @param idProof the idProof to set
     */
    public void setIdProof(String idProof) {
        this.idProof = idProof;
    }


    /**
     * @return the addressProof
     */
    public String getAddressProof() {
        return addressProof;
    }

    /**
     * @param addressProof the addressProof to set
     */
    public void setAddressProof(String addressProof) {
        this.addressProof = addressProof;
    }

    /**
     * @return the loanAmount
     */
    public String getLoanAmount() {
        return loanAmount;
    }

    /**
     * @param loanAmount the loanAmount to set
     */
    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    /**
     * @return the pmsbyPmjjbyCovrg
     */
    public String getPmsbyPmjjbyCovrg() {
        return pmsbyPmjjbyCovrg;
    }

    /**
     * @param pmsbyPmjjbyCovrg the pmsbyPmjjbyCovrg to set
     */
    public void setPmsbyPmjjbyCovrg(String pmsbyPmjjbyCovrg) {
        this.pmsbyPmjjbyCovrg = pmsbyPmjjbyCovrg;
    }

    /**
     * @return the approachFor
     */
    public String getApproachFor() {
        return approachFor;
    }

    /**
     * @param approachFor the approachFor to set
     */
    public void setApproachFor(String approachFor) {
        this.approachFor = approachFor;
    }

    /**
     * @return the pmsbyPmjjbyValidity
     */
    public String getPmsbyPmjjbyValidity() {
        return pmsbyPmjjbyValidity;
    }

    /**
     * @param pmsbyPmjjbyValidity the pmsbyPmjjbyValidity to set
     */
    public void setPmsbyPmjjbyValidity(String pmsbyPmjjbyValidity) {
        this.pmsbyPmjjbyValidity = pmsbyPmjjbyValidity;
    }

    /**
     * @return the presentFinancer
     */
    public String getPresentFinancer() {
        return presentFinancer;
    }

    /**
     * @param presentFinancer the presentFinancer to set
     */
    public void setPresentFinancer(String presentFinancer) {
        this.presentFinancer = presentFinancer;
    }

    /**
     * @return the appl1Occupation
     */
    public String getAppl1Occupation() {
        return appl1Occupation;
    }

    /**
     * @param appl1Occupation the appl1Occupation to set
     */
    public void setAppl1Occupation(String appl1Occupation) {
        this.appl1Occupation = appl1Occupation;
    }

    /**
     * @return the repayPeriod
     */
    public String getRepayPeriod() {
        return repayPeriod;
    }

    /**
     * @param repayPeriod the repayPeriod to set
     */
    public void setRepayPeriod(String repayPeriod) {
        this.repayPeriod = repayPeriod;
    }

    /**
     * @return the yrlyIncome
     */
    public String getYrlyIncome() {
        return yrlyIncome;
    }

    /**
     * @param yrlyIncome the yrlyIncome to set
     */
    public void setYrlyIncome(String yrlyIncome) {
        this.yrlyIncome = yrlyIncome;
    }

    /**
     * @return the familyOccupation
     */
    public String getFamilyOccupation() {
        return familyOccupation;
    }

    /**
     * @param familyOccupation the familyOccupation to set
     */
    public void setFamilyOccupation(String familyOccupation) {
        this.familyOccupation = familyOccupation;
    }

    /**
     * @return the bankMitraRemarks
     */
    public String getBankMitraRemarks() {
        return bankMitraRemarks;
    }

    /**
     * @param bankMitraRemarks the bankMitraRemarks to set
     */
    public void setBankMitraRemarks(String bankMitraRemarks) {
        this.bankMitraRemarks = bankMitraRemarks;
    }

    /**
     * @return the yrlyOthFamilyMembIncome
     */
    public String getYrlyOthFamilyMembIncome() {
        return yrlyOthFamilyMembIncome;
    }

    /**
     * @param yrlyOthFamilyMembIncome the yrlyOthFamilyMembIncome to set
     */
    public void setYrlyOthFamilyMembIncome(String yrlyOthFamilyMembIncome) {
        this.yrlyOthFamilyMembIncome = yrlyOthFamilyMembIncome;
    }

    /**
     * @return the bankManagRemarks
     */
    public String getBankManagRemarks() {
        return bankManagRemarks;
    }

    /**
     * @param bankManagRemarks the bankManagRemarks to set
     */
    public void setBankManagRemarks(String bankManagRemarks) {
        this.bankManagRemarks = bankManagRemarks;
    }

    /**
     * @return the yrlyFamilyIncome
     */
    public String getYrlyFamilyIncome() {
        return yrlyFamilyIncome;
    }

    /**
     * @param yrlyFamilyIncome the yrlyFamilyIncome to set
     */
    public void setYrlyFamilyIncome(String yrlyFamilyIncome) {
        this.yrlyFamilyIncome = yrlyFamilyIncome;
    }

    /**
     * @return the bcRecomendation
     */
    public String getBcRecomendation() {
        return bcRecomendation;
    }

    /**
     * @param bcRecomendation the bcRecomendation to set
     */
    public void setBcRecomendation(String bcRecomendation) {
        this.bcRecomendation = bcRecomendation;
    }

    /**
     * @return the bcRemarks
     */
    public String getBcRemarks() {
        return bcRemarks;
    }

    /**
     * @param bcRemarks the bcRemarks to set
     */
    public void setBcRemarks(String bcRemarks) {
        this.bcRemarks = bcRemarks;
    }

    /**
     * @return the fhgName
     */
    public String getFhgName() {
        return fhgName;
    }

    /**
     * @param fhgName the fhgName to set
     */
    public void setFhgName(String fhgName) {
        this.fhgName = fhgName;
    }

    /**
     * @return the operationMode
     */
    public String getOperationMode() {
        return operationMode;
    }

    /**
     * @param operationMode the operationMode to set
     */
    public void setOperationMode(String operationMode) {
        this.operationMode = operationMode;
    }

    /**
     * @return the sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * @param sex the sex to set
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * @return the panGirNo
     */
    public String getPanGirNo() {
        return panGirNo;
    }

    /**
     * @param panGirNo the panGirNo to set
     */
    public void setPanGirNo(String panGirNo) {
        this.panGirNo = panGirNo;
    }

    /**
     * @return the dobDate
     */
    public String getDobDate() {
        return dobDate;
    }

    /**
     * @param dobDate the dobDate to set
     */
    public void setDobDate(String dobDate) {
        this.dobDate = dobDate;
    }

    /**
     * @return the annualIncome
     */
    public String getAnnualIncome() {
        return annualIncome;
    }

    /**
     * @param annualIncome the annualIncome to set
     */
    public void setAnnualIncome(String annualIncome) {
        this.annualIncome = annualIncome;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the qualification
     */
    public String getQualification() {
        return qualification;
    }

    /**
     * @param qualification the qualification to set
     */
    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    /**
     * @return the eStatement
     */
    public String geteStatement() {
        return eStatement;
    }

    /**
     * @param eStatement the eStatement to set
     */
    public void seteStatement(String eStatement) {
        this.eStatement = eStatement;
    }

    /**
     * @return the houseLoan
     */
    public String getHouseLoan() {
        return houseLoan;
    }

    /**
     * @param houseLoan the houseLoan to set
     */
    public void setHouseLoan(String houseLoan) {
        this.houseLoan = houseLoan;
    }

    /**
     * @return the chequeBook
     */
    public String getChequeBook() {
        return chequeBook;
    }

    /**
     * @param chequeBook the chequeBook to set
     */
    public void setChequeBook(String chequeBook) {
        this.chequeBook = chequeBook;
    }

    /**
     * @return the vehicleLoan
     */
    public String getVehicleLoan() {
        return vehicleLoan;
    }

    /**
     * @param vehicleLoan the vehicleLoan to set
     */
    public void setVehicleLoan(String vehicleLoan) {
        this.vehicleLoan = vehicleLoan;
    }

    /**
     * @return the mobileBanking
     */
    public String getMobileBanking() {
        return mobileBanking;
    }

    /**
     * @param mobileBanking the mobileBanking to set
     */
    public void setMobileBanking(String mobileBanking) {
        this.mobileBanking = mobileBanking;
    }

    /**
     * @return the mutualFund
     */
    public String getMutualFund() {
        return mutualFund;
    }

    /**
     * @param mutualFund the mutualFund to set
     */
    public void setMutualFund(String mutualFund) {
        this.mutualFund = mutualFund;
    }

    /**
     * @return the internetBanking
     */
    public String getInternetBanking() {
        return internetBanking;
    }

    /**
     * @param internetBanking the internetBanking to set
     */
    public void setInternetBanking(String internetBanking) {
        this.internetBanking = internetBanking;
    }

    /**
     * @return the lifeInsurance
     */
    public String getLifeInsurance() {
        return lifeInsurance;
    }

    /**
     * @param lifeInsurance the lifeInsurance to set
     */
    public void setLifeInsurance(String lifeInsurance) {
        this.lifeInsurance = lifeInsurance;
    }

    /**
     * @return the creditCard
     */
    public String getCreditCard() {
        return creditCard;
    }

    /**
     * @param creditCard the creditCard to set
     */
    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    /**
     * @return the pension
     */
    public String getPension() {
        return pension;
    }

    /**
     * @param pension the pension to set
     */
    public void setPension(String pension) {
        this.pension = pension;
    }

    /**
     * @return the addOnOthers
     */
    public String getAddOnOthers() {
        return addOnOthers;
    }

    /**
     * @param addOnOthers the addOnOthers to set
     */
    public void setAddOnOthers(String addOnOthers) {
        this.addOnOthers = addOnOthers;
    }

    /**
     * @return the crossSellOthers
     */
    public String getCrossSellOthers() {
        return crossSellOthers;
    }

    /**
     * @param crossSellOthers the crossSellOthers to set
     */
    public void setCrossSellOthers(String crossSellOthers) {
        this.crossSellOthers = crossSellOthers;
    }

    /**
     * @return the atmRequest
     */
    public String getAtmRequest() {
        return atmRequest;
    }

    /**
     * @param atmRequest the atmRequest to set
     */
    public void setAtmRequest(String atmRequest) {
        this.atmRequest = atmRequest;
    }

    /**
     * @return the smsAlert
     */
    public String getSmsAlert() {
        return smsAlert;
    }

    /**
     * @param smsAlert the smsAlert to set
     */
    public void setSmsAlert(String smsAlert) {
        this.smsAlert = smsAlert;
    }

    /**
     * @return the emailId
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * @param emailId the emailId to set
     */
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    /**
     * @return the idProofType
     */
    public String getIdProofType() {
        return idProofType;
    }

    /**
     * @param idProofType the idProofType to set
     */
    public void setIdProofType(String idProofType) {
        this.idProofType = idProofType;
    }

    /**
     * @return the addressProofType
     */
    public String getAddressProofType() {
        return addressProofType;
    }

    /**
     * @param addressProofType the addressProofType to set
     */
    public void setAddressProofType(String addressProofType) {
        this.addressProofType = addressProofType;
    }

    /**
     * @return the introducerName
     */
    public String getIntroducerName() {
        return introducerName;
    }

    /**
     * @param introducerName the introducerName to set
     */
    public void setIntroducerName(String introducerName) {
        this.introducerName = introducerName;
    }

    /**
     * @return the introducerCustId
     */
    public String getIntroducerCustId() {
        return introducerCustId;
    }

    /**
     * @param introducerCustId the introducerCustId to set
     */
    public void setIntroducerCustId(String introducerCustId) {
        this.introducerCustId = introducerCustId;
    }

    /**
     * @return the introducerAccNo
     */
    public String getIntroducerAccNo() {
        return introducerAccNo;
    }

    /**
     * @param introducerAccNo the introducerAccNo to set
     */
    public void setIntroducerAccNo(String introducerAccNo) {
        this.introducerAccNo = introducerAccNo;
    }

    /**
     * @return the nomineeAccNature
     */
    public String getNomineeAccNature() {
        return nomineeAccNature;
    }

    /**
     * @param nomineeAccNature the nomineeAccNature to set
     */
    public void setNomineeAccNature(String nomineeAccNature) {
        this.nomineeAccNature = nomineeAccNature;
    }

    /**
     * @return the nomineeAge
     */
    public String getNomineeAge() {
        return nomineeAge;
    }

    /**
     * @param nomineeAge the nomineeAge to set
     */
    public void setNomineeAge(String nomineeAge) {
        this.nomineeAge = nomineeAge;
    }

    /**
     * @return the nomineeAccNo
     */
    public String getNomineeAccNo() {
        return nomineeAccNo;
    }

    /**
     * @param nomineeAccNo the nomineeAccNo to set
     */
    public void setNomineeAccNo(String nomineeAccNo) {
        this.nomineeAccNo = nomineeAccNo;
    }

    /**
     * @return the nomineeDob
     */
    public String getNomineeDob() {
        return nomineeDob;
    }

    /**
     * @param nomineeDob the nomineeDob to set
     */
    public void setNomineeDob(String nomineeDob) {
        this.nomineeDob = nomineeDob;
    }

    /**
     * @return the nomineeAdDetails
     */
    public String getNomineeAdDetails() {
        return nomineeAdDetails;
    }

    /**
     * @param nomineeAdDetails the nomineeAdDetails to set
     */
    public void setNomineeAdDetails(String nomineeAdDetails) {
        this.nomineeAdDetails = nomineeAdDetails;
    }

    /**
     * @return the nomineeGaurdianName
     */
    public String getNomineeGaurdianName() {
        return nomineeGaurdianName;
    }

    /**
     * @param nomineeGaurdianName the nomineeGaurdianName to set
     */
    public void setNomineeGaurdianName(String nomineeGaurdianName) {
        this.nomineeGaurdianName = nomineeGaurdianName;
    }

    /**
     * @return the nomineeName
     */
    public String getNomineeName() {
        return nomineeName;
    }

    /**
     * @param nomineeName the nomineeName to set
     */
    public void setNomineeName(String nomineeName) {
        this.nomineeName = nomineeName;
    }

    /**
     * @return the nomineeGuardianAge
     */
    public String getNomineeGuardianAge() {
        return nomineeGuardianAge;
    }

    /**
     * @param nomineeGuardianAge the nomineeGuardianAge to set
     */
    public void setNomineeGuardianAge(String nomineeGuardianAge) {
        this.nomineeGuardianAge = nomineeGuardianAge;
    }

    /**
     * @return the nimineeRelation
     */
    public String getNomineeRelation() {
        return nomineeRelation;
    }

    /**
     * @param nimineeRelation the nimineeRelation to set
     */
    public void setNomineeRelation(String nomineeRelation) {
        this.nomineeRelation = nomineeRelation;
    }

    /**
     * @return the nomineeGuardianAdd
     */
    public String getNomineeGuardianAdd() {
        return nomineeGuardianAdd;
    }

    /**
     * @param nomineeGuardianAdd the nomineeGuardianAdd to set
     */
    public void setNomineeGuardianAdd(String nomineeGuardianAdd) {
        this.nomineeGuardianAdd = nomineeGuardianAdd;
    }

    /**
     * @return the witness1Name
     */
    public String getWitness1Name() {
        return witness1Name;
    }

    /**
     * @param witness1Name the witness1Name to set
     */
    public void setWitness1Name(String witness1Name) {
        this.witness1Name = witness1Name;
    }

    /**
     * @return the witness2Name
     */
    public String getWitness2Name() {
        return witness2Name;
    }

    /**
     * @param witness2Name the witness2Name to set
     */
    public void setWitness2Name(String witness2Name) {
        this.witness2Name = witness2Name;
    }

    /**
     * @return the witness1Add
     */
    public String getWitness1Add() {
        return witness1Add;
    }

    /**
     * @param witness1Add the witness1Add to set
     */
    public void setWitness1Add(String witness1Add) {
        this.witness1Add = witness1Add;
    }

    /**
     * @return the witness2Add
     */
    public String getWitness2Add() {
        return witness2Add;
    }

    /**
     * @param witness2Add the witness2Add to set
     */
    public void setWitness2Add(String witness2Add) {
        this.witness2Add = witness2Add;
    }

    /**
     * @return the witness1Place
     */
    public String getWitness1Place() {
        return witness1Place;
    }

    /**
     * @param witness1Place the witness1Place to set
     */
    public void setWitness1Place(String witness1Place) {
        this.witness1Place = witness1Place;
    }

    /**
     * @return the witness2Place
     */
    public String getWitness2Place() {
        return witness2Place;
    }

    /**
     * @param witness2Place the witness2Place to set
     */
    public void setWitness2Place(String witness2Place) {
        this.witness2Place = witness2Place;
    }

    /**
     * @return the witness1Telephone
     */
    public String getWitness1Telephone() {
        return witness1Telephone;
    }

    /**
     * @param witness1Telephone the witness1Telephone to set
     */
    public void setWitness1Telephone(String witness1Telephone) {
        this.witness1Telephone = witness1Telephone;
    }

    /**
     * @return the witness2Telephone
     */
    public String getWitness2Telephone() {
        return witness2Telephone;
    }

    /**
     * @param witness2Telephone the witness2Telephone to set
     */
    public void setWitness2Telephone(String witness2Telephone) {
        this.witness2Telephone = witness2Telephone;
    }

    /**
     * @return the appl2Name
     */
    public String getAppl2Name() {
        return appl2Name;
    }

    /**
     * @param appl2Name the appl2Name to set
     */
    public void setAppl2Name(String appl2Name) {
        this.appl2Name = appl2Name;
    }

    /**
     * @return the appl2Pincode
     */
    public String getAppl2Pincode() {
        return appl2Pincode;
    }

    /**
     * @param appl2Pincode the appl2Pincode to set
     */
    public void setAppl2Pincode(String appl2Pincode) {
        this.appl2Pincode = appl2Pincode;
    }

    /**
     * @return the appl2FhgName
     */
    public String getAppl2FhgName() {
        return appl2FhgName;
    }

    /**
     * @param appl2FhgName the appl2FhgName to set
     */
    public void setAppl2FhgName(String appl2FhgName) {
        this.appl2FhgName = appl2FhgName;
    }

    /**
     * @return the appl2Telephone
     */
    public String getAppl2Telephone() {
        return appl2Telephone;
    }

    /**
     * @param appl2Telephone the appl2Telephone to set
     */
    public void setAppl2Telephone(String appl2Telephone) {
        this.appl2Telephone = appl2Telephone;
    }

    /**
     * @return the appl2Careof
     */
    public String getAppl2Careof() {
        return appl2Careof;
    }

    /**
     * @param appl2Careof the appl2Careof to set
     */
    public void setAppl2Careof(String appl2Careof) {
        this.appl2Careof = appl2Careof;
    }

    /**
     * @return the appl2Mobile
     */
    public String getAppl2Mobile() {
        return appl2Mobile;
    }

    /**
     * @param appl2Mobile the appl2Mobile to set
     */
    public void setAppl2Mobile(String appl2Mobile) {
        this.appl2Mobile = appl2Mobile;
    }

    /**
     * @return the appl2HouseNo
     */
    public String getAppl2HouseNo() {
        return appl2HouseNo;
    }

    /**
     * @param appl2HouseNo the appl2HouseNo to set
     */
    public void setAppl2HouseNo(String appl2HouseNo) {
        this.appl2HouseNo = appl2HouseNo;
    }

    /**
     * @return the appl2Sex
     */
    public String getAppl2Sex() {
        return appl2Sex;
    }

    /**
     * @param appl2Sex the appl2Sex to set
     */
    public void setAppl2Sex(String appl2Sex) {
        this.appl2Sex = appl2Sex;
    }

    /**
     * @return the appl2StreetNo
     */
    public String getAppl2StreetNo() {
        return appl2StreetNo;
    }

    /**
     * @param appl2StreetNo the appl2StreetNo to set
     */
    public void setAppl2StreetNo(String appl2StreetNo) {
        this.appl2StreetNo = appl2StreetNo;
    }

    /**
     * @return the appl2Dob
     */
    public String getAppl2Dob() {
        return appl2Dob;
    }

    /**
     * @param appl2Dob the appl2Dob to set
     */
    public void setAppl2Dob(String appl2Dob) {
        this.appl2Dob = appl2Dob;
    }

    /**
     * @return the appl2Landmark
     */
    public String getAppl2Landmark() {
        return appl2Landmark;
    }

    /**
     * @param appl2Landmark the appl2Landmark to set
     */
    public void setAppl2Landmark(String appl2Landmark) {
        this.appl2Landmark = appl2Landmark;
    }

    /**
     * @return the appl2Occupation
     */
    public String getAppl2Occupation() {
        return appl2Occupation;
    }

    /**
     * @param appl2Occupation the appl2Occupation to set
     */
    public void setAppl2Occupation(String appl2Occupation) {
        this.appl2Occupation = appl2Occupation;
    }

    /**
     * @return the appl2VillCity
     */
    public String getAppl2VillCity() {
        return appl2VillCity;
    }

    /**
     * @param appl2VillCity the appl2VillCity to set
     */
    public void setAppl2VillCity(String appl2VillCity) {
        this.appl2VillCity = appl2VillCity;
    }

    /**
     * @return the appl2Category
     */
    public String getAppl2Category() {
        return appl2Category;
    }

    /**
     * @param appl2Category the appl2Category to set
     */
    public void setAppl2Category(String appl2Category) {
        this.appl2Category = appl2Category;
    }

    /**
     * @return the appl2District
     */
    public String getAppl2District() {
        return appl2District;
    }

    /**
     * @param appl2District the appl2District to set
     */
    public void setAppl2District(String appl2District) {
        this.appl2District = appl2District;
    }

    /**
     * @return the appl2IdProof
     */
    public String getAppl2IdProof() {
        return appl2IdProof;
    }

    /**
     * @param appl2IdProof the appl2IdProof to set
     */
    public void setAppl2IdProof(String appl2IdProof) {
        this.appl2IdProof = appl2IdProof;
    }

    /**
     * @return the appl2State
     */
    public String getAppl2State() {
        return appl2State;
    }

    /**
     * @param appl2State the appl2State to set
     */
    public void setAppl2State(String appl2State) {
        this.appl2State = appl2State;
    }

    /**
     * @return the appl2AddProof
     */
    public String getAppl2AddProof() {
        return appl2AddProof;
    }

    /**
     * @param appl2AddProof the appl2AddProof to set
     */
    public void setAppl2AddProof(String appl2AddProof) {
        this.appl2AddProof = appl2AddProof;
    }

    /**
     * @return the familyMemb1Name
     */
    public String getFamilyMemb1Name() {
        return familyMemb1Name;
    }

    /**
     * @param familyMemb1Name the familyMemb1Name to set
     */
    public void setFamilyMemb1Name(String familyMemb1Name) {
        this.familyMemb1Name = familyMemb1Name;
    }

    /**
     * @return the familyMemb1Relation
     */
    public String getFamilyMemb1Relation() {
        return familyMemb1Relation;
    }

    /**
     * @param familyMemb1Relation the familyMemb1Relation to set
     */
    public void setFamilyMemb1Relation(String familyMemb1Relation) {
        this.familyMemb1Relation = familyMemb1Relation;
    }

    /**
     * @return the familyMemb1Sex
     */
    public String getFamilyMemb1Sex() {
        return familyMemb1Sex;
    }

    /**
     * @param familyMemb1Sex the familyMemb1Sex to set
     */
    public void setFamilyMemb1Sex(String familyMemb1Sex) {
        this.familyMemb1Sex = familyMemb1Sex;
    }

    /**
     * @return the familyMemb1Age
     */
    public String getFamilyMemb1Age() {
        return familyMemb1Age;
    }

    /**
     * @param familyMemb1Age the familyMemb1Age to set
     */
    public void setFamilyMemb1Age(String familyMemb1Age) {
        this.familyMemb1Age = familyMemb1Age;
    }

    /**
     * @return the familyMemb1Occup
     */
    public String getFamilyMemb1Occup() {
        return familyMemb1Occup;
    }

    /**
     * @param familyMemb1Occup the familyMemb1Occup to set
     */
    public void setFamilyMemb1Occup(String familyMemb1Occup) {
        this.familyMemb1Occup = familyMemb1Occup;
    }

    /**
     * @return the familyMemb1Qualification
     */
    public String getFamilyMemb1Qualification() {
        return familyMemb1Qualification;
    }

    /**
     * @param familyMemb1Qualification the familyMemb1Qualification to set
     */
    public void setFamilyMemb1Qualification(String familyMemb1Qualification) {
        this.familyMemb1Qualification = familyMemb1Qualification;
    }

    /**
     * @return the familyMemb2Name
     */
    public String getFamilyMemb2Name() {
        return familyMemb2Name;
    }

    /**
     * @param familyMemb2Name the familyMemb2Name to set
     */
    public void setFamilyMemb2Name(String familyMemb2Name) {
        this.familyMemb2Name = familyMemb2Name;
    }

    /**
     * @return the familyMemb2Relation
     */
    public String getFamilyMemb2Relation() {
        return familyMemb2Relation;
    }

    /**
     * @param familyMemb2Relation the familyMemb2Relation to set
     */
    public void setFamilyMemb2Relation(String familyMemb2Relation) {
        this.familyMemb2Relation = familyMemb2Relation;
    }

    /**
     * @return the familyMemb2Sex
     */
    public String getFamilyMemb2Sex() {
        return familyMemb2Sex;
    }

    /**
     * @param familyMemb2Sex the familyMemb2Sex to set
     */
    public void setFamilyMemb2Sex(String familyMemb2Sex) {
        this.familyMemb2Sex = familyMemb2Sex;
    }

    /**
     * @return the familyMemb2Age
     */
    public String getFamilyMemb2Age() {
        return familyMemb2Age;
    }

    /**
     * @param familyMemb2Age the familyMemb2Age to set
     */
    public void setFamilyMemb2Age(String familyMemb2Age) {
        this.familyMemb2Age = familyMemb2Age;
    }

    /**
     * @return the familyMemb2Occup
     */
    public String getFamilyMemb2Occup() {
        return familyMemb2Occup;
    }

    /**
     * @param familyMemb2Occup the familyMemb2Occup to set
     */
    public void setFamilyMemb2Occup(String familyMemb2Occup) {
        this.familyMemb2Occup = familyMemb2Occup;
    }

    /**
     * @return the familyMemb2Qualification
     */
    public String getFamilyMemb2Qualification() {
        return familyMemb2Qualification;
    }

    /**
     * @param familyMemb2Qualification the familyMemb2Qualification to set
     */
    public void setFamilyMemb2Qualification(String familyMemb2Qualification) {
        this.familyMemb2Qualification = familyMemb2Qualification;
    }

    /**
     * @return the familyMemb3Name
     */
    public String getFamilyMemb3Name() {
        return familyMemb3Name;
    }

    /**
     * @param familyMemb3Name the familyMemb3Name to set
     */
    public void setFamilyMemb3Name(String familyMemb3Name) {
        this.familyMemb3Name = familyMemb3Name;
    }

    /**
     * @return the familyMemb3Relation
     */
    public String getFamilyMemb3Relation() {
        return familyMemb3Relation;
    }

    /**
     * @param familyMemb3Relation the familyMemb3Relation to set
     */
    public void setFamilyMemb3Relation(String familyMemb3Relation) {
        this.familyMemb3Relation = familyMemb3Relation;
    }

    /**
     * @return the familyMemb3Sex
     */
    public String getFamilyMemb3Sex() {
        return familyMemb3Sex;
    }

    /**
     * @param familyMemb3Sex the familyMemb3Sex to set
     */
    public void setFamilyMemb3Sex(String familyMemb3Sex) {
        this.familyMemb3Sex = familyMemb3Sex;
    }

    /**
     * @return the familyMemb3Age
     */
    public String getFamilyMemb3Age() {
        return familyMemb3Age;
    }

    /**
     * @param familyMemb3Age the familyMemb3Age to set
     */
    public void setFamilyMemb3Age(String familyMemb3Age) {
        this.familyMemb3Age = familyMemb3Age;
    }

    /**
     * @return the familyMemb3Occup
     */
    public String getFamilyMemb3Occup() {
        return familyMemb3Occup;
    }

    /**
     * @param familyMemb3Occup the familyMemb3Occup to set
     */
    public void setFamilyMemb3Occup(String familyMemb3Occup) {
        this.familyMemb3Occup = familyMemb3Occup;
    }

    /**
     * @return the familyMemb3Qualification
     */
    public String getFamilyMemb3Qualification() {
        return familyMemb3Qualification;
    }

    /**
     * @param familyMemb3Qualification the familyMemb3Qualification to set
     */
    public void setFamilyMemb3Qualification(String familyMemb3Qualification) {
        this.familyMemb3Qualification = familyMemb3Qualification;
    }

    /**
     * @return the assetAmountDue
     */
    public String getLoanAmountDue() {
        return loanAmountDue;
    }

    /**
     * @param assetAmountDue the assetAmountDue to set
     */
    public void setLoanAmountDue(String loanAmountDue) {
        this.loanAmountDue = loanAmountDue;
    }

    /**
     * @return the liveStock
     */
    public String getLiveStock() {
        return liveStock;
    }

    /**
     * @param liveStock the liveStock to set
     */
    public void setLiveStock(String liveStock) {
        this.liveStock = liveStock;
    }

    /**
     * @return the agrlImpl
     */
    public String getAgrlImpl() {
        return agrlImpl;
    }

    /**
     * @param agrlImpl the agrlImpl to set
     */
    public void setAgrlImpl(String agrlImpl) {
        this.agrlImpl = agrlImpl;
    }

    /**
     * @return the activity
     */
    public String getActivity() {
        return activity;
    }

    /**
     * @param activity the activity to set
     */
    public void setActivity(String activity) {
        this.activity = activity;
    }

    /**
     * @return the assetVehicle
     */
    public String getAssetVehicle() {
        return assetVehicle;
    }

    /**
     * @param assetVehicle the assetVehicle to set
     */
    public void setAssetVehicle(String assetVehicle) {
        this.assetVehicle = assetVehicle;
    }

    /**
     * @return the loanReuired
     */
    public String getLoanReuired() {
        return loanReuired;
    }

    /**
     * @param loanReuired the loanReuired to set
     */
    public void setLoanReuired(String loanReuired) {
        this.loanReuired = loanReuired;
    }

    /**
     * @return the assetAnyOther
     */
    public String getAssetAnyOther() {
        return assetAnyOther;
    }

    /**
     * @param assetAnyOther the assetAnyOther to set
     */
    public void setAssetAnyOther(String assetAnyOther) {
        this.assetAnyOther = assetAnyOther;
    }

    /**
     * @return the loanAccNomineeName
     */
    public String getLoanAccNomineeName() {
        return loanAccNomineeName;
    }

    /**
     * @param loanAccNomineeName the loanAccNomineeName to set
     */
    public void setLoanAccNomineeName(String loanAccNomineeName) {
        this.loanAccNomineeName = loanAccNomineeName;
    }

    /**
     * @return the loanSource
     */
    public String getLoanSource() {
        return loanSource;
    }

    /**
     * @param loanSource the loanSource to set
     */
    public void setLoanSource(String loanSource) {
        this.loanSource = loanSource;
    }

    /**
     * @return the loanAccNomineeRelation
     */
    public String getLoanAccNomineeRelation() {
        return loanAccNomineeRelation;
    }

    /**
     * @param loanAccNomineeRelation the loanAccNomineeRelation to set
     */
    public void setLoanAccNomineeRelation(String loanAccNomineeRelation) {
        this.loanAccNomineeRelation = loanAccNomineeRelation;
    }

    /**
     * @return the loanPurpose
     */
    public String getLoanPurpose() {
        return loanPurpose;
    }

    /**
     * @param loanPurpose the loanPurpose to set
     */
    public void setLoanPurpose(String loanPurpose) {
        this.loanPurpose = loanPurpose;
    }

    /**
     * @return the loanAccNomineeAge
     */
    public String getLoanAccNomineeAge() {
        return loanAccNomineeAge;
    }

    /**
     * @param loanAccNomineeAge the loanAccNomineeAge to set
     */
    public void setLoanAccNomineeAge(String loanAccNomineeAge) {
        this.loanAccNomineeAge = loanAccNomineeAge;
    }

    /**
     * @return the amountBorrowed
     */
    public String getAmountBorrowed() {
        return amountBorrowed;
    }

    /**
     * @param amountBorrowed the amountBorrowed to set
     */
    public void setAmountBorrowed(String amountBorrowed) {
        this.amountBorrowed = amountBorrowed;
    }

    /**
     * @return the loanAccNomineeResident
     */
    public String getLoanAccNomineeResident() {
        return loanAccNomineeResident;
    }

    /**
     * @param loanAccNomineeResident the loanAccNomineeResident to set
     */
    public void setLoanAccNomineeResident(String loanAccNomineeResident) {
        this.loanAccNomineeResident = loanAccNomineeResident;
    }

    /**
     * @return the custInfo
     */
    public String getCustInfo() {
        return custInfo;
    }

    /**
     * @param custInfo the custInfo to set
     */
    public void setCustInfo(String custInfo) {
        this.custInfo = custInfo;
    }

    /**
     * @return the newCust
     */
    public String getNewCust() {
        return newCust;
    }

    /**
     * @param newCust the newCust to set
     */
    public void setNewCust(String newCust) {
        this.newCust = newCust;
    }

    /**
     * @return the avgWeeklySurplus
     */
    public String getAvgWeeklySurplus() {
        return avgWeeklySurplus;
    }

    /**
     * @param avgWeeklySurplus the avgWeeklySurplus to set
     */
    public void setAvgWeeklySurplus(String avgWeeklySurplus) {
        this.avgWeeklySurplus = avgWeeklySurplus;
    }

    /**
     * @return the weeklyInstalment
     */
    public String getWeeklyInstalment() {
        return weeklyInstalment;
    }

    /**
     * @param weeklyInstalment the weeklyInstalment to set
     */
    public void setWeeklyInstalment(String weeklyInstalment) {
        this.weeklyInstalment = weeklyInstalment;
    }

    /**
     * @return the jlgGroupName
     */
    public String getJlgGroupName() {
        return jlgGroupName;
    }

    /**
     * @param jlgGroupName the jlgGroupName to set
     */
    public void setJlgGroupName(String jlgGroupName) {
        this.jlgGroupName = jlgGroupName;
    }

    /**
     * @return the jlgLeader
     */
    public String getJlgLeader() {
        return jlgLeader;
    }

    /**
     * @param jlgLeader the jlgLeader to set
     */
    public void setJlgLeader(String jlgLeader) {
        this.jlgLeader = jlgLeader;
    }

    /**
     * @return the custId
     */
    public String getCustId() {
        return custId;
    }

    /**
     * @param custId the custId to set
     */
    public void setCustId(String custId) {
        this.custId = custId;
    }

    /**
     * @return the custTitle
     */
    public String getCustTitle() {
        return custTitle;
    }

    /**
     * @param custTitle the custTitle to set
     */
    public void setCustTitle(String custTitle) {
        this.custTitle = custTitle;
    }

    /**
     * @return the commHouseNo
     */
    public String getCommHouseNo() {
        return commHouseNo;
    }

    /**
     * @param commHouseNo the commHouseNo to set
     */
    public void setCommHouseNo(String commHouseNo) {
        this.commHouseNo = commHouseNo;
    }

    /**
     * @return the commStreetNo
     */
    public String getCommStreetNo() {
        return commStreetNo;
    }

    /**
     * @param commStreetNo the commStreetNo to set
     */
    public void setCommStreetNo(String commStreetNo) {
        this.commStreetNo = commStreetNo;
    }

    /**
     * @return the commLandMark
     */
    public String getCommLandMark() {
        return commLandMark;
    }

    /**
     * @param commLandMark the commLandMark to set
     */
    public void setCommLandMark(String commLandMark) {
        this.commLandMark = commLandMark;
    }

    /**
     * @return the commVillCity
     */
    public String getCommVillCity() {
        return commVillCity;
    }

    /**
     * @param commVillCity the commVillCity to set
     */
    public void setCommVillCity(String commVillCity) {
        this.commVillCity = commVillCity;
    }

    /**
     * @return the commDistrict
     */
    public String getCommDistrict() {
        return commDistrict;
    }

    /**
     * @param commDistrict the commDistrict to set
     */
    public void setCommDistrict(String commDistrict) {
        this.commDistrict = commDistrict;
    }

    /**
     * @return the commState
     */
    public String getCommState() {
        return commState;
    }

    /**
     * @param commState the commState to set
     */
    public void setCommState(String commState) {
        this.commState = commState;
    }

    /**
     * @return the commPinCode
     */
    public String getCommPinCode() {
        return commPinCode;
    }

    /**
     * @param commPinCode the commPinCode to set
     */
    public void setCommPinCode(String commPinCode) {
        this.commPinCode = commPinCode;
    }

    /**
     * @return the commTelephone
     */
    public String getCommTelephone() {
        return commTelephone;
    }

    /**
     * @param commTelephone the commTelephone to set
     */
    public void setCommTelephone(String commTelephone) {
        this.commTelephone = commTelephone;
    }

    /**
     * @return the introducerTitle
     */
    public String getIntroducerTitle() {
        return introducerTitle;
    }

    /**
     * @param introducerTitle the introducerTitle to set
     */
    public void setIntroducerTitle(String introducerTitle) {
        this.introducerTitle = introducerTitle;
    }

    /**
     * @return the rightThumb
     */
    public String getRightThumb() {
        return rightThumb;
    }

    /**
     * @param rightThumb the rightThumb to set
     */
    public void setRightThumb(String rightThumb) {
        this.rightThumb = rightThumb;
    }

    /**
     * @return the rightIndex
     */
    public String getRightIndex() {
        return rightIndex;
    }

    /**
     * @param rightIndex the rightIndex to set
     */
    public void setRightIndex(String rightIndex) {
        this.rightIndex = rightIndex;
    }

    /**
     * @return the rightMiddle
     */
    public String getRightMiddle() {
        return rightMiddle;
    }

    /**
     * @param rightMiddle the rightMiddle to set
     */
    public void setRightMiddle(String rightMiddle) {
        this.rightMiddle = rightMiddle;
    }

    /**
     * @return the rightRing
     */
    public String getRightRing() {
        return rightRing;
    }

    /**
     * @param rightRing the rightRing to set
     */
    public void setRightRing(String rightRing) {
        this.rightRing = rightRing;
    }

    /**
     * @return the rightSmall
     */
    public String getRightSmall() {
        return rightSmall;
    }

    /**
     * @param rightSmall the rightSmall to set
     */
    public void setRightSmall(String rightSmall) {
        this.rightSmall = rightSmall;
    }

    /**
     * @return the leftThumb
     */
    public String getLeftThumb() {
        return leftThumb;
    }

    /**
     * @param leftThumb the leftThumb to set
     */
    public void setLeftThumb(String leftThumb) {
        this.leftThumb = leftThumb;
    }

    /**
     * @return the leftIndex
     */
    public String getLeftIndex() {
        return leftIndex;
    }

    /**
     * @param leftIndex the leftIndex to set
     */
    public void setLeftIndex(String leftIndex) {
        this.leftIndex = leftIndex;
    }

    /**
     * @return the leftMiddle
     */
    public String getLeftMiddle() {
        return leftMiddle;
    }

    /**
     * @param leftMiddle the leftMiddle to set
     */
    public void setLeftMiddle(String leftMiddle) {
        this.leftMiddle = leftMiddle;
    }

    /**
     * @return the leftRing
     */
    public String getLeftRing() {
        return leftRing;
    }

    /**
     * @param leftRing the leftRing to set
     */
    public void setLeftRing(String leftRing) {
        this.leftRing = leftRing;
    }

    /**
     * @return the leftSmall
     */
    public String getLeftSmall() {
        return leftSmall;
    }

    /**
     * @param leftSmall the leftSmall to set
     */
    public void setLeftSmall(String leftSmall) {
        this.leftSmall = leftSmall;
    }

    /**
     * @return the BcoMobile
     */
    public String getBcoMobile() {
        return BcoMobile;
    }

    /**
     * @param BcoMobile the BcoMobile to set
     */
    public void setBcoMobile(String BcoMobile) {
        this.BcoMobile = BcoMobile;
    }

    /**
     * @return the BcoPin
     */
    public String getBcoPin() {
        return BcoPin;
    }

    /**
     * @param BcoPin the BcoPin to set
     */
    public void setBcoPin(String BcoPin) {
        this.BcoPin = BcoPin;
    }

    /**
     * @return the idProofFile
     */
    public String getIdProofFile() {
        return idProofFile;
    }

    /**
     * @param idProofFile the idProofFile to set
     */
    public void setIdProofFile(String idProofFile) {
        this.idProofFile = idProofFile;
    }

    /**
     * @return the addressProofFile
     */
    public String getAddressProofFile() {
        return addressProofFile;
    }

    /**
     * @param addressProofFile the addressProofFile to set
     */
    public void setAddressProofFile(String addressProofFile) {
        this.addressProofFile = addressProofFile;
    }

    /**
     * @return the annualTurnover
     */
    public String getAnnualTurnover() {
        return annualTurnover;
    }

    /**
     * @param annualTurnover the annualTurnover to set
     */
    public void setAnnualTurnover(String annualTurnover) {
        this.annualTurnover = annualTurnover;
    }

    /**
     * @return the blockCode
     */
    public String getBlockCode() {
        return blockCode;
    }

    /**
     * @param blockCode the blockCode to set
     */
    public void setBlockCode(String blockCode) {
        this.blockCode = blockCode;
    }

    /**
     * @return the custTitleCode
     */
    public String getCustTitleCode() {
        return custTitleCode;
    }

    /**
     * @param custTitleCode the custTitleCode to set
     */
    public void setCustTitleCode(String custTitleCode) {
        this.custTitleCode = custTitleCode;
    }

    /**
     * @return the custPin
     */
    public String getCustPin() {
        return custPin;
    }

    /**
     * @param custPin the custPin to set
     */
    public void setCustPin(String custPin) {
        this.custPin = custPin;
    }

    /**
     * @return the confirmCustPin
     */
    public String getConfirmCustPin() {
        return confirmCustPin;
    }

    /**
     * @param confirmCustPin the confirmCustPin to set
     */
    public void setConfirmCustPin(String confirmCustPin) {
        this.confirmCustPin = confirmCustPin;
    }

    /**
     * @return the permHouseNo
     */
    public String getPermHouseNo() {
        return permHouseNo;
    }

    /**
     * @param permHouseNo the permHouseNo to set
     */
    public void setPermHouseNo(String permHouseNo) {
        this.permHouseNo = permHouseNo;
    }

    /**
     * @return the permStreetNo
     */
    public String getPermStreetNo() {
        return permStreetNo;
    }

    /**
     * @param permStreetNo the permStreetNo to set
     */
    public void setPermStreetNo(String permStreetNo) {
        this.permStreetNo = permStreetNo;
    }

    /**
     * @return the permLandMark
     */
    public String getPermLandMark() {
        return permLandMark;
    }

    /**
     * @param permLandMark the permLandMark to set
     */
    public void setPermLandMark(String permLandMark) {
        this.permLandMark = permLandMark;
    }

    /**
     * @return the permVillCity
     */
    public String getPermVillCity() {
        return permVillCity;
    }

    /**
     * @param permVillCity the permVillCity to set
     */
    public void setPermVillCity(String permVillCity) {
        this.permVillCity = permVillCity;
    }

    /**
     * @return the permDistrict
     */
    public String getPermDistrict() {
        return permDistrict;
    }

    /**
     * @param permDistrict the permDistrict to set
     */
    public void setPermDistrict(String permDistrict) {
        this.permDistrict = permDistrict;
    }

    /**
     * @return the permState
     */
    public String getPermState() {
        return permState;
    }

    /**
     * @param permState the permState to set
     */
    public void setPermState(String permState) {
        this.permState = permState;
    }

    /**
     * @return the permPinCode
     */
    public String getPermPinCode() {
        return permPinCode;
    }

    /**
     * @param permPinCode the permPinCode to set
     */
    public void setPermPinCode(String permPinCode) {
        this.permPinCode = permPinCode;
    }

    /**
     * @return the applicantAddress
     */
    public String getApplicantAddress() {
        return applicantAddress;
    }

    /**
     * @param applicantAddress the applicantAddress to set
     */
    public void setApplicantAddress(String applicantAddress) {
        this.applicantAddress = applicantAddress;
    }

    /**
     * @return the nomineeDetails
     */
    public String getNomineeDetails() {
        return nomineeDetails;
    }

    /**
     * @param nomineeDetails the nomineeDetails to set
     */
    public void setNomineeDetails(String nomineeDetails) {
        this.nomineeDetails = nomineeDetails;
    }

    /**
     * @return the knownYear
     */
    public String getIntroducerKnownYear() {
        return introducerKnownYear;
    }

    /**
     * @param knownYear the knownYear to set
     */
    public void setIntroducerKnownYear(String introducerKnownYear) {
        this.introducerKnownYear = introducerKnownYear;
    }

    /**
     * @return the minorNomiGuarTitle
     */
    public String getMinorNomiGuarTitle() {
        return minorNomiGuarTitle;
    }

    /**
     * @param minorNomiGuarTitle the minorNomiGuarTitle to set
     */
    public void setMinorNomiGuarTitle(String minorNomiGuarTitle) {
        this.minorNomiGuarTitle = minorNomiGuarTitle;
    }

    /**
     * @return the minorNomiGuarName
     */
    public String getMinorNomiGuarName() {
        return minorNomiGuarName;
    }

    /**
     * @param minorNomiGuarName the minorNomiGuarName to set
     */
    public void setMinorNomiGuarName(String minorNomiGuarName) {
        this.minorNomiGuarName = minorNomiGuarName;
    }

    /**
     * @return the minorNomiGuarRelType
     */
    public String getMinorNomiGuarRelType() {
        return minorNomiGuarRelType;
    }

    /**
     * @param minorNomiGuarRelType the minorNomiGuarRelType to set
     */
    public void setMinorNomiGuarRelType(String minorNomiGuarRelType) {
        this.minorNomiGuarRelType = minorNomiGuarRelType;
    }

    /**
     * @return the minorNomiGuardRelCode
     */
    public String getMinorNomiGuarRelCode() {
        return minorNomiGuarRelCode;
    }

    /**
     * @param minorNomiGuardRelCode the minorNomiGuardRelCode to set
     */
    public void setMinorNomiGuarRelCode(String minorNomiGuarRelCode) {
        this.minorNomiGuarRelCode = minorNomiGuarRelCode;
    }

    /**
     * @return the savingACInfo
     */
    public String getSavingACInfo() {
        return savingACInfo;
    }

    /**
     * @param savingACInfo the savingACInfo to set
     */
    public void setSavingACInfo(String savingACInfo) {
        this.savingACInfo = savingACInfo;
    }

    /**
     * @return the areaBlockCode
     */
    public String getAreaBlockCode() {
        return areaBlockCode;
    }

    /**
     * @param areaBlockCode the areaBlockCode to set
     */
    public void setAreaBlockCode(String areaBlockCode) {
        this.areaBlockCode = areaBlockCode;
    }

    /**
     * @return the areaDistCode
     */
    public String getAreaDistCode() {
        return areaDistCode;
    }

    /**
     * @param areaDistCode the areaDistCode to set
     */
    public void setAreaDistCode(String areaDistCode) {
        this.areaDistCode = areaDistCode;
    }

    /**
     * @return the areaStateCode
     */
    public String getAreaStateCode() {
        return areaStateCode;
    }

    /**
     * @param areaStateCode the areaStateCode to set
     */
    public void setAreaStateCode(String areaStateCode) {
        this.areaStateCode = areaStateCode;
    }

    /**
     * @return the bcoId
     */
    public String getBcoId() {
        return bcoId;
    }

    /**
     * @param bcoId the bcoId to set
     */
    public void setBcoId(String bcoId) {
        this.bcoId = bcoId;
    }

    /**
     * @return the bcoName
     */
    public String getBcoName() {
        return bcoName;
    }

    /**
     * @param bcoName the bcoName to set
     */
    public void setBcoName(String bcoName) {
        this.bcoName = bcoName;
    }

    /**
     * @return the isFPCaptured
     */
    public String getIsFPCaptured() {
        return isFPCaptured;
    }

    /**
     * @param isFPCaptured the isFPCaptured to set
     */
    public void setIsFPCaptured(String isFPCaptured) {
        this.isFPCaptured = isFPCaptured;
    }

    /**
     * @return the appl1Address
     */
    public String getAppl1Address() {
        return appl1Address;
    }

    /**
     * @param appl1Address the appl1Address to set
     */
    public void setAppl1Address(String appl1Address) {
        this.appl1Address = appl1Address;
    }

    /**
     * @return the nomineeAddress
     */
    public String getNomineeAddress() {
        return nomineeAddress;
    }

    /**
     * @param nomineeAddress the nomineeAddress to set
     */
    public void setNomineeAddress(String nomineeAddress) {
        this.nomineeAddress = nomineeAddress;
    }

    /**
     * @return the dbAction
     */
    public String getDbAction() {
        return dbAction;
    }

    /**
     * @param dbAction the dbAction to set
     */
    public void setDbAction(String dbAction) {
        this.dbAction = dbAction;
    }

    /**
     * @return the srcOfIncomeDesc
     */
    public String getSrcOfIncomeDesc() {
        return srcOfIncomeDesc;
    }

    /**
     * @param srcOfIncomeDesc the srcOfIncomeDesc to set
     */
    public void setSrcOfIncomeDesc(String srcOfIncomeDesc) {
        this.srcOfIncomeDesc = srcOfIncomeDesc;
    }

    /**
     * @return the srcOfIncomeCode
     */
    public String getSrcOfIncomeCode() {
        return srcOfIncomeCode;
    }

    /**
     * @param srcOfIncomeCode the srcOfIncomeCode to set
     */
    public void setSrcOfIncomeCode(String srcOfIncomeCode) {
        this.srcOfIncomeCode = srcOfIncomeCode;
    }

    /**
     * @return the networthDesc
     */
    public String getNetworthDesc() {
        return networthDesc;
    }

    /**
     * @param networthDesc the networthDesc to set
     */
    public void setNetworthDesc(String networthDesc) {
        this.networthDesc = networthDesc;
    }

    /**
     * @return the networthCode
     */
    public String getNetworthCode() {
        return networthCode;
    }

    /**
     * @param networthCode the networthCode to set
     */
    public void setNetworthCode(String networthCode) {
        this.networthCode = networthCode;
    }

    /**
     * @return the annualTurnoverDesc
     */
    public String getAnnualTurnoverDesc() {
        return annualTurnoverDesc;
    }

    /**
     * @param annualTurnoverDesc the annualTurnoverDesc to set
     */
    public void setAnnualTurnoverDesc(String annualTurnoverDesc) {
        this.annualTurnoverDesc = annualTurnoverDesc;
    }

    /**
     * @return the annualTurnoverCode
     */
    public String getAnnualTurnoverCode() {
        return annualTurnoverCode;
    }

    /**
     * @param annualTurnoverCode the annualTurnoverCode to set
     */
    public void setAnnualTurnoverCode(String annualTurnoverCode) {
        this.annualTurnoverCode = annualTurnoverCode;
    }

    /**
     * @return the maritalStatutDesc
     */
    public String getMaritalStatutDesc() {
        return maritalStatutDesc;
    }

    /**
     * @param maritalStatutDesc the maritalStatutDesc to set
     */
    public void setMaritalStatutDesc(String maritalStatutDesc) {
        this.maritalStatutDesc = maritalStatutDesc;
    }

    /**
     * @return the maritalStatutCode
     */
    public String getMaritalStatutCode() {
        return maritalStatutCode;
    }

    /**
     * @param maritalStatutCode the maritalStatutCode to set
     */
    public void setMaritalStatutCode(String maritalStatutCode) {
        this.maritalStatutCode = maritalStatutCode;
    }

    /**
     * @return the glSubheadDesc
     */
    public String getGlSubheadDesc() {
        return glSubheadDesc;
    }

    /**
     * @param glSubheadDesc the glSubheadDesc to set
     */
    public void setGlSubheadDesc(String glSubheadDesc) {
        this.glSubheadDesc = glSubheadDesc;
    }

    /**
     * @return the glSubheadCode
     */
    public String getGlSubheadCode() {
        return glSubheadCode;
    }

    /**
     * @param glSubheadCode the glSubheadCode to set
     */
    public void setGlSubheadCode(String glSubheadCode) {
        this.glSubheadCode = glSubheadCode;
    }

    /**
     * @return the constitutionDesc
     */
    public String getConstitutionDesc() {
        return constitutionDesc;
    }

    /**
     * @param constitutionDesc the constitutionDesc to set
     */
    public void setConstitutionDesc(String constitutionDesc) {
        this.constitutionDesc = constitutionDesc;
    }

    /**
     * @return the constitutionCode
     */
    public String getConstitutionCode() {
        return constitutionCode;
    }

    /**
     * @param constitutionCode the constitutionCode to set
     */
    public void setConstitutionCode(String constitutionCode) {
        this.constitutionCode = constitutionCode;
    }

    /**
     * @return the casteDesc
     */
    public String getCasteDesc() {
        return casteDesc;
    }

    /**
     * @param casteDesc the casteDesc to set
     */
    public void setCasteDesc(String casteDesc) {
        this.casteDesc = casteDesc;
    }

    /**
     * @return the casteCode
     */
    public String getCasteCode() {
        return casteCode;
    }

    /**
     * @param casteCode the casteCode to set
     */
    public void setCasteCode(String casteCode) {
        this.casteCode = casteCode;
    }

    /**
     * @return the communityDesc
     */
    public String getCommunityDesc() {
        return communityDesc;
    }

    /**
     * @param communityDesc the communityDesc to set
     */
    public void setCommunityDesc(String communityDesc) {
        this.communityDesc = communityDesc;
    }

    /**
     * @return the communityCode
     */
    public String getCommunityCode() {
        return communityCode;
    }

    /**
     * @param communityCode the communityCode to set
     */
    public void setCommunityCode(String communityCode) {
        this.communityCode = communityCode;
    }

    /**
     * @return the schemeDesc
     */
    public String getSchemeDesc() {
        return schemeDesc;
    }

    /**
     * @param schemeDesc the schemeDesc to set
     */
    public void setSchemeDesc(String schemeDesc) {
        this.schemeDesc = schemeDesc;
    }

    /**
     * @return the schemeCode
     */
    public String getSchemeCode() {
        return schemeCode;
    }

    /**
     * @param schemeCode the schemeCode to set
     */
    public void setSchemeCode(String schemeCode) {
        this.schemeCode = schemeCode;
    }

    /**
     * @return the introducerStatusDesc
     */
    public String getIntroducerStatusDesc() {
        return introducerStatusDesc;
    }

    /**
     * @param introducerStatusDesc the introducerStatusDesc to set
     */
    public void setIntroducerStatusDesc(String introducerStatusDesc) {
        this.introducerStatusDesc = introducerStatusDesc;
    }

    /**
     * @return the introducerStatusCode
     */
    public String getIntroducerStatusCode() {
        return introducerStatusCode;
    }

    /**
     * @param introducerStatusCode the introducerStatusCode to set
     */
    public void setIntroducerStatusCode(String introducerStatusCode) {
        this.introducerStatusCode = introducerStatusCode;
    }

    /**
     * @return the applicationNo
     */
    public String getApplicationNo() {
        return applicationNo;
    }

    /**
     * @param applicationNo the applicationNo to set
     */
    public void setApplicationNo(String applicationNo) {
        this.applicationNo = applicationNo;
    }

    /**
     * @return the custTypeDesc
     */
    public String getCustTypeDesc() {
        return custTypeDesc;
    }

    /**
     * @param custTypeDesc the custTypeDesc to set
     */
    public void setCustTypeDesc(String custTypeDesc) {
        this.custTypeDesc = custTypeDesc;
    }

    /**
     * @return the custTypeCode
     */
    public String getCustTypeCode() {
        return custTypeCode;
    }

    /**
     * @param custTypeCode the custTypeCode to set
     */
    public void setCustTypeCode(String custTypeCode) {
        this.custTypeCode = custTypeCode;
    }

    /**
     * @return the permHusFathName
     */
    public String getPermHusFathName() {
        return permHusFathName;
    }

    /**
     * @param permHusFathName the permHusFathName to set
     */
    public void setPermHusFathName(String permHusFathName) {
        this.permHusFathName = permHusFathName;
    }

    /**
     * @return the permRelType
     */
    public String getPermRelType() {
        return permRelType;
    }

    /**
     * @param permRelType the permRelType to set
     */
    public void setPermRelType(String permRelType) {
        this.permRelType = permRelType;
    }

    /**
     * @return the permTelephone
     */
    public String getPermTelephone() {
        return permTelephone;
    }

    /**
     * @param permTelephone the permTelephone to set
     */
    public void setPermTelephone(String permTelephone) {
        this.permTelephone = permTelephone;
    }

    /**
     * @return the commHusFathName
     */
    public String getCommHusFathName() {
        return commHusFathName;
    }

    /**
     * @param commHusFathName the commHusFathName to set
     */
    public void setCommHusFathName(String commHusFathName) {
        this.commHusFathName = commHusFathName;
    }

    /**
     * @return the commRelationType
     */
    public String getCommRelationType() {
        return commRelationType;
    }

    /**
     * @param commRelationType the commRelationType to set
     */
    public void setCommRelationType(String commRelationType) {
        this.commRelationType = commRelationType;
    }

    /**
     * @return the areaVillageCode
     */
    public String getAreaVillageCode() {
        return areaVillageCode;
    }

    /**
     * @param areaVillageCode the areaVillageCode to set
     */
    public void setAreaVillageCode(String areaVillageCode) {
        this.areaVillageCode = areaVillageCode;
    }

    /**
     * @return the minorNomiGuarAge
     */
    public String getMinorNomiGuarAge() {
        return minorNomiGuarAge;
    }

    /**
     * @param minorNomiGuarAge the minorNomiGuarAge to set
     */
    public void setMinorNomiGuarAge(String minorNomiGuarAge) {
        this.minorNomiGuarAge = minorNomiGuarAge;
    }
    
    
}
