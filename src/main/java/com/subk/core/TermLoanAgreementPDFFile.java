/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.core;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Configuration;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.subk.entity.Loanorigination;


@Configuration
public class TermLoanAgreementPDFFile extends PDFGenerationAbstract {

    private String empty = " ";
    private static final Logger logger = Logger.getLogger(GenerateUndertakingPDFFile.class);
//    private static final ResourceBundle rb = ResourceBundle.getBundle("systemPath");
//    private static final ResourceBundle rb1 = ResourceBundle.getBundle("com.subk.Resources//subkResources");

    public String getPdf(Loanorigination loanData, String fileName,String pathAdd, String img, String jsPaths) {
        logger.info("************ ENTERED ************* ");

        String destPath = null;

        logger.debug("UBILoanOrigination Data: " + loanData.toString());
        logger.debug("FileName: " + fileName);

        try {
            destPath = pathAdd;
            destPath = destPath + fileName;

            //File file = new File(destPath);
            //file.getParentFile().mkdirs();
            new TermLoanAgreementPDFFile().createPdf(destPath, loanData, img, jsPaths);
        } catch (Exception ex) {
            logger.error("Error:....!!! " + ex.getMessage());
        }

        logger.info("destPathINATION PATH:...........!!!!!!!!!!  " + destPath);
        logger.info("************ ENDED.************* ");
        return destPath;
    }

    public void createPdf(String dest, Loanorigination loanData , String img, String jsPaths) {
        logger.debug("***********Entered***********");
        final String imgPath = img;
        final String mclrVal = loanData.getMclr() != null && !loanData.getMclr().equals("") ? loanData.getMclr() : "";//rb1.getString("MCLRL");

        String mclrMF = null;
//        BigDecimal mclrMF = new BigDecimal("0.0");

        String gender = loanData.getSex();
        if (gender.equals("MALE")) {
            mclrMF = loanData.getMclrMaleFemale() != null && !loanData.getMclrMaleFemale().equals("") ? loanData.getMclrMaleFemale() : "";//rb1.getString("MCLRL_MALE");
//                    loanData.getMclrMale();
        } else if (gender.equals("FEMALE")) {
            mclrMF = loanData.getMclrMaleFemale() != null && !loanData.getMclrMaleFemale().equals("") ? loanData.getMclrMaleFemale() : "";//rb1.getString("MCLRL_FEMALE");

//                    loanData.getMclrFemale();
        }

        Document document = null;
        PdfWriter writer = null;

        try {
            String appl1Address = (loanData.getAppl1address() != null && loanData.getAppl1address() != "" && !loanData.getAppl1address().equals("null") ? loanData.getAppl1address() : " ");
            document = new Document();
            writer = PdfWriter.getInstance(document, new FileOutputStream(dest, false));
            //writer.setPageEvent(new Header());
            HeaderFooterPageEvent event = new HeaderFooterPageEvent();
            writer.setPageEvent(event);
            document.open();
            Font canFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
            Font canFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
            Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

            Font font11n = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.NORMAL);
            Font font11b = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
            //File file = new File("C:\\"+loanData.getAppl1Name()+"_"+loanData.getApplicationId());
            // Get the output stream for writing PDF object
            //ServletOutputStream out = response.getOutputStream();
            //OutputStream oos = new FileOutputStream(file,true);
            //Document document = new Document();
            //PdfWriter writer = PdfWriter.getInstance(document, out);
//            document.open();
            document.addKeywords("Java, Servelet, PDF, iText");
            document.addAuthor("Raj Grover");
            document.addCreator("UBILoanOrigination");
            String applicantName = (loanData.getAppl1name() != null && loanData.getAppl1name() != "" && !loanData.getAppl1name().equals("null") ? loanData.getAppl1name() : " ");
            String applicantFhgName = (loanData.getFhgname() != null && loanData.getFhgname() != "" && !loanData.getFhgname().equals("null") ? loanData.getFhgname() : " ");
            String applicantAddress = (loanData.getAppl1address() != null && loanData.getAppl1address() != "" && !loanData.getAppl1address().equals("null") ? loanData.getAppl1address() : " ");

            Image img1 = Image.getInstance(imgPath);
            img1.setAbsolutePosition(250f, 740f);
            img1.scaleAbsolute(80f, 80f);
            document.add(img1);
            document.add(new Paragraph("\n"));
            Paragraph sp8 = new Paragraph("Form No. D28", canFont1);
            sp8.setAlignment(Element.ALIGN_RIGHT);
            document.add(sp8);
//            document.add(new Paragraph("\n"));
            document.add(new Paragraph("\n"));
            Paragraph sp9 = new Paragraph("\nAGREEMENT OF TERM LOAN\n\n", canFont);
            sp9.setAlignment(Element.ALIGN_CENTER);
            document.add(sp9);

//           Chunk glue = new Chunk(new VerticalPositionMark());
//            Paragraph rp26 = new Paragraph();
//            rp26.add(new Phrase("\n\nAGREEMENT OF TERM LOAN", SUBFONT));
//            rp26.add(new Chunk(glue));
//            rp26.add(new Phrase("Form No. D28", font11b));
//            rp26.setSpacingAfter(5);
//            document.add(rp26);
            String appl1Addresss = (loanData.getAppl1name() != null && !loanData.getAppl1name().equals("null") ? loanData.getAppl1name() : "");
            appl1Addresss += ",S/o,D/o,W/o " + (loanData.getFhgname() != null && !loanData.getFhgname().equals("null") ? loanData.getFhgname() : "")
                    + (loanData.getAppl1address() != null && !loanData.getAppl1address().equals("null") ? loanData.getAppl1address() : "");;
            String branchName = (loanData.getBranchname() != null && !loanData.getBranchname().equals("null") ? loanData.getBranchname() : "");
            String loanAmount = (loanData.getLoanamountsactioned() != null && loanData.getLoanamountsactioned() != " " && !loanData.getLoanamountsactioned().equals("null") ? loanData.getLoanamountsactioned() : "");
            //String loanAmount = (loanData.getLoanAmountSanctioned() != null && loanData.getLoanAmountSanctioned() != " " && !loanData.getLoanAmountSanctioned().equals("null") ? loanData.getLoanAmountSanctioned() : "");
            String commaloanAmount = getIndianCurrencyFormat(loanAmount);

            String rupees = NumberToWord1(loanAmount);
            Paragraph rp27 = new Paragraph(20, "This Deed made this......................day of.............................between ", redFont);
            Chunk add = new Chunk(appl1Addresss, redFont);
            add.setUnderline(0.6f, -2f);
            rp27.add(add);
            rp27.add(" having his/its/their Registered Office/Place of Business at ");
            Chunk add1 = new Chunk(appl1Addresss, redFont);
            add1.setUnderline(0.6f, -2f);
            rp27.add(add1);
            rp27.add(" (hereinfater called the ");
            Chunk brr = new Chunk("''BORROWER''", font11n);
            rp27.add(brr);
            rp27.add(" which expression shall unless excluded by or repugnant to the context include his/it/their heirs, legal representatives, successors and assigns) of the One Part and");
            Chunk brr1 = new Chunk(" UNITED BANK OF INDIA", font11n);
            rp27.add(brr1);
            rp27.add(", a body corporate constituted under the Banking Companies (Acquisition and Transfer of Undertakings) Act, 1970 and having its Head Office at No.11 Hemanta Basu Sarani, Kolkata- 700 001 and having one of its business places at ");
            Chunk branch = new Chunk(branchName, redFont);
            branch.setUnderline(0.6f, -2f);
            rp27.add(branch);
            rp27.add(" (hereinafter called the ‘‘Bank’’ which expression shall unless excluded by or repugnant to the context include its successors and assigns) of the Other Part.\n");
            rp27.add("\nWhere as the Borrower has applied to the Bank for a loan of Rs. ");
            Chunk ammount = new Chunk(commaloanAmount, font11b);
            ammount.setUnderline(0.6f, -2f);
            rp27.add(ammount);
//            rp27.add(underLine(commaloanAmount));
            rp27.add("/- ( ");
            Chunk ammount1 = new Chunk(rupees, redFont);
            ammount1.setUnderline(0.6f, -2f);
            rp27.add(ammount1);
//            rp27.add(underLine(rupees));
            rp27.add(" Rupees only) upon the basis of and for the " + "purposes set forth in the Borrower’s Proposal dated the  ");
            Chunk date = new Chunk((loanData.getUhupdateddatetime() != null && !loanData.getUhupdateddatetime().equals("null")
                    && !loanData.getUhupdateddatetime().isEmpty() ? returnOnlyDate(loanData.getUhupdateddatetime()) : empty), redFont);
            date.setUnderline(0.6f, -2f);
            rp27.add(date);
//            rp27.add();
            rp27.add(" day of ");
            Chunk date1 = new Chunk((loanData.getUhupdateddatetime() != null && !loanData.getUhupdateddatetime().equals("null")
                    && !loanData.getUhupdateddatetime().isEmpty() ? returnMonthandYear(loanData.getUhupdateddatetime()) : empty), redFont);
            date1.setUnderline(0.6f, -2f);
            rp27.add(date1);
//            rp27.add(underLine);
            rp27.add(" (hereinafter called the ");
            Chunk borrow = new Chunk("''BORROWER’S PROPOSAL'').\n", font11n);
            rp27.add(borrow);
            rp27.add("\nAnd where as the Bank has advanced/agreed to advance the said loan either at one time or by such installments and on such dates as has been/may be mutually agreed upon by and between the parties here to.\n"
                    + "\nAnd where as the Bank has advanced/agreed to advance the said loan upon the assurances, agreements, covenants terms and conditions set forth in these presents.\n");

            rp27.setAlignment(Element.ALIGN_JUSTIFIED);
            rp27.setSpacingAfter(5);
            document.add(rp27);
            Paragraph sidpara = new Paragraph("Now in consideration of the aforesaid it is hereby agreed by and between the parties hereto as follows :–", canFont1);
            document.add(sidpara);

            PdfPTable tablekk1 = new PdfPTable(2);
            tablekk1.setWidthPercentage(100f);
            tablekk1.setWidths(new float[]{0.7f, 15});
            tablekk1.setHorizontalAlignment(Element.ALIGN_LEFT);

            PdfPCell cs1 = new PdfPCell();
            Paragraph ps1 = new Paragraph("1.", redFont);
            cs1.addElement(ps1);
            cs1.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs1);

            PdfPCell cs2 = new PdfPCell();
            Paragraph ps2 = new Paragraph("The Borrower’s proposal shall be deemed to constitute the basis of this agreement and of the loan already advanced/agreed to be advanced by the Bank hereunder and the Borrower hereby warrants the correctness of each and every one of the statements, representations and particulars contained in the Borrower’s proposal and undertakes to carry out the project and programme therein set forth.\n", redFont);
            ps2.setAlignment(Element.ALIGN_JUSTIFIED);
            cs2.addElement(ps2);
            cs2.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs2);

            PdfPCell cs3 = new PdfPCell();
            Paragraph ps3 = new Paragraph("2.", redFont);
            cs3.addElement(ps3);
            cs3.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs3);

            PdfPCell cs4 = new PdfPCell();
            Paragraph ps4 = new Paragraph("The Borrower is aware that the Bank has approached the .............................."
                    + "for providing refinance for the said loan granted/to be granted under these presents and the Borrower agrees and undertakes to observe perform and discharge various duties and obligations imposed by the said ...................................... which are applicable to the Borrower and/or which are to be performed by the Borrower.\n", redFont);
            ps4.setAlignment(Element.ALIGN_JUSTIFIED);
            cs4.addElement(ps4);
            cs4.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs4);
            document.add(tablekk1);

            document.newPage();
            PdfPTable tablekk2 = new PdfPTable(2);
            tablekk2.setWidthPercentage(100f);
            tablekk2.setWidths(new float[]{0.7f, 15});
            tablekk2.setHorizontalAlignment(Element.ALIGN_LEFT);
            PdfPCell cs5 = new PdfPCell();
            Paragraph ps5 = new Paragraph("3.", redFont);
            cs5.addElement(ps5);
            cs5.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs5);

            PdfPCell cs6 = new PdfPCell();
            Paragraph ps6 = new Paragraph("The Borrower hereby agrees that the said loan shall be governed by and be subject to the terms contained herein as well as those contained in the Security Documents executed/to be executed by the Borrower in favour of the Bank in respect of the said loan except in so far as the security Documents may expressly or by necessary implication be modified by these presents.\n", redFont);
            ps6.setAlignment(Element.ALIGN_JUSTIFIED);
            cs6.addElement(ps6);
            cs6.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs6);

            PdfPCell cs7 = new PdfPCell();
            Paragraph ps7 = new Paragraph("4.", redFont);
            cs7.addElement(ps7);
            cs7.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs7);

            PdfPCell cs8 = new PdfPCell();
            Paragraph ps8 = new Paragraph("The Borrower hereby agrees and undertakes to utilise the said loan exclusively for the purposes set out in the Borrower’s proposal and not for any other purpose or purposes except with the prior consent in writing of the Bank.\n", redFont);
            cs8.addElement(ps8);
            cs8.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs8);

            PdfPCell cs9 = new PdfPCell();
            Paragraph ps9 = new Paragraph("5.", redFont);
            cs9.addElement(ps9);
            cs9.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs9);

            PdfPCell cs10 = new PdfPCell();
            Paragraph ps10 = new Paragraph("If any event happens hereafter which may affect the correctness of any of the statements, representations and or particulars set forth in the Borrower’s proposal, the Borrower undertakes to inform the Bank in writing of such event within 30 days of the happening thereof.\n", redFont);
            ps10.setAlignment(Element.ALIGN_JUSTIFIED);
            cs10.addElement(ps10);
            cs10.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs10);

            PdfPCell cs11 = new PdfPCell();
            Paragraph ps11 = new Paragraph("6.", redFont);
            cs11.addElement(ps11);
            cs11.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs11);

            PdfPCell cs12 = new PdfPCell();
            Paragraph ps12 = new Paragraph("The said loan shall be repaid to the Bank by the Borrower in installments as mentioned hereunder and so on till the said loan in entirely repaid with all interests, costs, charges, expenses etc. whatsoever. Notwithstanding what is stated hereinabove or in any of the Security Documents the Bank is hereby authorized without reference to the Borrower in the absolute discretion of the Bank to alter, modify, revise or vary the aforesaid repayment schedule and / or demand earlier recovery/repayment and / or extend the repayment period and/or alter the installment amount and / or the installment dates and the Borrower shall in any such event be bound to repay the said loan with all interests, costs, charges, expenses, etc. whatsoever in terms of such altered, varied, modified or revised schedule of repayment and in case of earlier demand to make payment of the aforesaid amounts within the period as stated in the notice of demand provided always that the Bank shall give at least 3 months’ notice to the Borrower before implementing such variation, alteration, modification or revision.\n", redFont);
            ps12.setAlignment(Element.ALIGN_JUSTIFIED);
            cs12.addElement(ps12);
            cs12.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs12);

            String mclrl = (loanData.getAppl1name() != null && !loanData.getAppl1name().equals("null") ? loanData.getAppl1name() : "");

            String mclrMFFinal = ((String.valueOf(mclrMF)) != null && !((String.valueOf(mclrMF)).equals("")) && (String.valueOf(mclrMF)) != "null" ? (String.valueOf(mclrMF)) : " ");
            PdfPCell cs13 = new PdfPCell();
            Paragraph ps13 = new Paragraph("7.", redFont);
            cs13.addElement(ps13);
            cs13.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs13);

            PdfPCell cs14 = new PdfPCell();
            Paragraph ps14 = new Paragraph("The Borrower shall pay to the Bank interest on the principal moneys outstanding from time to time in respect of the said loan at the rate of ", redFont);
            ps14.add(underLine(mclrMFFinal));
            ps14.add(" % per annum below/at/over the United Bank of India MCLR-Y which is at present ");
            ps14.add(underLine(mclrVal));
            ps14.add("% p.a., with ");
            ps14.add(underLine("monthly "));
            ps14.add("ests for value received/with interest thereon at the rate of ....................% p.a with ................rests for value received. Such interest  shall  be  charged   with  monthly/quarterly/half-yearly rests, interest being payable on...........................................or  beginning from the date of these presents, the first of such payment on account of interest to be for the period form the date of disbursement of this loan till the last day of the ........................as mentioned hereinabove. The Bank shall be entitled to charge interest at such other rate or rates and for such other period or periods and with such other rest or rests as may be notified to the Borrower by the Bank from time to time till such loan is repaid in full with all interests, costs, charges, expenses etc. whatsoever. The Borrower shall be liable to pay to the Bank .................%  p.a. as and by way of additional interest on any installment or interest not paid on its due date as mentioned in these presents from the due date of such installment or interest until the same is paid.  It is further agreed that in case any interest is not paid on the due date thereof as mentioned hereinabove such interest shall be added to the principal amount of the claim and shall carry usual rate of interest applicable to such principal amount in terms of this agreement without prejudice to the right of the Bank to charge additional interest on such amount and at the rates and for the period of the default as mentioned hereinabove.\n");
            ps14.setAlignment(Element.ALIGN_JUSTIFIED);
            cs14.addElement(ps14);
            cs14.setBorder(PdfPCell.NO_BORDER);
            tablekk2.addCell(cs14);
            document.add(tablekk2);

            document.newPage();
            PdfPTable tablekk3 = new PdfPTable(2);
            tablekk3.setWidthPercentage(100f);
            tablekk3.setWidths(new float[]{0.7f, 15});
            tablekk3.setHorizontalAlignment(Element.ALIGN_LEFT);
            PdfPCell cs15 = new PdfPCell();
            Paragraph ps15 = new Paragraph("8.", redFont);
            cs15.addElement(ps15);
            cs15.setBorder(PdfPCell.NO_BORDER);
            tablekk3.addCell(cs15);

            PdfPCell cs16 = new PdfPCell();
            Paragraph ps16 = new Paragraph("Notwithstanding anything contained herein or in any security documents executed/to be executed by the Borrower, the said loan and interest including any additional interest due thereon and all costs, charges, expenses etc. whatsoever shall at the Bank’s option become forthwith due and payable by the Borrower to the Bank and the Bank shall, at its option, be entitled or enforce its security upon the happening of the following events, namely :–\n"
                    + "a) If any installment of the principal moneys advanced under these presents remains unpaid upon the due date for payment thereof.\n"
                    + "b) If any interest due under these presents remains unpaid and in arrears for a period of ", redFont);
            Chunk rup = new Chunk("one", redFont);
            rup.setUnderline(0.6f, -2f);
            ps16.add(rup);
//             ps16.add(" month after the same shall have become due whether demanded or not.\n");
            ps16.add(" month after the same shall have become due whether demanded or not.\n"
                    + "c) If any representation or statement or particular made in the Borrower’s proposal is found to be incorrect or the Borrower commits any breach or default in the performance or observance of these presents and / or the Borrower’s proposal and / or the security documents and / or of any other terms and conditions relating to the said loan.\n"
                    + "d) If the Borrower enters into any arrangement or composition with the Borrower’s creditors or commits any other act which might result in the insolvency / winding up of the Borrower.\n"
                    + "e) If execution or distress is enforced or levied upon or against the whole or any part of the Borrower's property whether secured to the Bank or not.\n"
                    + "f) If the Borrower goes into liquidation except with prior approval in writing of the Bank for the purpose of amalgamation or reconstruction.\n"
                    + "g) If a receiver is appointed in respect of the whole or any part of the property of the Borrower.\n"
                    + "h) If the Borrower ceases or threatens to cease to carry on business or gives or threatens to give notice of the Borrower’s intention to do so.\n"
                    + "i) If is certified by an Accountant of a firm of accountants appointed by the Bank (which the Bank is entitled and authorised to do at any time in its sole discretion) that the liabilities of the Borrower exceed the Borrower’s assets or that the Borrower is carrying on business at a loss.\n"
                    + "j) If the Borrower shall without the consent in writing of the Bank attempt or purport to create  any mortgage, charge, pledge, hypothecation, lien or encumbrance over the  Borrower’s property or any part thereof which is/shall be the security for the repayment of the said loan with interests, costs, charges, expenses etc, except in favour of the Bank for securing any other obligations of the Borrower to the Bank.\n"
                    + "k) If the Borrower shall suspend payment to any of the Borrower’s creditors or threaten to do so.\n"
                    + "l) If any circumstance or event occurs which is prejudicial to or impairs or imperils or depreciates or jeopardises or is likely to prejudice, impair, imperil, depreciate or jeopardise any security given by the Borrower or any part thereof.\n"
                    + "m) If any event or circumstance occurs which would or is likely to prejudicially or adversely affect in any manner the implementation of the said project or the capacity of the Borrower to repay the said loan or any part thereof.\n");
            ps16.setAlignment(Element.ALIGN_JUSTIFIED);
            cs16.addElement(ps16);
            cs16.setBorder(PdfPCell.NO_BORDER);
            tablekk3.addCell(cs16);
            document.add(tablekk3);
            
            document.newPage();
            PdfPTable tablekk4 = new PdfPTable(2);
            tablekk4.setWidthPercentage(100f);
            tablekk4.setWidths(new float[]{0.7f, 15});
            tablekk4.setHorizontalAlignment(Element.ALIGN_LEFT);
            PdfPCell c17 = new PdfPCell();
            Paragraph p17 = new Paragraph("  ", redFont);
            c17.addElement(p17);
            c17.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(c17);

            PdfPCell c18 = new PdfPCell();
            Paragraph p18 = new Paragraph("n) If the said loan or any part thereof is utilised for any purpose other than the purpose set forth in the Borrower’s proposal.\n"
                    + "On the questions whether any of the above events or circumstances has happened, the decision of the Bank shall be conclusive and binding on the Borrower.", redFont);
            p18.setAlignment(Element.ALIGN_JUSTIFIED);
            c18.addElement(p18);
            c18.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(c18);
            
            PdfPCell cs17 = new PdfPCell();
            Paragraph ps17 = new Paragraph("9.", redFont);
            cs17.addElement(ps17);
            cs17.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(cs17);

            PdfPCell cs18 = new PdfPCell();
            Paragraph ps18 = new Paragraph("It is hereby expressly agreed that the Bank shall be at liberty to assign the debt and the benefit of these presents and the securities for the said loan and the security Documents to .................................................. (hereinafter called the" + "‘‘Refinancer’’) as security for any refinance obtained /to be obtained by the Bank from the said ...............................\n.............................(Refinancer) in respect of the said loan and the Borrower shall if and wherever required by the Bank to do so at the Borrower’s own expenses do and execute and join in doing and executing all such  acts, things, deeds, documents, assurances, etc. as the Bank may require for effecting such assignment.\n", redFont);
            ps18.setAlignment(Element.ALIGN_JUSTIFIED);
            cs18.addElement(ps18);
            cs18.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(cs18);

            PdfPCell cs19 = new PdfPCell();
            Paragraph ps19 = new Paragraph("10.", redFont);
            cs19.addElement(ps19);
            cs19.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(cs19);

            PdfPCell cs20 = new PdfPCell();
            Paragraph ps20 = new Paragraph("The Borrower shall not without the prior written consent of the Bank transfer or create / allow to be created in any manner any charge, lien, hypothecation, mortgage, pledge or other ecncumbrance whatsoever on any of the properties, assets, actionable claims etc. of the Borrower which are securities given to the Bank in respect of the said loan or create / allow to be created  any interest in such securities in favour of any party or person other than the Bank.\n", redFont);
            ps20.setAlignment(Element.ALIGN_JUSTIFIED);
            cs20.addElement(ps20);
            cs20.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(cs20);

            PdfPCell cs21 = new PdfPCell();
            Paragraph ps21 = new Paragraph("11.", redFont);
            cs21.addElement(ps21);
            cs21.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(cs21);

            PdfPCell cs22 = new PdfPCell();
            Paragraph ps22 = new Paragraph("The Borrower shall prepare detailed plans programme and estimates in respect of plantation constructional and installation works including construction of buildings, factories, workers / officers quarters and installation of machineries, plants, equipments etc. for irrigation including installation of deep tubewells etc. and any other development work provided for under the Borrower’s proposal. Any deviation from the procedure laid down in the Borrower’s proposal approved by ............................................................................... may be made by the Borrower only after obtaining prior consent in writing there for from the Bank and the said ...........................................................\n" + "........................... through the Bank. The Borrower shall forward the plans programmes and estimates to the Bank in duplicate. The Borrower shall also work out the detailed cost structure under intimation to the Bank. The Borrower agrees and undertakes to finance from out of the Borrower’s own resources any increase or escalation in the estimated / projected / budgeted cost arising out of any reason, circumstance, event or factor whatsoever.\n"
                    + "The Borrower shall not without the prior consent in writing of the Bank undertake any work or works other than those set forth in the Borrower’s proposal.\n ", redFont);
            ps22.setAlignment(Element.ALIGN_JUSTIFIED);
            cs22.addElement(ps22);
            cs22.setBorder(PdfPCell.NO_BORDER);
            tablekk4.addCell(cs22);
            document.add(tablekk4);
            
            document.newPage();
            PdfPTable tablekk5 = new PdfPTable(2);
            tablekk5.setWidthPercentage(100f);
            tablekk5.setWidths(new float[]{0.7f, 15});
            tablekk5.setHorizontalAlignment(Element.ALIGN_LEFT);
            
            PdfPCell cs23 = new PdfPCell();
            Paragraph ps23 = new Paragraph("12.", redFont);
            cs23.addElement(ps23);
            cs23.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(cs23);

            PdfPCell cs24 = new PdfPCell();
            Paragraph ps24 = new Paragraph("The Borrower shall insure and keep insured to the satisfaction of the Bank all properties and assets constituting the Bank’s security against fire, strike, riot, civil commotion and all other risks in a sum equivalent to its full market value with an Insurance Company approved by the Bank in the joint names of the Bank and the Borrower or otherwise as the Bank may require and shall duly and punctually pay all premiums and shall not do or suffer  to be done  any act which  may invalidate  or avoid such insurance  and shall deposit  such insurance policy or policies and all cover notes, premium receipts and other documents connected therewith with the Bank. Any money realized  from such insurance  shall at the option of the Bank be applied either reinstating   or replacing  the security or in repayment  of the said loan and or interest  including  additional interest, costs, charges, expenses etc. payable in respect of the said loan. If the Borrower shall make any default in insuring or keeping insured all or any property and assets forming the Bank’s security as aforesaid the Bank shall without prejudice to and without affecting its rights under Clause 8 here of  be at liberty (but not bound) to insure and keep the same insured and the Borrower shall on demand repay to the Bank all amount or amounts spent or incurred by it for effecting or keeping such insurance with interest at the rate applicable to the said loan.\n", redFont);
            ps24.setAlignment(Element.ALIGN_JUSTIFIED);
            cs24.addElement(ps24);
            cs24.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(cs24);

            PdfPCell cs25 = new PdfPCell();
            Paragraph ps25 = new Paragraph("13.", redFont);
            cs25.addElement(ps25);
            cs25.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(cs25);

            PdfPCell cs26 = new PdfPCell();
            Paragraph ps26 = new Paragraph("The Borrower shall allow the representatives and / or nominees of the Bank to visit and inspect from time to time the said project and the Borrower’s premises, plantations, factories and other properties and assets, books of accounts and all other relevant accounts, documents and records The costs and expenses of such visits and / or inspection shall be paid and borne by the Borrower.\n", redFont);
            ps26.setAlignment(Element.ALIGN_JUSTIFIED);
            cs26.addElement(ps26);
            cs26.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(cs26);

            PdfPCell cs27 = new PdfPCell();
            Paragraph ps27 = new Paragraph("14.", redFont);
            cs27.addElement(ps27);
            cs27.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(cs27);

            PdfPCell cs28 = new PdfPCell();
            Paragraph ps28 = new Paragraph("The Borrower shall implement the decisions and / or suggestions made or taken by the Bank for the purpose of ensuring proper utilization of the said loan in achieving the results contemplated under the Borrower’s proposal.\n", redFont);
            ps28.setAlignment(Element.ALIGN_JUSTIFIED);
            cs28.addElement(ps28);
            cs28.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(cs28);

            PdfPCell cs29 = new PdfPCell();
            Paragraph ps29 = new Paragraph("15.", redFont);
            cs29.addElement(ps29);
            cs29.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(cs29);

            PdfPCell cs30 = new PdfPCell();
            Paragraph ps30 = new Paragraph("The Borrower shall furnish the Bank with all such information, statements, particulars, estimates, reports etc. as the Bank may from time to time require as to the compliance with the terms of the said loan and shall also submit to the Bank all other periodical reports and information at such time or times and in such form or forms and in such manner and at such place or places and containing such particulars, information, details, estimates, reports  etc. as the Bank may call for in respect of the Borrower’s proposal for the purpose of ascertaining the utilization of the said loan or the results thereof for any other purpose connected with or in relation to the loan.\n", redFont);
            ps30.setAlignment(Element.ALIGN_JUSTIFIED);
            cs30.addElement(ps30);
            cs30.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(cs30);

            PdfPCell cs31 = new PdfPCell();
            Paragraph ps31 = new Paragraph("16.", redFont);
            cs31.addElement(ps31);
            cs31.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(cs31);

            PdfPCell cs32 = new PdfPCell();
            Paragraph ps32 = new Paragraph("The Bank shall be at liberty to furnish to .............................. any information, report, particulars, statements, estimates etc. or copies thereof or any other information whatsoever whether received by the Bank from the Borrower or otherwise in the Bank’s possession and the Borrower shall not be entitled to object to the same.\n", redFont);
            ps32.setAlignment(Element.ALIGN_JUSTIFIED);
            cs32.addElement(ps32);
            cs32.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(cs32);

            PdfPCell cs33 = new PdfPCell();
            Paragraph ps33 = new Paragraph("17.", redFont);
            cs33.addElement(ps33);
            cs33.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(cs33);

            PdfPCell cs34 = new PdfPCell();
            Paragraph ps34 = new Paragraph("The Borrower shall appoint qualified trained technical personnel for the proposed scheme in the Borrower’s proposal and shall appoint other staff strictly according to the requirements. The appointment or change in the technical personnel shall be made upon prior notice to and upon obtaining prior written approval of the Bank. The Bank shall be at liberty, if it thinks so fit, in its sole", redFont);
            ps34.setAlignment(Element.ALIGN_JUSTIFIED);
            cs34.addElement(ps34);
            cs34.setBorder(PdfPCell.NO_BORDER);
            tablekk5.addCell(cs34);
            document.add(tablekk5);
            document.newPage();
            PdfPTable tablekk6 = new PdfPTable(2);
            tablekk6.setWidthPercentage(100f);
            tablekk6.setWidths(new float[]{0.7f, 15});
            tablekk6.setHorizontalAlignment(Element.ALIGN_LEFT);
            
            PdfPCell c33 = new PdfPCell();
            Paragraph p33 = new Paragraph("   ", redFont);
            c33.addElement(p33);
            c33.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(c33);

            PdfPCell c34 = new PdfPCell();
            Paragraph p34 = new Paragraph(" discretion, to appoint Manager, Internal Auditor, Technical personnel, Consultancy Firm or any other managerial or supervisory personnel as it may deem necessary and the remuneration of such personnel shall be paid by the Borrower.\n", redFont);
            p34.setAlignment(Element.ALIGN_JUSTIFIED);
            c34.addElement(p34);
            c34.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(c34);

            PdfPCell cs35 = new PdfPCell();
            Paragraph ps35 = new Paragraph("18.", redFont);
            cs35.addElement(ps35);
            cs35.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(cs35);

            PdfPCell cs36 = new PdfPCell();
            Paragraph ps36 = new Paragraph("The Borrower shall not promote any undertaking other than the undertaking set out in the proposed scheme contained in the Borrower’s proposal on its own or as subsidiary of the Borrower without previous consent in writing of the Bank.\n", redFont);
            ps36.setAlignment(Element.ALIGN_JUSTIFIED);
            cs36.addElement(ps36);
            cs36.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(cs36);

            PdfPCell cs37 = new PdfPCell();
            Paragraph ps37 = new Paragraph("19.", redFont);
            cs37.addElement(ps37);
            cs37.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(cs37);

            PdfPCell cs38 = new PdfPCell();
            Paragraph ps38 = new Paragraph("The Borrower shall observe, carry out and implement all such suggestions, directions and instructions as the Bank may from time to time give to the Borrower for utilization of the said loan.\n", redFont);
            ps38.setAlignment(Element.ALIGN_JUSTIFIED);
            cs38.addElement(ps38);
            cs38.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(cs38);

            PdfPCell cs39 = new PdfPCell();
            Paragraph ps39 = new Paragraph("20.", redFont);
            cs39.addElement(ps39);
            cs39.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(cs39);

            PdfPCell cs40 = new PdfPCell();
            Paragraph ps40 = new Paragraph("If the Borrower makes default in payment of any moneys due to or claims of the Bank under these presents whether on account of Principal, interest  and / or additional interest and / or costs, charges and expenses or otherwise, the Bank would be at liberty to appoint  its nominee as Receiver without having resort to a Court of law and / or to a proceeding in Court, to take possession of the properties of the Borrower which might  be held by the Bank as security for the said loan under these presents or under any other security document (s) executed or to be executed by the Borrower in favour of the Bank.\n", redFont);
            ps40.setAlignment(Element.ALIGN_JUSTIFIED);
            cs40.addElement(ps40);
            cs40.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(cs40);

            PdfPCell cs41 = new PdfPCell();
            Paragraph ps41 = new Paragraph("21.", redFont);
            cs41.addElement(ps41);
            cs41.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(cs41);

            PdfPCell cs42 = new PdfPCell();
            Paragraph ps42 = new Paragraph("Any notice hereunder may be given by the Bank to the Borrower by leaving the same at the last known place of business of the Borrower in India or by sending the same by post to the Borrower  addressed  as aforesaid and if sent by post the notice shall be deemed to have been given at the time when it would be delivered in due course of post and in proving such notice when given by post it shall be sufficient to prove that the envelope containing  the notice was posted and a certificate  signed by any officer of the Bank that the envelope was so posted will be conclusive.\n", redFont);
            ps42.setAlignment(Element.ALIGN_JUSTIFIED);
            cs42.addElement(ps42);
            cs42.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(cs42);

            PdfPCell cs43 = new PdfPCell();
            Paragraph ps43 = new Paragraph("22.", redFont);
            cs43.addElement(ps43);
            cs43.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(cs43);

            PdfPCell cs44 = new PdfPCell();
            Paragraph ps44 = new Paragraph("Nothing contained in these presents shall be deemed to limit or affect prejudicially the rights and powers of the Bank under the security documents or letters of guarantee or any them or under any law.\n", redFont);
            ps44.setAlignment(Element.ALIGN_JUSTIFIED);
            cs44.addElement(ps44);
            cs44.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(cs44);

            PdfPCell cs45 = new PdfPCell();
            Paragraph ps45 = new Paragraph("23.", redFont);
            cs45.addElement(ps45);
            cs45.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(cs45);

            PdfPCell cs46 = new PdfPCell();
            Paragraph ps46 = new Paragraph("No delay in exercising of omissiojn to exercise any right, power or remedy accruing to the Bank upon any default or otherwise under these presents or under any security documents or letters of guarantee shall impair or prejudice any such right, power or remedy or shall be construed to be a waiver thereof or any acquiescence therein.\n", redFont);
            ps46.setAlignment(Element.ALIGN_JUSTIFIED);
            cs46.addElement(ps46);
            cs46.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(cs46);

            PdfPCell cs47 = new PdfPCell();
            Paragraph ps47 = new Paragraph("24.", redFont);
            cs47.addElement(ps47);
            cs47.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(cs47);

            PdfPCell cs48 = new PdfPCell();
            Paragraph ps48 = new Paragraph("Nothing contained in these presents and / or in any security documents executed / to be executed by the Borrower in favour of the  Bank in respect of the said loan shall in any way prejudice or affect any mortgage, hypothecation, pledge, lien or any other charge created /to be created by the Borrower in favour of the Bank for securing any other facility or accommodation (such as cash credit, overdraft, tea hypothecation etc.) granted / to be granted in its absolute discretion by the Bank to the Borrower.\n", redFont);
            ps48.setAlignment(Element.ALIGN_JUSTIFIED);
            cs48.addElement(ps48);
            cs48.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(cs48);

            PdfPCell cs49 = new PdfPCell();
            Paragraph ps49 = new Paragraph("25.", redFont);
            cs49.addElement(ps49);
            cs49.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(cs49);

            PdfPCell cs50 = new PdfPCell();
            Paragraph ps50 = new Paragraph("It is hereby expressly agreed that in case of any subrogation in place of the Bank in respect of the securities for the said loan, so far as the securities charged by the Borrower to the Bank for repayment of both the Borrower's indebtedness for the said loan and for the said any other credit facility are concerned, the Bank’s charge over the said securities for the said any other credit facility shall, if not in priority to the charge over the same securities in respect of the said loan, shall rank pari-passu with the charge over the said securities in respect of the said loan.\n", redFont);
            ps50.setAlignment(Element.ALIGN_JUSTIFIED);
            cs50.addElement(ps50);
            cs50.setBorder(PdfPCell.NO_BORDER);
            tablekk6.addCell(cs50);

            document.add(tablekk6);
            document.newPage();
//            document.add(new Paragraph("\n\n\n\n\n\n"));
            document.add(new Paragraph("In witness where of the borrower executed these presents on the day and the year first hereinabove written.\n\n", redFont));

            Paragraph rp54 = new Paragraph(17, "Signed and delivered by the above named Borrower or The Common Seal of the above named Borrower was hereunto affixed by.................................................................................\n"
                    + "who signed and delivered these presents.", redFont);
            rp54.setAlignment(Element.ALIGN_JUSTIFIED);
            rp54.setSpacingAfter(5);
            document.add(rp54);

            Paragraph rp55 = new Paragraph(17, ".............................................\n", redFont);
            rp55.setAlignment(Element.ALIGN_RIGHT);
            document.add(rp55);
            Paragraph rp56 = new Paragraph("(Signature)", redFont);
            rp56.setSpacingAfter(25);
            rp56.setIndentationLeft(440);
            document.add(rp56);

            Paragraph rp57 = new Paragraph(17, "Dated the ................................  Day of ..............................................\n"
                    + "Borrower: " + applicantName, font11n);
            rp57.setSpacingAfter(5);
            document.add(rp57);

        } catch (FileNotFoundException fnf) {
            logger.error("Error:.....!!! " + fnf.getMessage());
        } catch (DocumentException de) {
            logger.error("Error:.....!!! " + de.getMessage());
        } catch (IOException ex) {
            logger.error("Error:.....!!! " + ex.getMessage());
        }
        document.close();

        logger.debug("***********Ended createPdf()***********");
    }

    public Chunk underLine(String data) {
        Paragraph pp9 = new Paragraph();
        pp9.setAlignment(Element.ALIGN_JUSTIFIED);
        Chunk underline1 = new Chunk(data, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL));
        underline1.setUnderline(0.1f, -2f);
        pp9.add(underline1);
        return underline1;
    }

    private static String NumberToWord1(String number) {
        String twodigitword = "";
        String word = "";
        String[] HTLC = {"", "Hundred", "Thousand", "Lakh", "Crore"}; //H-hundread , T-Thousand, ..
        int split[] = {0, 2, 3, 5, 7, 9};
        String[] temp = new String[split.length];
        boolean addzero = true;
        int len1 = number.length();
        if (len1 > split[split.length - 1]) {
            System.out.println("Error. Maximum Allowed digits " + split[split.length - 1]);
            System.exit(0);
        }
        for (int l = 1; l < split.length; l++) {
            if (number.length() == split[l]) {
                addzero = false;
            }
        }
        if (addzero == true) {
            number = "0" + number;
        }
        int len = number.length();
        int j = 0;
        //spliting & putting numbers in temp array.
        while (split[j] < len) {
            int beg = len - split[j + 1];
            int end = beg + split[j + 1] - split[j];
            temp[j] = number.substring(beg, end);
            j = j + 1;
        }

        for (int k = 0; k < j; k++) {
            twodigitword = ConvertOnesTwos(temp[k]);
            if (k >= 1) {
                if (twodigitword.trim().length() != 0) {
                    word = twodigitword + " " + HTLC[k] + " " + word;
                }
            } else {
                word = twodigitword;
            }
        }
        return (word);
    }

    private static String ConvertOnesTwos(String t) {
        final String[] ones = {"", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
        final String[] tens = {"", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};

        String word = "";
        int num = Integer.parseInt(t);
        if (num % 10 == 0) {
            word = tens[num / 10] + " " + word;
        } else if (num < 20) {
            word = ones[num] + " " + word;
        } else {
            word = tens[(num - (num % 10)) / 10] + word;
            word = word + " " + ones[num % 10];
        }
        return word;
    }

    public String getIndianCurrencyFormat(String amount) {
        StringBuilder stringBuilder = new StringBuilder();
        char amountArray[] = amount.toCharArray();
        int a = 0, b = 0;
        for (int i = amountArray.length - 1; i >= 0; i--) {
            if (a < 3) {
                stringBuilder.append(amountArray[i]);
                a++;
            } else if (b < 2) {
                if (b == 0) {
                    stringBuilder.append(",");
                    stringBuilder.append(amountArray[i]);
                    b++;
                } else {
                    stringBuilder.append(amountArray[i]);
                    b = 0;
                }
            }
        }
        return stringBuilder.reverse().toString();
    }
}
