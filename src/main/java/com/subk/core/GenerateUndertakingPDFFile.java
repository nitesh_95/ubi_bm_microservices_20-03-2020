/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.core;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;
import com.subk.entity.Loanorigination;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Raj. k
 */
@Configuration
public class GenerateUndertakingPDFFile extends PDFGenerationAbstract {

	@Value("${MCLRL}")
	private String mclrl;         
			
    private String empty = " ";
    private static final Logger logger = Logger.getLogger(GenerateUndertakingPDFFile.class);
//    private static final ResourceBundle rb = ResourceBundle.getBundle("systemPath");
//    private static final ResourceBundle rb1 = ResourceBundle.getBundle("com.subk.Resources//subkResources");

    public String getPdf(Loanorigination loanData, String fileName,String pathAdd, String img, String jsPaths) {
        logger.info("************ ENTERED ************* ");

        String destPath = null;

        logger.debug("UBILoanOrigination Data: " + loanData.toString());
        logger.debug("FileName: " + fileName);

        try {
            destPath =pathAdd;
            destPath = destPath + fileName;

            //File file = new File(destPath);
            //file.getParentFile().mkdirs();
            new GenerateUndertakingPDFFile().createPdf(destPath, loanData, img, jsPaths);
        } catch (Exception ex) {
            logger.error("Error:....!!! " + ex.getMessage());
        }

        logger.info("destPathINATION PATH:...........!!!!!!!!!!  " + destPath);
        logger.info("************ ENDED.************* ");
        return destPath;
    }

    public void createPdf(String dest, Loanorigination loanData , String img, String jsPaths) {
        logger.debug("***********Entered***********");
        final String imgPath = img;
        final String mclrVal = mclrl;
    	BigDecimal mclrMF = new BigDecimal("0.0");

		String gender = loanData.getSex();
		if (gender.equals("MALE")) {
			if (loanData.getMclrMaleFemale() != null) {
				Double dd = new Double(loanData.getMclrMaleFemale());
				mclrMF = BigDecimal.valueOf(dd);
			}

		} else if (gender.equals("FEMALE")) {
			if (loanData.getMclrMaleFemale() != null) {
				Double dd = new Double(loanData.getMclrMaleFemale());
				mclrMF = BigDecimal.valueOf(dd);
			}
		}
        Document document = null;
        PdfWriter writer = null;

        try {
            String appl1Address = (loanData.getAppl1address() != null && loanData.getAppl1address() != "" && !loanData.getAppl1address().equals("null") ? loanData.getAppl1address() : " ");
            document = new Document();
            writer = PdfWriter.getInstance(document, new FileOutputStream(dest, false));
            //writer.setPageEvent(new Header());
            document.open();

            Font canFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
            Font canFont11 = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
            Font canFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 13, Font.BOLD);
            Font font14n = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL);
            Font font14b = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
            Font font13b = new Font(Font.FontFamily.TIMES_ROMAN, 13, Font.BOLD);
            Font font13bu = new Font(Font.FontFamily.TIMES_ROMAN, 13, Font.BOLD);
            Font font12n = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
            Font font12bi = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLDITALIC);
            Font font12b = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
            Font font12bu = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);

            Font font11b = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
            Font font11n = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.NORMAL);
            Font font11bu = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);

            Font font10b = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
            Font font10n = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

            Font font9b = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
            Font font9n = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL); //subash font used for table content
            Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
            Font redFont11 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);
            Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 12);
            Font SUBFONT = new Font(Font.getFamily("TIMES_ROMAN"), 12, Font.BOLD | Font.UNDERLINE);
            //Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 11);
            Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLDITALIC);
            //  Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLDITALIC);
            Font canFont2 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);

            Font canFont15 = new Font(Font.FontFamily.TIMES_ROMAN, 15, Font.BOLD);
            Font font11bi = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLDITALIC);

            //File file = new File("C:\\"+loanData.getAppl1Name()+"_"+loanData.getApplicationId());
            // Get the output stream for writing PDF object
            //ServletOutputStream out = response.getOutputStream();
            //OutputStream oos = new FileOutputStream(file,true);
            //Document document = new Document();
            //PdfWriter writer = PdfWriter.getInstance(document, out);
//            document.open();
            document.addKeywords("Java, Servelet, PDF, iText");
            document.addAuthor("Raj Grover");
            document.addCreator("UBILoanOrigination");

            Paragraph sp1 = new Paragraph("Letter of undertaking to deduct monthly installment", font13b);
            sp1.setAlignment(Element.ALIGN_CENTER);
            sp1.add(new Paragraph(" "));
            document.add(sp1);

            Paragraph subPara = new Paragraph("The Manager\n"
                    + "United Bank of India\n", font11n);
            subPara.add(underLine(loanData.getBranchname()));
            subPara.add(" Branch\n\n"
                    + "Dear Sir,\n\n\n");
            subPara.setAlignment(Element.ALIGN_LEFT);
            document.add(subPara);

            Paragraph sp3 = new Paragraph("Re: Authority to deduct monthly installment.\n\n\n", font14b);
            sp3.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(sp3);

            String instalmentAmt = (loanData.getInstalmentamount() != null && !loanData.getInstalmentamount().equals("null") ? loanData.getInstalmentamount() : " ");

            Paragraph sp4 = new Paragraph("On the strength of this letter, I authorize you to deduct Rs.", font11n);
            sp4.setAlignment(Element.ALIGN_JUSTIFIED);
            sp4.add(underLine(instalmentAmt));
            sp4.add("( Rupees ");
            sp4.add(underLine(NumberToWord.convert(Long.parseLong(instalmentAmt))));
            sp4.add(" only) every month from my (SB / CD) account number _____________________with your branch till the loan is fully liquidated.\n\n");

            document.add(sp4);

            Paragraph sp5 = new Paragraph("I do further authorize you to deduct EMI with higher amount in the event Bank’s MCLR-Y is revised upwards. I affirm that this authority would not be changed or revoked till the said loan account is fully liquidated.\n\n\n", font11n);
            sp5.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(sp5);

            Paragraph sp6 = new Paragraph("Place: _________________\n"
                    + "Date:  _________________\n\n\n\n", font11n);
            sp6.setAlignment(Element.ALIGN_LEFT);
            document.add(sp6);

            Paragraph sp7 = new Paragraph("Yours faithfully,\n\n\n\n\n", font11n);
            sp7.setAlignment(Element.ALIGN_LEFT);
            document.add(sp7);

            Paragraph sp8 = new Paragraph(underLine(loanData.getAppl1name() != null && !loanData.getAppl1name().equals("null") ? loanData.getAppl1name() : " ") + "\n"
                    + "(Full signature of the borrower)\n\n\n\n", font11n);
            sp8.setAlignment(Element.ALIGN_LEFT);
            document.add(sp8);

            //Page-2 start##################################################
            document.newPage();

            Paragraph sp9 = new Paragraph("Text of the consent clause to be taken from the Borrowers along with loan documents\n\n", font12bu);
            sp9.setAlignment(Element.ALIGN_LEFT);
            document.add(sp9);

            Paragraph sp10 = new Paragraph("I/we, understand that as a pre-condition relating to grant of the loan/advances/other non-fund based credit facilities to me/us, United Bank of India, requires my/our consent for the disclosure by the Bank of information and data relating to me/us, of the credit facility availed of/to be availed, by me/us, obligations assumed/to be assumed by me/us, in relation thereto and default, if any, committed by me/us, in discharge thereof.\n\n1. Accordingly, I/we, hereby agree and give consent for the disclosure by United Bank of India of all or any such ;\n"
                    + "\n"
                    + "(a) Information and data relating to me/us ;\n"
                    + "\n"
                    + "(b) The information or data relating to any credit facility availed of/to be availed, by me/us; and\n"
                    + "\n"
                    + "(c) Default, if any, committed by me/us, in discharge of my/our such obligation as United Bank of India may deem appropriate and necessary, to disclose and furnish to Credit Information Bureau (India) Ltd. and any other agency authorized in this behalf by RBI.\n"
                    + "\n"
                    + "2. I/we, declare that the information and data furnished by me/us to United Bank of India are true and correct.\n"
                    + "\n"
                    + "3. I/we, undertake that\n"
                    + "\n"
                    + "(a) The Credit Information Bureau (India) Ltd. and any other agency so authorized may use, process the said information and data disclosed by the bank in the manner as deemed fit by them; and\n"
                    + "\n"
                    + "(b) The Credit Information Bureau (India) Ltd. and any other agency so authorized may furnish for consideration, the processed information and data of products thereof prepared by them, to bank/financial institutions and other credit grantors of registered users, as may be specified by the Reserve Bank in this behalf.\n\n\n\n\n\n", font11n);
            sp10.setAlignment(Element.ALIGN_LEFT);
            document.add(sp10);

            Paragraph sp11 = new Paragraph(underLine(loanData.getAppl1name() != null && !loanData.getAppl1name().equals("null") ? loanData.getAppl1name() : " ") + "\n" + "Signature(s) of Borrower(s)\n", font11n);
            sp11.setAlignment(Element.ALIGN_LEFT);
            document.add(sp11);

            //Page-3 start##################################################
            document.newPage();

            Paragraph sp12 = new Paragraph("Place……………………….\n"
                    + "Date ……………………….\n"
                    + "Manager\n"
                    + "United Bank of India\n" + loanData.getBranchname() + " Branch", font11n);

            document.add(sp12);
            String applicantName = (loanData.getAppl1name() != null && loanData.getAppl1name() != "" && !loanData.getAppl1name().equals("null") ? loanData.getAppl1name() : " ");
            String applicantFhgName = (loanData.getFhgname() != null && loanData.getFhgname() != "" && !loanData.getFhgname().equals("null") ? loanData.getFhgname() : " ");
            String applicantAddress = (loanData.getAppl1address() != null && loanData.getAppl1address() != "" && !loanData.getAppl1address().equals("null") ? loanData.getAppl1address() : " ");

            Paragraph sp13 = new Paragraph("Declaration on execution of documents\n\n", font14n);
            sp13.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(sp13);

            Paragraph sp14 = new Paragraph("This is to place on record that I/we ", redFont11);
            sp14.add(underLine(applicantName + " S/o,W/o,D/o " + applicantFhgName + " residing at address " + applicantAddress));
            sp14.add(new Chunk(" (Mention name of Father/Husband, registered office wherever applicable, Age, official/business address)"
                    + "have executed the under noted documents on __________________ being present in the branch premises of United Bank of India " + loanData.getBranchname() + " Branch / or at ___________________________________________________________"
                    + "(Mention the name of the place)"
                    + "_______________________________ being explained to me / us in connection with my/our _______________ Account.\n I/we authorize Mr./Mrs./Miss ...............................................……………………………………."
                    + "of .................................................................... .................................................................................................. (address) to fill in the blank spaces of documents executed by me/us and the same have been filled in under my/our instructions and I/we have understood fully the contents and purpose of the said documents and the necessity of execution of such documents.\n"
                    + "Documents executed\n"
                    + "1.\n"
                    + "2.\n"
                    + "3.\n"
                    + "4.\n"
                    + "5.\n"
                    + "6.\n"
                    + "7.\n"
                    + "8.\n"
                    + "9.\n"
                    + "10.\n"
                    + "\n"
                    + "Signature of witness\n"
                    + "Full Name \n" + "Address\n"
                    + "Date ", font11n));
            sp14.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(sp14);

            Paragraph k1 = new Paragraph();
            k1.add(new Chunk("Signature of the executant\n", redFont11));
            k1.add(new Chunk("Full Name " + applicantName + "\n", redFont11));
            k1.add(new Chunk("Address " + applicantAddress + "\n", redFont11));
            k1.setAlignment(Element.ALIGN_RIGHT);
            document.add(k1);

            Paragraph sp15 = new Paragraph("(Witness should be taken, for Illiterate person and person putting vernacular signature and the witness should invariably be an employee of the Bank)\n\n", font11b);
            sp15.setAlignment(Element.ALIGN_LEFT);
            document.add(sp15);

            //Page-4 start##################################################
            document.newPage();

            Paragraph sp16 = new Paragraph("Demand Promissory Note\n\n\n\n", font13bu);
            sp16.setAlignment(Element.ALIGN_CENTER);
            document.add(sp16);

            Paragraph sp17 = new Paragraph("Place : ..................................                                                                             Rs: " + (loanData.getLoanamountsactioned() != null && !loanData.getLoanamountsactioned().equals("null")
                    && !loanData.getLoanamountsactioned().isEmpty() ? loanData.getLoanamountsactioned() : empty) + "\n"
                    + "Date : ...................................\n\n\n"
                    + "On demand I / We jointly and severally promise to pay UNITED BANK OF INDIA or order at Kolkata or ", font10n);
            sp17.add(underLine(loanData.getBranchname()));
            sp17.add(" the sum of Rupees ");
            sp17.add(underLine(loanData.getLoanamountsactioned() != null && !loanData.getLoanamountsactioned().equals("null")
                    && !loanData.getLoanamountsactioned().isEmpty() ? loanData.getLoanamountsactioned() : empty));
            sp17.add(" Loan amount in words: ");
            sp17.add(underLine(NumberToWord.convert(Long.parseLong(loanData.getLoanamountsactioned() != null && !loanData.getLoanamountsactioned().equals("null")
                    && !loanData.getLoanamountsactioned().isEmpty() ? loanData.getLoanamountsactioned() : empty))));
            sp17.add(" with interest thereon at the rate of ");
            sp17.add(underLine(String.valueOf(mclrMF)));
            sp17.add(" percent per annum below / at / over the United Bank of India MCLR-Y which is at present ");

            logger.info("**************mclr_fixed: " + Float.parseFloat(mclrVal));
            logger.info("**************mclr: " + mclrMF);

            BigDecimal appropriateRate = new BigDecimal(mclrVal);
            appropriateRate = appropriateRate.add(mclrMF);

            System.out.println("appropriateRate****************************: " + appropriateRate);
            sp17.add(underLine(String.valueOf(mclrVal)));
            sp17.add(" % per annum with ");
            sp17.add(underLine("monthly"));
            sp17.add(" rests for value received / with interest thereon at the rate of .......");
            sp17.add("% annum with ....................");
            sp17.add(" rests for value received.\n\n");
            sp17.setAlignment(Element.ALIGN_LEFT);
            document.add(sp17);

            Paragraph sp18 = new Paragraph("Address of executant(s):\n"
                    + "" + (loanData.getAppl1name() != null && !loanData.getAppl1name().equals("null")
                    && !loanData.getAppl1name().isEmpty() ? loanData.getAppl1name() : empty) + "	"
                    + "					\n"
                    + "" + (loanData.getPermhouseno() != null && !loanData.getPermhouseno().equals("null")
                    && !loanData.getPermhouseno().isEmpty() ? loanData.getPermhouseno() : empty) + ",\n"
                    + (loanData.getPermstreetno() != null && !loanData.getPermstreetno().equals("null")
                    && !loanData.getPermstreetno().isEmpty() ? loanData.getPermstreetno() : empty) + ","
                    + (loanData.getPermvillcity() != null && !loanData.getPermvillcity().equals("null")
                    && !loanData.getPermvillcity().isEmpty() ? loanData.getPermvillcity() : empty) + ","
                    + (loanData.getPermlandmark() != null && !loanData.getPermlandmark().equals("null")
                    && !loanData.getPermlandmark().isEmpty() ? loanData.getPermlandmark() : empty) + ","
                    + (loanData.getPermdistrict() != null && !loanData.getPermdistrict().equals("null")
                    && !loanData.getPermdistrict().isEmpty() ? loanData.getPermdistrict() : empty)
                    + "\n\n", font11n);
            sp18.setAlignment(Element.ALIGN_LEFT);
            document.add(sp18);

            PdfContentByte cbr40 = writer.getDirectContent();
            cbr40.saveState();
            cbr40.rectangle(360, 460, 45, 45);
            cbr40.stroke();
            cbr40.restoreState();

            //Page-5 start##################################################
            document.newPage();

            Paragraph sp19 = new Paragraph("LETTER OF LIEN\n", font12b);
            sp19.setAlignment(Element.ALIGN_LEFT);
            document.add(sp19);

            Paragraph sp20 = new Paragraph("Date ..............................................\n", font11n);
            sp20.setAlignment(Element.ALIGN_RIGHT);
            document.add(sp20);

            Paragraph sp21 = new Paragraph("To,\n", font11n);
            sp21.setAlignment(Element.ALIGN_LEFT);
            document.add(sp21);

            Paragraph sp22 = new Paragraph("UNITED BANK OF INDIA\n", font11n);
            sp22.add(loanData.getBranchname());
            sp22.add(" Branch\n");
            sp22.add("_____________________\n\n");
            sp22.setAlignment(Element.ALIGN_LEFT);
            document.add(sp22);

            Paragraph sp23 = new Paragraph("Dear Sir,\n", font11n);
            sp23.setAlignment(Element.ALIGN_LEFT);
            document.add(sp23);

            Paragraph sp24 = new Paragraph("In consideration of the advances already made and/or agreed to be made by you from time to time at your absolute discretion to me/us or to ................................ at my/ our request and also other advances or credit or accommodation that you may make form time to time to me/us or to anybody else at my/our request, I/We hereby give you a lien on all share stocks debentures stocks securities and cash belonging to me/us and which are now or may hereafter be held by you or be in your custody on my/our account  (hereinafter referred to us ‘‘the said securities’’) for the general outstanding balance of any and every loan, current, overdraft, cash credit or other account or accounts whatsoever with you and in respect where of I/We are or may become liable to you as borrower(s) guarantor(s) or otherwise and whether solely or jointly with others including Stamp duties, ordinary bank charges, commission, costs charges and expenses incidental to the said account and/or this security and interest at your usual rate or rates on similar account or accounts or at such other rate as may be fixed by you form time to time in respect of the said account or accounts and/or  I/We authorise you on non-payment by me/us to you on demand of the aforesaid advances or if whenever the value of the said securities held by you or in  your custody does not exceed my/our indebtedness by ................................................ Percent or is not made up to that amount on demand to realise in such manner as you may think fit all or any part of the said securities without further notice to me/us and you shall be entitled to attach to the said securities whatsoever stamps if any as may be required for making them valid in law or for effecting such realisation and do apply the proceeds after deduction of the costs and expenses incurred by you in that behalf towards satisfactions of my/our indebtedness to you crediting my/our account or accounts with the balance if any and in case of any deficit in the amount recovered out of the said securities and your dues from me/us / I/We shall remain liable to make good the same and I/We authorise you at my our expense to collect and receive and give receipts for and recover any dividends interest and money in relation to the said securities and apply the same towards satisfaction of my/our indebtedness to you as aforesaid and I/We undertake to execute and do all assurances and acts which may be necessary to effectuate this security and such realisation and pay all stamp duties incidental thereto and I/We further agree and declare that this security shall both as to my/our past and future indebtedness ensure for the benefit of any concern with which you may be incorporated by absorption, amalgamation or otherwise and also for the benefit of your assigns and I/We further declare that you will be at liberty to re-pledge or create any other security over all or any part of the said securities. This security will be a continuing security notwithstanding any partial payments or any fluctuations of accounts other existence of a credit balance on any account at any time and I/We declare that all present securities are my/our absolute property at my/our sole disposal and free from any charge or encumbrance and that all future securities hereunder shall be likewise my/our unencumbered disposable property and if and whenever the securities are held for my/our liability for any third party obligation to you, you shall be free without reference to me/us to deal with the principal debtor and any securities obligations or decrees and generally to act as if I/We were primarily liable and the provisions of your / General Guarantee Form in use for the time being to the full extent applicable hereto shall be deemed incorporated herein and any notice by way of request demand or otherwise hereunder may be given by you to me / us or any of us personally or may be left at the then or last known place of business or residence in India of me/us or any of us addressed to me/us or any of us or any be sent by post to me/us/or any of us addressed as aforesaid and if sent by post it shall be deemed to have been given at the time when it would be delivered in due course of post and in proving such notice when given by post it shall be sufficient to prove that the envelope containing the notice was posted and a certificate signed by your local Agent that the envelope was so posted shall be conclusive.  If by reason of absence from India or otherwise I/We or any of us cannot be given any such notice the same if inserted once   as an advertisement in a newspaper circulating in the District where the last known address is situated shall be deemed to have been effectually given and received on the day on which such advertisement appears.\n\n", font9n);
            sp24.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(sp24);

            Paragraph sp25 = new Paragraph("Yours faithfully,\n" + applicantName, font9n);
            sp25.setAlignment(Element.ALIGN_LEFT);
            document.add(sp25);

            //Page-6 start##################################################
            document.newPage();

            Paragraph sp26 = new Paragraph("HYPOTHECATION AGREEMENT\n", font10n);
            sp26.setAlignment(Element.ALIGN_LEFT);
            document.add(sp26);

            Paragraph sp27 = new Paragraph("(Goods, Book debts, Plant & Machineries)\n\n", font9n);
            sp27.setAlignment(Element.ALIGN_LEFT);
            document.add(sp27);

            String sanctionedLoanAmt = loanData.getLoanamountsactioned() != null && !loanData.getLoanamountsactioned().equals("null") && loanData.getLoanamountsactioned() != " " ? loanData.getLoanamountsactioned() : "";
            Paragraph sp28 = new Paragraph();
            sp28.add(new Chunk("UNITED BANK OF INDIA ", font10b));
            sp28.add(new Chunk("(hereinafter called ‘the bank’) having at the request of ", redFont11));
            sp28.add(underLine(applicantName + " S/o,W/o,D/o " + applicantFhgName + " residing at address" + applicantAddress));
            sp28.add(new Chunk(" (hereinafter called ‘the borrower’) agreed to grant accommodation to the borrower by way of Overdraft/Cash Credit Account up to a limit of Rs. ", font9n));
            sp28.add(new Chunk(underLine(sanctionedLoanAmt)));
            sp28.add(new Chunk(" on the security hereof and pursuant thereto the bank has called upon the borrower to execute the Deed of Hypothecation being these presents in favour of the bank which the borrower has agreed to do in the manner hereinafter appearing.\n\n", font9n));
            sp28.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(sp28);

            PdfPTable tablekk1 = new PdfPTable(2);
            tablekk1.setWidthPercentage(100f);
            tablekk1.setWidths(new float[]{1, 15});
            tablekk1.setHorizontalAlignment(Element.ALIGN_LEFT);

            PdfPCell cs1 = new PdfPCell();
            Paragraph ps1 = new Paragraph("1.", font9n);
            cs1.addElement(ps1);
            cs1.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs1);

            PdfPCell cs2 = new PdfPCell();
            Paragraph ps2 = new Paragraph("That the whole of the Borrower’s stock consisting of stocks of ……………………………………………………………………………………………...………………………………………………………………………………………………………………………whether raw or in process of manufacture and all products goods and movable property of any kind which now or hereafter from time to time during this security shall be brought into stored or be in or about the borrower’s premises or godowns at " + ""
                    + "...................................................................................."
                    + "(hereinafter called the ‘said goods’) or wherever else the same may be (including any goods in course of transit) with the benefit of all rights relating thereto on sale thereof or otherwise shall remain hypothecated to the bank and its assigns by way of first charge as security for the due repayment to the bank on demand of the balance of accounts at any time on the cash credit account and future indebtedness and liabilities of the borrower to the bank of any kind in any manner whether solely or jointly primary collateral accrued or accruing with all relative interest charges costs (as between attorney and client) and expenses.\n", font9n);
            ps2.setAlignment(Element.ALIGN_JUSTIFIED);
            cs2.addElement(ps2);
            cs2.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs2);

            PdfPCell cs3 = new PdfPCell();
            Paragraph ps3 = new Paragraph("2.", font9n);
            cs3.addElement(ps3);
            cs3.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs3);

            PdfPCell cs4 = new PdfPCell();
            Paragraph ps4 = new Paragraph("That all the borrower’s present and future book debts outstanding money receivable claims bills contracts engagements securities rights and movable assets (except property effectively otherwise hypothecated, charged or mortgaged to the bank) shall be hypothecated to the bank and its assigns by way of first charge as security for the payment to the bank on demand of the balance of accounts at any time and for the payment and discharge of all the borrower’s present and future indebtedness and liabilities to the bank of any kind in any manner whether solely or jointly primary or collateral accrued or accruing with all relative interest charges costs (as between attorney and client) and expenses.\n", font9n);
            ps4.setAlignment(Element.ALIGN_JUSTIFIED);
            cs4.addElement(ps4);
            cs4.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs4);

            PdfPCell cs5 = new PdfPCell();
            Paragraph ps5 = new Paragraph("3.", font9n);
            cs5.addElement(ps5);
            cs5.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs5);

            PdfPCell cs6 = new PdfPCell();
            Paragraph ps6 = new Paragraph("The borrower hereby hypothecates by way of first charge in favour of the bank and its assigns all plant, machinery, engines, boilers, appliances, tools and implements described in general terms in the schedule hereto (hereinafter referred to as ‘the plant & machinery’) which are now or shall hereafter at any time or from time to time during the continuance of the security hereby created be brought in or stored of to be in or about the borrower’s premises or godowns at……………………………………… ……………………………………………………………………………………………..……………………………………………………….. or wherever else the same may be (including plant & machinery in course of transit) as security for the due repayment to the bank on demand of the balance of accounts at any time on the said Overdraft/Cash Credit A/c or otherwise and for the payment of all present and future indebtedness and liabilities of the borrower to the bank of any kind in any manner whether solely or jointly primary or collateral accrued or accruing with all relative interest charges costs (as between attorney and client) and expenses.\n", font9n);
            ps6.setAlignment(Element.ALIGN_JUSTIFIED);
            cs6.addElement(ps6);
            cs6.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs6);

            PdfPCell cs7 = new PdfPCell();
            Paragraph ps7 = new Paragraph("4.", font9n);
            cs7.addElement(ps7);
            cs7.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs7);

            PdfPCell cs8 = new PdfPCell();
            Paragraph ps8 = new Paragraph("That interest at the rate of ", font9n);
            ps8.add(underLine(String.valueOf(mclrMF)));
            ps8.add(" per cent per annum over the United Bank of India’s MCLR-Y which is at present ");
            ps8.add(underLine(String.valueOf(mclrVal)));
            ps8.add(" % per annum with");
            ps8.add(underLine(" monthly"));
            ps8.add(" rests and/or at the rate of......... ");
            ps8.add("% per annum with .............  ");
            ps8.add(" rests at such other rate as may be communicated to the Borrower by the Bank from time to time shall be calculated on the daily balance of accounts and charged in accordance with the practice of the Bank. Provided however, the bank shall at any time and from time to time be entitled to change the rate and terms of interest and may thereafter charge interest at such rate and on such terms as the bank may specify and this agreement shall be constituted as if such revised rate and terms of interest were incorporated herein provided further however, the bank shall also be entitled to charge at its discretion such enhance rate of interest of the entire outstanding or on a portion thereof as the bank may fix for any irregularity and for such period as the irregularity continues or for such time as the bank deem if necessary regard being on to the nature of irregularity and charging of such enhanced rate of interest shall be without prejudice to the other rights and remedies of the bank.\n");
            ps8.setAlignment(Element.ALIGN_JUSTIFIED);
            cs8.addElement(ps8);
            cs8.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs8);

            //Page-7 start ##########################################################
            PdfPCell cs9 = new PdfPCell();
            Paragraph ps9 = new Paragraph("5.", font9n);
            cs9.addElement(ps9);
            cs9.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs9);

            PdfPCell cs10 = new PdfPCell();
            Paragraph ps10 = new Paragraph("That the Bank will from time to time advise the Borrower of the margin of value to be maintained by the Borrower in favour of the Bank and that the Borrower shall at all times maintain the said goods of a sufficient quantity and value (as estimated by the Bank) so as to provide the margin advised by the Bank from time to time and shall forthwith whenever necessary provide further goods (approved by the Bank) to restore such margin or pay the Bank equivalent in cash.\n", font9n);
            ps10.setAlignment(Element.ALIGN_JUSTIFIED);
            cs10.addElement(ps10);
            cs10.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs10);

            PdfPCell cs11 = new PdfPCell();
            Paragraph ps11 = new Paragraph("6.", font9n);
            cs11.addElement(ps11);
            cs11.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs11);

            PdfPCell cs12 = new PdfPCell();
            Paragraph ps12 = new Paragraph("That the Bank shall not be required to make or continue advances otherwise than at the Bank’s discretion and in no circumstances to an amount exceeding with interest thereon the sum of Rs ", font9n);
            ps12.add(underLine((loanData.getLoanamountsactioned() != null && !loanData.getLoanamountsactioned().equals("null")
                    && !loanData.getLoanamountsactioned().isEmpty() ? loanData.getLoanamountsactioned() : empty)));
            ps12.add(new Chunk(" being the limit mentioned as aforesaid or a sum equal to the value of the goods to be maintained by the Borrower hereunder less the margin of value which the Borrower is required to maintain as provided in Clause 5 hereof whichever may be the less.\n", font9n));
            ps12.setAlignment(Element.ALIGN_JUSTIFIED);
            cs12.addElement(ps12);
            cs12.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs12);

            PdfPCell cs13 = new PdfPCell();
            Paragraph ps13 = new Paragraph("7.", font9n);
            cs13.addElement(ps13);
            cs13.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs13);

            PdfPCell cs14 = new PdfPCell();
            Paragraph ps14 = new Paragraph("That the goods/plant & machineries or such portion thereof as the bank may require shall be kept and maintained by the Borrower at his risk and expense in good condition and fully insured against loss or damage by fire/pilferage, accident or from any cause whatsoever as may be required by the Bank with an Insurance Company of repute to be approved of by the Bank in the joint names of the Bank and the Borrower and the Insurance Policies shall be transferred and delivered to the Bank. The Borrower will be liable to make good any loss occasioned by the theft wastage, damage, deterioration, or any other cause whatsoever. The plant and machinery shall at all times remain at the risk of the Borrower and he shall be liable for all losses and damages thereto howsoever caused. The Borrower shall duly and punctually pay all premium for such policies of Insurance and shall produce from time to time to the Bank all receipts of such premium paid on such policies of Insurance.\n", font9n);
            ps14.setAlignment(Element.ALIGN_JUSTIFIED);
            cs14.addElement(ps14);
            cs14.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs14);

            PdfPCell cs15 = new PdfPCell();
            Paragraph ps15 = new Paragraph("8.", font9n);
            cs15.addElement(ps15);
            cs15.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs15);

            PdfPCell cs16 = new PdfPCell();
            Paragraph ps16 = new Paragraph("The goods, plant and machinery and each and every one of them shall be kept in possession of the Borrower at the said premises or godowns or at such other address as the Bank may by writing authorize free from distress, execution or other legal process, whatsoever and the Borrower shall on no occasion resell, assign or deal with nor shall the Borrower part with possession thereof or any portion thereof.\n", font9n);
            ps16.setAlignment(Element.ALIGN_JUSTIFIED);
            cs16.addElement(ps16);
            cs16.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs16);

            PdfPCell cs17 = new PdfPCell();
            Paragraph ps17 = new Paragraph("9.", font9n);
            cs17.addElement(ps17);
            cs17.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs17);

            PdfPCell cs18 = new PdfPCell();
            Paragraph ps18 = new Paragraph("All rents, rates, taxes, impositions and other outgoings payable by the Borrower in respect of the said premises or godowns where the goods, plant and machinery may be kept/ located shall be duly and punctually paid and discharged by the Borrower and the current receipts thereof shall be produced to the Bank by the Borrower on demand.\n", font9n);
            ps18.setAlignment(Element.ALIGN_JUSTIFIED);
            cs18.addElement(ps18);
            cs18.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs18);

            PdfPCell cs19 = new PdfPCell();
            Paragraph ps19 = new Paragraph("10.", font9n);
            cs19.addElement(ps19);
            cs19.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs19);

            PdfPCell cs20 = new PdfPCell();
            Paragraph ps20 = new Paragraph("The goods, plant and machinery shall be used for the purpose of the Borrower’s business of and for no other purpose whatsoever save with the previous consent of the Bank in writing.\n", font9n);
            ps20.setAlignment(Element.ALIGN_JUSTIFIED);
            cs20.addElement(ps20);
            cs20.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs20);

            PdfPCell cs21 = new PdfPCell();
            Paragraph ps21 = new Paragraph("11.", font9n);
            cs21.addElement(ps21);
            cs21.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs21);

            PdfPCell cs22 = new PdfPCell();
            Paragraph ps22 = new Paragraph("The Borrower hereby expressly warrants that the said premises or godowns in which the goods, plant and machinery are to be located are free from any mortgage encumbrances lispendenses other charge whatsoever except to the Bank.\n", font9n);
            ps22.setAlignment(Element.ALIGN_JUSTIFIED);
            cs22.addElement(ps22);
            cs22.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs22);

            //Page-8 start ##########################################################
            PdfPCell cs23 = new PdfPCell();
            Paragraph ps23 = new Paragraph("12.", font9n);
            cs23.addElement(ps23);
            cs23.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs23);

            PdfPCell cs24 = new PdfPCell();
            Paragraph ps24 = new Paragraph("That the Bank its Agents and Nominees shall be entitled at all times as if absolute owners and without notice to the Borrower but at the Borrower’s risk and expenses and if so required as Agent for and in the name of the Borrower to enter any place where the said goods/plant & machineries may be and inspect value insure and/or take charge and/or possession of all or any part of the said goods/ plant & machineries and/or affix Bank’s Sign Board at its discretion at such place or places as deemed fit by the Bank and on any default of the Borrower in payment of any money hereby secured or the performance of any obligation to the Bank hereunder or the occurrence of any circumstances in the opinion of the Bank endangering this security to recover receive appoint receivers or remove and/or sell by public auction or private contract or otherwise to take possession dispose of or deal with all or any part of the said goods/plant & machineries and to enforce, realize, settle, compromise and deal with any rights aforesaid without being bound to exercise any of these powers or liable for any losses in the exercise thereof and without prejudice to the Bank’s rights and remedies of suit or otherwise and notwithstanding there may be any pending suit or other proceeding the Borrower undertaking to transfer and deliver to the Bank all relative contracts, securities, bazaar chits, bills, notes, hundies and documents and agreeing to accept the bank’s accounts of sale (including the costs thereof) and realization and to pay any shortfall or deficiency thereby shown. Provided that subject to these powers of the Bank and so long as the same is not exercised the Borrower may in the ordinary course of business sell and realize any such goods on the express terms of payment or delivery to the Bank of the proceeds or documents thereof immediately on receipt thereof. If any plant and machinery or any portion thereof be lost or damaged all moneys received or receivable in respect of such Insurance shall be received by the Bank which may apply the same in making good the damage done or in replacing the said plant and machinery by other articles of similar description and such substituted articles shall become subject to the provisions of these presents in the same manner as the plant and machinery for which they have been substituted. The Bank may insure if it so desires apply the money in whole or in part in payment of the amounts payable by the Borrower to the Bank whether it has fallen due or not.\n", font9n);
            ps24.setAlignment(Element.ALIGN_JUSTIFIED);
            cs24.addElement(ps24);
            cs24.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs24);

            PdfPCell cs25 = new PdfPCell();
            Paragraph ps25 = new Paragraph("13.", font9n);
            cs25.addElement(ps25);
            cs25.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs25);

            PdfPCell cs26 = new PdfPCell();
            Paragraph ps26 = new Paragraph("That all the said goods/plant & machineries and all sales realizations and insurance proceeds thereof and all documents under this security shall be held as the Bank’s exclusive property specifically appropriated to this security and the Borrower will not create or suffer any mortgage, charge, lien or encumbrance affecting the same or any part thereof nor do or allow anything that may prejudice this security.\n", font9n);
            ps26.setAlignment(Element.ALIGN_JUSTIFIED);
            cs26.addElement(ps26);
            cs26.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs26);

            PdfPCell cs27 = new PdfPCell();
            Paragraph ps27 = new Paragraph("14.", font9n);
            cs27.addElement(ps27);
            cs27.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs27);

            PdfPCell cs28 = new PdfPCell();
            Paragraph ps28 = new Paragraph("That the Borrower will furnish to the Bank a statement at the end of each month or for such other period as may be advised by the Bank to the Borrower giving details of the said goods as also the cost and market value thereof and if so required by the Bank the price or manufacturing cost of the said goods and verify all statements, reports, returns, certificates and information execute all documents and do all thing which Bank may require to give effect hereto and the Borrower authorizes the Bank its Agents and nominees as Attorney for and in the name of the Borrower to do whatever the Borrower may be required to do hereunder.\n", font9n);
            ps28.setAlignment(Element.ALIGN_JUSTIFIED);
            cs28.addElement(ps28);
            cs28.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs28);

            PdfPCell cs29 = new PdfPCell();
            Paragraph ps29 = new Paragraph("15.", font9n);
            cs29.addElement(ps29);
            cs29.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs29);

            PdfPCell cs30 = new PdfPCell();
            Paragraph ps30 = new Paragraph("That the security will continue notwithstanding the existence of a credit balance on any account at any time or any partial payments or any fluctuation of account.\n", font9n);
            ps30.setAlignment(Element.ALIGN_JUSTIFIED);
            cs30.addElement(ps30);
            cs30.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs30);

            PdfPCell cs31 = new PdfPCell();
            Paragraph ps31 = new Paragraph("16.", font9n);
            cs31.addElement(ps31);
            cs31.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs31);

            PdfPCell cs32 = new PdfPCell();
            Paragraph ps32 = new Paragraph("That nothing herein shall operate to prejudice the Bank’s rights or remedies in respect of any present or future security guarantee, obligation or decree for any indebtedness or liability of the Borrower to the Bank.\n", font9n);
            ps32.setAlignment(Element.ALIGN_JUSTIFIED);
            cs32.addElement(ps32);
            cs32.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs32);

            PdfPCell cs33 = new PdfPCell();
            Paragraph ps33 = new Paragraph("17.", font9n);
            cs33.addElement(ps33);
            cs33.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs33);

            PdfPCell cs34 = new PdfPCell();
            Paragraph ps34 = new Paragraph("That it is declared that all the said goods and all present debts and assets aforesaid are the absolute property of the Borrower at the sole disposal of the Borrower and that the same is free from any prior charge or encumbrance and that all future goods/debts & assets hereunder shall be likewise the unencumbered disposable property of the Borrower. The borrower may in the ordinary course of business deal with the said assets on the express understanding that the same and all proceeds thereof are at all times held for the Bank as its exclusive property specifically and identifiably appropriated to the security and that the Borrower shall not create or suffer any mortgage charge lien or encumbrance to affect the same or any part thereof nor do or allow anything that may prejudice this security.\n", font9n);
            ps34.setAlignment(Element.ALIGN_JUSTIFIED);
            cs34.addElement(ps34);
            cs34.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(cs34);

            //document.add(tablekk1);
            //document.newPage();
            //from 9 to 14
            //PdfPTable tablekk1 = new PdfPTable(2);
            // table.setTotalWidth(288);
            //tablekk1.setWidthPercentage(100f);
            //tablekk1.setWidths(new float[]{1, 15});
            //tablekk1.setHorizontalAlignment(Element.ALIGN_LEFT);
            PdfPCell ck0 = new PdfPCell();
            Paragraph pk0 = new Paragraph(" ", redFont11);
            ck0.addElement(pk0);
            ck0.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck0);

            PdfPCell ck00 = new PdfPCell();
            Paragraph pk00 = new Paragraph("its exclusive property specifically and identifiably appropriated to the security and that the Borrower shall not create or suffer any mortgage charge lien or encumbrance to affect the same or any part thereof nor do or allow anything that may prejudice this security.", redFont11);
            pk00.setAlignment(Element.ALIGN_JUSTIFIED);
            ck00.addElement(pk00);
            ck00.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck00);

            PdfPCell ck1 = new PdfPCell();
            Paragraph pk1 = new Paragraph("18.", redFont11);
            ck1.addElement(pk1);
            ck1.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck1);

            PdfPCell ck2 = new PdfPCell();
            Paragraph pk2 = new Paragraph("That if the Borrower be more than one individual all shall be bound hereby jointly and "
                    + "severally and if the Borrower shall be a Firm, such Firm and all members from time to time "
                    + "thereof shall be bound hereby notwithstanding any changes in the constitution or style "
                    + "thereof and whether the firm shall consider of or be reduced to one individual.", redFont11);
            pk2.setAlignment(Element.ALIGN_JUSTIFIED);
            ck2.addElement(pk2);
            ck2.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck2);

            PdfPCell ck3 = new PdfPCell();
            Paragraph pk3 = new Paragraph("19.", redFont11);
            ck3.addElement(pk3);
            ck3.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck3);

            PdfPCell ck4 = new PdfPCell();
            Paragraph pk4 = new Paragraph("That if and whenever this security shall be held by the Bank for the Borrower’s liability for any third party’s obligations to the Bank the Bank shall be free without reference to the Borrower to deal with the principal debtor and any securities obligation or decrees and generally to act as if the Borrower were primarily liable and the provisions of the Bank’s General Guarantee Form in use for the time being to the full extent applicable hereto shall be deemed incorporated herein", redFont11);
            pk4.setAlignment(Element.ALIGN_JUSTIFIED);
            ck4.addElement(pk4);
            ck4.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck4);

            PdfPCell ck5 = new PdfPCell();
            Paragraph pk5 = new Paragraph("20.", redFont11);
            //pk5.setAlignment(Element.ALIGN_JUSTIFIED);
            ck5.addElement(pk5);
            ck5.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck5);

            PdfPCell ck6 = new PdfPCell();
            Paragraph pk6 = new Paragraph("The Borrower shall not so long as any money remains due and payable to the Bank under "
                    + "the provisions hereof create or purport or attempt or agree to create or concur in the creation "
                    + "of any mortgage or charge or encumbrance on the plant and machinery or on any part "
                    + "thereof/ goods/ debts & assets other than to the Bank.", redFont11);
            pk6.setAlignment(Element.ALIGN_JUSTIFIED);
            ck6.addElement(pk6);
            ck6.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck6);

            PdfPCell ck7 = new PdfPCell();
            Paragraph pk7 = new Paragraph("21.", redFont11);
            ck7.addElement(pk7);
            ck7.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck7);

            PdfPCell ck8 = new PdfPCell();
            Paragraph pk8 = new Paragraph("The Borrower hereby appoints the Bank and its agents to be the attorneys jointly and severally "
                    + "of the Borrower and in the name and on behalf of the Borrower to execute and do such "
                    + "documents and things as the Borrower may be required to execute and do hereunder.", redFont11);
            pk8.setAlignment(Element.ALIGN_JUSTIFIED);
            ck8.addElement(pk8);
            ck8.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck8);

            PdfPCell ck9 = new PdfPCell();
            Paragraph pk9 = new Paragraph("22.", redFont11);
            ck9.addElement(pk9);
            ck9.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck9);

            PdfPCell ck10 = new PdfPCell();
            Paragraph pk10 = new Paragraph("The Borrower hereby expressly warrants that the said premises or godowns in which the plant "
                    + "and machinery are to be located are free from any mortgage encumbrances lispendens other "
                    + "charge whatsoever except to the Bank.", redFont11);
            pk10.setAlignment(Element.ALIGN_JUSTIFIED);
            ck10.addElement(pk10);
            ck10.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck10);

            PdfPCell ck11 = new PdfPCell();
            Paragraph pk11 = new Paragraph("23.", redFont11);
            ck11.addElement(pk11);
            ck11.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck11);

            PdfPCell ck12 = new PdfPCell();
            Paragraph pk12 = new Paragraph("It is agreed and declared by and between the Borrower and the Bank as follows:", redFont11);
            pk12.setAlignment(Element.ALIGN_JUSTIFIED);
            ck12.addElement(pk12);
            ck12.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck12);

            PdfPCell ck13 = new PdfPCell();
            Paragraph pk13 = new Paragraph("a)", redFont11);
            ck13.addElement(pk13);
            ck13.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck13);

            PdfPCell ck14 = new PdfPCell();
            Paragraph pk14 = new Paragraph("The Borrower hereby declares that the Borrower is the absolute and sole beneficial owner of "
                    + "and is seized and possessed of or otherwise entitled to the goods/ plant and machinery/ debt "
                    + "& assets and that the same are and shall be free from all encumbrances, liens, attachments, "
                    + "mortgages, charges, lispendens or otherwise whatsoever.", redFont11);
            pk14.setAlignment(Element.ALIGN_JUSTIFIED);
            ck14.addElement(pk14);
            ck14.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck14);

            PdfPCell ck15 = new PdfPCell();
            Paragraph pk15 = new Paragraph("b)", redFont11);
            ck15.addElement(pk15);
            ck15.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck15);

            PdfPCell ck16 = new PdfPCell();
            Paragraph pk16 = new Paragraph("The Borrower shall permit the Bank and its servants, agents and all persons authorized by "
                    + "the Bank either alone or with workmen and others from time to time and at all reasonable "
                    + "times to enter upon the said premises or godowns or elsewhere and/ or inspect the "
                    + "goods/ plant and machinery and if upon such inspection it appears to the Bank that the "
                    + "plant and machinery or any of them or any part thereof requires repairs or replacements the "
                    + "Bank shall give notice thereof to the Borrower calling upon the Borrower to repair or replace\n"
                    + "the same.", redFont11);
            pk16.setAlignment(Element.ALIGN_JUSTIFIED);
            ck16.addElement(pk16);
            ck16.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck16);

            PdfPCell ck17 = new PdfPCell();
            Paragraph pk17 = new Paragraph("c)", redFont11);
            ck17.addElement(pk17);
            ck17.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck17);

            PdfPCell ck18 = new PdfPCell();
            Paragraph pk18 = new Paragraph("If default shall be made by the Borrower in keeping the goods/ plant and machinery in good "
                    + "condition and serviceable order and as aforesaid or so insured as aforesaid or in repairing or "
                    + "replacing the plant and machinery as aforesaid it shall be lawful for the Bank but not "
                    + "obligatory on the Bank to keep the goods/ plant and machinery in good condition and "
                    + "serviceable order and condition and insured and to replace or repair the same and all sums "
                    + "expended by the Bank for the above purposes or any of them together with interest thereon "
                    + "at the rate aforesaid shall until repayment be a charge on the goods/ plant and machinery "
                    + "and form part of the debt hereby secured and carry interest at the rate aforesaid.", redFont11);
            pk18.setAlignment(Element.ALIGN_JUSTIFIED);
            ck18.addElement(pk18);
            ck18.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck18);

            PdfPCell ck19 = new PdfPCell();
            Paragraph pk19 = new Paragraph("d)", redFont11);
            ck19.addElement(pk19);
            ck19.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck19);

            PdfPCell ck20 = new PdfPCell();
            Paragraph pk20 = new Paragraph("The Borrower shall not without written consent of the Bank first had and obtained remove "
                    + "the plant and machinery or any part thereof from the place where they are now at present "
                    + "and in case of such removal shall replace the same by equipment of equivalent nature and "
                    + "value PROVIDED THAT in the event of the Bank agreeing that any such part of the "
                    + "equipment so removed as aforesaid is redundant or has become worn out or obsolete and "
                    + "need not be replaced the same may be sold and the sale proceeds applied towards the "
                    + "satisfaction or payment of the Bank’s dues hereunder.", redFont11);
            pk20.setAlignment(Element.ALIGN_JUSTIFIED);
            ck20.addElement(pk18);
            ck20.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck20);

            PdfPCell ck21 = new PdfPCell();
            Paragraph pk21 = new Paragraph("e)", redFont11);
            ck21.addElement(pk21);
            ck21.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck21);

            PdfPCell ck22 = new PdfPCell();
            Paragraph pk22 = new Paragraph("In the event of the goods/ plant and machinery depreciating in the value to such an extent that in the opinion of the Bank further security to its satisfaction should be given the "
                    + "Borrower shall from time to time and at all times at its own cost furnish such security as "
                    + "may be considered reasonable and adequate by the Bank provided that the Bank’s decision "
                    + "on the nature and extent of the depreciation shall be final and conclusively binding on the "
                    + "Borrower.", redFont11);
            pk22.setAlignment(Element.ALIGN_JUSTIFIED);
            ck22.addElement(pk18);
            ck22.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck22);

            PdfPCell ck23 = new PdfPCell();
            Paragraph pk23 = new Paragraph("f)", redFont11);
            ck23.addElement(pk23);
            ck23.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck23);

            PdfPCell ck24 = new PdfPCell();
            Paragraph pk24 = new Paragraph("On any default of the Borrower in payment of any money hereby secured or the "
                    + "performance of any obligation to the Bank hereunder or the occurrence of any circumstances "
                    + "in the opinion of the Bank endangering this security the Bank shall be entitled at the "
                    + "Borrower’s risk and expenses as attorney for and in the name of the Borrower or otherwise to "
                    + "take possession to recover receive appoint receivers of any debts or assets under this "
                    + "security give notices and reminds to debtors and third parties liable therefor use for recover, "
                    + "receive and give receipts for the same and sell or realize by public auction or private contract "
                    + "or otherwise of all or any part of such goods/ plant and machinery debts or assets and to "
                    + "enforce, realize settle, compromise submit to arbitration or and deal with any rights aforesaid "
                    + "without being bound to exercise any of these powers or liable for any losses in the exercise "
                    + "thereof and without prejudice to the Bank’s rights and remedies of suit or otherwise and "
                    + "notwithstanding there may be any pending suit or other proceeding the Borrower "
                    + "undertaking to transfer and deliver to the Bank all relative documents, papers, contracts, "
                    + "securities, bazaar chits bills, notes, hundies and documents and agreeing to accept the "
                    + "Bank’s accounts of sale (including the costs thereof) and realization and to pay any short fall "
                    + "or deficiency thereby shown", redFont11);
            pk24.setAlignment(Element.ALIGN_JUSTIFIED);
            ck24.addElement(pk24);
            ck24.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck24);

            PdfPCell ck25 = new PdfPCell();
            Paragraph pk25 = new Paragraph("g)", redFont11);
            ck25.addElement(pk25);
            ck25.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck25);

            PdfPCell ck26 = new PdfPCell();
            Paragraph pk26 = new Paragraph("Upon any such sale as aforesaid the receipt of the Bank for the purchase money shall "
                    + "effectually discharge the purchaser or purchasers there from and from being concerned to "
                    + "see to the application thereof or being answerable for the loss or misapplication thereof.", redFont11);
            pk26.setAlignment(Element.ALIGN_JUSTIFIED);
            ck26.addElement(pk26);
            ck26.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck26);

            PdfPCell ck27 = new PdfPCell();
            Paragraph pk27 = new Paragraph("h)", redFont11);
            ck27.addElement(pk27);
            ck27.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck27);

            PdfPCell ck28 = new PdfPCell();
            Paragraph pk28 = new Paragraph("The Bank or any Receiver appointed by the Bank shall not be answerable or accountable "
                    + "for any involuntary losses which may happen in or about the exercise or execution of any of "
                    + "powers which may be vested in the Bank or the Receiver by virtue of these present.", redFont11);
            pk28.setAlignment(Element.ALIGN_JUSTIFIED);
            ck28.addElement(pk28);
            ck28.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck28);

            PdfPCell ck29 = new PdfPCell();
            Paragraph pk29 = new Paragraph("i)", redFont11);
            ck29.addElement(pk29);
            ck29.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck29);

            PdfPCell ck30 = new PdfPCell();
            Paragraph pk30 = new Paragraph(" The Borrower shall pay all costs charges and expenses as between attorney and client in "
                    + "anywise incurred or paid by the Bank incidental to or in connection with these presents or "
                    + "this security including expenses incurred by the Bank or the Receiver or the servants and "
                    + "agents of the Bank and/ or the Receiver towards inspections of the goods/ plant and "
                    + "machinery/ book debts & assets and incurred as well for the assertion or defence of the "
                    + "rights of the Bank as for the protection of the security of the plant and machinery and for the "
                    + "demand realization recovery of the said principal sum interest and other moneys payable to "
                    + "the Bank and the same shall on demand be paid by the Borrower to the Bank with interest "
                    + "thereon at the rate aforesaid from the time of the same having been so incurred and until "
                    + "such payment the same shall be a charge upon the goods/ plant and machinery/ book debts "
                    + "& assets.", redFont11);
            pk30.setAlignment(Element.ALIGN_JUSTIFIED);
            ck30.addElement(pk30);
            ck30.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck30);

            PdfPCell ck31 = new PdfPCell();
            Paragraph pk31 = new Paragraph("j)", redFont11);
            ck31.addElement(pk31);
            ck31.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck31);

            PdfPCell ck32 = new PdfPCell();
            Paragraph pk32 = new Paragraph("The properties comprised in the said recited Agreement shall be discharged from all "
                    + "rights of release and shall be released by the Bank only on payment by the Borrower to the "
                    + "Bank of the principal amount interest and other moneys secured by the said recited "
                    + "Agreement or by these presents. ", redFont11);
            pk32.setAlignment(Element.ALIGN_JUSTIFIED);
            ck32.addElement(pk32);
            ck32.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck32);

            PdfPCell ck33 = new PdfPCell();
            Paragraph pk33 = new Paragraph("k)", redFont11);
            ck33.addElement(pk33);
            ck33.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck33);

            PdfPCell ck34 = new PdfPCell();
            Paragraph pk34 = new Paragraph(" Nothing contained in these presents or in any enactment shall affect right of the Bank to "
                    + "enforce separately payment of the principal amount interest and other money secured by the "
                    + "said recited Agreement.", redFont11);
            pk34.setAlignment(Element.ALIGN_JUSTIFIED);
            ck34.addElement(pk34);
            ck34.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck34);

            PdfPCell ck35 = new PdfPCell();
            Paragraph pk35 = new Paragraph("l)", redFont11);
            ck35.addElement(pk35);
            ck35.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck35);

            PdfPCell ck36 = new PdfPCell();
            Paragraph pk36 = new Paragraph(" Any notice hereunder may be given by the Bank to the Borrower by leaving the same at "
                    + "the last known place of business of the Borrower in India or by sending the same by post to "
                    + "the Borrower addressed as aforesaid and if sent by post the notice shall be deemed to have "
                    + "been given at the time when it would be delivered in due course of post and in proving such "
                    + "notice when given by post it shall be sufficient to prove that the envelope containing the "
                    + "notice was posted and a certificate signed by any officer of the Bank that the envelope was so "
                    + "posted will be conclusive.", redFont11);
            pk36.setAlignment(Element.ALIGN_JUSTIFIED);
            ck36.addElement(pk36);
            ck36.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck36);

            PdfPCell ck37 = new PdfPCell();
            Paragraph pk37 = new Paragraph("m)", redFont11);
            ck37.addElement(pk37);
            ck37.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck37);

            PdfPCell ck38 = new PdfPCell();
            Paragraph pk38 = new Paragraph(" The Borrower hereby agrees that if any default is committed in the repayment of the loan "
                    + "and advances or in the repayment of interest thereon or any of the agreed installment of the "
                    + "loan on due date the bank and/ or the Reserve Bank of India will have an unqualified right to "
                    + "directors/ partners/ proprietors as defaulter in such manner and through such medium as "
                    + "the Bank or Reserve Bank of India in their absolute discretion may think fit.", redFont11);
            pk38.setAlignment(Element.ALIGN_JUSTIFIED);
            ck38.addElement(pk38);
            ck38.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck38);

            PdfPCell ck39 = new PdfPCell();
            Paragraph pk39 = new Paragraph("n)", redFont11);
            ck39.addElement(pk39);
            ck39.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck39);

            PdfPCell ck40 = new PdfPCell();
            Paragraph pk40 = new Paragraph(" These presents shall remain in full force and effect so long as all the sums under the said "
                    + "credit facility including interests and other moneys due and payable hereunder and under "
                    + "the said facility agreement are not fully paid.\n ", redFont11);
            pk40.setAlignment(Element.ALIGN_JUSTIFIED);
            ck40.addElement(pk40);
            ck40.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck40);

            PdfPCell ck41 = new PdfPCell();
            Paragraph pk41 = new Paragraph("o)", redFont11);
            ck41.addElement(pk41);
            ck41.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck41);

            PdfPCell ck42 = new PdfPCell();
            Paragraph pk42 = new Paragraph(" Nothing herein contained shall prejudice or affect any general or special lien to which the "
                    + "bank are or may be law or otherwise be entitled to or any rights or remedies of the bank in "
                    + "respect of any present or future security, guarantee, obligation or decree for any other "
                    + "indebtedness or liability of the borrower to the bank or shall preclude the bank from "
                    + "enforcing or having recourse to the security under these presents without enforcing or "
                    + "having recourse in the first instance to any other security held by the bank from the "
                    + "borrower and the bank shall be entitled to sue on any such securities without being bound "
                    + "to sue on all such securities. ", redFont11);
            pk42.setAlignment(Element.ALIGN_JUSTIFIED);
            ck42.addElement(pk42);
            ck42.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck42);

            PdfPCell ck43 = new PdfPCell();
            Paragraph pk43 = new Paragraph("p)", redFont11);
            ck43.addElement(pk43);
            ck43.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck43);

            PdfPCell ck44 = new PdfPCell();
            Paragraph pk44 = new Paragraph(" This security shall not be prejudiced by any collateral or other security now or hereafter "
                    + "held or to the held by the bank for any money hereby secured or by any release, exchange or "
                    + "variation of the security, and the bank for any money hereby secured or by any release, "
                    + "exchange or variation of the security, and the bank may give time for payment to or make "
                    + "any other arrangement with any surety or cosignatory without prejudice to the borrower’s "
                    + "liability hereunder and all money received by the bank from the borrower or any person "
                    + "liable to pay the same may be applied by the bank to may account to which the same may be "
                    + "applicable.\n", redFont11);
            pk44.setAlignment(Element.ALIGN_JUSTIFIED);
            ck44.addElement(pk44);
            ck44.setBorder(PdfPCell.NO_BORDER);
            tablekk1.addCell(ck44);

            document.add(tablekk1);
            document.add(new Paragraph("\n"));
//                        document.add(new Paragraph("\n"));
//                        document.add(new Paragraph("\n"));

            Paragraph pkk24 = new Paragraph("THE SCHEDULE ABOVE REFERRED TO: ", canFont);
            pkk24.setAlignment(Element.ALIGN_LEFT);
            pkk24.setSpacingAfter(15);
            document.add(pkk24);

//             document.add(new Paragraph("\n"));
//             document.add(new Paragraph("\n"));
            Paragraph pkk25 = new Paragraph("Dated this              day of                 20", redFont);
            pkk25.setAlignment(Element.ALIGN_LEFT);
            pkk25.setSpacingAfter(15);
            document.add(pkk25);

//             document.add(new Paragraph("\n"));
//             document.add(new Paragraph("\n"));
            Paragraph pkk26 = new Paragraph(applicantName + "\n" + "Signature of the borrower", redFont);
            pkk26.setAlignment(Element.ALIGN_RIGHT);
            pkk26.setSpacingAfter(15);
            document.add(pkk26);

            document.newPage();

            Paragraph pkk27 = new Paragraph("UNITED BANK OF INDIA", canFont15);
            pkk27.setAlignment(Element.ALIGN_LEFT);
            //pkk26.setSpacingAfter(5);
            document.add(pkk27);

            Paragraph pkk28 = new Paragraph("AGREEMENT FOR TERM LOAN AND HYPOTHECATION", canFont);
            pkk28.setAlignment(Element.ALIGN_LEFT);
            //pkk26.setSpacingAfter(5);
            document.add(pkk28);

//            String applicantName = (loanData.getAppl1Name() != null && loanData.getAppl1Name() != "" && !loanData.getAppl1Name().equals("null") ? loanData.getAppl1Name() : " ");
//            String applicantFhgName = (loanData.getFhgName() != null && loanData.getFhgName() != "" && !loanData.getFhgName().equals("null") ? loanData.getFhgName() : " ");
//            String applicantAddress = (loanData.getAppl1Address() != null && loanData.getAppl1Address() != "" && !loanData.getAppl1Address().equals("null") ? loanData.getAppl1Address() : " ");
            String branchName = loanData.getBranchname();

            Paragraph pkk29 = new Paragraph();
            pkk29.add(new Chunk("This Agreement ", canFont));
            pkk29.add(new Chunk("...................................... made at ", redFont11));

            Chunk branchName_underline = new Chunk(branchName, redFont11);
            branchName_underline.setUnderline(0.1f, -2f);
            pkk29.add(branchName_underline);

            pkk29.add(new Chunk(" the ..................... day "
                    + "of .......................................... Two thousand "
                    + "and ...........................................................................Between ", redFont11));

            Chunk applicantName_underline = new Chunk(applicantName, redFont11);
            applicantName_underline.setUnderline(0.1f, -2f);
            pkk29.add(applicantName_underline);

            pkk29.add(new Chunk(" S/o,W/o,D/o  ", redFont11));

            Chunk applicantFhgName_underline = new Chunk(applicantFhgName, redFont11);
            applicantFhgName_underline.setUnderline(0.1f, -2f);
            pkk29.add(applicantFhgName_underline);

            pkk29.add(new Chunk(" residing at address ", redFont11));

            Chunk applicantAddress_underline = new Chunk(applicantAddress, redFont11);
            applicantAddress_underline.setUnderline(0.1f, -2f);
            pkk29.add(applicantAddress_underline);

            pkk29.add(new Chunk(" referred to as ‘‘the "
                    + "Borrower’’ which expression shall, include heirs, executors, administrators and legal representatives of the One part and "
                    + "United Bank of India, a body corporate constituted under the Banking Companies (Acquisition and Transfer of "
                    + "Undertakings) Act, 1970 and having its Head Office at Kolkata, and a Branch Office amongst other places "
                    + "at ", redFont11));

            pkk29.add(branchName_underline);
            pkk29.add(new Chunk(" (hereinafter referred to as ‘‘The Bank’’ which expression shall include "
                    + "its successors and assigns) of the other part.", redFont11));

            pkk29.setAlignment(Element.ALIGN_JUSTIFIED);
            pkk29.setSpacingAfter(5);
            document.add(pkk29);

            String loanAmount = (loanData.getLoanamountsactioned() != null && loanData.getLoanamountsactioned() != " " && !loanData.getLoanamountsactioned().equals("null") ? loanData.getLoanamountsactioned() : "");
            Paragraph pkk30 = new Paragraph();
            pkk30.add(new Chunk("WHEREAS ", redFont));
            pkk30.add(new Chunk("the Borrower has applied to the Bank for a loan to the extent of Rs ", redFont11));
            Chunk loanAmount_underline = new Chunk(loanAmount, redFont11);
            loanAmount_underline.setUnderline(0.1f, -2f);
            pkk30.add(loanAmount_underline);
            pkk30.add(new Chunk(" for the express purpose and "
                    + "upon the basis set out in the Borrower’s Loan Application dated ", redFont11));
            pkk30.add(underLine(loanData.getUhupdateddatetime() != null && !loanData.getUhupdateddatetime().equals("null")
                    && !loanData.getUhupdateddatetime().isEmpty() ? convertToDate(loanData.getUhupdateddatetime()) : empty));

            pkk30.add(new Chunk(" which the Bank agreed to do upon having ", redFont11));
            pkk30.add(new Chunk("repayment thereof secured on the terms and conditions and in the manner hereinafter mentioned. ", redFont11));
            pkk30.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk30);

            Paragraph pkk31 = new Paragraph();
            pkk31.add(new Chunk("NOW IN CONSIDERATION OF THE PREMISES IT IS HEREBY AGREED ", redFont11));
            pkk31.add(new Chunk(" by and between the parties as follows : ", redFont11));
            pkk31.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk31);

            String loanAmountSanctioned = (loanData.getLoanamountsactioned() != null && loanData.getLoanamountsactioned() != " " && !loanData.getLoanamountsactioned().equals("null") ? loanData.getLoanamountsactioned() : " ");

            Paragraph pkk32 = new Paragraph();
            pkk32.add(new Chunk("1. (a) The Borrower shall repay to the Bank at.............................................................................the said loan of "
                    + "Rs ", redFont11));
            Chunk loanSanctionedAmount_underline = new Chunk(loanAmountSanctioned, redFont11);
            loanSanctionedAmount_underline.setUnderline(0.1f, -2f);
            pkk32.add(loanSanctionedAmount_underline);

            pkk32.add(new Chunk(" by the installments, on the respective days and in the manner mentioned in Schedule hereto "
                    + "provided that if the amount of loan actually disbursed is less than Rs ", redFont11));
            pkk32.add(loanSanctionedAmount_underline);

            pkk32.add(new Chunk(" then the amount of installments payable as aforesaid shall be reduced pro-rata but the same shall be payable on the stipulated days "
                    + "mentioned in Schedule of repayment.", redFont11));

            pkk32.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk32);

            Paragraph pkk33 = new Paragraph("(b) The Borrower shall so long as the said loan or any part thereof remains unpaid pay to the Bank interest thereon or on "
                    + "the balance thereof due from time to time at the rate of ", redFont11);
            pkk33.add(underLine(String.valueOf(mclrMF)));
            pkk33.add(" % per annum over the United "
                    + "Bank of India MCLR-Y which is at present ");
            pkk33.add(underLine(String.valueOf(mclrVal)));
            pkk33.add(" % p.a., with ");
            pkk33.add(underLine("monthly "));
            pkk33.add("rests for value received/with "
                    + "interest thereon at the rate of ......");
//            pkk33.add(underLine(String.valueOf(appropriateRate)));
            pkk33.add("% p.a. with .........");
//            pkk33.add(underLine((loanData.getNoOfInstalments()!= null && !loanData.getNoOfInstalments().equals("null")
//                    && !loanData.getNoOfInstalments().isEmpty() ? loanData.getNoOfInstalments() : empty)));
            pkk33.add(" rests for value received plus ");
            //pkk33.add(underLine(String.valueOf(appropriateRate)));
            pkk33.add("....................... % p.a. additional rate in the event of default by the Borrower in punctual payment of "
                    + "interest and / or of any installments of principal with quarterly/half yearly rests in 30th day of March, June, "
                    + "September and December in each year and so that on default in payment of interest as stipulated above interest "
                    + "shall be charged and payable on the footing of compound interest at the rates and in the manner mentioned "
                    + "above and all such interest (including compound and penal interest) shall be calculated and charged in "
                    + "accordance with the usual practice of the Bank and shall be hereby secured.");
            pkk33.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk33);

            Paragraph pkk35 = new Paragraph("2. The borrower both hereby hypothecate and charge by way of first charge in favour of the Bank the "
                    + "equipment.........…………………………............................ kept or found at......................................................... which may "
                    + "at any time hereafter during the continuance of this security be kept or found upon or in the borrowers premises/at other "
                    + "place, situated in vill : .................................................................................. Taluka........................................ "
                    + "district................................. or in the cause of transit of wherever else the same may be or held by any party anywhere to "
                    + "the order and disposition of the Borrower and whether now belonging or which may at any time hereafter during the "
                    + "continuance of this security belong to Borrower (hereinafter for security’s save collectively referred to as Hypothecation "
                    + "of.................................................)", redFont11);
            pkk35.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk35);

            Paragraph pkk36 = new Paragraph("OR", redFont11);
            pkk36.setAlignment(Element.ALIGN_CENTER);
            document.add(pkk36);

            Paragraph pkk37 = new Paragraph("3. The Borrower both hereby hypothecate and charge by way of first charge in favour of the Bank the entire live-stock "
                    + "of the Borrower including buffaloes, cows, goats, dry cattle, bulls, young calves, sheep, lambs and also horses, donkeys, "
                    + "ponies, camels, pigs and all other animals whatsoever now stabled or kept or found which may at any time hereafter during "
                    + "the continuance of this security be stabled or kept or found upon or in the Borrower’s premises, dairy farm, farm house, "
                    + "shed, Barn and /or stables situated in Village............. Taluka......................... District............................... Or at any other "
                    + "place or in the Course of transit of wherever else the same may be or held by any party any where to the order and "
                    + "disposition of the Borrower and whether now belonging or which may at any time hereafter during the continuance of this "
                    + "security belong to the Borrower (hereinafter for brevity’s sake collectively referred to as  ", redFont11);
            pkk37.setAlignment(Element.ALIGN_JUSTIFIED);

// Paragraph pbranchname = new Paragraph();
            Chunk underline = new Chunk("''the Hypothecated cattle livestock'').");
            underline.setUnderline(0.1f, -2f);
            pkk37.add(underline);

            document.add(pkk37);

            document.add(new Paragraph("\n"));
            Paragraph pkk40 = new Paragraph();
            pkk40.add(new Chunk("AND ALSO ", redFont11));
            pkk40.add(new Chunk("the entire stock of cattle feed, fodder, dairy equipments such as milk pans, fat testing machines, cream "
                    + "separators, weighing/measuring equipments, dairy utensils, and all other machinery and plant and equipments as also all "
                    + "other tools, implements, utensils, applicances and things with all the spares, stores replacements and accessories "
                    + "whatsoever pertaining thereto as also any carts, tongas and all other vehicles, whatsoever which may be stored or lying on "
                    + "the Borrower’s said premises dairy farm, farm house, shed, or any part thereof or in any godown or anywhere else or "
                    + "which may be in the course of transit or held by any party any where to the order and disposition of the Borrower and "
                    + "whether now belonging or which may at any time hereafter during the continuance of this security belong to the Borrower "
                    + "(hereinafter for brevity’s sake collectively referred to as ", redFont11));
            pkk40.setAlignment(Element.ALIGN_JUSTIFIED);

            Chunk underline1 = new Chunk("''the hypothecated callte-stock and equipment.'').", redFont11);
            underline1.setUnderline(0.1f, -2f);
            pkk40.add(underline1);
            document.add(pkk40);
            document.add(new Paragraph("\n"));

            Paragraph pkk38 = new Paragraph("OR", redFont11);
            pkk38.setAlignment(Element.ALIGN_CENTER);
            document.add(pkk38);

            Paragraph pkk39 = new Paragraph("4. AND ALSO the entire stock of hens, cocks, chicks, chickens, broilers fowls and other domestic birds of the "
                    + "Borrower now kept or contained or which may at any time hereafter during the continuance of the securities be kept or "
                    + "stocked upon the Borrower’s premises/poultry farm/farm house situated in the Village of ........................................ "
                    + "Taluka .............................. District........................................... or any other place or in course of transit wherever else the "
                    + "same may be of held by any party any where to the order and disposition of the Borrower and whether now belonging or "
                    + "which may at any time hereafter during the continuance of this security belong to the Borrower (and all of which are "
                    + "hereafter for brevity’s sake collectively referred to as ", redFont11);
            pkk39.setAlignment(Element.ALIGN_JUSTIFIED);

            Chunk underline2 = new Chunk("‘‘the hypothecated poultry livestock’’).", redFont11);
            underline2.setUnderline(0.1f, -2f);
            pkk39.add(underline2);

            document.add(pkk39);
            document.add(new Paragraph("\n"));

            Paragraph pkk41 = new Paragraph();
            pkk41.add(new Chunk("AND ALSO ", redFont11));
            pkk41.add(new Chunk("all the stock of poultry food of whatever kind and nature, eggs, brooders, waterers chick, feeders, layer "
                    + "feeders, egg trays and boxes, poultry crate, incubator, hatcher, egg grading equipment air conditioner, feed grinder and "
                    + "other machinery, plant and equipments whatsoever together with all the spares, store, replacement and accessories "
                    + "whatsoever pertaining thereto which may be stored upon or enclosed or installed or be lying in the Borrower’s said "
                    + "premises/poultry farm/farm house of any part thereof anywhere else or which may be in course of transit "
                    + "to ................................................. of wherever else the same may be or held by any party at any time to the order and "
                    + "disposition of the borrower and whether now belonging or which may at any time hereafter during the continuance of this "
                    + "security belong to the Borrower (and all of which are hereinafter for brevity’s sake collectively referred to as ‘‘the "
                    + "hypothecated poultry stock and equipment’’) [ all of which the said hypothecated cattle live-stock and hypothecated cattle "
                    + "stock and equipment and hypothecated poultry livestock and hypothecated poultry stock and equipment are hereinafter "
                    + "referred to as ‘‘the hypotheca’’ which expression shall include all or any of them] as also the sale proceeds of the "
                    + "hypotheca as security or due repayment by the Borrower to the Bank at................................ of the said loan of Rs.", redFont11));

            pkk41.add(underLine(loanAmount));
            pkk41.add(new Chunk(" by the installments on the days and the manner aforesaid together with all interest due and "
                    + "payable thereon (including compound interest and penal interest payable hereunder) and also for all costs charges and "
                    + "expenses (the legal costs being between the Advocate and client) incurred by the Bank for the protection, preservation, "
                    + "defence and perfection of this security and for attempted or actual realisation therof and for recovery of the moneys due "
                    + "hereunder by the Borrower to the Bank and hereby secured or expressed so to be. ", redFont11));
            pkk41.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk41);

            Paragraph pkk42 = new Paragraph("OR", redFont11);
            pkk42.setAlignment(Element.ALIGN_CENTER);
            document.add(pkk42);

            Paragraph pkk43 = new Paragraph();
            pkk43.add(new Chunk("AND ALSO ", redFont11));
            pkk43.add(new Chunk("all the stock of spawn, fry, fingerling, fish now kept or stored or contained or which may at any time "
                    + "hereafter during the continuance of this security be kept or stored or stocked upon the Borrower’s premises / fishery tank / "
                    + "fishery farm house situated in the village of .........................................................P.S. ......................District......................or "
                    + "which may be caught from or kept or stored in any water area or at any other place or in course of transit wherever else the "
                    + "same may be or held by any party anywhere to the order and disposition of the Borrower and whether now belonging or "
                    + "which may at any time hereafter during the continuance of this security belong to the Borrower (and all of which are "
                    + "hereafter for brevity’s sake collectively referred to as‘‘the hypothecated fish-stock’’). "
                    + "AND ALSO all the stock of fish fee, manure, chemical fertilizers, crafts, gears, nursery and breeding equipments, "
                    + "medicines, chemicals, weighing equipments, utensils as also all other tools, implements, applicances and things with all "
                    + "the spares, stores, replacements and accessories whatsoever pertaining thereto as also any cart, vehicle, van whatsoever "
                    + "which may be stored or lying on the Borrower’s said premises, fishery tank, fish farm, farm house, shed or any part thereof "
                    + "or in any godown or anywhere else or which may be in course of transit or held by any party anywhere to the order and "
                    + "disposition of the Borrower and whether now belonging or which may at any time hereafter during the continuance of this"
                    + "security belong to the Borrower (hereinafter for brevity’s sake collectively reffered to as ‘‘the hypothecated fish-stock and ", redFont11));
            pkk43.setAlignment(Element.ALIGN_JUSTIFIED);

            Chunk underline3 = new Chunk("‘‘the hypothecated fish stock and equipment’’).", redFont11);
            underline3.setUnderline(0.1f, -2f);
            pkk43.add(underline3);

            document.add(pkk43);

            document.add(new Paragraph("\n"));
            Paragraph pkk44 = new Paragraph("6. The Borrower shall not during the continuance of this Agreement sell, dispose of, pledge, hypothecate or otherwise "
                    + "charge, encumber, or in any manner, part with the possession of the hypotheca or any part thereof nor shall the Borrower "
                    + "do or permit to be done any act whereby the security hereinbefore expressly given to the Bank shall in any way be "
                    + "prejudicially affected or whereby any distress or attachment or execution may be levied thereon by any creditor or other "
                    + "person including any government or municipal authority or body\n\n", redFont11);
            pkk44.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk44);

            Paragraph pkk45 = new Paragraph("7. So long as any money remains due in respect of the said loan the Borrower shall not remove of cause or prement to "
                    + "be to remove the hypotheca from the Borrower’s permises at ........................................where the same are presently kept or "
                    + "to which (with the consent of the Bank) the same may be hereafter removed, except in the manner and to the extent "
                    + "allowed by the Bank or except for effecting necessary repairs thereto. The Bank shall be entitled to put up and the "
                    + "Borrower hereby gives his consent to the Bank to put up the Bank’s name board on the hypotheca or at the place where the "
                    + "hypotheca are or may be stored, at such time and in such manner as the Bank may deem proper.\n\n", redFont11);
            pkk45.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk45);

            Paragraph pkk46 = new Paragraph("8. The Borrower will at time pay all taxes, assessments, dues and outgoings payable in respect of the hypotheca and "
                    + "will also keep the same in a marketable state and in thorogh working order and will not make any alteration therein "
                    + "without the previous written consent of the Bank\n\n", redFont11);
            pkk46.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk46);

            Paragraph pkk47 = new Paragraph("9. The Borrower undertakes to identify the Bank against every and any kind of loss or damage by reason of damage to "
                    + "or destruction or loss of the hypotheca from any cause whatsoever or by reason of any claims by third parties.\n\n", redFont11);
            pkk47.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk47);

            Paragraph pkk48 = new Paragraph("10. The Borrower shall notify the Bank at once of any change in his present address given below and also the address to "
                    + "which the hypotheca may be moved subject nevertheless to the provisions of clause 4 above, ‘‘Provided however that as "
                    + "far as possible the Borrower shall give prior intimation to the Bank whenever Borrower intends to move the hypotheca "
                    + "from the existing premises and shall obtain the significance of the Bank to the movement of the hypotheca’’.\n\n", redFont11);
            pkk48.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk48);

            Paragraph pkk49 = new Paragraph("11. The Borrower hereby irrevocably empowers the Bank and any person or persons from time to time authorised by the "
                    + "Bank in that behalf and without previous notice to the Borrower to enter the said premises or any other premises "
                    + "whatsoever for the purpose of inspection or valuation or taking possession of the hypotheca pursuant to clause 10 herein "
                    + "contained or for any other purpose mentioned in the said clause and to remain on the premises so long as the Bank may "
                    + "think necessary.\n\n", redFont11);
            pkk49.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk49);

            Paragraph pkk50 = new Paragraph("12. Notwithstanding anything herein contained to the contrary the whole of the said loan or the entire balance thereof "
                    + "outstanding for the time being shall at the option of the Bank become forthwith due and payable by the Borrower to the "
                    + "Bank and the security hereunder shall at the option of the Bank become enforceable immediately upon the happening of "
                    + "any of the following events, namely :\n ", redFont11);
            pkk50.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk50);

            Paragraph pkk51 = new Paragraph("(a) Any installment of principal or interest or any part thereof, in respect of the said loan being unpaid for a period of 15 "
                    + "days after the respective due dates for payment thereof. ", redFont11);
            pkk51.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk51);

            Paragraph pkk52 = new Paragraph("(b) Any representation or statement in the Borrower’s Loan Application being found to be materially incorrect; \n", redFont11);
            pkk52.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk52);

            Paragraph pkk53 = new Paragraph("(c) The Borrower committing any breach or default in the performance or observance of any term of condition contained "
                    + "in these presents or in the said Loan Application. \n", redFont11);
            pkk53.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk53);

            Paragraph pkk54 = new Paragraph("(d) Exclusion or distress or other process being enforced or levied upon or against the whole or any part of the "
                    + "Borrower’s property whether secured to the Bank or not.\n", redFont11);
            pkk54.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk54);

            Paragraph pkk55 = new Paragraph("(e) The Borrower being adjudicated insolvent or taking advantage of any law for the relief of insolvent debtors or "
                    + "entering into any arrangement or composition with his creditors or committing any act of insolvency;\n", redFont11);
            pkk55.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk55);

            Paragraph pkk56 = new Paragraph("(f) If the Borrower shall without the consent in writing of the Bank create or attempts or purport to create any mortgage, "
                    + "charge, pledge, hypothecation or lien or encumbrance on the hypotheca which is the subject of the Bank’s security "
                    + "hereunder or any part thereof ;\n", redFont11);
            pkk56.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(pkk56);

            //Rupesh 15 to 23
            document.newPage();

            Paragraph rp1 = new Paragraph(17, "13. If the Borrower makes any default in payment of any installment thereof as mentioned above or if loan or any part of such installment on the respective due dates  of payment thereof as mentioned above or if any event or circumstances shall occur which shall in the opinion of the Bank be prejudicial to endanger or be likely to endanger this security to if any other event or circumstances mentioned in clause 9 above happens or  occurs, the Bank if it thinks fit shall be entitled at the risk and expense of the Borrower without any notice at any time or times after such default or event or circumstance occurs or happens to enter (and for that purpose to do any necessary act deed or thing) and remain upon any place where the hypotheca or the books of account of receiver, appoint receivers, of all or any part of the hypotheca or books of account and thereupon forthwith or at any time and from time to time but after giving not less than 48 hours notice at least to sell either by public auction or private contract or otherwise dispose of or deal with the hypotheca in such manner and upon such ultimate balance due in the said Loan account and interest due thereon upto the date of such application. And to enforce, realise, settle, compromise and deal with any rights aforesaid without being bound to exercise any of these powers or being liable for any losses in the exercise thereof and without prejudice to the Bank’s rights and remedies of suit or otherwise and notwithstanding there may be any pending suit or other proceeding. The Borower hereby also agrees to accept the Bank’s accounts of sales realisation and to pay any shortfall or deficiency thereby shown. And if the net sum realised by such sale shall be insufficient to pay the total amount secured hereunder the Bank shall be at liberty to apply any other money or moneys in the hands of the Bank standing to the credit of or belonging to the Borrower in or towards the payment of the balance and in the event of there being still a deficiency, the Borrower shall forthwith pay such deficiency. Provided that nothing herein contained shall in any manner prejudice or affect the Bank’s remedy against the Borrower personally.\n", font11n);
            rp1.setAlignment(Element.ALIGN_JUSTIFIED);
            rp1.setSpacingAfter(5);
            document.add(rp1);

            Paragraph rp2 = new Paragraph(17, "14. The Borrower hereby declares and guarantees that the hypotheca are and shall remain the absolute and unencumbered property of the Borrower with full power of disposition thereover.\n", font11n);
            rp2.setAlignment(Element.ALIGN_JUSTIFIED);
            rp2.setSpacingAfter(5);
            document.add(rp2);

            Paragraph rp3 = new Paragraph(17, "15. The Borrower shall furnish (and verify all statement and) information from time to time as required by the Bank and give execute and necessary documents required to give effect to this security.\n", font11n);
            rp3.setAlignment(Element.ALIGN_JUSTIFIED);
            rp3.setSpacingAfter(5);
            document.add(rp3);

            Paragraph rp4 = new Paragraph(17, "16. The Borrower shall, whenever required by the Bank, give full particulars to the Bank of all the assets of the Borrower and of the hypotheca and shall at all times allow the Bank of the authorised agent of the Bank inspection of hypotheca and of all records of the Borrower in reference thereto and shall allow the Bank or its agents to value the same. All costs charges and expenses incurred by the Bank and incidental to such inspection and valuation shall be paid to the Bank forthwith on demand (the Bank statement being conclusive), and until payment, shall with interest at the rate aforesaid be a charge upon the hypotheca. Any such valuation shall be conclusive and binding on the borrower both in and out of court.\n", font11n);
            rp4.setAlignment(Element.ALIGN_JUSTIFIED);
            rp4.setSpacingAfter(5);
            document.add(rp4);

            Paragraph rp5 = new Paragraph(17, "17. The Borrower shall at all times during the continuance of this security keep and maintain such margin of security in favour of the Bank (hereinafter called ‘‘the said margin’’) as may be required by the Bank from time to time. If and often as the said margin shall fail to be maintained, then the Borrower shall forthwith (according as the Bank may require) either hypothecate to the Bank further goods or tangible movable property or goods approved by the Bank and of sufficient value to make up the deficiency or shall reduce the amount for the time being due to the Bank by a cash payment so as to maintain the said margin.\n", font11n);
            rp5.setAlignment(Element.ALIGN_JUSTIFIED);
            rp5.setSpacingAfter(5);
            document.add(rp5);

            Paragraph rp6 = new Paragraph(17, "18. (a) The Borrower shall at its own expense insure and keep insured the hypotheca against fire and such other risks as the Bank shall from time to time require for the full market value thereof in one or more insurance offices approved by the Bank and shall deliver to the Bank the policies of insurance duly assigned to the Banks and shall keep on foot and maintain such insurances throughout the continuance of the security and deliver to the Bank the renewal receipt therefor.\n", font11n);
            rp6.setAlignment(Element.ALIGN_JUSTIFIED);
            rp6.setSpacingAfter(5);
            document.add(rp6);

            //PAGE REPESH 2ND.
            document.newPage();
            Paragraph rp7 = new Paragraph(17, "(b)In default of the Borrower doing so, the Bank may (but shall not be bound to) keep in good working order and condition and render marketable the hypotheca or effect or renew such insurance and may pay all taxes, assessments dues and outgoing payable by the Borrower. Any premium paid by the Bank or any other amount paid by the Bank under this clause and any costs, charges and expenses whatsoever incur by the Bank shall be repaid by the Borrower on demand forthwith AND until repayment with interest at the rate aforesaid the same shall be a charge on the hypotheca. All sums received under such insurance shall be applied in or towards liquidation of the amount for the time being due hereunder to the Bank.\n", font11n);
            rp7.setAlignment(Element.ALIGN_JUSTIFIED);
            rp7.setSpacingAfter(5);
            document.add(rp7);

            Paragraph rp8 = new Paragraph(17, "19. The Borrower agree to accept as conclusive proof of the correctness of any sum claimed to be due from him to the Bank under this Agreement a statement of account made out from the books of the bank and signed by the Manager/Dy. Manager and / or other duly authorised officer of the Bank without the production of any other voucher, document or paper.\n", font11n);
            rp8.setAlignment(Element.ALIGN_JUSTIFIED);
            rp8.setSpacingAfter(5);
            document.add(rp8);

            Paragraph rp9 = new Paragraph(17, "20. The Bank shall not in any way be liable or responsible for any damage or depreciation which the hypotheca or any part thereof may suffer or sustain on any account whatsoever while the same shall at any time come into possession of the Bank or any receiver appointed by the Bank.\n", font11n);
            rp9.setAlignment(Element.ALIGN_JUSTIFIED);
            rp9.setSpacingAfter(5);
            document.add(rp9);

            Paragraph rp10 = new Paragraph(17, "21. The Borrower shall inform the Bank promptly of any notice or intimation received from any Government, semi-Government, Revenue, Municipal of local or other authorities regarding any default delay, etc. by the Borrower in payment of any dues or in the performance of the Borrower’s obligation towards them.\n", font11n);
            rp10.setAlignment(Element.ALIGN_JUSTIFIED);
            rp10.setSpacingAfter(5);
            document.add(rp10);

            Paragraph rp11 = new Paragraph(17, "22. The Borrower shall provide to the Bank all such informations as the Bank shall from time to time at its discretion require, including information relating to the financial conditions of the Borrower.\n", font11n);
            rp11.setAlignment(Element.ALIGN_JUSTIFIED);
            rp11.setSpacingAfter(5);
            document.add(rp11);

            Paragraph rp12 = new Paragraph(17, "23. The Borrower agree to open and / or maintain with the Bank a Saving Bank/Current Account and keep the account in sufficient funds and hereby irrevocably authorises the Bank to debit the same or any other account of the Borrower with the Bank with the amount of each installment of the loan and interest thereon payable hereunder as and when it falls due. The Bank shall also have right to set-off the Balance due in the said account or in such other account against the balance due in the said Loan Account together with all interest costs, charges and expenses and other moneys payable by the Borrower hereunder at any time after such Balance or interest of such costs, charges and expenses and other moneys has/have become payable by the Borrower under the terms hereof.\n", font11n);
            rp12.setAlignment(Element.ALIGN_JUSTIFIED);
            rp12.setSpacingAfter(5);
            document.add(rp12);

            Paragraph rp13 = new Paragraph(17, "24. Nothing herein shall operate to prejudice the Bank’s rights or remedies in respect of any present or future security, guarantee obligation or decree for any indebtedness or liability of the Borrower to the Bank.\n", font11n);
            rp13.setAlignment(Element.ALIGN_JUSTIFIED);
            rp13.setSpacingAfter(5);
            document.add(rp13);

            Paragraph rp14 = new Paragraph(17, "25. The Borrower confirms that he has not borrowed any money from any other Bank or Co-Operative Society or from any other source whatsoever and whomsoever further that so long as the Borrower continues to be indebted or liable to the Bank in the said Loan Account or in any other Account or manner the Borrower shall not without the previous written consent of the Bank borrow any moneys from any other Bank of financier or from any such other source.\n", font11n);
            rp14.setAlignment(Element.ALIGN_JUSTIFIED);
            rp14.setSpacingAfter(5);
            document.add(rp14);

            Paragraph rp15 = new Paragraph(17, "26. The Borrower hereby agrees that the Bank may hold the hypotheca and the proceed thereof not only as a security for the said loan but also as collateral security for any other moneys now due or which may at any time be due from the Borrower to the bank whether singly or jointly with another or others and that in addition to any general lien or similar right to which the Bank may be entitled by law the Bank may at any time and without notice to the Borrower combine or consolidate all or any of Borrower’s Accounts with and liabilities to the Bank and set off or transfer any sum or sums standing to the credit of any one or more of such accounts in or towards the satisfaction of any of the liabilities of the Borrower to the Bank on any other account or in any other respect whether such liabilities be actual or contingent primary or collateral and several or joint.\n", font11n);
            rp15.setAlignment(Element.ALIGN_JUSTIFIED);
            rp15.setSpacingAfter(5);
            document.add(rp15);

            //RUPESH NEXT PAGE...
            document.newPage();
            Paragraph rp16 = new Paragraph(17, "27. Where the Borrower is more than one individual, each one of them shall be bound and liable hereunder jointly and severally with the other or others of them and all covenants condition agreements herein contained shall be performed by them and each of them jointly and severally and any act or default by any of them shall be deemed to be an act or default by all of them. Where the Borrower is a firm such firm and all partners thereof from time to time shall be bound hereunder both joinly and severally notwithstanding any changes in the constitution or style thereof and notwithstanding that the firm shall consist of or be reduced to one individual.\n", font11n);
            rp16.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(rp16);

            Paragraph rp17 = new Paragraph(17, "28. That the Borrower hereby irrevocable gives his consent to and agrees to the amounts due hereunder being recovered as a public demand/public moneys in terms of any legislation (now existing or hereafter to be enacted) relating to recoveries thereof and further irrevocably consents and agrees to submit to the jurisdiction of the appropriate body or authority or tribunal appointed under any such legislation or under any rules or regulations made under pursuant to such legislation. The Borrower also irrevocably undertakes to execute such further writing as may be necessary to give effect to this clause.\n", font11n);
            rp17.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(rp17);

            Paragraph rp18 = new Paragraph(17, "29. Any notice by way of request or otherwise hereunder may be given by the Bank to the Borrower personally or may be left at the address given below or at the then or last known place of business or residence of the Borrower in the Republic of India as the case may be addressed to the Borrower or may be sent by post to the Borrower as aforesaid and if sent by post, such notice shall be deemed to have been given at the time when it would be delivered in due course of post, and in proving such notice when given by post, it shall be sufficient to prove that the envelope containing the notice was posted, and a certificate signed by the Bank’s Local Manager or Agent or Accountant that the envelope was so posted shall be conclusive. If by reason of absence of the Borrower from the place mentioned below or otherwise, any such notice to the Borrower cannot be given then the same, if inserted once as an advertisement in a newspaper circulating in the district where the Borrower was last known to reside or carry on business, shall be deemed to have been effectually given and received on the day on which such advertisement appears. Consent clause I/we hereby agree to that in case of default in repayment of loan ; the Bank is at liberty to inform RBI/CIBIL for displaying my/our name in the website or any media as deemed fit, by the Bank.", font11n);
            rp18.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(rp18);

            Paragraph rp19 = new Paragraph(17, "In Witness whereof the Borrower has executed this agreement the day and year first above wirtten.", font11n);
            rp19.setSpacingAfter(5);
            document.add(rp19);

            document.add(new Paragraph("SCHEDULE OF REPAYMENT\n", font11b));

            Paragraph rp20 = new Paragraph("Amount of Installments                                                             Date of Payment of Installments", font11n);
            document.add(rp20);

            Paragraph rp21 = new Paragraph("Rs.", font11n);
            rp21.setSpacingAfter(5);
            document.add(rp21);

            Paragraph rp22 = new Paragraph("Rs.", font11n);
            rp22.setSpacingAfter(5);
            document.add(rp22);

            Paragraph rp23 = new Paragraph("Rs.", font11n);
            rp23.setSpacingAfter(5);
            document.add(rp23);

            Paragraph rp24 = new Paragraph("Rs.", font11n);
            rp24.setSpacingAfter(5);
            document.add(rp23);

            document.add(new Paragraph("Signed by the above named                                                                                               (Signature’/s of Borrower/s)\n", font11n));
            document.add(new Paragraph("        " + loanData.getAppl1name() != null && !loanData.getAppl1name().equals("null") ? loanData.getAppl1name() : "", font11b));
            document.add(new Paragraph("  (Name/s of Borrower/s)\n", font11n));
//            document.add(new Paragraph("...................................................\n"
//                    + "...................................................\n"
//                    + "...................................................\n"
//                    + "..................................................."));
            System.out.println("addressLength***************: " + appl1Address.length());
            Paragraph rp58 = new Paragraph("Address / es of the Borrower/s :  ", font11n);
            rp58.add("\n");
            String addValArr[] = appl1Address.split(",");
            if (appl1Address.length() > 25) {
                for (int i = 0; i < 3; i++) {

                    rp58.add(underLine(addValArr[i]));
                    rp58.add(underLine(" , "));
                }

                rp58.add("\n");
                for (int i = 3; i < addValArr.length; i++) {
                    rp58.add(underLine(addValArr[i]));
                    rp58.add(underLine(" , "));
                }
            }
            Chunk c = new Chunk(underLine(appl1Address));

            document.add(rp58);
            document.add(new Paragraph("Witness :\n"
                    + "1.\n"
                    + "2.\n", font11b));
            rp24.add(underLine(appl1Address));

            Paragraph rp25 = new Paragraph("(N.B. : Item 2, 3, 4,5, may be ticked according to nature of Hypothecation)", font11bi);
            rp25.setAlignment(Element.ALIGN_CENTER);
            document.add(rp25);

            //RUPESH NEXT PAGE........
            document.newPage();
//            AttributedString as= new AttributedString("Example text string");
//            as.addAttribute(TextAttribute.FONT, font11bi);
            Chunk glue = new Chunk(new VerticalPositionMark());
            Paragraph rp26 = new Paragraph();
            rp26.add(new Phrase("AGREEMENT OF TERM LOAN", SUBFONT));
            rp26.add(new Chunk(glue));
            rp26.add(new Phrase("Form No. D28", font11b));
            rp26.setSpacingAfter(5);
            document.add(rp26);

            String appl1Addresss = (loanData.getAppl1name() != null && !loanData.getAppl1name().equals("null") ? loanData.getAppl1name() : "");
            appl1Addresss += ",S/o,D/o,W/o " + (loanData.getFhgname() != null && !loanData.getFhgname().equals("null") ? loanData.getFhgname() : "")
                    + (loanData.getAppl1address() != null && !loanData.getAppl1address().equals("null") ? loanData.getAppl1address() : "");;
            branchName = (loanData.getBranchname() != null && !loanData.getBranchname().equals("null") ? loanData.getBranchname() : "");

            String rupees = NumberToWord.convert(Long.parseLong(loanAmount));
            Paragraph rp27 = new Paragraph(20, "This Deed made this.........................day of..........................................................................................................................\nbetween ", font11n);
            rp27.add(underLine(appl1Addresss));
            rp27.add(" having his / its / their Registered Office/ Place of Business at ");
            rp27.add(underLine(appl1Addresss));
            rp27.add(" (hereinfater called the ‘‘BORROWER’’  which expression shall unless excluded by or repugnant to the context include his/it/their heirs, legal representatives, successors and assigns) of the One Part and UNITED BANK OF INDIA, a body corporate constituted under the Banking Companies (Acquisition and Transfer of Undertakings) Act, 1970 and having its Head Office at No.11 Hemanta Basu Sarani, Kolkata- 700 001 and having one of its business places at ");
            rp27.add(underLine(branchName));
            rp27.add(" (hereinafter called the ‘‘Bank’’ which expression shall unless excluded by or repugnant to the context include its successors and assigns) of the Other Part.\n");
            rp27.add("WHEREAS the Borrower has applied to the Bank for a loan of Rs. ");
            rp27.add(underLine(loanAmount));
            rp27.add(" (Rupees ");
            rp27.add(underLine(rupees));
            rp27.add(") upon the basis of and for the " + "purposes set forth in the Borrower’s Proposal dated the  ");
            rp27.add(underLine(loanData.getUhupdateddatetime() != null && !loanData.getUhupdateddatetime().equals("null")
                    && !loanData.getUhupdateddatetime().isEmpty() ? returnOnlyDate(loanData.getUhupdateddatetime()) : empty));
            rp27.add(" day of ");
            rp27.add(underLine(loanData.getUhupdateddatetime() != null && !loanData.getUhupdateddatetime().equals("null")
                    && !loanData.getUhupdateddatetime().isEmpty() ? returnMonthandYear(loanData.getUhupdateddatetime()) : empty));
            rp27.add(" (hereinafter called the ‘‘BORROWER’S PROPOSAL’’).\n");
            rp27.add("AND WHEREAS the Bank has advanced/agreed to advance the said loan either at one time or by such installments and on such dates as has been/may be mutually agreed upon by and between the parties hereto.\n"
                    + "AND WHEREAS the Bank has advanced/agreed to advance the said loan upon the assurances, agreements, covenants terms and conditions set forth in these presents.\n"
                    + "Now in consideration of the aforesaid it is hereby agreed by and between the parties hereto as follows :–");

            rp27.setAlignment(Element.ALIGN_JUSTIFIED);
            rp27.setSpacingAfter(5);
            document.add(rp27);

            Paragraph rp28 = new Paragraph(17, "1. The Borrower’s proposal shall be deemed to constitute the basis of this agreement and of the loan already advanced/agreed to be advanced by the Bank hereunder and the Borrower hereby warrants the correctness of each and every one of the statements, representations and particulars contained in the Borrower’s proposal and undertakes to carry out the project and programme therein set forth.\n", font11n);
            rp28.setAlignment(Element.ALIGN_JUSTIFIED);
            rp28.setSpacingAfter(5);
            document.add(rp28);

            Paragraph rp29 = new Paragraph(17, "2. The Borrower is aware that the Bank has approached the .............................................................................................\n"
                    + ".............................................. for providing refinance for the said loan granted/to be granted under these presents and the Borrower agrees and undertakes to observe perform and discharge various duties and obligations imposed by the said ...................................... which are applicable to the Borrower and/or which are to be performed by the Borrower.\n", font11n);
            rp29.setAlignment(Element.ALIGN_JUSTIFIED);
            rp29.setSpacingAfter(5);
            document.add(rp29);

            Paragraph rp30 = new Paragraph(17, "3. The Borrower hereby agrees that the said loan shall be governed by and be subject to the terms contained herein as well as those contained in the Security Documents executed/to be executed by the Borrower in favour of the Bank in respect of the said loan except in so far as the security Documents may expressly or by necessary implication be modified by these presents.\n", font11n);
            rp30.setAlignment(Element.ALIGN_JUSTIFIED);
            rp30.setSpacingAfter(5);
            document.add(rp30);

            Paragraph rp31 = new Paragraph(17, "4. The Borrower hereby agrees and undertakes to utilise the said loan exclusively for the purposes set out in the Borrower’s proposal and not for any other purpose or purposes except with the prior consent in writing of the Bank.\n", font11n);
            rp31.setAlignment(Element.ALIGN_JUSTIFIED);
            rp31.setSpacingAfter(5);
            document.add(rp31);

            document.newPage();
            Paragraph rp32 = new Paragraph(17, "5. If any event happens hereafter which may affect the correctness of any of the statements, representations and or particulars set forth in the Borrower’s proposal, the Borrower undertakes to inform the Bank in writing of such event within 30 days of the happening thereof.\n", font11n);
            rp32.setAlignment(Element.ALIGN_JUSTIFIED);
            rp32.setSpacingAfter(5);
            document.add(rp32);

            Paragraph rp33 = new Paragraph(17, "6. The said loan shall be repaid to the Bank by the Borrower in installments as mentioned hereunder and so on till the said loan in entirely repaid with all interests, costs, charges, expenses etc. whatsoever. Notwithstanding what is stated hereinabove or in any of the Security Documents the Bank is hereby authorized without reference to the Borrower in the absolute discretion of the Bank to alter, modify, revise or vary the aforesaid repayment schedule and / or demand earlier recovery/repayment and / or extend the repayment period and/or alter the installment amount and / or the installment dates and the Borrower shall in any such event be bound to repay the said loan with all interests, costs, charges, expenses, etc. whatsoever in terms of such altered, varied, modified or revised schedule of repayment and in case of earlier demand to make payment of the aforesaid amounts within the period as stated in the notice of demand provided always that the Bank shall give at least 3 months’ notice to the Borrower before implementing such variation, alteration, modification or revision.\n", font11n);
            rp33.setAlignment(Element.ALIGN_JUSTIFIED);
            rp33.setSpacingAfter(5);
            document.add(rp33);

            String mclrl = (loanData.getAppl1name() != null && !loanData.getAppl1name().equals("null") ? loanData.getAppl1name() : "");

            //Paragraph rp34 = new Paragraph(17, "7. The Borrower shall pay to the Bank interest on the principal moneys outstanding from time to time in respect of the said loan at the rate of .........................% per annum below/at/over the United Bank of India MCLR-Y which is at present ......................% p.a., with ................................  rests for value received/with interest thereon at the rate of ..........................% p.a with .................................................  rests for value received. Such interest  shall  be  charged   with  monthly/quarterly/half-yearly rests, interest being payable on...........................................or  beginning from the date of these presents, the first of such payment on account of interest to be for the period form the date of disbursement of this loan till the last day of the ........................as mentioned hereinabove. The Bank shall be entitled to charge interest at such other rate or rates and for such other period or periods and with such other rest or rests as may be notified to the Borrower by the Bank from time to time till such loan is repaid in full with all interests, costs, charges, expenses etc. whatsoever. The Borrower shall be liable to pay to the Bank .................%  p.a. as and by way of additional interest on any installment or interest not paid on its due date as mentioned in these presents from the due date of such installment or interest until the same is paid.  It is further agreed that in case any interest is not paid on the due date thereof as mentioned hereinabove such interest shall be added to the principal amount of the claim and shall carry usual rate of interest applicable to such principal amount in terms of this agreement without prejudice to the right of the Bank to charge additional interest on such amount and at the rates and for the period of the default as mentioned hereinabove.\n", font11n);
            Paragraph rp34 = new Paragraph(17, "7. The Borrower shall pay to the Bank interest on the principal moneys outstanding from time to time in respect of the said loan at the rate of ", font11n);
            rp34.add(underLine(String.valueOf(mclrMF)));
            rp34.add(" % per annum below/at/over the United Bank of India MCLR-Y which is at present ");
            rp34.add(underLine(String.valueOf(loanData.getMclr())));
            rp34.add("% p.a., with ");
            rp34.add(underLine("monthly "));
            rp34.add("rests for value received/with interest thereon at the rate of ....................");
//            rp34.add(underLine(String.valueOf(appropriateRate)));
            rp34.add("% p.a with ................");
//            rp34.add( underLine((loanData.getNoOfInstalments()!= null && !loanData.getNoOfInstalments().equals("null")
//                    && !loanData.getNoOfInstalments().isEmpty() ? loanData.getNoOfInstalments() : empty)));
            rp34.add("  rests for value received. Such interest  shall  be  charged   with  monthly/quarterly/half-yearly rests, interest being payable on...........................................or  beginning from the date of these presents, the first of such payment on account of interest to be for the period form the date of disbursement of this loan till the last day of the ........................as mentioned hereinabove. The Bank shall be entitled to charge interest at such other rate or rates and for such other period or periods and with such other rest or rests as may be notified to the Borrower by the Bank from time to time till such loan is repaid in full with all interests, costs, charges, expenses etc. whatsoever. The Borrower shall be liable to pay to the Bank .................%  p.a. as and by way of additional interest on any installment or interest not paid on its due date as mentioned in these presents from the due date of such installment or interest until the same is paid.  It is further agreed that in case any interest is not paid on the due date thereof as mentioned hereinabove such interest shall be added to the principal amount of the claim and shall carry usual rate of interest applicable to such principal amount in terms of this agreement without prejudice to the right of the Bank to charge additional interest on such amount and at the rates and for the period of the default as mentioned hereinabove.\n");
            rp34.setAlignment(Element.ALIGN_JUSTIFIED);
            rp34.setSpacingAfter(5);
            document.add(rp34);

            Paragraph rp35 = new Paragraph(17, "8. Notwithstanding anything contained herein or in any security documents executed/to be executed by the Borrower, the said loan and interest including any additional interest due thereon and all costs, charges, expenses etc. whatsoever shall at the Bank’s option become forthwith due and payable by the Borrower to the Bank and the Bank shall, at its option, be entitled or enforce its security upon the happening of the following events, namely :–\n"
                    + "a) If any installment of the principal moneys advanced under these presents remains unpaid upon the due date for payment thereof.\n"
                    + "b) If any interest due under these presents remains unpaid and in arrears for a period of ", redFont11);
            rp35.add(underLine("one"));
            rp35.add(new Chunk(" month after the same shall have become due whether demanded or not.\n"
                    + "c) If any representation or statement or particular made in the Borrower’s proposal is found to be incorrect or the Borrower commits any breach or default in the performance or observance of these presents and / or the Borrower’s proposal and / or the security documents and / or of any other terms and conditions relating to the said loan.\n"
                    + "d) If the Borrower enters into any arrangement or composition with the Borrower’s creditors or commits any other act which might result in the insolvency / winding up of the Borrower.\n", redFont11));
            rp35.setAlignment(Element.ALIGN_JUSTIFIED);
            rp35.setSpacingAfter(5);
            document.add(rp35);

            //Rupesh new page....
            document.newPage();
            Paragraph rp36 = new Paragraph(17, "e) If execution or distress is enforced or levied upon or against the whole or any part of the Borrower's property whether secured to the Bank or not.\n"
                    + "f) If the Borrower goes into liquidation except with prior approval in writing of the Bank for the purpose of amalgamation or reconstruction.\n"
                    + "g) If a receiver is appointed in respect of the whole or any part of the property of the Borrower.\n"
                    + "h) If the Borrower ceases or threatens to cease to carry on business or gives or threatens to give notice of the Borrower’s intention to do so.\n"
                    + "i) If is certified by an Accountant of a firm of accountants appointed by the Bank (which the Bank is entitled and authorised to do at any time in its sole discretion) that the liabilities of the Borrower exceed the Borrower’s assets or that the Borrower is carrying on business at a loss.\n"
                    + "j) If the Borrower shall without the consent in writing of the Bank attempt or purport to create  any mortgage, charge, pledge, hypothecation, lien or encumbrance over the  Borrower’s property or any part thereof which is/shall be the security for the repayment of the said loan with interests, costs, charges, expenses etc, except in favour of the Bank for securing any other obligations of the Borrower to the Bank.\n"
                    + "k) If the Borrower shall suspend payment to any of the Borrower’s creditors or threaten to do so.\n"
                    + "l) If any circumstance or event occurs which is prejudicial to or impairs or imperils or depreciates or jeopardises or is likely to prejudice, impair, imperil, depreciate or jeopardise any security given by the Borrower or any part thereof.\n"
                    + "m) If any event or circumstance occurs which would or is likely to prejudicially or adversely affect in any manner the implementation of the said project or the capacity of the Borrower to repay the said loan or any part thereof.\n"
                    + "n) If the said loan or any part thereof is utilised for any purpose other than the purpose set forth in the Borrower’s proposal.\n"
                    + "On the questions whether any of the above events or circumstances has happened, the decision of the Bank shall be conclusive and binding on the Borrower.", font11n);
            rp36.setAlignment(Element.ALIGN_JUSTIFIED);
            rp36.setSpacingAfter(5);
            document.add(rp36);

            rp36.setSpacingBefore(5);
            Paragraph rp37 = new Paragraph(17, "9. It is hereby expressly agreed that the Bank shall be at liberty to assign the debt and the benefit of these presents and the securities for the said loan and the security Documents to .................................................. (hereinafter called the" + "‘‘Refinancer’’) as security for any refinance obtained /to be obtained by the Bank from the said ...............................\n.............................(Refinancer) in respect of the said loan and the Borrower shall if and wherever required by the Bank to do so at the Borrower’s own expenses do and execute and join in doing and executing all such  acts, things, deeds, documents, assurances, etc. as the Bank may require for effecting such assignment.\n", font11n);
            rp37.setAlignment(Element.ALIGN_JUSTIFIED);
            rp37.setSpacingAfter(5);
            document.add(rp37);

            Paragraph rp38 = new Paragraph(17, "10. The Borrower shall not without the prior written consent of the Bank transfer or create / allow to be created in any manner any charge, lien, hypothecation, mortgage, pledge or other ecncumbrance whatsoever on any of the properties, assets, actionable claims etc. of the Borrower which are securities given to the Bank in respect of the said loan or create / allow to be created  any interest in such securities in favour of any party or person other than the Bank.\n", font11n);
            rp38.setAlignment(Element.ALIGN_JUSTIFIED);
            rp38.setSpacingAfter(5);
            document.add(rp38);

            Paragraph rp39 = new Paragraph(17, "11. The Borrower shall prepare detailed plans programme and estimates in respect of plantation constructional and installation works including construction of buildings, factories, workers / officers quarters and installation of machineries, plants, equipments etc. for irrigation including installation of deep tubewells etc. and any other development work provided for under the Borrower’s proposal. Any deviation from the procedure laid down in the Borrower’s proposal approved by ............................................................................... may be made by the Borrower only after obtaining prior consent in writing there for from the Bank and the said ...........................................................\n" + "........................... through the Bank. The Borrower shall forward the plans programmes and estimates to the Bank in duplicate. The Borrower shall also work out the detailed cost structure under intimation to the Bank. The Borrower agrees and undertakes to finance from out of the Borrower’s own resources any increase or escalation in the estimated / projected / budgeted cost arising out of any reason, circumstance, event or factor whatsoever.\n", font11n);
            rp39.setAlignment(Element.ALIGN_JUSTIFIED);
            rp39.setSpacingAfter(5);
            document.add(rp39);

            document.newPage();
            document.add(new Paragraph("The Borrower shall not without the prior consent in writing of the Bank undertake any work or works other than those set forth in the Borrower’s proposal.\n", font11n));

            Paragraph rp40 = new Paragraph(17, "12. The Borrower shall insure and keep insured to the satisfaction of the Bank all properties and assets constituting the Bank’s security against fire, strike, riot, civil commotion and all other risks in a sum equivalent to its full market value with an Insurance Company approved by the Bank in the joint names of the Bank and the Borrower or otherwise as the Bank may require and shall duly and punctually pay all premiums and shall not do or suffer  to be done  any act which  may invalidate  or avoid such insurance  and shall deposit  such insurance policy or policies and all cover notes, premium receipts and other documents connected therewith with the Bank. Any money realized  from such insurance  shall at the option of the Bank be applied either reinstating   or replacing  the security or in repayment  of the said loan and or interest  including  additional interest, costs, charges, expenses etc. payable in respect of the said loan. If the Borrower shall make any default in insuring or keeping insured all or any property and assets forming the Bank’s security as aforesaid the Bank shall without prejudice to and without affecting its rights under Clause 8 here of  be at liberty (but not bound) to insure and keep the same insured and the Borrower shall on demand repay to the Bank all amount or amounts spent or incurred by it for effecting or keeping such insurance with interest at the rate applicable to the said loan.\n", font11n);
            rp40.setAlignment(Element.ALIGN_JUSTIFIED);
            rp40.setSpacingAfter(5);
            document.add(rp40);

            Paragraph rp41 = new Paragraph(17, "13. The Borrower shall allow the representatives and / or nominees of the Bank to visit and inspect from time to time the said project and the Borrower’s premises, plantations, factories and other properties and assets, books of accounts and all other relevant accounts, documents and records The costs and expenses of such visits and / or inspection shall be paid and borne by the Borrower.\n", font11n);
            rp41.setAlignment(Element.ALIGN_JUSTIFIED);
            rp41.setSpacingAfter(5);
            document.add(rp41);

            Paragraph rp42 = new Paragraph(17, "14. The Borrower shall implement the decisions and / or suggestions made or taken by the Bank for the purpose of ensuring proper utilization of the said loan in achieving the results contemplated under the Borrower’s proposal.\n", font11n);
            rp42.setAlignment(Element.ALIGN_JUSTIFIED);
            rp42.setSpacingAfter(5);
            document.add(rp42);

            Paragraph rp43 = new Paragraph(17, "15. The Borrower shall furnish the Bank with all such information, statements, particulars, estimates, reports etc. as the Bank may from time to time require as to the compliance with the terms of the said loan and shall also submit to the Bank all other periodical reports and information at such time or times and in such form or forms and in such manner and at such place or places and containing such particulars, information, details, estimates, reports  etc. as the Bank may call for in respect of the Borrower’s proposal for the purpose of ascertaining the utilization of the said loan or the results thereof for any other purpose connected with or in relation to the loan.\n", font11n);
            rp43.setAlignment(Element.ALIGN_JUSTIFIED);
            rp43.setSpacingAfter(5);
            document.add(rp43);

            Paragraph rp44 = new Paragraph(17, "16. The Bank shall be at liberty to furnish to ....................................................................................................... any information, report, particulars, statements, estimates etc. or copies thereof or any other information whatsoever whether received by the Bank from the Borrower or otherwise in the Bank’s possession and the Borrower shall not be entitled to object to the same.\n", font11n);
            rp44.setAlignment(Element.ALIGN_JUSTIFIED);
            rp44.setSpacingAfter(5);
            document.add(rp44);

            Paragraph rp45 = new Paragraph(17, "17. The Borrower shall appoint qualified trained technical personnel for the proposed scheme in the Borrower’s proposal and shall appoint other staff strictly according to the requirements. The appointment or change in the technical personnel shall be made upon prior notice to and upon obtaining prior written approval of the Bank. The Bank shall be at liberty, if it thinks so fit, in its sole discretion, to appoint Manager, Internal Auditor, Technical personnel, Consultancy Firm or any other managerial or supervisory personnel as it may deem necessary and the remuneration of such personnel shall be paid by the Borrower.\n", font11n);
            rp45.setAlignment(Element.ALIGN_JUSTIFIED);
            rp45.setSpacingAfter(5);
            document.add(rp45);

            Paragraph rp46 = new Paragraph(17, "18. The Borrower shall not promote any undertaking other than the undertaking set out in the proposed scheme contained in the Borrower’s proposal on its own or as subsidiary of the Borrower without previous consent in writing of the Bank.\n", font11n);
            rp46.setAlignment(Element.ALIGN_JUSTIFIED);
            rp46.setSpacingAfter(5);
            document.add(rp46);

            Paragraph rp47 = new Paragraph(17, "19. The Borrower shall observe, carry out and implement all such suggestions, directions and instructions as the Bank may from time to time give to the Borrower for utilization of the said loan.\n", font11n);
            rp47.setAlignment(Element.ALIGN_JUSTIFIED);
            rp47.setSpacingAfter(5);
            document.add(rp47);

            document.newPage();
            Paragraph rp48 = new Paragraph(17, "20. If the Borrower makes default in payment of any moneys due to or claims of the Bank under these presents whether on account of Principal, interest  and / or additional interest and / or costs, charges and expenses or otherwise, the Bank would be at liberty to appoint  its nominee as Receiver without having resort to a Court of law and / or to a proceeding in Court, to take possession of the properties of the Borrower which might  be held by the Bank as security for the said loan under these presents or under any other security document (s) executed or to be executed by the Borrower in favour of the Bank.\n", font11n);
            rp48.setAlignment(Element.ALIGN_JUSTIFIED);
            rp48.setSpacingAfter(5);
            document.add(rp48);

            Paragraph rp49 = new Paragraph(17, "21. Any notice hereunder may be given by the Bank to the Borrower by leaving the same at the last known place of business of the Borrower in India or by sending the same by post to the Borrower  addressed  as aforesaid and if sent by post the notice shall be deemed to have been given at the time when it would be delivered in due course of post and in proving such notice when given by post it shall be sufficient to prove that the envelope containing  the notice was posted and a certificate  signed by any officer of the Bank that the envelope was so posted will be conclusive.\n", font11n);
            rp49.setAlignment(Element.ALIGN_JUSTIFIED);
            rp49.setSpacingAfter(5);
            document.add(rp49);

            Paragraph rp50 = new Paragraph(17, "22. Nothing contained in these presents shall be deemed to limit or affect prejudicially the rights and powers of the Bank under the security documents or letters of guarantee or any them or under any law.\n", font11n);
            rp50.setAlignment(Element.ALIGN_JUSTIFIED);
            rp50.setSpacingAfter(5);
            document.add(rp50);

            Paragraph rp51 = new Paragraph(17, "23. No delay in exercising of omissiojn to exercise any right, power or remedy accruing to the Bank upon any default or otherwise under these presents or under any security documents or letters of guarantee shall impair or prejudice any such right, power or remedy or shall be construed to be a waiver thereof or any acquiescence therein.\n", font11n);
            rp51.setAlignment(Element.ALIGN_JUSTIFIED);
            rp51.setSpacingAfter(5);
            document.add(rp51);

            Paragraph rp52 = new Paragraph(17, "24. Nothing contained in these presents and / or in any security documents executed / to be executed by the Borrower in favour of the  Bank in respect of the said loan shall in any way prejudice or affect any mortgage, hypothecation, pledge, lien or any other charge created /to be created by the Borrower in favour of the Bank for securing any other facility or accommodation (such as cash credit, overdraft, tea hypothecation etc.) granted / to be granted in its absolute discretion by the Bank to the Borrower.\n", font11n);
            rp52.setAlignment(Element.ALIGN_JUSTIFIED);
            rp52.setSpacingAfter(5);
            document.add(rp52);

            Paragraph rp53 = new Paragraph(17, "25. It is hereby expressly agreed that in case of any subrogation in place of the Bank in respect of the securities for the said loan, so far as the securities charged by the Borrower to the Bank for repayment of both the Borrower's indebtedness for the said loan and for the said any other credit facility are concerned, the Bank’s charge over the said securities for the said any other credit facility shall, if not in priority to the charge over the same securities in respect of the said loan, shall rank pari-passu with the charge over the said securities in respect of the said loan.\n", font11n);
            rp53.setAlignment(Element.ALIGN_JUSTIFIED);
            rp53.setSpacingAfter(5);
            document.add(rp53);

            document.add(new Paragraph("IN WITNESS WHEREOF THE Borrower executed these presents on the day and the year first hereinabove written.\n", font11n));

            Paragraph rp54 = new Paragraph(17, "Signed and delivered by the above named Borrower or The Common Seal of the above named Borrower was hereunto affixed by.............................................................................................................................................................................\n"
                    + "who signed and delivered these presents.", font11n);
            rp54.setAlignment(Element.ALIGN_JUSTIFIED);
            rp54.setSpacingAfter(5);
            document.add(rp54);

            Paragraph rp55 = new Paragraph(17, ".............................................\n", font11n);
            rp55.setAlignment(Element.ALIGN_RIGHT);
            document.add(rp55);
//            rp55.add(new Phrase("(Signature)", font11n));
////            rp55.setAlignment(Element.ALIGN_RIGHT);
//            rp55.setIndentationLeft(30);
//            rp55.setSpacingAfter(5);
            Paragraph rp56 = new Paragraph("(Signature)", font11n);
            rp56.setSpacingAfter(25);
//            rp56.setAlignment(Element.ALIGN_RIGHT);
            rp56.setIndentationLeft(440);
            document.add(rp56);

            document.add(new Paragraph("UNITED BANK OF INDIA\n", font12b));
            document.add(new Paragraph("AGREEMENT OF TERM LOAN\n\n", font12b));

            Paragraph rp57 = new Paragraph(17, "Dated the ................................  Day of ..............................................\n"
                    + "Borrower " + applicantName, font11n);
//            rp57.setAlignment(Element.ALIGN_JUSTIFIED);
            rp57.setSpacingAfter(5);
            document.add(rp57);

        } catch (FileNotFoundException fnf) {
            logger.error("Error:.....!!! " + fnf.getMessage());
        } catch (DocumentException de) {
            logger.error("Error:.....!!! " + de.getMessage());
        } catch (IOException ex) {
            logger.error("Error:.....!!! " + ex.getMessage());
        }
        document.close();

        logger.debug("***********Ended createPdf()***********");
    }

    public Chunk underLine(String data) {
        Paragraph pp9 = new Paragraph();
        pp9.setAlignment(Element.ALIGN_JUSTIFIED);
        Chunk underline1 = new Chunk(data, new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL));
        underline1.setUnderline(0.1f, -2f);
        pp9.add(underline1);
        return underline1;
    }
}
