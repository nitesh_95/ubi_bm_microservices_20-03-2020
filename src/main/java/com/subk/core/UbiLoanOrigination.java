package com.subk.core;

import java.math.BigDecimal;

/**
 *
 * @author subashkumar.s
 */
public class UbiLoanOrigination {

    private String branchName;
    private String transType;
    private String applicationId;
    private String bcVerificationStatus;
    private String bcUpdatedDatetime;
    private String fxVerificationStatus;
    private String fxRemarks;
    private String fxUpdatedDatetime;
    private String teVerificationStatus;
    private String teRemarks;
    private String teUpdatedDatetime;
    private String uhVerificationStatus;
    private String uhRemarks;
    private String uhUpdatedDatetime;
    private String bankVerificationStatus;
    private String bankRemarks;
    private String bankUpdatedDatetime;
    private String dbAction;
    private String applicationNo;
    private String groupId;
    private String groupName;
    private String custInfo;
    private String newCust;
    private String subkId;
    private String sbAccNo;
    private String custId;
    private String BcoMobile;
    private String BcoPin;
    private String savingACInfo;
    private String bcoId;
    private String bcoName;
    //step2
    private String custTitle;
    private String custTitleCode;
    private String appl1Name;
    private String custTypeCode;
    private String familyExp;
    private String jlgGroupName;
    private String jlgLeader;
//    private String familySust;
    private String appl1Careof;
//    private String mobileChrgs;
    private String permHouseNo;
//    private String internetChrgs;
    private String permStreetNo;
//    private String televChrgs;
    private String permLandMark;
//    private String fuelChrgs;
    private String permVillCity;
//    private String electrBill;
    private String permDistrict;
//    private String studyExp;
    private String permState;
    private String blockCode;
//    private String insurancePrem;
    private String permPinCode;
    private String applicantAddress;
    private String nomineeDetails;
    private String srcOfIncomeCode;
//    private String networthCode;
//    private String annualTurnoverCode;
    private String maritalStatutCode;
    private String glSubheadCode;
    private String constitutionCode;
    private String casteCode;
    private String communityCode;
    private String schemeCode;
    private String introducerStatusCode;
    private String appl1Address;
//    private String panchTax;
    private String telephone;
//    private String miscExp;
    private String mobileNo;
//    private String totalyrlyfamExp;
    private String yrlySurplus;
    private String idProof;
    private String idProofImage;
//    private String avgWeeklySurplus;
    private String addressProof;
    private String addressProofImage;
    private String loanAmount;
    private String pmsbyPmjjbyCovrg;
    private String approachFor;
    private String pmsbyPmjjbyValidity;
    private String presentFinancer;
    private String appl1Occupation;
    private String repayPeriod;
    private String apprxyrlyApp1Income;
    private String weeklyInstalment;
    private String familyMembsOccupation;
    private String bankMitraRemarks;
    private String yrlyOthFamilyMembIncome;
    private String bankManagRemarks;
    private String totalYrlyFamilyIncome;
    private String runningSince;
    private String yearsofExp;
    //step3
    private String bcRecomendation;
    private String bcRemarks;
    //step4
    private String newSubkId;
    private String appl1Image;
    private String permHusFathName;
    private String permRelType;
    private String permTelephone;
    private String commHusFathName;
    private String commRelationType;
    private String commHouseNo;
    private String commStreetNo;
    private String commLandMark;
    private String commVillCity;
    private String commDistrict;
    private String commState;
    private String commPinCode;
    private String commTelephone;
    private String areaVillageCode;
    private String areaBlockCode;
    private String areaDistCode;
    private String areaStateCode;
    private String fhgType;
    private String fhgName;
    private String operationMode;
    private String sex;
    private String panGirNo;
    private String dobDate;
    private String commVillage;
//    private String annualIncome;
    private String annualTurnover;
    private String category;
    private String qualification;
    private String eStatement;
    private String houseLoan;
    private String chequeBook;
    private String vehicleLoan;
    private String mobileBanking;
    private String mutualFund;
    private String internetBanking;
    private String lifeInsurance;
    private String creditCard;
    private String pension;
    private String addOnOthers;
    private String crossSellOthers;
    private String atmRequest;
    private String smsAlert;
    private String emailId;
    private String idProofType;
    private String addressProofType;
    //step5
    private String introducerTitle;
    private String introducerName;
    private String introducerCustId;
    private String introducerAccNo;
    private String introducerKnownYear;
    //step6
    private String nomineeAccNature;
    private String nomineeAge;
    private String nomineeAccNo;
    private String nomineeDob;
    private String nomineeAdDetails;
    private String nomineeGaurdianName;
    private String nomineeName;
    private String nomineeAddress;
    private String nomineeGuardianAge;
    private String nomineeRelation;
    private String nomineeGuardianAdd;
    private String minorNomiGuarTitle;
    private String minorNomiGuarName;
    private String minorNomiGuarAge;
    private String minorNomiGuarRelType;
    private String minorNomiGuarRelCode;
    private String NomineeCity;
    //step7
    private String witness1Name;
    private String witness2Name;
    private String witness1Add;
    private String witness2Add;
    private String witness1Place;
    private String witness2Place;
    private String witness1Telephone;
    private String witness2Telephone;
    //step8
    private String appl2Name;
    private String appl2Pincode;
    private String appl2FhgName;
    private String appl2Telephone;
    private String appl2Careof;
    private String appl2Mobile;
    private String appl2HouseNo;
    private String appl2Sex;
    private String appl2StreetNo;
    private String appl2Dob;
    private String appl2Landmark;
    private String appl2Occupation;
    private String appl2VillCity;
    private String appl2Category;
    private String appl2District;
    private String appl2IdProof;
    private String appl2State;
    private String appl2AddProof;
    //step-9
    private String rightThumb;
    private String rightIndex;
    private String rightMiddle;
    private String rightRing;
    private String rightSmall;
    private String leftThumb;
    private String leftIndex;
    private String leftMiddle;
    private String leftRing;
    private String leftSmall;
    private String custPin;
    private String confirmCustPin;
    //step10
    private String familyMemb1Name;
    private String familyMemb1Relation;
    private String familyMemb1Sex;
    private String familyMemb1Age;
    private String familyMemb1Occup;
    private String familyMemb1Qualification;
    private String familyMemb2Name;
    private String familyMemb2Relation;
    private String familyMemb2Sex;
    private String familyMemb2Age;
    private String familyMemb2Occup;
    private String familyMemb2Qualification;
    private String familyMemb3Name;
    private String familyMemb3Relation;
    private String familyMemb3Sex;
    private String familyMemb3Age;
    private String familyMemb3Occup;
    private String familyMemb3Qualification;
    private String loanAmountDue;
    private String liveStock;
    private String agrlImpl;
    private String activity;
    private String assetVehicle;
    private String loanReuired;
    private String assetAnyOther;
    private String loanAccNomineeName;
    private String loanSource;
    private String loanAccNomineeRelation;
    private String loanPurpose;
    private String loanAccNomineeAge;
    private String amountBorrowed;
    private String loanAccNomineeResident;
    private String isFPCaptured;
    private String solId;
    private String loanAmountSanctioned;
    private String noOfInstalments;
    private String interestRate;
    private String instalmentAmount;
    private String activityCode;
    private String Education;
    private String UH_Recommended_LoanAmount;
    private String UH_Recommended_InstalmentAmount;
    private String UH_Recommended_NoOfMonths;
    
    private BigDecimal mclrl;
    private BigDecimal mclrMale;
    private BigDecimal mclrFemale;
    
    //done by abhi
    private String mclr;
    private String mclr_male_female;
    
    private String applicantphotopath;
    private String ekyc_ApplicantPhoto;

    public String getApplicantphotopath() {
        return applicantphotopath;
    }

    public void setApplicantphotopath(String applicantphotopath) {
        this.applicantphotopath = applicantphotopath;
    }

    public String getEkyc_ApplicantPhoto() {
        return ekyc_ApplicantPhoto;
    }

    public void setEkyc_ApplicantPhoto(String ekyc_ApplicantPhoto) {
        this.ekyc_ApplicantPhoto = ekyc_ApplicantPhoto;
    }

    public BigDecimal getMclrl() {
        return mclrl;
    }

    public void setMclrl(BigDecimal mclrl) {
        this.mclrl = mclrl;
    }

    public BigDecimal getMclrMale() {
        return mclrMale;
    }

    public void setMclrMale(BigDecimal mclrMale) {
        this.mclrMale = mclrMale;
    }

    public BigDecimal getMclrFemale() {
        return mclrFemale;
    }

    public void setMclrFemale(BigDecimal mclrFemale) {
        this.mclrFemale = mclrFemale;
    }
    
    
    
    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }
    
    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getLoanAmountSanctioned() {
        return loanAmountSanctioned;
    }

    public void setLoanAmountSanctioned(String loanAmountSanctioned) {
        this.loanAmountSanctioned = loanAmountSanctioned;
    }

    public String getNoOfInstalments() {
        return noOfInstalments;
    }

    public void setNoOfInstalments(String noOfInstalments) {
        this.noOfInstalments = noOfInstalments;
    }

    public String getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }

    public String getInstalmentAmount() {
        return instalmentAmount;
    }

    public void setInstalmentAmount(String instalmentAmount) {
        this.instalmentAmount = instalmentAmount;
    }

    public String getApplicationId() {

        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getBcVerificationStatus() {
        return bcVerificationStatus;
    }

    public void setBcVerificationStatus(String bcVerificationStatus) {
        this.bcVerificationStatus = bcVerificationStatus;
    }

    public String getBcUpdatedDatetime() {
        return bcUpdatedDatetime;
    }

    public void setBcUpdatedDatetime(String bcUpdatedDatetime) {
        this.bcUpdatedDatetime = bcUpdatedDatetime;
    }

    public String getFxVerificationStatus() {
        return fxVerificationStatus;
    }

    public void setFxVerificationStatus(String fxVerificationStatus) {
        this.fxVerificationStatus = fxVerificationStatus;
    }

    public String getFxRemarks() {
        return fxRemarks;
    }

    public void setFxRemarks(String fxRemarks) {
        this.fxRemarks = fxRemarks;
    }

    public String getFxUpdatedDatetime() {
        return fxUpdatedDatetime;
    }

    public void setFxUpdatedDatetime(String fxUpdatedDatetime) {
        this.fxUpdatedDatetime = fxUpdatedDatetime;
    }

    public String getTeVerificationStatus() {
        return teVerificationStatus;
    }

    public void setTeVerificationStatus(String teVerificationStatus) {
        this.teVerificationStatus = teVerificationStatus;
    }

    public String getTeRemarks() {
        return teRemarks;
    }

    public void setTeRemarks(String teRemarks) {
        this.teRemarks = teRemarks;
    }

    public String getTeUpdatedDatetime() {
        return teUpdatedDatetime;
    }

    public void setTeUpdatedDatetime(String teUpdatedDatetime) {
        this.teUpdatedDatetime = teUpdatedDatetime;
    }

    public String getUhVerificationStatus() {
        return uhVerificationStatus;
    }

    public void setUhVerificationStatus(String uhVerificationStatus) {
        this.uhVerificationStatus = uhVerificationStatus;
    }

    public String getUhRemarks() {
        return uhRemarks;
    }

    public void setUhRemarks(String uhRemarks) {
        this.uhRemarks = uhRemarks;
    }

    public String getUhUpdatedDatetime() {
        return uhUpdatedDatetime;
    }

    public void setUhUpdatedDatetime(String uhUpdatedDatetime) {
        this.uhUpdatedDatetime = uhUpdatedDatetime;
    }

    public String getBankVerificationStatus() {
        return bankVerificationStatus;
    }

    public void setBankVerificationStatus(String bankVerificationStatus) {
        this.bankVerificationStatus = bankVerificationStatus;
    }

    public String getBankRemarks() {
        return bankRemarks;
    }

    public void setBankRemarks(String bankRemarks) {
        this.bankRemarks = bankRemarks;
    }

    public String getBankUpdatedDatetime() {
        return bankUpdatedDatetime;
    }

    public void setBankUpdatedDatetime(String bankUpdatedDatetime) {
        this.bankUpdatedDatetime = bankUpdatedDatetime;
    }

    /**
     * @return the dbAction
     */
    public String getDbAction() {
        return dbAction;
    }

    /**
     * @return the applicationNo
     */
    public String getApplicationNo() {
        return applicationNo;
    }

    /**
     * @return the groupId
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @return the custInfo
     */
    public String getCustInfo() {
        return custInfo;
    }

    /**
     * @return the newCust
     */
    public String getNewCust() {
        return newCust;
    }

    /**
     * @return the subkId
     */
    public String getSubkId() {
        return subkId;
    }

    /**
     * @return the sbAccNo
     */
    public String getSbAccNo() {
        return sbAccNo;
    }

    /**
     * @return the custId
     */
    public String getCustId() {
        return custId;
    }

    /**
     * @return the BcoMobile
     */
    public String getBcoMobile() {
        return BcoMobile;
    }

    /**
     * @return the BcoPin
     */
    public String getBcoPin() {
        return BcoPin;
    }

    /**
     * @return the savingACInfo
     */
    public String getSavingACInfo() {
        return savingACInfo;
    }

    /**
     * @return the bcoId
     */
    public String getBcoId() {
        return bcoId;
    }

    /**
     * @return the bcoName
     */
    public String getBcoName() {
        return bcoName;
    }

    /**
     * @return the custTitle
     */
    public String getCustTitle() {
        return custTitle;
    }

    /**
     * @return the custTitleCode
     */
    public String getCustTitleCode() {
        return custTitleCode;
    }

    /**
     * @return the appl1Name
     */
    public String getAppl1Name() {
        return appl1Name;
    }

    /**
     * @return the custTypeCode
     */
    public String getCustTypeCode() {
        return custTypeCode;
    }

    /**
     * @return the familyExp
     */
    public String getFamilyExp() {
        return familyExp;
    }

    /**
     * @return the jlgGroupName
     */
    public String getJlgGroupName() {
        return jlgGroupName;
    }

    /**
     * @return the jlgLeader
     */
    public String getJlgLeader() {
        return jlgLeader;
    }

    /**
     * @return the familySust
     */
//    public String getFamilySust() {
//        return familySust;
//    }
    /**
     * @return the appl1Careof
     */
    public String getAppl1Careof() {
        return appl1Careof;
    }

    /**
     * @return the mobileChrgs //
     */
//    public String getMobileChrgs() {
//        return mobileChrgs;
//    }
    /**
     * @return the permHouseNo
     */
    public String getPermHouseNo() {
        return permHouseNo;
    }

    /**
     * @return the internetChrgs
     */
//    public String getInternetChrgs() {
//        return internetChrgs;
//    }
    /**
     * @return the permStreetNo
     */
    public String getPermStreetNo() {
        return permStreetNo;
    }

    /**
     * @return the televChrgs
     */
//    public String getTelevChrgs() {
//        return televChrgs;
//    }
    /**
     * @return the permLandMark
     */
    public String getPermLandMark() {
        return permLandMark;
    }

    /**
     * @return the fuelChrgs
     */
//    public String getFuelChrgs() {
//        return fuelChrgs;
//    }
    /**
     * @return the permVillCity
     */
    public String getPermVillCity() {
        return permVillCity;
    }

    /**
     * @return the electrBill
     */
//    public String getElectrBill() {
//        return electrBill;
//    }
    /**
     * @return the permDistrict
     */
    public String getPermDistrict() {
        return permDistrict;
    }

    /**
     * @return the studyExp
     */
//    public String getStudyExp() {
//        return studyExp;
//    }
    /**
     * @return the permState
     */
    public String getPermState() {
        return permState;
    }

    /**
     * @return the blockCode
     */
    public String getBlockCode() {
        return blockCode;
    }

    /**
     * @return the insurancePrem
     */
//    public String getInsurancePrem() {
//        return insurancePrem;
//    }
    /**
     * @return the permPinCode
     */
    public String getPermPinCode() {
        return permPinCode;
    }

    /**
     * @return the applicantAddress
     */
    public String getApplicantAddress() {
        return applicantAddress;
    }

    /**
     * @return the nomineeDetails
     */
    public String getNomineeDetails() {
        return nomineeDetails;
    }

    /**
     * @return the srcOfIncomeCode
     */
    public String getSrcOfIncomeCode() {
        return srcOfIncomeCode;
    }

    /**
     * @return the networthCode
     */
//    public String getNetworthCode() {
//        return networthCode;
//    }
    /**
     * @return the annualTurnoverCode
     */
//    public String getAnnualTurnoverCode() {
//        return annualTurnoverCode;
//    }
    /**
     * @return the maritalStatutCode
     */
    public String getMaritalStatutCode() {
        return maritalStatutCode;
    }

    /**
     * @return the glSubheadCode
     */
    public String getGlSubheadCode() {
        return glSubheadCode;
    }

    /**
     * @return the constitutionCode
     */
    public String getConstitutionCode() {
        return constitutionCode;
    }

    /**
     * @return the casteCode
     */
    public String getCasteCode() {
        return casteCode;
    }

    /**
     * @return the communityCode
     */
    public String getCommunityCode() {
        return communityCode;
    }

    /**
     * @return the schemeCode
     */
    public String getSchemeCode() {
        return schemeCode;
    }

    /**
     * @return the introducerStatusCode
     */
    public String getIntroducerStatusCode() {
        return introducerStatusCode;
    }

    /**
     * @return the appl1Address
     */
    public String getAppl1Address() {
        return appl1Address;
    }

    /**
     * @return the panchTax
     */
//    public String getPanchTax() {
//        return panchTax;
//    }
    /**
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * @return the miscExp
     */
//    public String getMiscExp() {
//        return miscExp;
//    }
    /**
     * @return the mobileNo
     */
    public String getMobileNo() {
        return mobileNo;
    }

    /**
     * @return the totalyrlyfamExp
     */
//    public String getTotalyrlyfamExp() {
//        return totalyrlyfamExp;
//    }
    /**
     * @return the yrlySurplus
     */
    public String getYrlySurplus() {
        return yrlySurplus;
    }

    /**
     * @return the idProof
     */
    public String getIdProof() {
        return idProof;
    }

    /**
     * @return the idProofImage
     */
    public String getIdProofImage() {
        return idProofImage;
    }

    /**
     * @return the avgWeeklySurplus
     */
//    public String getAvgWeeklySurplus() {
//        return avgWeeklySurplus;
//    }
    /**
     * @return the addressProof
     */
    public String getAddressProof() {
        return addressProof;
    }

    /**
     * @return the addressProofImage
     */
    public String getAddressProofImage() {
        return addressProofImage;
    }

    /**
     * @return the loanAmount
     */
    public String getLoanAmount() {
        return loanAmount;
    }

    /**
     * @return the pmsbyPmjjbyCovrg
     */
    public String getPmsbyPmjjbyCovrg() {
        return pmsbyPmjjbyCovrg;
    }

    /**
     * @return the approachFor
     */
    public String getApproachFor() {
        return approachFor;
    }

    /**
     * @return the pmsbyPmjjbyValidity
     */
    public String getPmsbyPmjjbyValidity() {
        return pmsbyPmjjbyValidity;
    }

    /**
     * @return the presentFinancer
     */
    public String getPresentFinancer() {
        return presentFinancer;
    }

    /**
     * @return the appl1Occupation
     */
    public String getAppl1Occupation() {
        return appl1Occupation;
    }

    /**
     * @return the repayPeriod
     */
    public String getRepayPeriod() {
        return repayPeriod;
    }

    /**
     * @return the apprxyrlyApp1Income
     */
    public String getApprxyrlyApp1Income() {
        return apprxyrlyApp1Income;
    }

    /**
     * @return the weeklyInstalment
     */
    public String getWeeklyInstalment() {
        return weeklyInstalment;
    }

    /**
     * @return the familyMembsOccupation
     */
    public String getFamilyMembsOccupation() {
        return familyMembsOccupation;
    }

    /**
     * @return the bankMitraRemarks
     */
    public String getBankMitraRemarks() {
        return bankMitraRemarks;
    }

    /**
     * @return the yrlyOthFamilyMembIncome
     */
    public String getYrlyOthFamilyMembIncome() {
        return yrlyOthFamilyMembIncome;
    }

    /**
     * @return the bankManagRemarks
     */
    public String getBankManagRemarks() {
        return bankManagRemarks;
    }

    /**
     * @return the totalYrlyFamilyIncome
     */
    public String getTotalYrlyFamilyIncome() {
        return totalYrlyFamilyIncome;
    }

    /**
     * @return the bcRecomendation
     */
    public String getBcRecomendation() {
        return bcRecomendation;
    }

    /**
     * @return the bcRemarks
     */
    public String getBcRemarks() {
        return bcRemarks;
    }

    /**
     * @return the newSubkId
     */
    public String getNewSubkId() {
        return newSubkId;
    }

    /**
     * @return the appl1Image
     */
    public String getAppl1Image() {
        return appl1Image;
    }

    /**
     * @return the permHusFathName
     */
    public String getPermHusFathName() {
        return permHusFathName;
    }

    /**
     * @return the permRelType
     */
    public String getPermRelType() {
        return permRelType;
    }

    /**
     * @return the permTelephone
     */
    public String getPermTelephone() {
        return permTelephone;
    }

    /**
     * @return the commHusFathName
     */
    public String getCommHusFathName() {
        return commHusFathName;
    }

    /**
     * @return the commRelationType
     */
    public String getCommRelationType() {
        return commRelationType;
    }

    /**
     * @return the commHouseNo
     */
    public String getCommHouseNo() {
        return commHouseNo;
    }

    /**
     * @return the commStreetNo
     */
    public String getCommStreetNo() {
        return commStreetNo;
    }

    /**
     * @return the commLandMark
     */
    public String getCommLandMark() {
        return commLandMark;
    }

    /**
     * @return the commVillCity
     */
    public String getCommVillCity() {
        return commVillCity;
    }

    /**
     * @return the commDistrict
     */
    public String getCommDistrict() {
        return commDistrict;
    }

    /**
     * @return the commState
     */
    public String getCommState() {
        return commState;
    }

    /**
     * @return the commPinCode
     */
    public String getCommPinCode() {
        return commPinCode;
    }

    /**
     * @return the commTelephone
     */
    public String getCommTelephone() {
        return commTelephone;
    }

    /**
     * @return the areaVillageCode
     */
    public String getAreaVillageCode() {
        return areaVillageCode;
    }

    /**
     * @return the areaBlockCode
     */
    public String getAreaBlockCode() {
        return areaBlockCode;
    }

    /**
     * @return the areaDistCode
     */
    public String getAreaDistCode() {
        return areaDistCode;
    }

    /**
     * @return the areaStateCode
     */
    public String getAreaStateCode() {
        return areaStateCode;
    }

    /**
     * @return the fhgType
     */
    public String getFhgType() {
        return fhgType;
    }

    /**
     * @return the fhgName
     */
    public String getFhgName() {
        return fhgName;
    }

    /**
     * @return the operationMode
     */
    public String getOperationMode() {
        return operationMode;
    }

    /**
     * @return the sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * @return the panGirNo
     */
    public String getPanGirNo() {
        return panGirNo;
    }

    /**
     * @return the dobDate
     */
    public String getDobDate() {
        return dobDate;
    }

//    /**
//     * @return the annualIncome
//     */
//    public String getAnnualIncome() {
//        return annualIncome;
//    }
    /**
     * @return the annualTurnover
     */
    public String getAnnualTurnover() {
        return annualTurnover;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @return the qualification
     */
    public String getQualification() {
        return qualification;
    }

    /**
     * @return the eStatement
     */
    public String geteStatement() {
        return eStatement;
    }

    /**
     * @return the houseLoan
     */
    public String getHouseLoan() {
        return houseLoan;
    }

    /**
     * @return the chequeBook
     */
    public String getChequeBook() {
        return chequeBook;
    }

    /**
     * @return the vehicleLoan
     */
    public String getVehicleLoan() {
        return vehicleLoan;
    }

    /**
     * @return the mobileBanking
     */
    public String getMobileBanking() {
        return mobileBanking;
    }

    /**
     * @return the mutualFund
     */
    public String getMutualFund() {
        return mutualFund;
    }

    /**
     * @return the internetBanking
     */
    public String getInternetBanking() {
        return internetBanking;
    }

    /**
     * @return the lifeInsurance
     */
    public String getLifeInsurance() {
        return lifeInsurance;
    }

    /**
     * @return the creditCard
     */
    public String getCreditCard() {
        return creditCard;
    }

    /**
     * @return the pension
     */
    public String getPension() {
        return pension;
    }

    /**
     * @return the addOnOthers
     */
    public String getAddOnOthers() {
        return addOnOthers;
    }

    /**
     * @return the crossSellOthers
     */
    public String getCrossSellOthers() {
        return crossSellOthers;
    }

    /**
     * @return the atmRequest
     */
    public String getAtmRequest() {
        return atmRequest;
    }

    /**
     * @return the smsAlert
     */
    public String getSmsAlert() {
        return smsAlert;
    }

    /**
     * @return the emailId
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * @return the idProofType
     */
    public String getIdProofType() {
        return idProofType;
    }

    /**
     * @return the addressProofType
     */
    public String getAddressProofType() {
        return addressProofType;
    }

    /**
     * @return the introducerTitle
     */
    public String getIntroducerTitle() {
        return introducerTitle;
    }

    /**
     * @return the introducerName
     */
    public String getIntroducerName() {
        return introducerName;
    }

    /**
     * @return the introducerCustId
     */
    public String getIntroducerCustId() {
        return introducerCustId;
    }

    /**
     * @return the introducerAccNo
     */
    public String getIntroducerAccNo() {
        return introducerAccNo;
    }

    /**
     * @return the introducerKnownYear
     */
    public String getIntroducerKnownYear() {
        return introducerKnownYear;
    }

    /**
     * @return the nomineeAccNature
     */
    public String getNomineeAccNature() {
        return nomineeAccNature;
    }

    /**
     * @return the nomineeAge
     */
    public String getNomineeAge() {
        return nomineeAge;
    }

    /**
     * @return the nomineeAccNo
     */
    public String getNomineeAccNo() {
        return nomineeAccNo;
    }

    /**
     * @return the nomineeDob
     */
    public String getNomineeDob() {
        return nomineeDob;
    }

    /**
     * @return the nomineeAdDetails
     */
    public String getNomineeAdDetails() {
        return nomineeAdDetails;
    }

    /**
     * @return the nomineeGaurdianName
     */
    public String getNomineeGaurdianName() {
        return nomineeGaurdianName;
    }

    /**
     * @return the nomineeName
     */
    public String getNomineeName() {
        return nomineeName;
    }

    /**
     * @return the nomineeAddress
     */
    public String getNomineeAddress() {
        return nomineeAddress;
    }

    /**
     * @return the nomineeGuardianAge
     */
    public String getNomineeGuardianAge() {
        return nomineeGuardianAge;
    }

    /**
     * @return the nomineeRelation
     */
    public String getNomineeRelation() {
        return nomineeRelation;
    }

    /**
     * @return the nomineeGuardianAdd
     */
    public String getNomineeGuardianAdd() {
        return nomineeGuardianAdd;
    }

    /**
     * @return the minorNomiGuarTitle
     */
    public String getMinorNomiGuarTitle() {
        return minorNomiGuarTitle;
    }

    /**
     * @return the minorNomiGuarName
     */
    public String getMinorNomiGuarName() {
        return minorNomiGuarName;
    }

    /**
     * @return the minorNomiGuarAge
     */
    public String getMinorNomiGuarAge() {
        return minorNomiGuarAge;
    }

    /**
     * @return the minorNomiGuarRelType
     */
    public String getMinorNomiGuarRelType() {
        return minorNomiGuarRelType;
    }

    /**
     * @return the minorNomiGuarRelCode
     */
    public String getMinorNomiGuarRelCode() {
        return minorNomiGuarRelCode;
    }

    /**
     * @return the witness1Name
     */
    public String getWitness1Name() {
        return witness1Name;
    }

    /**
     * @return the witness2Name
     */
    public String getWitness2Name() {
        return witness2Name;
    }

    /**
     * @return the witness1Add
     */
    public String getWitness1Add() {
        return witness1Add;
    }

    /**
     * @return the witness2Add
     */
    public String getWitness2Add() {
        return witness2Add;
    }

    /**
     * @return the witness1Place
     */
    public String getWitness1Place() {
        return witness1Place;
    }

    /**
     * @return the witness2Place
     */
    public String getWitness2Place() {
        return witness2Place;
    }

    /**
     * @return the witness1Telephone
     */
    public String getWitness1Telephone() {
        return witness1Telephone;
    }

    /**
     * @return the witness2Telephone
     */
    public String getWitness2Telephone() {
        return witness2Telephone;
    }

    /**
     * @return the appl2Name
     */
    public String getAppl2Name() {
        return appl2Name;
    }

    /**
     * @return the appl2Pincode
     */
    public String getAppl2Pincode() {
        return appl2Pincode;
    }

    /**
     * @return the appl2FhgName
     */
    public String getAppl2FhgName() {
        return appl2FhgName;
    }

    /**
     * @return the appl2Telephone
     */
    public String getAppl2Telephone() {
        return appl2Telephone;
    }

    /**
     * @return the appl2Careof
     */
    public String getAppl2Careof() {
        return appl2Careof;
    }

    /**
     * @return the appl2Mobile
     */
    public String getAppl2Mobile() {
        return appl2Mobile;
    }

    /**
     * @return the appl2HouseNo
     */
    public String getAppl2HouseNo() {
        return appl2HouseNo;
    }

    /**
     * @return the appl2Sex
     */
    public String getAppl2Sex() {
        return appl2Sex;
    }

    /**
     * @return the appl2StreetNo
     */
    public String getAppl2StreetNo() {
        return appl2StreetNo;
    }

    /**
     * @return the appl2Dob
     */
    public String getAppl2Dob() {
        return appl2Dob;
    }

    /**
     * @return the appl2Landmark
     */
    public String getAppl2Landmark() {
        return appl2Landmark;
    }

    /**
     * @return the appl2Occupation
     */
    public String getAppl2Occupation() {
        return appl2Occupation;
    }

    /**
     * @return the appl2VillCity
     */
    public String getAppl2VillCity() {
        return appl2VillCity;
    }

    /**
     * @return the appl2Category
     */
    public String getAppl2Category() {
        return appl2Category;
    }

    /**
     * @return the appl2District
     */
    public String getAppl2District() {
        return appl2District;
    }

    /**
     * @return the appl2IdProof
     */
    public String getAppl2IdProof() {
        return appl2IdProof;
    }

    /**
     * @return the appl2State
     */
    public String getAppl2State() {
        return appl2State;
    }

    /**
     * @return the appl2AddProof
     */
    public String getAppl2AddProof() {
        return appl2AddProof;
    }

    /**
     * @return the rightThumb
     */
    public String getRightThumb() {
        return rightThumb;
    }

    /**
     * @return the rightIndex
     */
    public String getRightIndex() {
        return rightIndex;
    }

    /**
     * @return the rightMiddle
     */
    public String getRightMiddle() {
        return rightMiddle;
    }

    /**
     * @return the rightRing
     */
    public String getRightRing() {
        return rightRing;
    }

    /**
     * @return the rightSmall
     */
    public String getRightSmall() {
        return rightSmall;
    }

    /**
     * @return the leftThumb
     */
    public String getLeftThumb() {
        return leftThumb;
    }

    /**
     * @return the leftIndex
     */
    public String getLeftIndex() {
        return leftIndex;
    }

    /**
     * @return the leftMiddle
     */
    public String getLeftMiddle() {
        return leftMiddle;
    }

    /**
     * @return the leftRing
     */
    public String getLeftRing() {
        return leftRing;
    }

    /**
     * @return the leftSmall
     */
    public String getLeftSmall() {
        return leftSmall;
    }

    /**
     * @return the custPin
     */
    public String getCustPin() {
        return custPin;
    }

    /**
     * @return the confirmCustPin
     */
    public String getConfirmCustPin() {
        return confirmCustPin;
    }

    /**
     * @return the familyMemb1Name
     */
    public String getFamilyMemb1Name() {
        return familyMemb1Name;
    }

    /**
     * @return the familyMemb1Relation
     */
    public String getFamilyMemb1Relation() {
        return familyMemb1Relation;
    }

    /**
     * @return the familyMemb1Sex
     */
    public String getFamilyMemb1Sex() {
        return familyMemb1Sex;
    }

    /**
     * @return the familyMemb1Age
     */
    public String getFamilyMemb1Age() {
        return familyMemb1Age;
    }

    /**
     * @return the familyMemb1Occup
     */
    public String getFamilyMemb1Occup() {
        return familyMemb1Occup;
    }

    /**
     * @return the familyMemb1Qualification
     */
    public String getFamilyMemb1Qualification() {
        return familyMemb1Qualification;
    }

    /**
     * @return the familyMemb2Name
     */
    public String getFamilyMemb2Name() {
        return familyMemb2Name;
    }

    /**
     * @return the familyMemb2Relation
     */
    public String getFamilyMemb2Relation() {
        return familyMemb2Relation;
    }

    /**
     * @return the familyMemb2Sex
     */
    public String getFamilyMemb2Sex() {
        return familyMemb2Sex;
    }

    /**
     * @return the familyMemb2Age
     */
    public String getFamilyMemb2Age() {
        return familyMemb2Age;
    }

    /**
     * @return the familyMemb2Occup
     */
    public String getFamilyMemb2Occup() {
        return familyMemb2Occup;
    }

    /**
     * @return the familyMemb2Qualification
     */
    public String getFamilyMemb2Qualification() {
        return familyMemb2Qualification;
    }

    /**
     * @return the familyMemb3Name
     */
    public String getFamilyMemb3Name() {
        return familyMemb3Name;
    }

    /**
     * @return the familyMemb3Relation
     */
    public String getFamilyMemb3Relation() {
        return familyMemb3Relation;
    }

    /**
     * @return the familyMemb3Sex
     */
    public String getFamilyMemb3Sex() {
        return familyMemb3Sex;
    }

    /**
     * @return the familyMemb3Age
     */
    public String getFamilyMemb3Age() {
        return familyMemb3Age;
    }

    /**
     * @return the familyMemb3Occup
     */
    public String getFamilyMemb3Occup() {
        return familyMemb3Occup;
    }

    /**
     * @return the familyMemb3Qualification
     */
    public String getFamilyMemb3Qualification() {
        return familyMemb3Qualification;
    }

    /**
     * @return the loanAmountDue
     */
    public String getLoanAmountDue() {
        return loanAmountDue;
    }

    /**
     * @return the liveStock
     */
    public String getLiveStock() {
        return liveStock;
    }

    /**
     * @return the agrlImpl
     */
    public String getAgrlImpl() {
        return agrlImpl;
    }

    /**
     * @return the activity
     */
    public String getActivity() {
        return activity;
    }

    /**
     * @return the assetVehicle
     */
    public String getAssetVehicle() {
        return assetVehicle;
    }

    /**
     * @return the loanReuired
     */
    public String getLoanReuired() {
        return loanReuired;
    }

    /**
     * @return the assetAnyOther
     */
    public String getAssetAnyOther() {
        return assetAnyOther;
    }

    /**
     * @return the loanAccNomineeName
     */
    public String getLoanAccNomineeName() {
        return loanAccNomineeName;
    }

    /**
     * @return the loanSource
     */
    public String getLoanSource() {
        return loanSource;
    }

    /**
     * @return the loanAccNomineeRelation
     */
    public String getLoanAccNomineeRelation() {
        return loanAccNomineeRelation;
    }

    /**
     * @return the loanPurpose
     */
    public String getLoanPurpose() {
        return loanPurpose;
    }

    /**
     * @return the loanAccNomineeAge
     */
    public String getLoanAccNomineeAge() {
        return loanAccNomineeAge;
    }

    /**
     * @return the amountBorrowed
     */
    public String getAmountBorrowed() {
        return amountBorrowed;
    }

    /**
     * @return the loanAccNomineeResident
     */
    public String getLoanAccNomineeResident() {
        return loanAccNomineeResident;
    }

    /**
     * @return the isFPCaptured
     */
    public String getIsFPCaptured() {
        return isFPCaptured;
    }

    /**
     * @param dbAction the dbAction to set
     */
    public void setDbAction(String dbAction) {
        this.dbAction = dbAction;
    }

    /**
     * @param applicationNo the applicationNo to set
     */
    public void setApplicationNo(String applicationNo) {
        this.applicationNo = applicationNo;
    }

    /**
     * @param groupId the groupId to set
     */
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @param custInfo the custInfo to set
     */
    public void setCustInfo(String custInfo) {
        this.custInfo = custInfo;
    }

    /**
     * @param newCust the newCust to set
     */
    public void setNewCust(String newCust) {
        this.newCust = newCust;
    }

    /**
     * @param subkId the subkId to set
     */
    public void setSubkId(String subkId) {
        this.subkId = subkId;
    }

    /**
     * @param sbAccNo the sbAccNo to set
     */
    public void setSbAccNo(String sbAccNo) {
        this.sbAccNo = sbAccNo;
    }

    /**
     * @param custId the custId to set
     */
    public void setCustId(String custId) {
        this.custId = custId;
    }

    /**
     * @param BcoMobile the BcoMobile to set
     */
    public void setBcoMobile(String BcoMobile) {
        this.BcoMobile = BcoMobile;
    }

    /**
     * @param BcoPin the BcoPin to set
     */
    public void setBcoPin(String BcoPin) {
        this.BcoPin = BcoPin;
    }

    /**
     * @param savingACInfo the savingACInfo to set
     */
    public void setSavingACInfo(String savingACInfo) {
        this.savingACInfo = savingACInfo;
    }

    /**
     * @param bcoId the bcoId to set
     */
    public void setBcoId(String bcoId) {
        this.bcoId = bcoId;
    }

    /**
     * @param bcoName the bcoName to set
     */
    public void setBcoName(String bcoName) {
        this.bcoName = bcoName;
    }

    /**
     * @param custTitle the custTitle to set
     */
    public void setCustTitle(String custTitle) {
        this.custTitle = custTitle;
    }

    /**
     * @param custTitleCode the custTitleCode to set
     */
    public void setCustTitleCode(String custTitleCode) {
        this.custTitleCode = custTitleCode;
    }

    /**
     * @param appl1Name the appl1Name to set
     */
    public void setAppl1Name(String appl1Name) {
        this.appl1Name = appl1Name;
    }

    /**
     * @param custTypeCode the custTypeCode to set
     */
    public void setCustTypeCode(String custTypeCode) {
        this.custTypeCode = custTypeCode;
    }

    /**
     * @param familyExp the familyExp to set
     */
    public void setFamilyExp(String familyExp) {
        this.familyExp = familyExp;
    }

    /**
     * @param jlgGroupName the jlgGroupName to set
     */
    public void setJlgGroupName(String jlgGroupName) {
        this.jlgGroupName = jlgGroupName;
    }

    /**
     * @param jlgLeader the jlgLeader to set
     */
    public void setJlgLeader(String jlgLeader) {
        this.jlgLeader = jlgLeader;
    }

    /**
     * @param familySust the familySust to set
     */
//    public void setFamilySust(String familySust) {
//        this.familySust = familySust;
//    }
    /**
     * @param appl1Careof the appl1Careof to set
     */
    public void setAppl1Careof(String appl1Careof) {
        this.appl1Careof = appl1Careof;
    }

    /**
     * @param mobileChrgs the mobileChrgs to set
     */
//    public void setMobileChrgs(String mobileChrgs) {
//        this.mobileChrgs = mobileChrgs;
//    }
    /**
     * @param permHouseNo the permHouseNo to set
     */
    public void setPermHouseNo(String permHouseNo) {
        this.permHouseNo = permHouseNo;
    }

    /**
     * @param internetChrgs the internetChrgs to set
     */
//    public void setInternetChrgs(String internetChrgs) {
//        this.internetChrgs = internetChrgs;
//    }
    /**
     * @param permStreetNo the permStreetNo to set
     */
    public void setPermStreetNo(String permStreetNo) {
        this.permStreetNo = permStreetNo;
    }

    /**
     * @param televChrgs the televChrgs to set
     */
//    public void setTelevChrgs(String televChrgs) {
//        this.televChrgs = televChrgs;
//    }
    /**
     * @param permLandMark the permLandMark to set
     */
    public void setPermLandMark(String permLandMark) {
        this.permLandMark = permLandMark;
    }

    /**
     * @param fuelChrgs the fuelChrgs to set
     */
//    public void setFuelChrgs(String fuelChrgs) {
//        this.fuelChrgs = fuelChrgs;
//    }
    /**
     * @param permVillCity the permVillCity to set
     */
    public void setPermVillCity(String permVillCity) {
        this.permVillCity = permVillCity;
    }

    /**
     * @param electrBill the electrBill to set
     */
//    public void setElectrBill(String electrBill) {
//        this.electrBill = electrBill;
//    }
    /**
     * @param permDistrict the permDistrict to set
     */
    public void setPermDistrict(String permDistrict) {
        this.permDistrict = permDistrict;
    }

    /**
     * @param studyExp the studyExp to set
     */
//    public void setStudyExp(String studyExp) {
//        this.studyExp = studyExp;
//    }
    /**
     * @param permState the permState to set
     */
    public void setPermState(String permState) {
        this.permState = permState;
    }

    /**
     * @param blockCode the blockCode to set
     */
    public void setBlockCode(String blockCode) {
        this.blockCode = blockCode;
    }

    /**
     * @param insurancePrem the insurancePrem to set
     */
//    public void setInsurancePrem(String insurancePrem) {
//        this.insurancePrem = insurancePrem;
//    }
    /**
     * @param permPinCode the permPinCode to set
     */
    public void setPermPinCode(String permPinCode) {
        this.permPinCode = permPinCode;
    }

    /**
     * @param applicantAddress the applicantAddress to set
     */
    public void setApplicantAddress(String applicantAddress) {
        this.applicantAddress = applicantAddress;
    }

    /**
     * @param nomineeDetails the nomineeDetails to set
     */
    public void setNomineeDetails(String nomineeDetails) {
        this.nomineeDetails = nomineeDetails;
    }

    /**
     * @param srcOfIncomeCode the srcOfIncomeCode to set
     */
    public void setSrcOfIncomeCode(String srcOfIncomeCode) {
        this.srcOfIncomeCode = srcOfIncomeCode;
    }

    /**
     * @param networthCode the networthCode to set
     */
//    public void setNetworthCode(String networthCode) {
//        this.networthCode = networthCode;
//    }
    /**
     * @param annualTurnoverCode the annualTurnoverCode to set
     */
//    public void setAnnualTurnoverCode(String annualTurnoverCode) {
//        this.annualTurnoverCode = annualTurnoverCode;
//    }
    /**
     * @param maritalStatutCode the maritalStatutCode to set
     */
    public void setMaritalStatutCode(String maritalStatutCode) {
        this.maritalStatutCode = maritalStatutCode;
    }

    /**
     * @param glSubheadCode the glSubheadCode to set
     */
    public void setGlSubheadCode(String glSubheadCode) {
        this.glSubheadCode = glSubheadCode;
    }

    /**
     * @param constitutionCode the constitutionCode to set
     */
    public void setConstitutionCode(String constitutionCode) {
        this.constitutionCode = constitutionCode;
    }

    /**
     * @param casteCode the casteCode to set
     */
    public void setCasteCode(String casteCode) {
        this.casteCode = casteCode;
    }

    /**
     * @param communityCode the communityCode to set
     */
    public void setCommunityCode(String communityCode) {
        this.communityCode = communityCode;
    }

    /**
     * @param schemeCode the schemeCode to set
     */
    public void setSchemeCode(String schemeCode) {
        this.schemeCode = schemeCode;
    }

    /**
     * @param introducerStatusCode the introducerStatusCode to set
     */
    public void setIntroducerStatusCode(String introducerStatusCode) {
        this.introducerStatusCode = introducerStatusCode;
    }

    /**
     * @param appl1Address the appl1Address to set
     */
    public void setAppl1Address(String appl1Address) {
        this.appl1Address = appl1Address;
    }

    /**
     * @param panchTax the panchTax to set
     */
//    public void setPanchTax(String panchTax) {
//        this.panchTax = panchTax;
//    }
    /**
     * @param telephone the telephone to set
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * @param miscExp the miscExp to set
     */
//    public void setMiscExp(String miscExp) {
//        this.miscExp = miscExp;
//    }
    /**
     * @param mobileNo the mobileNo to set
     */
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    /**
     * @param totalyrlyfamExp the totalyrlyfamExp to set
     */
//    public void setTotalyrlyfamExp(String totalyrlyfamExp) {
//        this.totalyrlyfamExp = totalyrlyfamExp;
//    }
    /**
     * @param yrlySurplus the yrlySurplus to set
     */
    public void setYrlySurplus(String yrlySurplus) {
        this.yrlySurplus = yrlySurplus;
    }

    /**
     * @param idProof the idProof to set
     */
    public void setIdProof(String idProof) {
        this.idProof = idProof;
    }

    /**
     * @param idProofImage the idProofImage to set
     */
    public void setIdProofImage(String idProofImage) {
        this.idProofImage = idProofImage;
    }

    /**
     * @param avgWeeklySurplus the avgWeeklySurplus to set
     */
//    public void setAvgWeeklySurplus(String avgWeeklySurplus) {
//        this.avgWeeklySurplus = avgWeeklySurplus;
//    }
    /**
     * @param addressProof the addressProof to set
     */
    public void setAddressProof(String addressProof) {
        this.addressProof = addressProof;
    }

    /**
     * @param addressProofImage the addressProofImage to set
     */
    public void setAddressProofImage(String addressProofImage) {
        this.addressProofImage = addressProofImage;
    }

    /**
     * @param loanAmount the loanAmount to set
     */
    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    /**
     * @param pmsbyPmjjbyCovrg the pmsbyPmjjbyCovrg to set
     */
    public void setPmsbyPmjjbyCovrg(String pmsbyPmjjbyCovrg) {
        this.pmsbyPmjjbyCovrg = pmsbyPmjjbyCovrg;
    }

    /**
     * @param approachFor the approachFor to set
     */
    public void setApproachFor(String approachFor) {
        this.approachFor = approachFor;
    }

    /**
     * @param pmsbyPmjjbyValidity the pmsbyPmjjbyValidity to set
     */
    public void setPmsbyPmjjbyValidity(String pmsbyPmjjbyValidity) {
        this.pmsbyPmjjbyValidity = pmsbyPmjjbyValidity;
    }

    /**
     * @param presentFinancer the presentFinancer to set
     */
    public void setPresentFinancer(String presentFinancer) {
        this.presentFinancer = presentFinancer;
    }

    /**
     * @param appl1Occupation the appl1Occupation to set
     */
    public void setAppl1Occupation(String appl1Occupation) {
        this.appl1Occupation = appl1Occupation;
    }

    /**
     * @param repayPeriod the repayPeriod to set
     */
    public void setRepayPeriod(String repayPeriod) {
        this.repayPeriod = repayPeriod;
    }

    /**
     * @param apprxyrlyApp1Income the apprxyrlyApp1Income to set
     */
    public void setApprxyrlyApp1Income(String apprxyrlyApp1Income) {
        this.apprxyrlyApp1Income = apprxyrlyApp1Income;
    }

    /**
     * @param weeklyInstalment the weeklyInstalment to set
     */
    public void setWeeklyInstalment(String weeklyInstalment) {
        this.weeklyInstalment = weeklyInstalment;
    }

    /**
     * @param familyMembsOccupation the familyMembsOccupation to set
     */
    public void setFamilyMembsOccupation(String familyMembsOccupation) {
        this.familyMembsOccupation = familyMembsOccupation;
    }

    /**
     * @param bankMitraRemarks the bankMitraRemarks to set
     */
    public void setBankMitraRemarks(String bankMitraRemarks) {
        this.bankMitraRemarks = bankMitraRemarks;
    }

    /**
     * @param yrlyOthFamilyMembIncome the yrlyOthFamilyMembIncome to set
     */
    public void setYrlyOthFamilyMembIncome(String yrlyOthFamilyMembIncome) {
        this.yrlyOthFamilyMembIncome = yrlyOthFamilyMembIncome;
    }

    /**
     * @param bankManagRemarks the bankManagRemarks to set
     */
    public void setBankManagRemarks(String bankManagRemarks) {
        this.bankManagRemarks = bankManagRemarks;
    }

    /**
     * @param totalYrlyFamilyIncome the totalYrlyFamilyIncome to set
     */
    public void setTotalYrlyFamilyIncome(String totalYrlyFamilyIncome) {
        this.totalYrlyFamilyIncome = totalYrlyFamilyIncome;
    }

    /**
     * @param bcRecomendation the bcRecomendation to set
     */
    public void setBcRecomendation(String bcRecomendation) {
        this.bcRecomendation = bcRecomendation;
    }

    /**
     * @param bcRemarks the bcRemarks to set
     */
    public void setBcRemarks(String bcRemarks) {
        this.bcRemarks = bcRemarks;
    }

    /**
     * @param newSubkId the newSubkId to set
     */
    public void setNewSubkId(String newSubkId) {
        this.newSubkId = newSubkId;
    }

    /**
     * @param appl1Image the appl1Image to set
     */
    public void setAppl1Image(String appl1Image) {
        this.appl1Image = appl1Image;
    }

    /**
     * @param permHusFathName the permHusFathName to set
     */
    public void setPermHusFathName(String permHusFathName) {
        this.permHusFathName = permHusFathName;
    }

    /**
     * @param permRelType the permRelType to set
     */
    public void setPermRelType(String permRelType) {
        this.permRelType = permRelType;
    }

    /**
     * @param permTelephone the permTelephone to set
     */
    public void setPermTelephone(String permTelephone) {
        this.permTelephone = permTelephone;
    }

    /**
     * @param commHusFathName the commHusFathName to set
     */
    public void setCommHusFathName(String commHusFathName) {
        this.commHusFathName = commHusFathName;
    }

    /**
     * @param commRelationType the commRelationType to set
     */
    public void setCommRelationType(String commRelationType) {
        this.commRelationType = commRelationType;
    }

    /**
     * @param commHouseNo the commHouseNo to set
     */
    public void setCommHouseNo(String commHouseNo) {
        this.commHouseNo = commHouseNo;
    }

    /**
     * @param commStreetNo the commStreetNo to set
     */
    public void setCommStreetNo(String commStreetNo) {
        this.commStreetNo = commStreetNo;
    }

    /**
     * @param commLandMark the commLandMark to set
     */
    public void setCommLandMark(String commLandMark) {
        this.commLandMark = commLandMark;
    }

    /**
     * @param commVillCity the commVillCity to set
     */
    public void setCommVillCity(String commVillCity) {
        this.commVillCity = commVillCity;
    }

    /**
     * @param commDistrict the commDistrict to set
     */
    public void setCommDistrict(String commDistrict) {
        this.commDistrict = commDistrict;
    }

    /**
     * @param commState the commState to set
     */
    public void setCommState(String commState) {
        this.commState = commState;
    }

    /**
     * @param commPinCode the commPinCode to set
     */
    public void setCommPinCode(String commPinCode) {
        this.commPinCode = commPinCode;
    }

    /**
     * @param commTelephone the commTelephone to set
     */
    public void setCommTelephone(String commTelephone) {
        this.commTelephone = commTelephone;
    }

    /**
     * @param areaVillageCode the areaVillageCode to set
     */
    public void setAreaVillageCode(String areaVillageCode) {
        this.areaVillageCode = areaVillageCode;
    }

    /**
     * @param areaBlockCode the areaBlockCode to set
     */
    public void setAreaBlockCode(String areaBlockCode) {
        this.areaBlockCode = areaBlockCode;
    }

    /**
     * @param areaDistCode the areaDistCode to set
     */
    public void setAreaDistCode(String areaDistCode) {
        this.areaDistCode = areaDistCode;
    }

    /**
     * @param areaStateCode the areaStateCode to set
     */
    public void setAreaStateCode(String areaStateCode) {
        this.areaStateCode = areaStateCode;
    }

    /**
     * @param fhgType the fhgType to set
     */
    public void setFhgType(String fhgType) {
        this.fhgType = fhgType;
    }

    /**
     * @param fhgName the fhgName to set
     */
    public void setFhgName(String fhgName) {
        this.fhgName = fhgName;
    }

    /**
     * @param operationMode the operationMode to set
     */
    public void setOperationMode(String operationMode) {
        this.operationMode = operationMode;
    }

    /**
     * @param sex the sex to set
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * @param panGirNo the panGirNo to set
     */
    public void setPanGirNo(String panGirNo) {
        this.panGirNo = panGirNo;
    }

    /**
     * @param dobDate the dobDate to set
     */
    public void setDobDate(String dobDate) {
        this.dobDate = dobDate;
    }
//
//    /**
//     * @param annualIncome the annualIncome to set
//     */
//    public void setAnnualIncome(String annualIncome) {
//        this.annualIncome = annualIncome;
//    }

    /**
     * @param annualTurnover the annualTurnover to set
     */
    public void setAnnualTurnover(String annualTurnover) {
        this.annualTurnover = annualTurnover;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @param qualification the qualification to set
     */
    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    /**
     * @param eStatement the eStatement to set
     */
    public void seteStatement(String eStatement) {
        this.eStatement = eStatement;
    }

    /**
     * @param houseLoan the houseLoan to set
     */
    public void setHouseLoan(String houseLoan) {
        this.houseLoan = houseLoan;
    }

    /**
     * @param chequeBook the chequeBook to set
     */
    public void setChequeBook(String chequeBook) {
        this.chequeBook = chequeBook;
    }

    /**
     * @param vehicleLoan the vehicleLoan to set
     */
    public void setVehicleLoan(String vehicleLoan) {
        this.vehicleLoan = vehicleLoan;
    }

    /**
     * @param mobileBanking the mobileBanking to set
     */
    public void setMobileBanking(String mobileBanking) {
        this.mobileBanking = mobileBanking;
    }

    /**
     * @param mutualFund the mutualFund to set
     */
    public void setMutualFund(String mutualFund) {
        this.mutualFund = mutualFund;
    }

    /**
     * @param internetBanking the internetBanking to set
     */
    public void setInternetBanking(String internetBanking) {
        this.internetBanking = internetBanking;
    }

    /**
     * @param lifeInsurance the lifeInsurance to set
     */
    public void setLifeInsurance(String lifeInsurance) {
        this.lifeInsurance = lifeInsurance;
    }

    /**
     * @param creditCard the creditCard to set
     */
    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    /**
     * @param pension the pension to set
     */
    public void setPension(String pension) {
        this.pension = pension;
    }

    /**
     * @param addOnOthers the addOnOthers to set
     */
    public void setAddOnOthers(String addOnOthers) {
        this.addOnOthers = addOnOthers;
    }

    /**
     * @param crossSellOthers the crossSellOthers to set
     */
    public void setCrossSellOthers(String crossSellOthers) {
        this.crossSellOthers = crossSellOthers;
    }

    /**
     * @param atmRequest the atmRequest to set
     */
    public void setAtmRequest(String atmRequest) {
        this.atmRequest = atmRequest;
    }

    /**
     * @param smsAlert the smsAlert to set
     */
    public void setSmsAlert(String smsAlert) {
        this.smsAlert = smsAlert;
    }

    /**
     * @param emailId the emailId to set
     */
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    /**
     * @param idProofType the idProofType to set
     */
    public void setIdProofType(String idProofType) {
        this.idProofType = idProofType;
    }

    /**
     * @param addressProofType the addressProofType to set
     */
    public void setAddressProofType(String addressProofType) {
        this.addressProofType = addressProofType;
    }

    /**
     * @param introducerTitle the introducerTitle to set
     */
    public void setIntroducerTitle(String introducerTitle) {
        this.introducerTitle = introducerTitle;
    }

    /**
     * @param introducerName the introducerName to set
     */
    public void setIntroducerName(String introducerName) {
        this.introducerName = introducerName;
    }

    /**
     * @param introducerCustId the introducerCustId to set
     */
    public void setIntroducerCustId(String introducerCustId) {
        this.introducerCustId = introducerCustId;
    }

    /**
     * @param introducerAccNo the introducerAccNo to set
     */
    public void setIntroducerAccNo(String introducerAccNo) {
        this.introducerAccNo = introducerAccNo;
    }

    /**
     * @param introducerKnownYear the introducerKnownYear to set
     */
    public void setIntroducerKnownYear(String introducerKnownYear) {
        this.introducerKnownYear = introducerKnownYear;
    }

    /**
     * @param nomineeAccNature the nomineeAccNature to set
     */
    public void setNomineeAccNature(String nomineeAccNature) {
        this.nomineeAccNature = nomineeAccNature;
    }

    /**
     * @param nomineeAge the nomineeAge to set
     */
    public void setNomineeAge(String nomineeAge) {
        this.nomineeAge = nomineeAge;
    }

    /**
     * @param nomineeAccNo the nomineeAccNo to set
     */
    public void setNomineeAccNo(String nomineeAccNo) {
        this.nomineeAccNo = nomineeAccNo;
    }

    /**
     * @param nomineeDob the nomineeDob to set
     */
    public void setNomineeDob(String nomineeDob) {
        this.nomineeDob = nomineeDob;
    }

    /**
     * @param nomineeAdDetails the nomineeAdDetails to set
     */
    public void setNomineeAdDetails(String nomineeAdDetails) {
        this.nomineeAdDetails = nomineeAdDetails;
    }

    /**
     * @param nomineeGaurdianName the nomineeGaurdianName to set
     */
    public void setNomineeGaurdianName(String nomineeGaurdianName) {
        this.nomineeGaurdianName = nomineeGaurdianName;
    }

    /**
     * @param nomineeName the nomineeName to set
     */
    public void setNomineeName(String nomineeName) {
        this.nomineeName = nomineeName;
    }

    /**
     * @param nomineeAddress the nomineeAddress to set
     */
    public void setNomineeAddress(String nomineeAddress) {
        this.nomineeAddress = nomineeAddress;
    }

    /**
     * @param nomineeGuardianAge the nomineeGuardianAge to set
     */
    public void setNomineeGuardianAge(String nomineeGuardianAge) {
        this.nomineeGuardianAge = nomineeGuardianAge;
    }

    /**
     * @param nomineeRelation the nomineeRelation to set
     */
    public void setNomineeRelation(String nomineeRelation) {
        this.nomineeRelation = nomineeRelation;
    }

    /**
     * @param nomineeGuardianAdd the nomineeGuardianAdd to set
     */
    public void setNomineeGuardianAdd(String nomineeGuardianAdd) {
        this.nomineeGuardianAdd = nomineeGuardianAdd;
    }

    /**
     * @param minorNomiGuarTitle the minorNomiGuarTitle to set
     */
    public void setMinorNomiGuarTitle(String minorNomiGuarTitle) {
        this.minorNomiGuarTitle = minorNomiGuarTitle;
    }

    /**
     * @param minorNomiGuarName the minorNomiGuarName to set
     */
    public void setMinorNomiGuarName(String minorNomiGuarName) {
        this.minorNomiGuarName = minorNomiGuarName;
    }

    /**
     * @param minorNomiGuarAge the minorNomiGuarAge to set
     */
    public void setMinorNomiGuarAge(String minorNomiGuarAge) {
        this.minorNomiGuarAge = minorNomiGuarAge;
    }

    /**
     * @param minorNomiGuarRelType the minorNomiGuarRelType to set
     */
    public void setMinorNomiGuarRelType(String minorNomiGuarRelType) {
        this.minorNomiGuarRelType = minorNomiGuarRelType;
    }

    /**
     * @param minorNomiGuarRelCode the minorNomiGuarRelCode to set
     */
    public void setMinorNomiGuarRelCode(String minorNomiGuarRelCode) {
        this.minorNomiGuarRelCode = minorNomiGuarRelCode;
    }

    /**
     * @param witness1Name the witness1Name to set
     */
    public void setWitness1Name(String witness1Name) {
        this.witness1Name = witness1Name;
    }

    /**
     * @param witness2Name the witness2Name to set
     */
    public void setWitness2Name(String witness2Name) {
        this.witness2Name = witness2Name;
    }

    /**
     * @param witness1Add the witness1Add to set
     */
    public void setWitness1Add(String witness1Add) {
        this.witness1Add = witness1Add;
    }

    /**
     * @param witness2Add the witness2Add to set
     */
    public void setWitness2Add(String witness2Add) {
        this.witness2Add = witness2Add;
    }

    /**
     * @param witness1Place the witness1Place to set
     */
    public void setWitness1Place(String witness1Place) {
        this.witness1Place = witness1Place;
    }

    /**
     * @param witness2Place the witness2Place to set
     */
    public void setWitness2Place(String witness2Place) {
        this.witness2Place = witness2Place;
    }

    /**
     * @param witness1Telephone the witness1Telephone to set
     */
    public void setWitness1Telephone(String witness1Telephone) {
        this.witness1Telephone = witness1Telephone;
    }

    /**
     * @param witness2Telephone the witness2Telephone to set
     */
    public void setWitness2Telephone(String witness2Telephone) {
        this.witness2Telephone = witness2Telephone;
    }

    /**
     * @param appl2Name the appl2Name to set
     */
    public void setAppl2Name(String appl2Name) {
        this.appl2Name = appl2Name;
    }

    /**
     * @param appl2Pincode the appl2Pincode to set
     */
    public void setAppl2Pincode(String appl2Pincode) {
        this.appl2Pincode = appl2Pincode;
    }

    /**
     * @param appl2FhgName the appl2FhgName to set
     */
    public void setAppl2FhgName(String appl2FhgName) {
        this.appl2FhgName = appl2FhgName;
    }

    /**
     * @param appl2Telephone the appl2Telephone to set
     */
    public void setAppl2Telephone(String appl2Telephone) {
        this.appl2Telephone = appl2Telephone;
    }

    /**
     * @param appl2Careof the appl2Careof to set
     */
    public void setAppl2Careof(String appl2Careof) {
        this.appl2Careof = appl2Careof;
    }

    /**
     * @param appl2Mobile the appl2Mobile to set
     */
    public void setAppl2Mobile(String appl2Mobile) {
        this.appl2Mobile = appl2Mobile;
    }

    /**
     * @param appl2HouseNo the appl2HouseNo to set
     */
    public void setAppl2HouseNo(String appl2HouseNo) {
        this.appl2HouseNo = appl2HouseNo;
    }

    /**
     * @param appl2Sex the appl2Sex to set
     */
    public void setAppl2Sex(String appl2Sex) {
        this.appl2Sex = appl2Sex;
    }

    /**
     * @param appl2StreetNo the appl2StreetNo to set
     */
    public void setAppl2StreetNo(String appl2StreetNo) {
        this.appl2StreetNo = appl2StreetNo;
    }

    /**
     * @param appl2Dob the appl2Dob to set
     */
    public void setAppl2Dob(String appl2Dob) {
        this.appl2Dob = appl2Dob;
    }

    /**
     * @param appl2Landmark the appl2Landmark to set
     */
    public void setAppl2Landmark(String appl2Landmark) {
        this.appl2Landmark = appl2Landmark;
    }

    /**
     * @param appl2Occupation the appl2Occupation to set
     */
    public void setAppl2Occupation(String appl2Occupation) {
        this.appl2Occupation = appl2Occupation;
    }

    /**
     * @param appl2VillCity the appl2VillCity to set
     */
    public void setAppl2VillCity(String appl2VillCity) {
        this.appl2VillCity = appl2VillCity;
    }

    /**
     * @param appl2Category the appl2Category to set
     */
    public void setAppl2Category(String appl2Category) {
        this.appl2Category = appl2Category;
    }

    /**
     * @param appl2District the appl2District to set
     */
    public void setAppl2District(String appl2District) {
        this.appl2District = appl2District;
    }

    /**
     * @param appl2IdProof the appl2IdProof to set
     */
    public void setAppl2IdProof(String appl2IdProof) {
        this.appl2IdProof = appl2IdProof;
    }

    /**
     * @param appl2State the appl2State to set
     */
    public void setAppl2State(String appl2State) {
        this.appl2State = appl2State;
    }

    /**
     * @param appl2AddProof the appl2AddProof to set
     */
    public void setAppl2AddProof(String appl2AddProof) {
        this.appl2AddProof = appl2AddProof;
    }

    /**
     * @param rightThumb the rightThumb to set
     */
    public void setRightThumb(String rightThumb) {
        this.rightThumb = rightThumb;
    }

    /**
     * @param rightIndex the rightIndex to set
     */
    public void setRightIndex(String rightIndex) {
        this.rightIndex = rightIndex;
    }

    /**
     * @param rightMiddle the rightMiddle to set
     */
    public void setRightMiddle(String rightMiddle) {
        this.rightMiddle = rightMiddle;
    }

    /**
     * @param rightRing the rightRing to set
     */
    public void setRightRing(String rightRing) {
        this.rightRing = rightRing;
    }

    /**
     * @param rightSmall the rightSmall to set
     */
    public void setRightSmall(String rightSmall) {
        this.rightSmall = rightSmall;
    }

    /**
     * @param leftThumb the leftThumb to set
     */
    public void setLeftThumb(String leftThumb) {
        this.leftThumb = leftThumb;
    }

    /**
     * @param leftIndex the leftIndex to set
     */
    public void setLeftIndex(String leftIndex) {
        this.leftIndex = leftIndex;
    }

    /**
     * @param leftMiddle the leftMiddle to set
     */
    public void setLeftMiddle(String leftMiddle) {
        this.leftMiddle = leftMiddle;
    }

    /**
     * @param leftRing the leftRing to set
     */
    public void setLeftRing(String leftRing) {
        this.leftRing = leftRing;
    }

    /**
     * @param leftSmall the leftSmall to set
     */
    public void setLeftSmall(String leftSmall) {
        this.leftSmall = leftSmall;
    }

    /**
     * @param custPin the custPin to set
     */
    public void setCustPin(String custPin) {
        this.custPin = custPin;
    }

    /**
     * @param confirmCustPin the confirmCustPin to set
     */
    public void setConfirmCustPin(String confirmCustPin) {
        this.confirmCustPin = confirmCustPin;
    }

    /**
     * @param familyMemb1Name the familyMemb1Name to set
     */
    public void setFamilyMemb1Name(String familyMemb1Name) {
        this.familyMemb1Name = familyMemb1Name;
    }

    /**
     * @param familyMemb1Relation the familyMemb1Relation to set
     */
    public void setFamilyMemb1Relation(String familyMemb1Relation) {
        this.familyMemb1Relation = familyMemb1Relation;
    }

    /**
     * @param familyMemb1Sex the familyMemb1Sex to set
     */
    public void setFamilyMemb1Sex(String familyMemb1Sex) {
        this.familyMemb1Sex = familyMemb1Sex;
    }

    /**
     * @param familyMemb1Age the familyMemb1Age to set
     */
    public void setFamilyMemb1Age(String familyMemb1Age) {
        this.familyMemb1Age = familyMemb1Age;
    }

    /**
     * @param familyMemb1Occup the familyMemb1Occup to set
     */
    public void setFamilyMemb1Occup(String familyMemb1Occup) {
        this.familyMemb1Occup = familyMemb1Occup;
    }

    /**
     * @param familyMemb1Qualification the familyMemb1Qualification to set
     */
    public void setFamilyMemb1Qualification(String familyMemb1Qualification) {
        this.familyMemb1Qualification = familyMemb1Qualification;
    }

    /**
     * @param familyMemb2Name the familyMemb2Name to set
     */
    public void setFamilyMemb2Name(String familyMemb2Name) {
        this.familyMemb2Name = familyMemb2Name;
    }

    /**
     * @param familyMemb2Relation the familyMemb2Relation to set
     */
    public void setFamilyMemb2Relation(String familyMemb2Relation) {
        this.familyMemb2Relation = familyMemb2Relation;
    }

    /**
     * @param familyMemb2Sex the familyMemb2Sex to set
     */
    public void setFamilyMemb2Sex(String familyMemb2Sex) {
        this.familyMemb2Sex = familyMemb2Sex;
    }

    /**
     * @param familyMemb2Age the familyMemb2Age to set
     */
    public void setFamilyMemb2Age(String familyMemb2Age) {
        this.familyMemb2Age = familyMemb2Age;
    }

    /**
     * @param familyMemb2Occup the familyMemb2Occup to set
     */
    public void setFamilyMemb2Occup(String familyMemb2Occup) {
        this.familyMemb2Occup = familyMemb2Occup;
    }

    /**
     * @param familyMemb2Qualification the familyMemb2Qualification to set
     */
    public void setFamilyMemb2Qualification(String familyMemb2Qualification) {
        this.familyMemb2Qualification = familyMemb2Qualification;
    }

    /**
     * @param familyMemb3Name the familyMemb3Name to set
     */
    public void setFamilyMemb3Name(String familyMemb3Name) {
        this.familyMemb3Name = familyMemb3Name;
    }

    /**
     * @param familyMemb3Relation the familyMemb3Relation to set
     */
    public void setFamilyMemb3Relation(String familyMemb3Relation) {
        this.familyMemb3Relation = familyMemb3Relation;
    }

    /**
     * @param familyMemb3Sex the familyMemb3Sex to set
     */
    public void setFamilyMemb3Sex(String familyMemb3Sex) {
        this.familyMemb3Sex = familyMemb3Sex;
    }

    /**
     * @param familyMemb3Age the familyMemb3Age to set
     */
    public void setFamilyMemb3Age(String familyMemb3Age) {
        this.familyMemb3Age = familyMemb3Age;
    }

    /**
     * @param familyMemb3Occup the familyMemb3Occup to set
     */
    public void setFamilyMemb3Occup(String familyMemb3Occup) {
        this.familyMemb3Occup = familyMemb3Occup;
    }

    /**
     * @param familyMemb3Qualification the familyMemb3Qualification to set
     */
    public void setFamilyMemb3Qualification(String familyMemb3Qualification) {
        this.familyMemb3Qualification = familyMemb3Qualification;
    }

    /**
     * @param loanAmountDue the loanAmountDue to set
     */
    public void setLoanAmountDue(String loanAmountDue) {
        this.loanAmountDue = loanAmountDue;
    }

    /**
     * @param liveStock the liveStock to set
     */
    public void setLiveStock(String liveStock) {
        this.liveStock = liveStock;
    }

    /**
     * @param agrlImpl the agrlImpl to set
     */
    public void setAgrlImpl(String agrlImpl) {
        this.agrlImpl = agrlImpl;
    }

    /**
     * @param activity the activity to set
     */
    public void setActivity(String activity) {
        this.activity = activity;
    }

    /**
     * @param assetVehicle the assetVehicle to set
     */
    public void setAssetVehicle(String assetVehicle) {
        this.assetVehicle = assetVehicle;
    }

    /**
     * @param loanReuired the loanReuired to set
     */
    public void setLoanReuired(String loanReuired) {
        this.loanReuired = loanReuired;
    }

    /**
     * @param assetAnyOther the assetAnyOther to set
     */
    public void setAssetAnyOther(String assetAnyOther) {
        this.assetAnyOther = assetAnyOther;
    }

    /**
     * @param loanAccNomineeName the loanAccNomineeName to set
     */
    public void setLoanAccNomineeName(String loanAccNomineeName) {
        this.loanAccNomineeName = loanAccNomineeName;
    }

    /**
     * @param loanSource the loanSource to set
     */
    public void setLoanSource(String loanSource) {
        this.loanSource = loanSource;
    }

    /**
     * @param loanAccNomineeRelation the loanAccNomineeRelation to set
     */
    public void setLoanAccNomineeRelation(String loanAccNomineeRelation) {
        this.loanAccNomineeRelation = loanAccNomineeRelation;
    }

    /**
     * @param loanPurpose the loanPurpose to set
     */
    public void setLoanPurpose(String loanPurpose) {
        this.loanPurpose = loanPurpose;
    }

    /**
     * @param loanAccNomineeAge the loanAccNomineeAge to set
     */
    public void setLoanAccNomineeAge(String loanAccNomineeAge) {
        this.loanAccNomineeAge = loanAccNomineeAge;
    }

    /**
     * @param amountBorrowed the amountBorrowed to set
     */
    public void setAmountBorrowed(String amountBorrowed) {
        this.amountBorrowed = amountBorrowed;
    }

    /**
     * @param loanAccNomineeResident the loanAccNomineeResident to set
     */
    public void setLoanAccNomineeResident(String loanAccNomineeResident) {
        this.loanAccNomineeResident = loanAccNomineeResident;
    }

    /**
     * @param isFPCaptured the isFPCaptured to set
     */
    public void setIsFPCaptured(String isFPCaptured) {
        this.isFPCaptured = isFPCaptured;
    }

    /**
     * @return the NomineeCity
     */
    public String getNomineeCity() {
        return NomineeCity;
    }

    /**
     * @param NomineeCity the NomineeCity to set
     */
    public void setNomineeCity(String NomineeCity) {
        this.NomineeCity = NomineeCity;
    }

    /**
     * @return the solId
     */
    public String getSolId() {
        return solId;
    }

    /**
     * @param solId the solId to set
     */
    public void setSolId(String solId) {
        this.solId = solId;
    }

    /**
     * @return the activityCode
     */
    public String getActivityCode() {
        return activityCode;
    }

    /**
     * @param activityCode the activityCode to set
     */
    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    /**
     * @return the Education
     */
    public String getEducation() {
        return Education;
    }

    /**
     * @param Education the Education to set
     */
    public void setEducation(String Education) {
        this.Education = Education;
    }

    /**
     * @return the runningSince
     */
    public String getRunningSince() {
        return runningSince;
    }

    /**
     * @param runningSince the runningSince to set
     */
    public void setRunningSince(String runningSince) {
        this.runningSince = runningSince;
    }

    /**
     * @return the yearsofExp
     */
    public String getYearsofExp() {
        return yearsofExp;
    }

    /**
     * @param yearsofExp the yearsofExp to set
     */
    public void setYearsofExp(String yearsofExp) {
        this.yearsofExp = yearsofExp;
    }

    public String getUH_Recommended_LoanAmount() {
        return UH_Recommended_LoanAmount;
    }

    public void setUH_Recommended_LoanAmount(String UH_Recommended_LoanAmount) {
        this.UH_Recommended_LoanAmount = UH_Recommended_LoanAmount;
    }

    public String getUH_Recommended_InstalmentAmount() {
        return UH_Recommended_InstalmentAmount;
    }

    public void setUH_Recommended_InstalmentAmount(String UH_Recommended_InstalmentAmount) {
        this.UH_Recommended_InstalmentAmount = UH_Recommended_InstalmentAmount;
    }

    public String getUH_Recommended_NoOfMonths() {
        return UH_Recommended_NoOfMonths;
    }

    public void setUH_Recommended_NoOfMonths(String UH_Recommended_NoOfMonths) {
        this.UH_Recommended_NoOfMonths = UH_Recommended_NoOfMonths;
    }

    public String getMclr() {
        return mclr;
    }

    public void setMclr(String mclr) {
        this.mclr = mclr;
    }

    public String getMclr_male_female() {
        return mclr_male_female;
    }

    public void setMclr_male_female(String mclr_male_female) {
        this.mclr_male_female = mclr_male_female;
    }

    public String getCommVillage() {
        return commVillage;
    }

    public void setCommVillage(String commVillage) {
        this.commVillage = commVillage;
    }

    @Override
    public String toString() {
        return "UbiLoanOrigination{" + "branchName=" + branchName + ", transType=" + transType + ", applicationId=" + applicationId + ", bcVerificationStatus=" + bcVerificationStatus + ", bcUpdatedDatetime=" + bcUpdatedDatetime + ", fxVerificationStatus=" + fxVerificationStatus + ", fxRemarks=" + fxRemarks + ", fxUpdatedDatetime=" + fxUpdatedDatetime + ", teVerificationStatus=" + teVerificationStatus + ", teRemarks=" + teRemarks + ", teUpdatedDatetime=" + teUpdatedDatetime + ", uhVerificationStatus=" + uhVerificationStatus + ", uhRemarks=" + uhRemarks + ", uhUpdatedDatetime=" + uhUpdatedDatetime + ", bankVerificationStatus=" + bankVerificationStatus + ", bankRemarks=" + bankRemarks + ", bankUpdatedDatetime=" + bankUpdatedDatetime + ", dbAction=" + dbAction + ", applicationNo=" + applicationNo + ", groupId=" + groupId + ", groupName=" + groupName + ", custInfo=" + custInfo + ", newCust=" + newCust + ", subkId=" + subkId + ", sbAccNo=" + sbAccNo + ", custId=" + custId + ", BcoMobile=" + BcoMobile + ", BcoPin=" + BcoPin + ", savingACInfo=" + savingACInfo + ", bcoId=" + bcoId + ", bcoName=" + bcoName + ", custTitle=" + custTitle + ", custTitleCode=" + custTitleCode + ", appl1Name=" + appl1Name + ", custTypeCode=" + custTypeCode + ", familyExp=" + familyExp + ", jlgGroupName=" + jlgGroupName + ", jlgLeader=" + jlgLeader + ", appl1Careof=" + appl1Careof + ", permHouseNo=" + permHouseNo + ", permStreetNo=" + permStreetNo + ", permLandMark=" + permLandMark + ", permVillCity=" + permVillCity + ", permDistrict=" + permDistrict + ", permState=" + permState + ", blockCode=" + blockCode + ", permPinCode=" + permPinCode + ", applicantAddress=" + applicantAddress + ", nomineeDetails=" + nomineeDetails + ", srcOfIncomeCode=" + srcOfIncomeCode + ", maritalStatutCode=" + maritalStatutCode + ", glSubheadCode=" + glSubheadCode + ", constitutionCode=" + constitutionCode + ", casteCode=" + casteCode + ", communityCode=" + communityCode + ", schemeCode=" + schemeCode + ", introducerStatusCode=" + introducerStatusCode + ", appl1Address=" + appl1Address + ", telephone=" + telephone + ", mobileNo=" + mobileNo + ", yrlySurplus=" + yrlySurplus + ", idProof=" + idProof + ", idProofImage=" + idProofImage + ", addressProof=" + addressProof + ", addressProofImage=" + addressProofImage + ", loanAmount=" + loanAmount + ", pmsbyPmjjbyCovrg=" + pmsbyPmjjbyCovrg + ", approachFor=" + approachFor + ", pmsbyPmjjbyValidity=" + pmsbyPmjjbyValidity + ", presentFinancer=" + presentFinancer + ", appl1Occupation=" + appl1Occupation + ", repayPeriod=" + repayPeriod + ", apprxyrlyApp1Income=" + apprxyrlyApp1Income + ", weeklyInstalment=" + weeklyInstalment + ", familyMembsOccupation=" + familyMembsOccupation + ", bankMitraRemarks=" + bankMitraRemarks + ", yrlyOthFamilyMembIncome=" + yrlyOthFamilyMembIncome + ", bankManagRemarks=" + bankManagRemarks + ", totalYrlyFamilyIncome=" + totalYrlyFamilyIncome + ", runningSince=" + runningSince + ", yearsofExp=" + yearsofExp + ", bcRecomendation=" + bcRecomendation + ", bcRemarks=" + bcRemarks + ", newSubkId=" + newSubkId + ", appl1Image=" + appl1Image + ", permHusFathName=" + permHusFathName + ", permRelType=" + permRelType + ", permTelephone=" + permTelephone + ", commHusFathName=" + commHusFathName + ", commRelationType=" + commRelationType + ", commHouseNo=" + commHouseNo + ", commStreetNo=" + commStreetNo + ", commLandMark=" + commLandMark + ", commVillCity=" + commVillCity + ", commDistrict=" + commDistrict + ", commState=" + commState + ", commPinCode=" + commPinCode + ", commTelephone=" + commTelephone + ", areaVillageCode=" + areaVillageCode + ", areaBlockCode=" + areaBlockCode + ", areaDistCode=" + areaDistCode + ", areaStateCode=" + areaStateCode + ", fhgType=" + fhgType + ", fhgName=" + fhgName + ", operationMode=" + operationMode + ", sex=" + sex + ", panGirNo=" + panGirNo + ", dobDate=" + dobDate + ", commVillage=" + commVillage + ", annualTurnover=" + annualTurnover + ", category=" + category + ", qualification=" + qualification + ", eStatement=" + eStatement + ", houseLoan=" + houseLoan + ", chequeBook=" + chequeBook + ", vehicleLoan=" + vehicleLoan + ", mobileBanking=" + mobileBanking + ", mutualFund=" + mutualFund + ", internetBanking=" + internetBanking + ", lifeInsurance=" + lifeInsurance + ", creditCard=" + creditCard + ", pension=" + pension + ", addOnOthers=" + addOnOthers + ", crossSellOthers=" + crossSellOthers + ", atmRequest=" + atmRequest + ", smsAlert=" + smsAlert + ", emailId=" + emailId + ", idProofType=" + idProofType + ", addressProofType=" + addressProofType + ", introducerTitle=" + introducerTitle + ", introducerName=" + introducerName + ", introducerCustId=" + introducerCustId + ", introducerAccNo=" + introducerAccNo + ", introducerKnownYear=" + introducerKnownYear + ", nomineeAccNature=" + nomineeAccNature + ", nomineeAge=" + nomineeAge + ", nomineeAccNo=" + nomineeAccNo + ", nomineeDob=" + nomineeDob + ", nomineeAdDetails=" + nomineeAdDetails + ", nomineeGaurdianName=" + nomineeGaurdianName + ", nomineeName=" + nomineeName + ", nomineeAddress=" + nomineeAddress + ", nomineeGuardianAge=" + nomineeGuardianAge + ", nomineeRelation=" + nomineeRelation + ", nomineeGuardianAdd=" + nomineeGuardianAdd + ", minorNomiGuarTitle=" + minorNomiGuarTitle + ", minorNomiGuarName=" + minorNomiGuarName + ", minorNomiGuarAge=" + minorNomiGuarAge + ", minorNomiGuarRelType=" + minorNomiGuarRelType + ", minorNomiGuarRelCode=" + minorNomiGuarRelCode + ", NomineeCity=" + NomineeCity + ", witness1Name=" + witness1Name + ", witness2Name=" + witness2Name + ", witness1Add=" + witness1Add + ", witness2Add=" + witness2Add + ", witness1Place=" + witness1Place + ", witness2Place=" + witness2Place + ", witness1Telephone=" + witness1Telephone + ", witness2Telephone=" + witness2Telephone + ", appl2Name=" + appl2Name + ", appl2Pincode=" + appl2Pincode + ", appl2FhgName=" + appl2FhgName + ", appl2Telephone=" + appl2Telephone + ", appl2Careof=" + appl2Careof + ", appl2Mobile=" + appl2Mobile + ", appl2HouseNo=" + appl2HouseNo + ", appl2Sex=" + appl2Sex + ", appl2StreetNo=" + appl2StreetNo + ", appl2Dob=" + appl2Dob + ", appl2Landmark=" + appl2Landmark + ", appl2Occupation=" + appl2Occupation + ", appl2VillCity=" + appl2VillCity + ", appl2Category=" + appl2Category + ", appl2District=" + appl2District + ", appl2IdProof=" + appl2IdProof + ", appl2State=" + appl2State + ", appl2AddProof=" + appl2AddProof + ", rightThumb=" + rightThumb + ", rightIndex=" + rightIndex + ", rightMiddle=" + rightMiddle + ", rightRing=" + rightRing + ", rightSmall=" + rightSmall + ", leftThumb=" + leftThumb + ", leftIndex=" + leftIndex + ", leftMiddle=" + leftMiddle + ", leftRing=" + leftRing + ", leftSmall=" + leftSmall + ", custPin=" + custPin + ", confirmCustPin=" + confirmCustPin + ", familyMemb1Name=" + familyMemb1Name + ", familyMemb1Relation=" + familyMemb1Relation + ", familyMemb1Sex=" + familyMemb1Sex + ", familyMemb1Age=" + familyMemb1Age + ", familyMemb1Occup=" + familyMemb1Occup + ", familyMemb1Qualification=" + familyMemb1Qualification + ", familyMemb2Name=" + familyMemb2Name + ", familyMemb2Relation=" + familyMemb2Relation + ", familyMemb2Sex=" + familyMemb2Sex + ", familyMemb2Age=" + familyMemb2Age + ", familyMemb2Occup=" + familyMemb2Occup + ", familyMemb2Qualification=" + familyMemb2Qualification + ", familyMemb3Name=" + familyMemb3Name + ", familyMemb3Relation=" + familyMemb3Relation + ", familyMemb3Sex=" + familyMemb3Sex + ", familyMemb3Age=" + familyMemb3Age + ", familyMemb3Occup=" + familyMemb3Occup + ", familyMemb3Qualification=" + familyMemb3Qualification + ", loanAmountDue=" + loanAmountDue + ", liveStock=" + liveStock + ", agrlImpl=" + agrlImpl + ", activity=" + activity + ", assetVehicle=" + assetVehicle + ", loanReuired=" + loanReuired + ", assetAnyOther=" + assetAnyOther + ", loanAccNomineeName=" + loanAccNomineeName + ", loanSource=" + loanSource + ", loanAccNomineeRelation=" + loanAccNomineeRelation + ", loanPurpose=" + loanPurpose + ", loanAccNomineeAge=" + loanAccNomineeAge + ", amountBorrowed=" + amountBorrowed + ", loanAccNomineeResident=" + loanAccNomineeResident + ", isFPCaptured=" + isFPCaptured + ", solId=" + solId + ", loanAmountSanctioned=" + loanAmountSanctioned + ", noOfInstalments=" + noOfInstalments + ", interestRate=" + interestRate + ", instalmentAmount=" + instalmentAmount + ", activityCode=" + activityCode + ", Education=" + Education + ", UH_Recommended_LoanAmount=" + UH_Recommended_LoanAmount + ", UH_Recommended_InstalmentAmount=" + UH_Recommended_InstalmentAmount + ", UH_Recommended_NoOfMonths=" + UH_Recommended_NoOfMonths + ", mclrl=" + mclrl + ", mclrMale=" + mclrMale + ", mclrFemale=" + mclrFemale + ", mclr=" + mclr + ", mclr_male_female=" + mclr_male_female + ", applicantphotopath=" + applicantphotopath + ", ekyc_ApplicantPhoto=" + ekyc_ApplicantPhoto + '}';
    }

  
    
    
}
