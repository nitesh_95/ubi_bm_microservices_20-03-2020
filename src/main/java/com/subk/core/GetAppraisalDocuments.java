package com.subk.core;

import java.io.File;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.subk.entity.Loanorigination;

@Configuration
public class GetAppraisalDocuments {

	@Value("${MCLRL}")
	private String mclrl;
	@Value("${applicantimagepath}")
	private String pathAdd;
//	private static final ResourceBundle rb = ResourceBundle.getBundle("application.properties");

	public String getFXAppraisalPDF(Loanorigination loanData, String pathAdd) {
		String rootdirectory = pathAdd;
		System.out.println("RootDirectory: " + rootdirectory);
		String filePath = null;
		File soliddir = new File(rootdirectory + File.separator + loanData.getSolid());
		System.out.println("Dir:*****: " + soliddir);
		if (soliddir.exists()) {
			System.out.println("inside if..........");
			File applidir = new File(soliddir + File.separator + loanData.getApplicationid());
			if (applidir.exists()) {
				System.out.println("appliDir path:*****: " + soliddir);
				File appliFxappraisalpdf = new File(applidir + File.separator + "FxAppraisal.pdf");
				if (appliFxappraisalpdf.exists()) {
					System.out.println("appliFxappraisal:*******: " + appliFxappraisalpdf);
					filePath = rootdirectory + File.separator + loanData.getSolid() + File.separator
							+ loanData.getApplicationid() + File.separator + "FxAppraisal.pdf";
				} else {
					System.out.println("Else block.................");
					filePath = null;
					return filePath;
				}
			} else {
				System.out.println("inside else............");
				filePath = null;
				return filePath;
			}
		} else {
			System.out.println("inside else............");
			filePath = null;
			return filePath;
		}
		return filePath;
	}

	public String getKYCDOCPDF(Loanorigination loanData, String pathAdd) {
		String rootdirectory = pathAdd;
		String filePath = null;
		File soliddir = new File(rootdirectory + "\\" + loanData.getSolid());
		if (soliddir.exists()) {
			File applidir = new File(soliddir + "\\" + loanData.getApplicationid());
			if (applidir.exists()) {
				File appliFxappraisalpdf = new File(applidir + "\\ApplicantKYCDoc.pdf");

				if (appliFxappraisalpdf.exists()) {
					
					
					filePath = rootdirectory + "\\" + loanData.getSolid() + "\\" + loanData.getApplicationid()
							+ "\\ApplicantKYCDoc.pdf";
					System.out.println(filePath);
				} else {
					filePath = null;
					return filePath;
				}
			} else {
				filePath = null;
				return filePath;
			}
		} else {
			filePath = null;
			return filePath;
		}
		return filePath;
	}
public String getFXAppraisalPDFs(Loanorigination loanData, String pathAdd) {
	String rootdirectory = pathAdd;
	System.out.println("RootDirectory: " + rootdirectory);
	String filePath = null;
	File soliddir = new File(rootdirectory + File.separator + loanData.getSolid());
	System.out.println("Dir:*****: " + soliddir);
	if (soliddir.exists()) {
		System.out.println("inside if..........");
		File applidir = new File(soliddir + File.separator + loanData.getApplicationid());
		if (applidir.exists()) {
			System.out.println("appliDir path:*****: " + soliddir);
			File appliFxappraisalpdf = new File(applidir + File.separator + "ApplicantKYCDoc.pdf");
			if (appliFxappraisalpdf.exists()) {
				System.out.println("ApplicantKYCDoc:*******: " + appliFxappraisalpdf);
				filePath = rootdirectory + File.separator + loanData.getSolid() + File.separator
						+ loanData.getApplicationid() + File.separator + "ApplicantKYCDoc.pdf";
			} else {
				System.out.println("Else block.................");
				filePath = null;
				return filePath;
			}
		} else {
			System.out.println("inside else............");
			filePath = null;
			return filePath;
		}
	} else {
		System.out.println("inside else............");
		filePath = null;
		return filePath;
	}
	return filePath;
}
}

