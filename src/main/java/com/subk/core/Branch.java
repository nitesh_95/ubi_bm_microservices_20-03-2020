/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.core;

/**
 *
 * @author Murali
 */
public class Branch {
    private String solId;
    private String branchName;
    private String status;
    private String userId;
    private String password;

    /**
     * @return the solId
     */
    public String getSolId() {
        return solId;
    }

    /**
     * @param solId the solId to set
     */
    public void setSolId(String solId) {
        this.solId = solId;
    }

    /**
     * @return the branchName
     */
    public String getBranchName() {
        return branchName;
    }

    /**
     * @param branchName the branchName to set
     */
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
}
