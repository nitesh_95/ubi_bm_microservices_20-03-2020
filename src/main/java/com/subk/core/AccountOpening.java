
package com.subk.core;

import java.io.FileOutputStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.SpecialSymbol;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.subk.entity.Loanorigination;

@Configuration
public class AccountOpening extends PDFGenerationAbstract {

	@Value("${MCLRL}")
	private String mclrl;

//    public static final Logger logger = Logger.getLogger(GeneratePDFFile.class);
//    public static final ResourceBundle rb = ResourceBundle.getBundle("systemPath");
	Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
	Font redFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD, BaseColor.WHITE);
	Font redFont2 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);

	@Override
	public String getPdf(Loanorigination loanData, String fileName, String pathAdd, String img, String jsPaths) {
		logger.info("************ ENTERED ************* ");

		String destPath = null;

		logger.debug("UBILoanOrigination Data: " + loanData.toString());
		logger.debug("FileName: " + fileName);
		try {
			destPath = pathAdd;
			destPath = destPath + fileName;
			new AccountOpening().createPdf(destPath, loanData, img, jsPaths);
		} catch (Exception ex) {
			logger.error("Error:....!!! " + ex.getMessage());
		}

		logger.info("DESTINATION PATH:...........!!!!!!!!!!  " + destPath);
		logger.info("************ ENDED.************* ");
		return destPath;
	}

	@Override
	public void createPdf(String dest, Loanorigination loanData, String img, String jsPaths) {
		logger.debug("***********Entered***********");
		final String imgPath = img;
		Document document = null;
		PdfWriter writer = null;
		try {
			document = new Document();
			writer = PdfWriter.getInstance(document, new FileOutputStream(dest, false));
			document.open();
			Font canFont = new Font(Font.FontFamily.TIMES_ROMAN, 15, Font.BOLD);
			Font canFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
			Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
			Font subFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
			Font subFont5 = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);
			Font subFont2 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD, BaseColor.WHITE);
			Font subFont3 = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD, BaseColor.WHITE);
			Font subFont4 = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
			Image img1 = Image.getInstance(imgPath);
			img1.setAbsolutePosition(50f, 760f);
			img1.scaleAbsolute(50f, 50f);
			document.add(img1);
			Paragraph p1 = new Paragraph("UNITED BANK OF INDIA", canFont);
			p1.setAlignment(Element.ALIGN_CENTER);
			document.add(p1);
			Paragraph p2 = new Paragraph("ACCOUNT OPENING FORM FOR INDIVIDUAL(S)", canFont1);
			p2.setAlignment(Element.ALIGN_CENTER);
			p2.setSpacingAfter(10);
			document.add(p2);
			Paragraph p3 = new Paragraph(
					"Branch: _______________________SOL ID _________                                                                                          Date:      |      |     ",
					subFont);
			p3.setSpacingAfter(5);
			document.add(p3);
			SpecialSymbol s = new SpecialSymbol();
			String tick = "\u221A";
			// Chunk tick = new Chunk("\u221A",subFont);
			// System.out.println(tick);
			String space = "                                                                                                                                                     ";
			Paragraph p4 = new Paragraph();

			p4.add(new Chunk("Account No.", subFont));
			p4.add(new Chunk(space, subFont));
			p4.add(new Chunk("KYC Type (", subFont));
//            p4.add(new Chunk("\u221A",subFont));
			p4.add(new Chunk(")", subFont));

			p4.setSpacingAfter(2);
			document.add(p4);
			PdfPTable table1 = new PdfPTable(20);
			table1.setWidthPercentage(100f);
			table1.setWidths(new float[] { 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f,
					0.3f, 0.6f, 0.3f, 1.5f, 0.3f, 0.5f, 0.3f });
			table1.addCell(getCell("1", PdfPCell.ALIGN_CENTER));
			table1.addCell(getCell("2", PdfPCell.ALIGN_CENTER));
			table1.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table1.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table1.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table1.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table1.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table1.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table1.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table1.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table1.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table1.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table1.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			PdfPCell cell0 = new PdfPCell(new Phrase("  ", subFont1));
			cell0.setBackgroundColor(BaseColor.GRAY);
			table1.addCell(cell0);
			table1.addCell(getCell("Normal", PdfPCell.ALIGN_CENTER));
			table1.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table1.addCell(getCell("Simplified(Low Risk)", PdfPCell.ALIGN_CENTER));
			table1.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table1.addCell(getCell("Small", PdfPCell.ALIGN_CENTER));
			table1.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			document.add(table1);
			Paragraph p5 = new Paragraph(
					"I /We request you to open my/our deposit account with your Branch/Bank as under: [Tick () relevant type of acount]  *Mandatory",
					subFont1);
			p5.setSpacingAfter(2);
			document.add(p5);
			PdfPTable table2 = new PdfPTable(8);
			table2.setWidthPercentage(100f);
			table2.setWidths(new float[] { 1.2f, 0.3f, 1.2f, 0.3f, 1.2f, 0.3f, 1.2f, 0.3f });
			PdfPCell cell1 = new PdfPCell(new Phrase(
					"                                                                         Type of Account",
					subFont2));
			cell1.setColspan(8);
			cell1.setBackgroundColor(BaseColor.GRAY);
			table2.addCell(cell1);
			table2.addCell(getCell(" Savings Bank A/c", PdfPCell.ALIGN_LEFT));
			table2.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table2.addCell(getCell(" Current A/c", PdfPCell.ALIGN_LEFT));
			table2.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table2.addCell(getCell(" Term Deposits", PdfPCell.ALIGN_LEFT));
			table2.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table2.addCell(getCell(" Others", PdfPCell.ALIGN_LEFT));
			table2.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			document.add(table2);
			Paragraph p6 = new Paragraph("Operating Instructions (Please mark in appropriate box):", subFont);
			p6.setSpacingAfter(2);
			document.add(p6);
			PdfPTable table3 = new PdfPTable(12);
			table3.setWidthPercentage(100f);
			table3.setWidths(new float[] { 0.4f, 0.2f, 1.5f, 0.2f, 1.6f, 0.2f, 0.6f, 0.2f, 1.7f, 0.2f, 1.8f, 0.2f });
			table3.addCell(getCell(" Self", PdfPCell.ALIGN_LEFT));
			table3.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table3.addCell(getCell(" Either or Survivor", PdfPCell.ALIGN_LEFT));
			table3.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table3.addCell(getCell(" Former or Survivor", PdfPCell.ALIGN_LEFT));
			table3.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table3.addCell(getCell(" Jointly", PdfPCell.ALIGN_LEFT));
			table3.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table3.addCell(getCell(" Anyone or Survivor/s", PdfPCell.ALIGN_LEFT));
			table3.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table3.addCell(getCell(" Others(Please Specify)", PdfPCell.ALIGN_LEFT));
			table3.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			PdfPCell cell2 = new PdfPCell(new Phrase("Initial Deposits Rs.", subFont1));
			cell2.setColspan(12);
			table3.addCell(cell2);
			document.add(table3);
			PdfPTable table4 = new PdfPTable(41);
			table4.setWidthPercentage(100f);
			table4.setWidths(new float[] { 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f,
					0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f,
					0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 1 });
			PdfPCell c1 = new PdfPCell(new Phrase(
					"*Full Name, in CAPITAL Letters ( In the order of first, middle and last name, leaving a space between words)",
					subFont2));
			c1.setColspan(38);
			c1.setBackgroundColor(BaseColor.GRAY);
			c1.setBorder(PdfPCell.NO_BORDER);
			table4.addCell(c1);
			PdfPCell c2 = new PdfPCell(new Phrase("@Gender()*", subFont2));
			c2.setColspan(3);
			c2.setBackgroundColor(BaseColor.GRAY);
			c2.setBorder(PdfPCell.NO_BORDER);
			table4.addCell(c2);
			for (int i = 0; i < 39; i++) {
				table4.addCell(getCell("J", PdfPCell.ALIGN_CENTER));
			}
			table4.addCell(getCell1("@", PdfPCell.ALIGN_CENTER));
			table4.addCell(getCell("M/F/T", PdfPCell.ALIGN_CENTER));
			for (int j = 0; j < 39; j++) {
				table4.addCell(getCell("Y", PdfPCell.ALIGN_CENTER));
			}
			table4.addCell(getCell1("@", PdfPCell.ALIGN_CENTER));
			table4.addCell(getCell("M/F/T", PdfPCell.ALIGN_CENTER));
			for (int k = 0; k < 39; k++) {
				table4.addCell(getCell("O", PdfPCell.ALIGN_CENTER));
			}
			table4.addCell(getCell1("@", PdfPCell.ALIGN_CENTER));
			table4.addCell(getCell("M/F/T", PdfPCell.ALIGN_CENTER));
			document.add(table4);
			PdfPTable table5 = new PdfPTable(32);
			table5.setWidthPercentage(100f);
			table5.setWidths(new float[] { 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f,
					0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f,
					0.5f, 0.5f, 0.5f });
			PdfPCell c3 = new PdfPCell(new Phrase("*Date of Birth (dd/mm/yyyy)", subFont2));
			c3.setColspan(9);
			c3.setBackgroundColor(BaseColor.GRAY);
			c3.setBorder(PdfPCell.NO_BORDER);
			table5.addCell(c3);
			PdfPCell c4 = new PdfPCell(new Phrase("  PAN (if not available,please attach Form 60)", subFont2));
			c4.setColspan(12);
			c4.setBackgroundColor(BaseColor.GRAY);
			c4.setBorder(PdfPCell.NO_BORDER);
			table5.addCell(c4);
			PdfPCell c5 = new PdfPCell(new Phrase("     Customer ID (if any Existing)", subFont2));
			c5.setColspan(11);
			c5.setBackgroundColor(BaseColor.GRAY);
			c5.setBorder(PdfPCell.NO_BORDER);
			table5.addCell(c5);
			for (int a = 1; a <= 9; a++) {
				table5.addCell(getCell("1", PdfPCell.ALIGN_CENTER));
			}
			table5.addCell(getCell1("", PdfPCell.ALIGN_CENTER));
			for (int b = 1; b <= 10; b++) {
				table5.addCell(getCell("1", PdfPCell.ALIGN_CENTER));
			}
			table5.addCell(getCell1("", PdfPCell.ALIGN_CENTER));
			for (int c = 1; c <= 11; c++) {
				table5.addCell(getCell("1", PdfPCell.ALIGN_CENTER));
			}
			for (int a = 1; a <= 9; a++) {
				table5.addCell(getCell("1", PdfPCell.ALIGN_CENTER));
			}
			table5.addCell(getCell1("", PdfPCell.ALIGN_CENTER));
			for (int b = 1; b <= 10; b++) {
				table5.addCell(getCell("1", PdfPCell.ALIGN_CENTER));
			}
			table5.addCell(getCell1("", PdfPCell.ALIGN_CENTER));
			for (int c = 1; c <= 11; c++) {
				table5.addCell(getCell("1", PdfPCell.ALIGN_CENTER));
			}
			for (int a = 1; a <= 9; a++) {
				table5.addCell(getCell("1.", PdfPCell.ALIGN_CENTER));
			}
			table5.addCell(getCell1("", PdfPCell.ALIGN_CENTER));
			for (int b = 1; b <= 10; b++) {
				table5.addCell(getCell("1", PdfPCell.ALIGN_CENTER));
			}
			table5.addCell(getCell1("", PdfPCell.ALIGN_CENTER));
			for (int c = 1; c <= 11; c++) {
				table5.addCell(getCell("1", PdfPCell.ALIGN_CENTER));
			}
			document.add(table5);
			PdfPTable table6 = new PdfPTable(5);
			table6.setWidthPercentage(100f);
			table6.setWidths(new float[] { 0.5f, 1, 1, 1, 1 });
			table6.addCell(getCell1("SI.No", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell1("*Maiden name (if any)", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell1("*Mother's Name", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell1("*Father's Name", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell1("Husband's Name", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell("1.", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell("LAKSHMI BHAVANI", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell("LAKSHMI NARAYANA  ", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell("2.", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell("3.", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table6.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			document.add(table6);
			PdfPTable table7 = new PdfPTable(7);
			table7.setWidthPercentage(100f);
			table7.setWidths(new float[] { 0.5f, 1, 1, 1, 1, 1, 1 });
			PdfPCell cell3 = new PdfPCell(new Phrase("SL.No.", subFont2));
			cell3.setRowspan(2);
			cell3.setBackgroundColor(BaseColor.GRAY);
			table7.addCell(cell3);
			PdfPCell cell4 = new PdfPCell(new Phrase("CKYC Number if alloted", subFont2));
			cell4.setRowspan(2);
			cell4.setBackgroundColor(BaseColor.GRAY);
			table7.addCell(cell4);
			PdfPCell cell5 = new PdfPCell(new Phrase("*Relationship with 1st Applicant", subFont2));
			cell5.setRowspan(2);
			cell5.setBackgroundColor(BaseColor.GRAY);
			table7.addCell(cell5);
			PdfPCell cell6 = new PdfPCell(new Phrase("Annual Income (in Rs)", subFont2));
			cell6.setRowspan(2);
			cell6.setBackgroundColor(BaseColor.GRAY);
			table7.addCell(cell6);
			PdfPCell cell7 = new PdfPCell(new Phrase("     If Residence for Tax purpose outside India ", subFont2));
			cell7.setColspan(3);
			cell7.setBackgroundColor(BaseColor.GRAY);
			table7.addCell(cell7);
			table7.addCell(getCell2("Country Code (as per ISO3166)", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell2("Place of Birth", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell2("TIN /Equivalent of Jurisdiction Country", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("1.", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("2.", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("3.", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table7.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			PdfPCell c6 = new PdfPCell(new Phrase(
					"#FILL UP THE COLUMNS BELOW CHOOSING THE CATEGORY CODE (EXTREME LEFT FROM TABLE-A BELOW. ",
					subFont3));
			c6.setColspan(7);
			c6.setBackgroundColor(BaseColor.GRAY);
			table7.addCell(c6);
			document.add(table7);
			Paragraph p10 = new Paragraph("", subFont4);
			p10.setAlignment(Element.ALIGN_CENTER);
			Chunk head1 = new Chunk(
					"Example: Service 01,Pensioner 04,NRI-02 etc.for Other Mention CODE No. and Specify the Category.\n");
			head1.setUnderline(1, -2f);
			p10.add(head1);
			Chunk head2 = new Chunk(
					"Example: If Occupation is ''Others'' and category is House Wife mention 05 (House Wife).For staff mention SPF No.instead of CODE No.");
			head2.setUnderline(1, -2f);
			p10.add(head2);
			p10.setSpacingAfter(6);
			document.add(p10);
			PdfPTable table8 = new PdfPTable(8);
			table8.setWidthPercentage(100f);
			table8.setWidths(new float[] { 0.5f, 1, 1, 1, 1, 1, 1, 1 });
			table8.addCell(getCell2("SL.No", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell2("*Occupation", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell2("*Sector (if occupation is service)", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell2("*Status", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell2("*Marital Status", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell2("*Nationality", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell2("*Residential Status", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell2(" Educational Qualification", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("1.", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("2.", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("3.", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			table8.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			document.add(table8);
			Paragraph p11 = new Paragraph("TABLE - A", subFont);
			p11.setAlignment(Element.ALIGN_CENTER);
			p11.setSpacingAfter(2);
			document.add(p11);
			PdfPTable table9 = new PdfPTable(8);
			table9.setWidthPercentage(100f);
			table9.setWidths(new float[] { 0.6f, 0.9f, 0.9f, 1, 1, 0.8f, 1.4f, 1 });
			table9.addCell(getCell2("#CODE", PdfPCell.ALIGN_CENTER));
			table9.addCell(getCell2("Occupation", PdfPCell.ALIGN_CENTER));
			table9.addCell(getCell2("Sector", PdfPCell.ALIGN_CENTER));
			table9.addCell(getCell2("Status", PdfPCell.ALIGN_CENTER));
			table9.addCell(getCell2("Marital Status", PdfPCell.ALIGN_CENTER));
			table9.addCell(getCell2("Nationality", PdfPCell.ALIGN_CENTER));
			table9.addCell(getCell2("Residential Status", PdfPCell.ALIGN_CENTER));
			table9.addCell(getCell2("Educational Qualification", PdfPCell.ALIGN_CENTER));
			table9.addCell(getCell("01.", PdfPCell.ALIGN_CENTER));
			table9.addCell(getCell(" Service", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Private", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Minor", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Married", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Indian", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Resident Individuals (RI)", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Non-Matric", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell("02.", PdfPCell.ALIGN_CENTER));
			table9.addCell(getCell(" Professional", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Public", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Sr.Citizen", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Un Married", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Others", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Non Resident Individuals (NRI)", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" SSC/HSC", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell("03.", PdfPCell.ALIGN_CENTER));
			table9.addCell(getCell(" Bussiness", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Government", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Staff(SPF No)", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Others", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Forien National (FI)", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Graduate", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell("04.", PdfPCell.ALIGN_CENTER));
			table9.addCell(getCell(" Pensioner", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Ex Staff", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Person of India Origin (PIO)", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Post graduate", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell("05.", PdfPCell.ALIGN_CENTER));
			table9.addCell(getCell(" Others", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell(" Others/Gen", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table9.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			PdfPCell cell8 = new PdfPCell(new Phrase(" Facilities Required:", subFont2));
			cell8.setColspan(2);
			cell8.setBackgroundColor(BaseColor.GRAY);
			table9.addCell(cell8);
			PdfPCell cell9 = new PdfPCell(new Phrase(
					" Cheque Book/Without Cheque Book/ATM Debit Card/Mobile Banking/Internet Banking (View/Transaction)",
					subFont1));
			cell9.setColspan(6);
			table9.addCell(cell9);
			document.add(table9);
			Paragraph p12 = new Paragraph(
					"Strike out which is not applicable with signature 1)                                    2)                                  3)                          ",
					subFont1);
			p12.setAlignment(Element.ALIGN_LEFT);
			p12.setSpacingAfter(5);
			document.add(p12);
			PdfPTable table10 = new PdfPTable(3);
			table10.setWidthPercentage(100f);
			table10.setWidths(new float[] { 1, 1, 1 });
			PdfPCell cell10 = new PdfPCell(new Phrase(
					"                                                              Name and Address of Employer",
					subFont2));
			cell10.setColspan(3);
			cell10.setBackgroundColor(BaseColor.GRAY);
			table10.addCell(cell10);
			table10.addCell(getCell3("1st Applicant", PdfPCell.ALIGN_CENTER));
			table10.addCell(getCell3("2nd Applicant", PdfPCell.ALIGN_CENTER));
			table10.addCell(getCell3("3rd Applicant", PdfPCell.ALIGN_CENTER));
			table10.addCell(getCell("    \n" + "   \n" + "   \n" + "   \n", PdfPCell.ALIGN_CENTER));
			table10.addCell(getCell("    ", PdfPCell.ALIGN_CENTER));
			table10.addCell(getCell("    ", PdfPCell.ALIGN_CENTER));
			table10.setSpacingAfter(1);
			document.add(table10);
			Paragraph p45 = new Paragraph("Code No.307/2(R) / SSP / 30,00,000Nos / 05-2018", subFont1);
			document.add(p45);
			document.newPage();
			Paragraph p13 = new Paragraph("(2)", subFont);
			p13.setAlignment(Element.ALIGN_CENTER);
			p13.setSpacingAfter(5);
			document.add(p13);

			PdfPTable table11 = new PdfPTable(11);
			table11.setWidthPercentage(100f);
			table11.setWidths(new float[] { 1.4f, 1.8f, 0.3f, 0.9f, 0.3f, 0.8f, 0.3f, 1.4f, 0.3f, 1, 0.3f });
			table11.addCell(getCell2(" ()Type of Address", PdfPCell.ALIGN_LEFT));
			table11.addCell(getCell3(" Residential/Business", PdfPCell.ALIGN_LEFT));
			table11.addCell(getCell3("  ", PdfPCell.ALIGN_LEFT));
			table11.addCell(getCell3(" Residential", PdfPCell.ALIGN_LEFT));
			table11.addCell(getCell3("  ", PdfPCell.ALIGN_LEFT));
			table11.addCell(getCell3(" Bussines", PdfPCell.ALIGN_LEFT));
			table11.addCell(getCell3("  ", PdfPCell.ALIGN_LEFT));
			table11.addCell(getCell3(" Registered Office", PdfPCell.ALIGN_LEFT));
			table11.addCell(getCell3("  ", PdfPCell.ALIGN_LEFT));
			table11.addCell(getCell3(" Unspecified", PdfPCell.ALIGN_LEFT));
			table11.addCell(getCell3("  ", PdfPCell.ALIGN_LEFT));
			table11.setSpacingAfter(5);
			document.add(table11);
			String space1 = "                                                                          ";
			PdfPTable table12 = new PdfPTable(4);
			table12.setWidthPercentage(100f);
			table12.setWidths(new float[] { 1.6f, 1, 1, 1 });
			PdfPCell cell11 = new PdfPCell(new Phrase(space1 + "*Present/Communication Address", subFont2));
			cell11.setColspan(4);
			cell11.setBackgroundColor(BaseColor.GRAY);
			table12.addCell(cell11);
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell3("1st Applicant", PdfPCell.ALIGN_CENTER));
			table12.addCell(getCell3("2nd Applicant", PdfPCell.ALIGN_CENTER));
			table12.addCell(getCell3("3rd Applicant", PdfPCell.ALIGN_CENTER));
			table12.addCell(getCell(" Flat No/Bldb Name:", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell(" Street/Road & Area/Locality/Village:", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell(" Post Office & PIN:", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell(" City and District:", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell(" State:", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell(" Country:", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell(" Landline No./Mobile:", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell(" E-mail:", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell(" Aadhaar No./Enrollment No.:", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table12.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			document.add(table12);
			Paragraph p14 = new Paragraph(
					"Note: a)If AADHAAR is not done,write ENROLLMENT No. AADHAAR to be submitted within six months else Account will be frozen.\n",
					subFont4);
			p14.add("           b)If residence outside India for Tax purpose,Overseas Address on separate sheet to be attached to this Form.");
			p14.setAlignment(Element.ALIGN_LEFT);
			p14.setSpacingAfter(5);
			document.add(p14);
			PdfPTable table13 = new PdfPTable(4);
			table13.setWidthPercentage(100f);
			table13.setWidths(new float[] { 1.6f, 1, 1, 1 });
			PdfPCell cell12 = new PdfPCell(new Phrase(space1 + "*Permanent Address", subFont2));
			cell12.setColspan(4);
			cell12.setBackgroundColor(BaseColor.GRAY);
			table13.addCell(cell12);
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell3("1st Applicant", PdfPCell.ALIGN_CENTER));
			table13.addCell(getCell3("2nd Applicant", PdfPCell.ALIGN_CENTER));
			table13.addCell(getCell3("3rd Applicant", PdfPCell.ALIGN_CENTER));
			table13.addCell(getCell(" Flat No/Bldb Name:", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell(" Street/Road & Area/Locality/Village:", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell(" Post Office & PIN:", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell(" City and District:", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell(" State:", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell(" Country:", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell(" Landline No./Mobile:", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell(" E-mail:", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell(" Aadhaar No./Enrollment No.:", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			table13.addCell(getCell("  ", PdfPCell.ALIGN_LEFT));
			document.add(table13);
			Paragraph p15 = new Paragraph(
					"For NRI Applicants copy of passport must be submitted as Identification document.", subFont);
			p15.setAlignment(Element.ALIGN_LEFT);
			p15.setSpacingAfter(5);
			document.add(p15);
			PdfPTable table14 = new PdfPTable(10);
			table14.setWidthPercentage(100f);
			table14.setWidths(new float[] { 1, 0.3f, 1, 1, 0.3f, 1, 1, 0.3f, 1, 1 });
			PdfPCell cell17 = new PdfPCell(new Phrase(" *PROOF OF IDENTITY (POI)", subFont2));
			cell17.setRowspan(2);
			cell17.setBackgroundColor(BaseColor.GRAY);
			table14.addCell(cell17);
			PdfPCell cell13 = new PdfPCell(new Phrase(
					"                                                                            Photo Identity ",
					subFont2));
			cell13.setColspan(9);
			cell13.setBackgroundColor(BaseColor.GRAY);
			table14.addCell(cell13);
			PdfPCell cell14 = new PdfPCell(new Phrase("                  1st Applicant ", subFont));
			cell14.setColspan(3);
			table14.addCell(cell14);
			PdfPCell cell15 = new PdfPCell(new Phrase("                  2nd Applicant ", subFont));
			cell15.setColspan(3);
			table14.addCell(cell15);
			PdfPCell cell16 = new PdfPCell(new Phrase("                 3rd Applicant ", subFont));
			cell16.setColspan(3);
			table14.addCell(cell16);
			table14.addCell(getCell(" Documents", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell("()", PdfPCell.ALIGN_CENTER));
			table14.addCell(getCell("Number", PdfPCell.ALIGN_CENTER));
			table14.addCell(getCell("Expiry Date", PdfPCell.ALIGN_CENTER));
			table14.addCell(getCell("()", PdfPCell.ALIGN_CENTER));
			table14.addCell(getCell("Number", PdfPCell.ALIGN_CENTER));
			table14.addCell(getCell("Expiry Date", PdfPCell.ALIGN_CENTER));
			table14.addCell(getCell("()", PdfPCell.ALIGN_CENTER));
			table14.addCell(getCell("Number", PdfPCell.ALIGN_CENTER));
			table14.addCell(getCell("Expiry Date", PdfPCell.ALIGN_CENTER));
			table14.addCell(getCell(" Passport", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" Voter Id", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" PAN Card", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" Driving License", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" UID", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" NREGA JOB CARD", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" Others", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(
					getCell(" Simplified Measures Account (Documents type code as per CKYC)", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table14.setSpacingAfter(5);
			document.add(table14);
			Paragraph p7 = new Paragraph();
			p7.setLeading(0, 0.8f);
			p7.setAlignment(Element.ALIGN_JUSTIFIED);
			Chunk tpara = new Chunk("For Current A/c:\n", subFont);
			p7.add(tpara);
			Chunk tpara1 = new Chunk("Annual Turnover (last yr.) Rs.\nEstimated turnover for present year Rs.\n\n",
					subFont1);
			p7.add(tpara1);
			Chunk tpara2 = new Chunk("Particulars of Accounts with other Banks Declaration:", subFont);
			p7.add(tpara2);
			Chunk tpara3 = new Chunk(
					"I/We certify that I/We do not have any borrowal account with any other Bank/Branches.\nI/We have borrowal account with (Mention the name of the Bank, Branch address&A/C No.\n\n",
					subFont1);
			p7.add(tpara3);

			PdfPTable table15 = new PdfPTable(8);
			table15.setWidthPercentage(100f);
			table15.setWidths(new float[] { 1, 0.2f, 0.8f, 0.2f, 0.8f, 0.2f, 0.8f, 2 });
			PdfPCell cell18 = new PdfPCell(
					new Phrase("                                 *PROOF OF ADDRESS (POA)", subFont2));
			cell18.setColspan(7);
			cell18.setBackgroundColor(BaseColor.GRAY);
			table15.addCell(cell18);
			table15.addCell(getCell2("Declaration For Current Accounts", PdfPCell.ALIGN_CENTER));
			table15.addCell(getCell("  ", PdfPCell.ALIGN_CENTER));
			PdfPCell cell19 = new PdfPCell(new Phrase("     1st Applicant", subFont));
			cell19.setColspan(2);
			table15.addCell(cell19);
			PdfPCell cell20 = new PdfPCell(new Phrase("      2nd Applicant", subFont));
			cell20.setColspan(2);
			table15.addCell(cell20);
			PdfPCell cell21 = new PdfPCell(new Phrase("       3rd Applicant", subFont));
			cell21.setColspan(2);
			table15.addCell(cell21);
			PdfPCell cell22 = new PdfPCell();
			cell22.setRowspan(8);
			cell22.addElement(p7);
			table15.addCell(cell22);
			PdfPCell cell23 = new PdfPCell(
					new Phrase("                () Type of Documents of (POA)and write the number", subFont));
			cell23.setColspan(7);
			table15.addCell(cell23);
			table15.addCell(getCell(" Documents", PdfPCell.ALIGN_CENTER));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell3("Number", PdfPCell.ALIGN_CENTER));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell3("Number", PdfPCell.ALIGN_CENTER));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell3("Number", PdfPCell.ALIGN_CENTER));
			table15.addCell(getCell(" Passport", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" Voter Id", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" PAN Card", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" Driving License", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" UID", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" NREGA JOB CARD", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			PdfPCell cell24 = new PdfPCell(new Phrase(" 1)\n \n2)\n \n3)\n \n        Signature of Declarant", subFont));
			cell24.setRowspan(3);
			table15.addCell(cell24);
			table15.addCell(getCell(" Others", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(
					getCell(" Simplified Measures Account (Document type code as per CKYC)", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell("   ", PdfPCell.ALIGN_LEFT));
			table15.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table15.setSpacingAfter(5);
			document.add(table15);
			document.newPage();
			Paragraph p16 = new Paragraph("(3)", subFont);
			p16.setAlignment(Element.ALIGN_CENTER);
			p16.setSpacingAfter(5);
			document.add(p16);
			Paragraph p17 = new Paragraph();
			p17.setLeading(0, 1.1f);
			p17.setAlignment(Element.ALIGN_JUSTIFIED);
			Chunk para1 = new Chunk("FOR Term Deposit\n", subFont);
			para1.setUnderline(1, -2f);
			p17.add(para1);
			Chunk bullet = new Chunk("\u2022", subFont);

			p17.add(bullet);
			Chunk para2 = new Chunk(" Period for Term Deposit/Recurring Deposit: ", subFont);
			p17.add(para2);
			Chunk para3 = new Chunk("_____days/_____months(s)________year(s)\n", subFont1);
			p17.add(para3);
			p17.add(bullet);
			Chunk para4 = new Chunk(" RD monthly installment: ", subFont);
			p17.add(para4);
			Chunk para5 = new Chunk("                         Rs.___________________\n", subFont1);
			p17.add(para5);
			p17.add(bullet);
			Chunk para6 = new Chunk(" Term deposit initial Deposit Amount: ", subFont);
			p17.add(para6);
			Chunk para7 = new Chunk("    Rs.___________________\n", subFont1);
			p17.add(para7);
			p17.add(bullet);
			Chunk para8 = new Chunk(" Mode of Deposit for opening A/c: ", subFont);
			p17.add(para8);
			Chunk para9 = new Chunk(
					"Cash/Transfer from A/C No_____________________/If by Cheque,Cheque No: ____________________\n",
					subFont1);
			p17.add(para9);
			p17.add(bullet);
			Chunk para10 = new Chunk(" Renewal Instruction: ", subFont);
			p17.add(para10);
			Chunk para11 = new Chunk(
					"I/We authorize the Bank to automatically renew the matured Term Deposit with/ without accured interest for _____________ days/months/years at the prevailing rate of interest unless otherwise instructed by me/us. I/We opt for auto renewal of my/our Term Deposit for________ times. Notice on maturity of Term Deposit either/renewal shall be sent in Present/Permanent Address. Or not required.(Strike out which is not applicable)\n",
					subFont1);
			p17.add(para11);
			p17.add(bullet);
			Chunk par1 = new Chunk(
					" In the absence of specific instruction, Term Deposit will be renewed on the same terms and conditions at rate prevailing at the time of renewal for three times.\n",
					subFont1);
			p17.add(par1);
			document.add(p17);
			Paragraph p20 = new Paragraph();
			p20.setLeading(0, 1.1f);
			p20.setAlignment(Element.ALIGN_JUSTIFIED);
			p20.add(bullet);
			Chunk para12 = new Chunk(" In case of Bonanza/Savings/CurrentAccount: ", subFont);
			p20.add(para12);
			Chunk para13 = new Chunk("Minimum balance to be maintained in the SB/Current A/C___________________\n",
					subFont1);
			p20.add(para13);
			p20.add(bullet);
			Chunk par2 = new Chunk(
					" Amount per unit of Fixed Deposit_____________________________ Period of Fixed Deposit______ Days ______ Month________Year\n",
					subFont1);
			p20.add(par2);
			p20.add(bullet);
			Chunk para14 = new Chunk(" Interest payment frequency in case of Fixed Deposit:", subFont);
			p20.add(para14);
			Chunk para15 = new Chunk("Monthly/Quarterly/Half yearly/Yearly(Strike out which is not applicable)\n",
					subFont1);
			p20.add(para15);
			p20.add(bullet);
			Chunk par3 = new Chunk(
					" Standing Instructions: Please debit monthly installment of RD Account from my SB/CD Account No._________________\n",
					subFont1);
			p20.add(par3);
			p20.add(bullet);
			Chunk par4 = new Chunk(
					" Please credit monthly/quarterly/Half yearly/Yearly interest on Fixed Deposit to my Bank Account No._________________ \n",
					subFont1);
			p20.add(par4);
			document.add(p20);
			Paragraph pk1 = new Paragraph();
			pk1.setAlignment(Element.ALIGN_CENTER);
			pk1.add(bullet);
			Chunk par5 = new Chunk(" (Strike out which is not applicable)\n\n", subFont1);
			pk1.add(par5);
			document.add(pk1);
			Paragraph p18 = new Paragraph();
			p18.setLeading(0, 0.8f);
			p18.setAlignment(Element.ALIGN_JUSTIFIED);
			Chunk para16 = new Chunk("Declaration:\n", subFont);
			p18.add(para16);
			Chunk para17 = new Chunk(
					"I/We wish to allow premature repayment of the Fixed/Term Deposit having operating instructions as ‘‘either or survivors’’,‘‘anyone or suvivors’’ or ‘‘former or survivors’’ in ine with the operating instructions of the deposits. In case of joint Fixed/Term deposits having operating instructions as ‘‘either or survivors’’,‘‘anyone or survivors’’ or ‘‘former or survivors’’, the Bank shall repay the deposit/s before the maturity of the deposit/s in case such a request in accordance with the operating instructions of the respective deposit/s, along with relevant documents as may be specified by the Bank from time to time. The same would be applicable even in the event of death of any of the joint depositors prior to maturity of the deposit. Any such repayment before maturity shall constitute a valid discharge of the Bank's obligation against all concerned including but not limited to the nominee/legal heirs of the depositors or any one claiming under them.",
					subFont1);
			p18.add(para17);
			p18.setSpacingAfter(7);
			document.add(p18);
			PdfPTable table16 = new PdfPTable(4);
			table16.setWidthPercentage(100f);
			table16.setWidths(new float[] { 1, 1, 0.5f, 0.5f });
			table16.addCell(getCell2("Name", PdfPCell.ALIGN_CENTER));
			table16.addCell(getCell2("Specimen Signature", PdfPCell.ALIGN_CENTER));
			PdfPCell cell25 = new PdfPCell(new Phrase("                               Photograph", subFont2));
			cell25.setColspan(2);
			cell25.setBackgroundColor(BaseColor.GRAY);
			table16.addCell(cell25);
			PdfPCell cell26 = new PdfPCell(
					new Phrase("                         (Please sign in black ink inside the blocks)", subFont));
			cell26.setColspan(2);
			table16.addCell(cell26);
			table16.addCell(getCell("1", PdfPCell.ALIGN_CENTER));
			table16.addCell(getCell("2", PdfPCell.ALIGN_CENTER));
			table16.addCell(getCell("1.\n\n\n", PdfPCell.ALIGN_LEFT));
			table16.addCell(getCell("\n\n\n", PdfPCell.ALIGN_LEFT));
			PdfPCell cell27 = new PdfPCell(new Phrase("  ", subFont1));
			cell27.setRowspan(2);
			table16.addCell(cell27);
			PdfPCell cell28 = new PdfPCell(new Phrase("  ", subFont1));
			cell28.setRowspan(2);
			table16.addCell(cell28);
			table16.addCell(getCell("2.\n\n\n", PdfPCell.ALIGN_LEFT));
			table16.addCell(getCell("\n\n\n", PdfPCell.ALIGN_LEFT));
			PdfPCell cell29 = new PdfPCell(new Phrase("3.\n\n\n", subFont1));
			cell29.setRowspan(2);
			table16.addCell(cell29);
			PdfPCell cell30 = new PdfPCell(new Phrase("  ", subFont1));
			cell30.setRowspan(2);
			table16.addCell(cell30);
			table16.addCell(getCell("3", PdfPCell.ALIGN_CENTER));
			PdfPCell cell31 = new PdfPCell(new Phrase(
					"Paste photograph\nwith full face and\nsign across it in\npresence of the branch officer",
					subFont1));
			cell31.setRowspan(3);
			table16.addCell(cell31);
			PdfPCell cell33 = new PdfPCell(new Phrase("  ", subFont1));
			cell33.setRowspan(2);
			table16.addCell(cell33);
			PdfPCell cell32 = new PdfPCell(new Phrase(
					"                                   (Bank Official in whose presence signed)\n\nName:\n\n\nSPF No:"
							+ space1 + "               Signature:",
					subFont1));
			cell32.setColspan(2);
			table16.addCell(cell32);
			table16.setSpacingAfter(4);
			document.add(table16);
			PdfPTable table26 = new PdfPTable(1);
			table26.setWidthPercentage(100f);
			table26.setWidths(new float[] { 1 });
			table26.addCell(getCell2("Form 60 (To be filled by those who do not have PAN)", PdfPCell.ALIGN_LEFT));
			document.add(table26);
			Paragraph p21 = new Paragraph();
			p21.setLeading(0, 0.8f);
			Chunk para18 = new Chunk("FormNo.60\n", subFont);
			para18.setUnderline(1, -2f);
			p21.add(para18);
			Chunk para21 = new Chunk(
					"Amount of Transaction Rs.               Date of Transaction:       /       /            ()Mode of Transaction:",
					subFont);
			p21.add(para21);
			Chunk para19 = new Chunk(" Cash/ Cheque/ Card/ Draft/ Banker's Cheque/ Online transfer/ Other \n",
					subFont1);
			p21.add(para19);
			p21.setSpacingAfter(2);
			document.add(p21);
			PdfPTable table17 = new PdfPTable(5);
			table17.setWidthPercentage(100f);
			table17.setWidths(new float[] { 1, 1, 1, 1, 1 });
			PdfPCell cell34 = new PdfPCell(
					new Phrase("                        If Pan Applied Not Yet Generated", subFont));
			cell34.setColspan(3);
			table17.addCell(cell34);
			PdfPCell cell35 = new PdfPCell(new Phrase("              #If PAN Not Applied for", subFont));
			cell35.setColspan(2);
			table17.addCell(cell35);
			table17.addCell(getCell3("Applicant (s)", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell3("Date of Application for PAN", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell3("Acknowledgment Number", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell3("Agricultural Income Rs.", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell3("Other than Agricultural Income Rs.", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell("1st Applicant", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell("2nd Applicant", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell("3rd Applicant", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table17.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			document.add(table17);
			Paragraph p22 = new Paragraph();
			p22.setLeading(0, 0.8f);
			Chunk para22 = new Chunk("#If estimated income ", subFont4);
			p22.add(para22);
			Chunk para23 = new Chunk("(including income of spouse, minor child etc.)", subFont5);
			para23.setUnderline(1, -2f);
			p22.add(para23);
			Chunk para24 = new Chunk(
					"from other than Agricultural income exceeds maximum amount not chargeable to tax,From 60 will be acceptable ONLY if PAN has been applied for.",
					subFont4);
			p22.add(para24);
			document.add(p22);
			document.newPage();
			Paragraph p19 = new Paragraph("(4)", subFont);
			p19.setAlignment(Element.ALIGN_CENTER);
			p19.setSpacingAfter(5);
			document.add(p19);
			PdfPTable table18 = new PdfPTable(1);
			table18.setWidthPercentage(100f);
			table18.setWidths(new float[] { 1 });
			table18.addCell(getCell(
					" I/We 1)Sri                                               2)Sri                                             3) Sri                                declare\n"
							+ " that what is stated above is true to the best of my knowledge and belief, I/We further declare that I do not have a Permanent Account Number and"
							+ " my/our estimated total income (income of spouse, minor child etc. as per section 64 of Income Tax Act 1961 for the financial year in which the"
							+ " above transaction is held will be less than maximum not chargeable to tax.\n"
							+ " verified today the _____________________ day of __________________20                                place:\n\n"
							+ " (Signature of the Declarant(s) 1)                                2)                                3) ",
					PdfPCell.ALIGN_LEFT));
			document.add(table18);
			Paragraph p24 = new Paragraph("In Case Of Minor Account Holder", subFont);
			p24.setSpacingAfter(2);
			document.add(p24);
			PdfPTable table19 = new PdfPTable(6);
			table19.setWidthPercentage(100f);
			table19.setWidths(new float[] { 1, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f });
			PdfPCell cell36 = new PdfPCell(
					new Phrase("Name of the Guardian:\n(In case of Minor):\n(Attach proof for minor's DOB)", subFont1));
			cell36.setRowspan(3);
			table19.addCell(cell36);
			PdfPCell cell37 = new PdfPCell(new Phrase(space1 + "Relationship with minor ( tick one)", subFont1));
			cell37.setColspan(5);
			table19.addCell(cell37);
			table19.addCell(getCell("F & NG", PdfPCell.ALIGN_CENTER));
			table19.addCell(getCell("M & NG", PdfPCell.ALIGN_CENTER));
			table19.addCell(getCell("Legal", PdfPCell.ALIGN_CENTER));
			table19.addCell(getCell("De facto", PdfPCell.ALIGN_CENTER));
			table19.addCell(getCell("Others", PdfPCell.ALIGN_CENTER));
			table19.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table19.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table19.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table19.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table19.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			document.add(table19);
			Paragraph p25 = new Paragraph(
					"*In case of legal guardian(guardian appointed by Court),enclose copy of the court order.\n"
							+ " Declaration by guardian : I shall represent the said minor in all future transactions of any description in the above account until the said minor"
							+ " attains majority. I indentify the bank against the claim of the above minor for any withdrawal/transactions made by me in his/her account. Further I"
							+ " declare that the money withdrawn from the account will be utilized for the benefit of the minor only. \n\n",
					subFont1);
			p25.add("Signature __________________________");
			p25.setLeading(0, 1);
			p25.setSpacingAfter(4);
			document.add(p25);
			PdfPTable table20 = new PdfPTable(5);
			table20.setWidthPercentage(100f);
			table20.setWidths(new float[] { 1.1f, 1, 1, 0.3f, 1 });
			table20.addCell(getCell3("Name", PdfPCell.ALIGN_CENTER));
			table20.addCell(getCell3("Address", PdfPCell.ALIGN_CENTER));
			table20.addCell(getCell3("Relationship with depositor/minor/if any", PdfPCell.ALIGN_CENTER));
			table20.addCell(getCell3("Age", PdfPCell.ALIGN_CENTER));
			table20.addCell(getCell3("If nominee is minor his/her date of birth", PdfPCell.ALIGN_CENTER));
			table20.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
			table20.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
			table20.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
			table20.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
			table20.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
			PdfPCell cell38 = new PdfPCell(new Phrase(
					"In case the nominee is minor on this date...........please mention (name, age, address)of the person appointed to receive the amount of the deposit on behalf of the nominee in the event of my/our/minor's death during the minority of the nominee.\n\nName-\nAge-\nAddress-",
					subFont1));
			cell38.setRowspan(3);
			table20.addCell(cell38);
			PdfPCell cell39 = new PdfPCell(
					new Phrase("1)\n\n2)\n\n3)\n \n    (Signature of the depositor)\n\nPlace:\nDate:", subFont1));
			cell39.setRowspan(3);
			table20.addCell(cell39);
			PdfPCell cell40 = new PdfPCell(new Phrase(
					"Signature to be witnessed by one person/Thumb impression to be witnessed by two persons.\n\n\n",
					subFont1));
			cell40.setColspan(3);
			table20.addCell(cell40);
			PdfPCell cell41 = new PdfPCell(new Phrase("\n\n", subFont1));
			cell41.setColspan(3);
			table20.addCell(cell41);
			PdfPCell cell42 = new PdfPCell(new Phrase(
					"              (Name,Signature&address of the witness(s))\n\n\n\nPlace:                                               Date:",
					subFont1));
			cell42.setColspan(3);
			table20.addCell(cell42);
			Paragraph p26 = new Paragraph();
			p26.setLeading(0, 1);
			p26.setAlignment(Element.ALIGN_JUSTIFIED);
			Chunk para25 = new Chunk("Nomination:DA1 Required/Not Required", subFont);
			p26.add(para25);
			Chunk para26 = new Chunk(
					"(Strike out which is not applicable with signature)_____________________________\n"
							+ "Nomination under section 45ZA of Banking Regulation Act 1949 and Rule 2(1) of the Banking (Nomination) rules 1985 in respect of Bank"
							+ " Deposits, I/We nominate the following person to whom in the event of my/our/minor's death the amount of deposit in the above account may be"
							+ " returned by United Bank of India________________Branch (Sol ID______________) \n",
					subFont1);
			p26.add(para26);
			Chunk para27 = new Chunk("Particulars of Nominee:\n", subFont);
			p26.add(para27);
			p26.add(table20);
			PdfPTable table21 = new PdfPTable(1);
			table21.setWidthPercentage(100f);
			table21.setWidths(new float[] { 1 });
			PdfPCell cs4 = new PdfPCell();
			cs4.addElement(p26);
			table21.addCell(cs4);
			document.add(table21);
			Paragraph p27 = new Paragraph();
			p27.setLeading(0, 0.8f);
			p27.setAlignment(Element.ALIGN_JUSTIFIED);
			Chunk para28 = new Chunk(
					"Note-In case of Minor Accounts, Guardians shall submit KYC details in seperate sheet along with proof of identify and address.\n",
					subFont4);
			p27.add(para28);
			Chunk para20 = new Chunk("Applicant Declaration:\n ", subFont);
			p27.add(para20);
			Chunk para29 = new Chunk(
					"I/We hereby declare that the details furnished above the true and correct to the best of my/out knowledge and belief and I/We undertake to inform"
							+ " you of any changes there in immediately. In case any of the above information is found to be false or untrue or misleading or misrepresenting, I am"
							+ " aware that I may be held liable. I/We hereby consent of receiving information from Central KYC Registry through SMS/Email on above registered"
							+ " number/E-Mail address.\n",
					subFont1);
			p27.add(para29);
			document.add(p27);
			Paragraph p28 = new Paragraph();
			p28.setLeading(0, 0.8f);
			p28.setAlignment(Element.ALIGN_JUSTIFIED);
			Chunk para31 = new Chunk(
					" I/We agree to be bound by the Bank's rules and regulations govering__________________account from time to time. I/We will maintain"
							+ " minimum balance in the account and on the event of fall in the minimum balance the Bank may realize the service charge. I/We certify that the"
							+ " above information is correct. Kindly allow me/us to open the account. Other information(s) are enclosed herewith"
							+ " I/We have read and understood the most Important Terms and Conditions of SB/CD accounts and have submitted the signed copy of the same to"
							+ " Bank to be attached to our account opening form.\n ",
					subFont1);
			p28.add(para31);
			Chunk para30 = new Chunk("Signature of Applicants\n\n", subFont);
			p28.add(para30);
			Chunk para32 = new Chunk(
					"1)_____________________________2)_____________________________3)_______________________________",
					subFont1);
			p28.add(para32);
			p28.setSpacingAfter(6);
			document.add(p28);
			PdfPTable table22 = new PdfPTable(7);
			table22.setWidthPercentage(40f);
			table22.setWidths(new float[] { 1.5f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f });
			table22.addCell(getCell2("SCHEME CODE", PdfPCell.ALIGN_CENTER));
			table22.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table22.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table22.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table22.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table22.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table22.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			Paragraph p29 = new Paragraph();
			Chunk para33 = new Chunk("For Bank use only:", subFont);
			p29.add(para33);
			Paragraph p41 = new Paragraph();
			Chunk para34 = new Chunk("Purpose of opening account__________________", subFont);
			p41.add(para34);
			Paragraph p40 = new Paragraph();
			p40.setAlignment(Element.ALIGN_JUSTIFIED);
			p40.setLeading(0, 1.3f);
			Chunk para35 = new Chunk(
					"I/We have met the account opener/s Mr./Ms.________________Mr./Ms._______________Mr./Ms.______________in person and hereby confirm that I/We have obtained and verified all relevant documents with original and confirm that KYC Norms are fully complied with and account opening is complete in all respects.Signed MITC document for SB/CD Account received from the customer(s)\n"
							+ "Signature_________________SPF No.____________Signature of the 2nd Officer____________SPF No.____________Name of the Officer_________________Name of the Officer________________\n"
							+ "1.Account opened on:____________________2.Letter of thanks sent to customer on:__________________3.Acknowledgment received from customer on:_______________",
					subFont1);
			p40.add(para35);

			PdfPTable table23 = new PdfPTable(7);
			table23.setWidthPercentage(50f);
			table23.setWidths(new float[] { 1.6f, 0.4f, 0.3f, 0.6f, 0.3f, 0.4f, 0.3f });
			table23.addCell(getCell2("Risk Classification:", PdfPCell.ALIGN_CENTER));
			table23.addCell(getCell("Low", PdfPCell.ALIGN_CENTER));
			table23.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table23.addCell(getCell("Medium", PdfPCell.ALIGN_CENTER));
			table23.addCell(getCell("", PdfPCell.ALIGN_CENTER));
			table23.addCell(getCell("High", PdfPCell.ALIGN_CENTER));
			table23.addCell(getCell("", PdfPCell.ALIGN_CENTER));

			PdfPTable table27 = new PdfPTable(2);
			table27.setWidthPercentage(100f);
			table27.setWidths(new float[] { 0.5f, 2 });
			table27.addCell(getCell3("For Bank use only:", PdfPCell.ALIGN_LEFT));
			PdfPCell cell45 = new PdfPCell();
			cell45.addElement(table22);
			table27.addCell(cell45);
			table27.addCell(getCell3("Purpose of opening account____________", PdfPCell.ALIGN_LEFT));
			PdfPCell cell46 = new PdfPCell();
			cell46.addElement(table23);
			table27.addCell(cell46);
			PdfPCell cell47 = new PdfPCell();
			cell47.setColspan(2);
			cell47.addElement(p40);
			table27.addCell(cell47);
			document.add(table27);
			Paragraph p30 = new Paragraph("(Please tear the below portion and hand it over to the customer)\n",
					subFont);
			p30.add("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ");
			document.add(p30);
			Paragraph p31 = new Paragraph(
					"I/We acknowledge nomination made by you in favour of_________________________age__________in respect of your account No.__________________on the basis of your declaration in DA1 format.\nDate:                                                                                                                              Authorized Officer",
					subFont1);
			p31.setAlignment(Element.ALIGN_JUSTIFIED);
			PdfPTable table25 = new PdfPTable(1);
			table25.setWidthPercentage(100f);
			table25.setWidths(new float[] { 1 });
			PdfPCell cs6 = new PdfPCell();
			cs6.addElement(p31);
			table25.addCell(cs6);
			document.add(table25);
			document.close();
		} catch (Exception e) {

		}

	}

	public PdfPCell getCell(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text, redFont));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setPaddingBottom(1.8f);
		cell.setBorder(PdfPCell.BOX);
		return cell;
	}

	public PdfPCell getCell1(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text, redFont1));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBackgroundColor(BaseColor.GRAY);
		cell.setPaddingBottom(1.5f);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}

	public PdfPCell getCell2(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text, redFont1));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBackgroundColor(BaseColor.GRAY);
		cell.setPaddingBottom(1.8f);
		cell.setBorder(PdfPCell.BOX);
		return cell;
	}

	public PdfPCell getCell3(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text, redFont2));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setPaddingBottom(1.8f);
		cell.setBorder(PdfPCell.BOX);
		return cell;
	}
}
