/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subk.core;

/**
 *
 * @author Suresh
 */
public class Dashboard {
    
    private int approvedCount;
    private int approvedAmount;
    private int pendingCount;
    private int pendingAmount;
    private int returnedCount;
    private int returnedAmount;
    private int rejectedCount;
    private int rejectedAmount;
    private int receivedCount;
    private int receivedAmount;
    
    private int savingsOpenedCount;
    private int savingsOpenedAmount;
    private int savingsPendingCount;
    private int savingsPendingAmount;
    private int loanOpenedCount;
    private int loanOpenedAmount;
    private int loanPendingCount;
    private int loanPendingAmount;
    private int loanDisbursedCount;
    private int loanDisbursedAmount;
    private int loanDisbursedPendingCount;
    private int loanDisbursedPendingAmount;
    
    private int demandDueCount;
    private int demandDueAmount;
    private int collectionCount;
    private int collectionAmount;
    private int ftodCount;
    private int ftodAmount;
    private int odCount;
    private int odAmount;
    private int npaCount;
    private int npaAmount;
    
    private String error;
    private String fromDate;
    private String toDate;

    public int getApprovedCount() {
        return approvedCount;
    }

    public void setApprovedCount(int approvedCount) {
        this.approvedCount = approvedCount;
    }

    public int getApprovedAmount() {
        return approvedAmount;
    }

    public void setApprovedAmount(int approvedAmount) {
        this.approvedAmount = approvedAmount;
    }

    public int getPendingCount() {
        return pendingCount;
    }

    public void setPendingCount(int pendingCount) {
        this.pendingCount = pendingCount;
    }

    public int getPendingAmount() {
        return pendingAmount;
    }

    public void setPendingAmount(int pendingAmount) {
        this.pendingAmount = pendingAmount;
    }

    public int getReturnedCount() {
        return returnedCount;
    }

    public void setReturnedCount(int returnedCount) {
        this.returnedCount = returnedCount;
    }

    public int getReturnedAmount() {
        return returnedAmount;
    }

    public void setReturnedAmount(int returnedAmount) {
        this.returnedAmount = returnedAmount;
    }

    public int getRejectedCount() {
        return rejectedCount;
    }

    public void setRejectedCount(int rejectedCount) {
        this.rejectedCount = rejectedCount;
    }

    public int getRejectedAmount() {
        return rejectedAmount;
    }

    public void setRejectedAmount(int rejectedAmount) {
        this.rejectedAmount = rejectedAmount;
    }

    public int getReceivedCount() {
        return receivedCount;
    }

    public void setReceivedCount(int receivedCount) {
        this.receivedCount = receivedCount;
    }

    public int getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(int receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public int getSavingsOpenedCount() {
        return savingsOpenedCount;
    }

    public void setSavingsOpenedCount(int savingsOpenedCount) {
        this.savingsOpenedCount = savingsOpenedCount;
    }

    public int getSavingsOpenedAmount() {
        return savingsOpenedAmount;
    }

    public void setSavingsOpenedAmount(int savingsOpenedAmount) {
        this.savingsOpenedAmount = savingsOpenedAmount;
    }
    
    public int getSavingsPendingCount() {
        return savingsPendingCount;
    }

    public void setSavingsPendingCount(int savingsPendingCount) {
        this.savingsPendingCount = savingsPendingCount;
    }

    public int getSavingsPendingAmount() {
        return savingsPendingAmount;
    }

    public void setSavingsPendingAmount(int savingsPendingAmount) {
        this.savingsPendingAmount = savingsPendingAmount;
    }
    
    public int getLoanOpenedCount() {
        return loanOpenedCount;
    }

    public void setLoanOpenedCount(int loanOpenedCount) {
        this.loanOpenedCount = loanOpenedCount;
    }

    public int getLoanOpenedAmount() {
        return loanOpenedAmount;
    }

    public void setLoanOpenedAmount(int loanOpenedAmount) {
        this.loanOpenedAmount = loanOpenedAmount;
    }
    
    public int getLoanPendingCount() {
        return loanPendingCount;
    }

    public void setLoanPendingCount(int loanPendingCount) {
        this.loanPendingCount = loanPendingCount;
    }

    public int getLoanPendingAmount() {
        return loanPendingAmount;
    }

    public void setLoanPendingAmount(int loanPendingAmount) {
        this.loanPendingAmount = loanPendingAmount;
    }
    
    public int getLoanDisbursedCount() {
        return loanDisbursedCount;
    }

    public void setLoanDisbursedCount(int loanDisbursedCount) {
        this.loanDisbursedCount = loanDisbursedCount;
    }

    public int getLoanDisbursedAmount() {
        return loanDisbursedAmount;
    }

    public void setLoanDisbursedAmount(int loanDisbursedAmount) {
        this.loanDisbursedAmount = loanDisbursedAmount;
    }

    public int getLoanDisbursedPendingCount() {
        return loanDisbursedPendingCount;
    }

    public void setLoanDisbursedPendingCount(int loanDisbursedPendingCount) {
        this.loanDisbursedPendingCount = loanDisbursedPendingCount;
    }

    public int getLoanDisbursedPendingAmount() {
        return loanDisbursedPendingAmount;
    }

    public void setLoanDisbursedPendingAmount(int loanDisbursedPendingAmount) {
        this.loanDisbursedPendingAmount = loanDisbursedPendingAmount;
    }
    
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the demandDueCount
     */
    public int getDemandDueCount() {
        return demandDueCount;
    }

    /**
     * @param demandDueCount the demandDueCount to set
     */
    public void setDemandDueCount(int demandDueCount) {
        this.demandDueCount = demandDueCount;
    }

    /**
     * @return the demandDueAmount
     */
    public int getDemandDueAmount() {
        return demandDueAmount;
    }

    /**
     * @param demandDueAmount the demandDueAmount to set
     */
    public void setDemandDueAmount(int demandDueAmount) {
        this.demandDueAmount = demandDueAmount;
    }

    /**
     * @return the collectionCount
     */
    public int getCollectionCount() {
        return collectionCount;
    }

    /**
     * @param collectionCount the collectionCount to set
     */
    public void setCollectionCount(int collectionCount) {
        this.collectionCount = collectionCount;
    }

    /**
     * @return the collectionAmount
     */
    public int getCollectionAmount() {
        return collectionAmount;
    }

    /**
     * @param collectionAmount the collectionAmount to set
     */
    public void setCollectionAmount(int collectionAmount) {
        this.collectionAmount = collectionAmount;
    }

    /**
     * @return the ftodCount
     */
    public int getFtodCount() {
        return ftodCount;
    }

    /**
     * @param ftodCount the ftodCount to set
     */
    public void setFtodCount(int ftodCount) {
        this.ftodCount = ftodCount;
    }

    /**
     * @return the ftodAmount
     */
    public int getFtodAmount() {
        return ftodAmount;
    }

    /**
     * @param ftodAmount the ftodAmount to set
     */
    public void setFtodAmount(int ftodAmount) {
        this.ftodAmount = ftodAmount;
    }

    /**
     * @return the odCount
     */
    public int getOdCount() {
        return odCount;
    }

    /**
     * @param odCount the odCount to set
     */
    public void setOdCount(int odCount) {
        this.odCount = odCount;
    }

    /**
     * @return the odAmount
     */
    public int getOdAmount() {
        return odAmount;
    }

    /**
     * @param odAmount the odAmount to set
     */
    public void setOdAmount(int odAmount) {
        this.odAmount = odAmount;
    }

    /**
     * @return the npaCount
     */
    public int getNpaCount() {
        return npaCount;
    }

    /**
     * @param npaCount the npaCount to set
     */
    public void setNpaCount(int npaCount) {
        this.npaCount = npaCount;
    }

    /**
     * @return the npaAmount
     */
    public int getNpaAmount() {
        return npaAmount;
    }

    /**
     * @param npaAmount the npaAmount to set
     */
    public void setNpaAmount(int npaAmount) {
        this.npaAmount = npaAmount;
    }
    
    
}
