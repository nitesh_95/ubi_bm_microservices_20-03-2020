package com.subk.util;

public class Response {
	private String status;
	private String remarks;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "Response [status=" + status + ", remarks=" + remarks + "]";
	}

}