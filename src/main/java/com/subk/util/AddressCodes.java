package com.subk.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.subk.repository.AddProofRepository;
import com.subk.repository.CastRepository;
import com.subk.repository.IdProofRepository;
import com.subk.repository.OccupationRepository;
import com.subk.repository.OperationRepository;
import com.subk.repository.PdfAddressRepository;
import com.subk.repository.RelationRepository;

@Service
public class AddressCodes {
  
	@Autowired
	PdfAddressRepository addressrepository;
	@Autowired
	IdProofRepository idProofRepository;
	@Autowired
	AddProofRepository addProofRepository;
	@Autowired
	OccupationRepository occupationRepository;
	@Autowired
	RelationRepository relationRepository;
	@Autowired
	CastRepository castRepository;
	@Autowired
	OperationRepository operationRepository;
	
	public String getState(String statecode) {
		String state = addressrepository.getStateName(statecode);
		System.out.println("state:::::::::::::::::::::"+state);
		return state;
	}

	public String getdistrict(String statecode, String district) {
		String dist = addressrepository.getDistrictName(statecode, district);
		System.out.println("dist:::::::::::::::::::::"+dist);
		return dist;
	}

	public String getSubDistrict(String statecode, String district, String subdistrict) {
		String subdist = addressrepository.getSubDistrictName(statecode, district, subdistrict);
		System.out.println("subdist:::::::::::::::::::::"+subdist);
		return subdist;
	}

	public String getVillage(String statecode, String district, String subdistrict, String village) {
		String vill = addressrepository.getVillageName(statecode, district, subdistrict, village);
		System.out.println("vill:::::::::::::::::::::"+vill);
		return vill;
	}

	public String getcommVillage(String state, String district, String village) {
		String vill = addressrepository.getcommVillageName(state, district, village);
		System.out.println("commvill:::::::::::::::::::::"+vill);
		return vill;
	}

	public String getIdProof(String idproofcode) {
		System.out.println("Id proof code::::::::::: "+idproofcode);
		String idproofname = idProofRepository.getIdProofName(idproofcode);
		System.out.println("idproofname:::::::::::::::::::::"+idproofname);
		return idproofname;
	}

	public String getAddProof(String addproofcode) {
		String addproofname = addProofRepository.getAddProofName(addproofcode);
		System.out.println("addproofname:::::::::::::::::::::"+addproofname);
		return addproofname;
	}

	public String getOccupation(String occupationcode) {
		String occupationname = occupationRepository.getOccupationName(occupationcode);
		System.out.println("occupationname:::::::::::::::::::::"+occupationname);
		return occupationname;
	}

	public String getRelation(String relationcode) {
		String relationname = relationRepository.getRelationName(relationcode);
		System.out.println("relationname:::::::::::::::::::::"+relationname);
		return relationname;
	}

	public String getCast(String castcode) {
		String castname = castRepository.getCastName(castcode);
		System.out.println("castname:::::::::::::::::::::"+castname);
		return castname;
	}
	public String getOperation(String operationcode) {
		String operationname = operationRepository.getOperationName(operationcode);
		System.out.println("operationname:::::::::::::::::::::"+operationname);
		return operationname;
	}
}
