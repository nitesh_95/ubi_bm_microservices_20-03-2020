package com.subk.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.subk.entity.MasterOccupation;


@Repository
public interface OccupationRepository extends JpaRepository<MasterOccupation,String>{

	@Query("select description from MasterOccupation where code = ?1")
	String getOccupationName(String occupationcode);
	

}
