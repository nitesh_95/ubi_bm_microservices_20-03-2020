package com.subk.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.subk.entity.ManagerLogin;
@Repository
public interface BranchRepository extends JpaRepository<ManagerLogin, String>{

	

	@Query("select branchName from ManagerLogin where solid=?1")
	String getBranch(String solid);
	
}
