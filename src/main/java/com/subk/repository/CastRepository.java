package com.subk.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.subk.entity.MasterCaste;

@Repository
public interface CastRepository extends JpaRepository<MasterCaste,String>{
	
	@Query("select description from MasterCaste where code = ?1")
	String getCastName(String castcode);

}
