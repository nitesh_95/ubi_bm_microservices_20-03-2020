package com.subk.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.subk.entity.MasterRelationCode;
@Repository
public interface RelationRepository extends JpaRepository<MasterRelationCode,String>{

	@Query("select description from MasterRelationCode where code = ?1")
	String getRelationName(String relationcode);

}
