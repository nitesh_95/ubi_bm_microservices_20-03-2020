package com.subk.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.subk.entity.MasterOperation;

@Repository
public interface OperationRepository extends JpaRepository<MasterOperation,String>{

	@Query("select description from MasterOperation where code = ?1")
	String getOperationName(String operationcode);

}
