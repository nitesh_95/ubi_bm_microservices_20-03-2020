package com.subk.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.subk.entity.Loanorigination;

@Repository
public interface PdfRepository extends JpaRepository<Loanorigination, String>{

}
