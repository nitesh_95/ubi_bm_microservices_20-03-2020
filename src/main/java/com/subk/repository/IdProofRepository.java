package com.subk.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.subk.entity.MasterIdproof;

@Repository
public interface IdProofRepository extends JpaRepository<MasterIdproof,String>{

	@Query("select description from MasterIdproof where code = ?1")
	String getIdProofName(String idproofcode);

}
