package com.subk.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.subk.entity.MasterAddproof;


@Repository
public interface AddProofRepository extends JpaRepository<MasterAddproof,String>{

	@Query("select description from MasterAddproof where code = ?1")
	String getAddProofName(String addproofcode);

	
}
