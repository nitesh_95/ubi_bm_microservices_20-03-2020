package com.subk.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.subk.entity.MasterUbiLocationData;

@Repository
public interface PdfAddressRepository extends JpaRepository<MasterUbiLocationData, Integer> {

	@Query("select statedesc from MasterUbiLocationData where areastatecode = ?1 group by statedesc")
	String getStateName(String statecode);

	@Query("select districtdesc from MasterUbiLocationData where areastatecode =?1 and districtcode =?2 group by districtdesc")
	String getDistrictName(String statecode,String district);

	@Query("select subdistrictdesc from MasterUbiLocationData where areastatecode = ?1 AND districtcode = ?2 AND subdistrictcode = ?3 group by subdistrictdesc")
	String getSubDistrictName(String statecode, String district, String subdistrict);

	@Query("select villagedesc from MasterUbiLocationData where areastatecode = ?1 AND districtcode = ?2 AND subdistrictcode = ?3 AND villagecode = ?4")
	String getVillageName(String statecode, String district, String subdistrict, String village);

	@Query("select villagedesc from MasterUbiLocationData where areastatecode = ?1 AND districtcode = ?2 AND villagecode = ?3")
	String getcommVillageName(String state, String district, String village);
	
	

}
