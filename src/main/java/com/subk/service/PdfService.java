package com.subk.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.subk.core.ConsentClauseBorrowerUndertakingPDFFIle;
import com.subk.core.DemandPromissoryNotePDFFIle;
import com.subk.core.EMIDeductionUndertakingPDFFile;
import com.subk.core.ExecutionDocumentsUndertakingPDFFIle;
import com.subk.core.GenerateLoanPDFFile;
import com.subk.core.GeneratePDFFile;
import com.subk.core.GeneratePrimaryAppPDF;
import com.subk.core.GenerateSanctionPDFFile;
import com.subk.core.GetAppraisalDocuments;
import com.subk.core.HypothecationAgreementPDFFile;
import com.subk.core.HypothecationTermLoanAgreementPDFFile;
import com.subk.core.LetterOfLienPDFFile;
import com.subk.core.TermLoanAgreementPDFFile;
import com.subk.entity.Loanorigination;
import com.subk.model.Model;
import com.subk.repository.AddProofRepository;
import com.subk.repository.BranchRepository;
import com.subk.repository.CastRepository;
import com.subk.repository.IdProofRepository;
import com.subk.repository.OccupationRepository;
import com.subk.repository.OperationRepository;
import com.subk.repository.PdfAddressRepository;
import com.subk.repository.PdfRepository;
import com.subk.repository.RelationRepository;
@Service
public class PdfService {
	Logger log = LoggerFactory.getLogger(PdfService.class);
	@Autowired
	private PdfRepository repository;
	@Autowired
	PdfAddressRepository addressrepository;
	@Autowired
	IdProofRepository idProofRepository;
	@Autowired
	AddProofRepository addProofRepository;
	@Autowired
	OccupationRepository occupationRepository;
	@Autowired
	RelationRepository relationRepository;
	@Autowired
	CastRepository castRepository;
	@Autowired
	OperationRepository operationRepository;
	@Autowired
	BranchRepository branchRepository;
	
	

	public String getCustomerById(String applicationid, String pdfType, String pathAdd, String img, String jsPaths)
			throws RecordNotFoundException {
		Optional<Loanorigination> employee = repository.findById(applicationid);
		System.out.println("Path Address:::::::::::::::::::   " + pathAdd);
		String fileurl = null;
		if (employee.isPresent()) {
			Loanorigination loanData = employee.get();

//			 Permaddress
			String state = getState(loanData.getPermstate());
			String district = getdistrict(loanData.getPermstate(), loanData.getPermdistrict());
			String subdistrict = getSubDistrict(loanData.getPermstate(), loanData.getPermdistrict(),
					loanData.getPermsubdistrict());
			String village = getVillage(loanData.getPermstate(), loanData.getPermdistrict(),
					loanData.getPermsubdistrict(), loanData.getPermvillage());
			
			loanData.setPermstate(state);
			loanData.setPermdistrict(district);
			loanData.setPermsubdistrict(subdistrict);
			loanData.setPermvillage(village);
		
			// CommState
			String comstate = getState(loanData.getCommstate());
			String comdistrict = getdistrict(loanData.getCommstate(), loanData.getCommdistrict());
			String comsubdistrict = getSubDistrict(loanData.getCommstate(),loanData.getCommdistrict(),loanData.getAreadistcode());
			String comvillage = getVillage(loanData.getCommstate(), loanData.getCommdistrict(),loanData.getAreadistcode(),
					loanData.getCommvillage());
			loanData.setCommstate(comstate);
			loanData.setCommdistrict(comdistrict);
			loanData.setAreadistcode(comsubdistrict);
			loanData.setCommvillage(comvillage);
//            loanData.setAppl2occupation(getOccupation(loanData.getAppl2occupation()));
			fileurl = generatePdf(loanData, pdfType, pathAdd, img, jsPaths);
			log.info("fileurllllllllllll:::::::::::::" + fileurl);
		} else {
			throw new RecordNotFoundException("No customer record exist for given id");
		}
		System.out.println("fileurllllllllllll11111111111:::::::::::::" + fileurl);
		return fileurl;
	}

	public String businessLocation(Loanorigination loandata) {
		return commercialAddress(loandata);
		
	}
//	String fileName = null;

	public String generatePdf(Loanorigination loanData, String pdfType, String pathAdd, String img, String jsPaths) {

		String filePath = null;
		Model model = new Model();
		String fileName = pdfType + "_" + loanData.getAppl1name() + "_" + loanData.getApplicationid() + ".pdf";
		if (pdfType.equalsIgnoreCase("primary")) {
			loanData.setIdprooftype(getIdProof(loanData.getIdprooftype()));
			loanData.setAppl1occupation(getOccupation(loanData.getAppl1occupation()));
			loanData.setFamilymembsoccupation(getOccupation(loanData.getFamilymembsoccupation()));
			loanData.setBranchname(getBranchName(loanData.getSolid()));
			filePath = new GeneratePrimaryAppPDF().getPdf(loanData, fileName, pathAdd, img, jsPaths);
		} else if (pdfType.equalsIgnoreCase("saving")) {
			loanData.setAppl1occupation(getOccupation(loanData.getAppl1occupation()));
			loanData.setCategory(getCast(loanData.getCategory()));
			loanData.setIdprooftype(getIdProof(loanData.getIdprooftype()));
            loanData.setAddressprooftype(getAddProof(loanData.getAddressprooftype()));
            loanData.setOperationmode(getOperation(loanData.getOperationmode()));
            loanData.setNomineerelation(getRelation(loanData.getNomineerelation()));
			filePath = new GeneratePDFFile().getPdf(loanData, fileName, pathAdd, img, jsPaths);
		} else if (pdfType.equalsIgnoreCase("loan")) {
			loanData.setAppl1occupation(getOccupation(loanData.getAppl1occupation()));
			loanData.setCategory(getCast(loanData.getCategory()));
			loanData.setFamilymemb1relation(getRelation(loanData.getFamilymemb1relation()));
			loanData.setFamilymemb1Occup(getOccupation(loanData.getFamilymemb1Occup()));
			loanData.setFamilymemb2occup(getOccupation(loanData.getFamilymemb2occup()));
			loanData.setFamilymemb2relation(getRelation(loanData.getFamilymemb2relation()));
			loanData.setFamilymemb3relation(getRelation(loanData.getFamilymemb3relation()));
			loanData.setFamilymemb3occup(getOccupation(loanData.getFamilymemb3occup()));
			loanData.setLoanaccnomineerelation(getRelation(loanData.getLoanaccnomineerelation()));
			loanData.setBranchname(getBranchName(loanData.getSolid()));
			filePath = new GenerateLoanPDFFile().getPdf(loanData, fileName, pathAdd, img, jsPaths);
		} else if (pdfType.equalsIgnoreCase("sanction")) {
			loanData.setBranchname(getBranchName(loanData.getSolid()));
			filePath = new GenerateSanctionPDFFile().getPdf(loanData, fileName, pathAdd, img, jsPaths);
		}
//		else if (pdfType.equalsIgnoreCase("undertaking")) {
//			filePath = new GenerateUndertakingPDFFile().getPdf(loanData, fileName, pathAdd, img, jsPaths);
//		}
		else if (pdfType.equalsIgnoreCase("fxappraisal")) {
			filePath = new GetAppraisalDocuments().getFXAppraisalPDF(loanData, pathAdd);
		} else if (pdfType.equalsIgnoreCase("kycdoc")) {
			filePath = new GetAppraisalDocuments().getFXAppraisalPDFs(loanData, pathAdd);
		} else if (pdfType.equalsIgnoreCase("emideduction")) {
			loanData.setBranchname(getBranchName(loanData.getSolid()));
			filePath = new EMIDeductionUndertakingPDFFile().getPdf(loanData, fileName, pathAdd, img, jsPaths);
		} else if (pdfType.equalsIgnoreCase("consentclauseforborrower")) {
			filePath = new ConsentClauseBorrowerUndertakingPDFFIle().getPdf(loanData, fileName, pathAdd, img, jsPaths);
		} else if (pdfType.equalsIgnoreCase("declarationonexecutionofdocuments")) {
			loanData.setBranchname(getBranchName(loanData.getSolid()));
			filePath = new ExecutionDocumentsUndertakingPDFFIle().getPdf(loanData, fileName, pathAdd, img, jsPaths);
		} else if (pdfType.equalsIgnoreCase("demandpromissorynote")) {
			filePath = new DemandPromissoryNotePDFFIle().getPdf(loanData, fileName, pathAdd, img, jsPaths);
		} else if (pdfType.equalsIgnoreCase("LETTEROFLIEN")) {
			loanData.setBranchname(getBranchName(loanData.getSolid()));
			filePath = new LetterOfLienPDFFile().getPdf(loanData, fileName, pathAdd, img, jsPaths);
		} else if (pdfType.equalsIgnoreCase("HYPOTHECATIONAGREEMENT")) {
			filePath = new HypothecationAgreementPDFFile().getPdf(loanData, fileName, pathAdd, img, jsPaths);
		} else if (pdfType.equalsIgnoreCase("AGREEMENTFORTERMLOANANDHYPOTHECATION")) {
			loanData.setBranchname(getBranchName(loanData.getSolid()));
			filePath = new HypothecationTermLoanAgreementPDFFile().getPdf(loanData, fileName, pathAdd, img, jsPaths);
		} else if (pdfType.equalsIgnoreCase("AGREEMENTOFTERMLOAN")) {
			loanData.setBranchname(getBranchName(loanData.getSolid()));
			System.out.println("*********************************");
			filePath = new TermLoanAgreementPDFFile().getPdf(loanData, fileName, pathAdd, img, jsPaths);
		}
		log.info("filepath in service::::::::::::::" + filePath);
		return filePath;
	}

	public String getState(String statecode) {
		String state = addressrepository.getStateName(statecode);
		return state;
	}

	public String getdistrict(String statecode, String district) {
		System.out.println("State Code:: "+statecode+ " District ::  "+district);
		String dist = addressrepository.getDistrictName(statecode, district);
		return dist;
	}

	public String getSubDistrict(String statecode, String district, String subdistrict) {
		String subdist = addressrepository.getSubDistrictName(statecode, district, subdistrict);
		return subdist;
	}

	public String getVillage(String statecode, String district, String subdistrict, String village) {
		String vill = addressrepository.getVillageName(statecode, district, subdistrict, village);
		return vill;
	}

	public String getcommVillage(String state, String district, String village) {
		String vill = addressrepository.getcommVillageName(state, district, village);
		return vill;
	}

	public String getIdProof(String idproofcode) {
		String idproofname = idProofRepository.getIdProofName(idproofcode);
		return idproofname;
	}

	public String getAddProof(String addproofcode) {
		String addproofname = addProofRepository.getAddProofName(addproofcode);
		return addproofname;
	}

	public String getOccupation(String occupationcode) {
		String occupationname = occupationRepository.getOccupationName(occupationcode);
		return occupationname;
	}

	public String getRelation(String relationcode) {
		String relationname = relationRepository.getRelationName(relationcode);
		return relationname;
	}

	public String getCast(String castcode) {
		String castname = castRepository.getCastName(castcode);
		return castname;
	}
	public String getOperation(String operationcode) {
		String operationname = operationRepository.getOperationName(operationcode);
		return operationname;
	}
	public String getBranchName(String solid) {
		String branch = branchRepository.getBranch(solid);
		return branch;
	}
	public String commercialAddress(Loanorigination loandata) {
		String commercialAddress;
		commercialAddress = loandata.getCommhouseno()+","+loandata.getCommstreetno()+","+
		loandata.getCommlandmark()+","+loandata.getCommvillcity()+","+loandata.getCommdistrict()+","+
				loandata.getCommstate()+","+loandata.getCommpincode();
		return commercialAddress;
	
	}
	public String homeAddress(Loanorigination loandata) {
		String homeAddress;
		homeAddress = loandata.getPermhouseno()+","+loandata.getPermstreetno()+","+loandata.getPermlandmark()+","+
		loandata.getPermvillcity()+","+loandata.getPermdistrict()+","+loandata.getPermstate()+","+
				loandata.getPermpincode();
		return homeAddress;
	}
	

}
