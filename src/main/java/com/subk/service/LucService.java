package com.subk.service;

import org.springframework.stereotype.Service;

import com.subk.core.GetAppraisalDocumentsLuc;

@Service
public class LucService {
	public String getCustomerById(String applicationid, String pdfType, String applicantImagePath, String solid)
			throws RecordNotFoundException {
		String fileurl = null;
		fileurl = generatePdf(pdfType, applicationid, solid, applicantImagePath);
		return fileurl;
	}

	public String generatePdf(String pdfType, String applicationid, String solid, String applicantImagePath) {

		String filePath = null;
		if (pdfType.equalsIgnoreCase("LUCpdf")) {
			filePath = new GetAppraisalDocumentsLuc().getFXAppraisalPDF(applicationid, solid, applicantImagePath);
		} else if (pdfType.equalsIgnoreCase("CibilReportUpload")) {
			filePath = new GetAppraisalDocumentsLuc().getFXAppraisalPDFs(applicationid, solid, applicantImagePath);
		}
			else if (pdfType.equalsIgnoreCase("ApplicantPhoto")) {
				filePath = new GetAppraisalDocumentsLuc().getFXAppraisalImages(applicationid, solid, applicantImagePath);
		}else if (pdfType.equalsIgnoreCase("ApplicantImages")) {
			filePath = new GetAppraisalDocumentsLuc().getFXAppraisalzip(applicationid, solid, applicantImagePath);
		}
			else {
			filePath = new GetAppraisalDocumentsLuc().getFXAppraisalzip(applicationid, solid, applicantImagePath);
		}
		return filePath;

	}

}
