package com.subk.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.subk.model.Model;
import com.subk.service.PdfService;
import com.subk.service.RecordNotFoundException;
import com.subk.util.Response;

@RestController
@CrossOrigin
public class PdfController {
	@Value("${applicantimagepath}")
	private String pathAdd;
	@Value("${imgPath}")
	private String img;
	@Value("${jsPath}")
	private String jsPaths;
	
	Logger log = LoggerFactory.getLogger(PdfController.class);
	@Autowired
	private PdfService service;

	@GetMapping("/{applicationid}")
	public String getCustomerById(@PathVariable("applicationid") String applicationId, String pdfType, String pathAdd,
			String img, String jsPath) throws RecordNotFoundException {
		String entity = service.getCustomerById(applicationId, pdfType, pathAdd, img, jsPaths);
		log.info(applicationId);
		return entity;
	}

	String fileName = null;

	@PostMapping("/customer/generatepdf")

	public void processForm(HttpServletRequest request, HttpServletResponse response, @RequestBody Model model)
			throws RecordNotFoundException, IOException {
		OutputStream out = response.getOutputStream();
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition",
				"attachment;filename=" + model.getPdfType() + "_" + model.getApplicationid() + ".pdf");
		Response resp = new Response();
		if (model.getApplicationid() == null) {
			resp.setRemarks("Application Id is not Present");
			resp.setStatus(HttpStatus.OK.toString());
		}
		String filepath = service.getCustomerById(model.getApplicationid(), model.getPdfType(), pathAdd, img, jsPaths);
		log.info("Applicationid " + filepath);
		log.info(model.getPdfType());
		if (filepath != null) {
			FileInputStream in = new FileInputStream(new File(filepath));
            out = response.getOutputStream();
            byte[] outputByte = new byte[4096];
            //copy binary content to output stream
            while (in.read(outputByte, 0, 4096) != -1) {
                out.write(outputByte, 0, 4096);
            }
		} else {
			System.out.println("file is not present");
		}
	}
}
