package com.subk.controller;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.subk.service.LucService;
import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

@CrossOrigin
@RestController

public class LucController {
	Logger log = LoggerFactory.getLogger(LucController.class);
//	@Value("${pathAddress}")
//	private String pathAdd;

//	@Value("${pathLocation}")
//	private String pathAddr;
	@Value("${applicantimagepath}")
	private String applicantImagePath;
	@Autowired
	private LucService service;

	FileInputStream in = null;
	ServletOutputStream out = null;

	@PostMapping("/customer/getpdf")
	public void getPdfData(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("solid") String solid, @RequestParam("applicationid") String applicationid,
			@RequestParam("pdfType") String pdfType) {

		try {
			System.out.println(pdfType);
			String filePath = service.getCustomerById(applicationid, pdfType, applicantImagePath, solid);
			if (filePath != null) {

				response.setContentType("application/octet-stream");
				response.setHeader("Content-Disposition",
						"attachment;filename=" + pdfType + "_" + applicationid + ".pdf");

				in = new FileInputStream(new File(filePath));
				out = response.getOutputStream();
				byte[] outputByte = new byte[4096];
				while (in.read(outputByte, 0, 4096) != -1) {
					out.write(outputByte, 0, 4096);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@PostMapping("/customer/getzip")
	public void getPdfDatazip(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("solid") String solid, @RequestParam("applicationid") String applicationid) {
		String zipType = "ApplicantImages";
		try {
			String filePath = service.getCustomerById(applicationid, zipType, applicantImagePath, solid);
			if (filePath != null) {

				response.setContentType("application/zip");
				response.setHeader("Content-Disposition",
						"attachment;filename=" + zipType + "_" + applicationid + ".zip");

				in = new FileInputStream(new File(filePath));
				out = response.getOutputStream();
				byte[] outputByte = new byte[4096];
				while (in.read(outputByte, 0, 4096) != -1) {
					out.write(outputByte, 0, 4096);
					System.out.println(applicantImagePath);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@GetMapping("/customer/getimage")
	public void getImages(HttpServletRequest request, HttpServletResponse response, @RequestParam("solid") String solid,
			@RequestParam("applicationid") String applicationid, @RequestParam("pdfType") String pdfType) {

		BufferedOutputStream output = null;
		try {
			String filePath = service.getCustomerById(applicationid, pdfType, applicantImagePath, solid);
			if (filePath != null) {

				response.setContentType("images/jpg");
				response.setHeader("Content-Disposition",
						"attachment;filename=" + pdfType + "_" + applicationid + ".jpg");

				out = response.getOutputStream();
				InputStream targetStream = new FileInputStream(new File(filePath));
				byte[] byteArray = IOUtils.toByteArray(targetStream);
				response.setContentType("image/jpg");
				response.setContentLength(byteArray.length);
				ByteArrayInputStream input = new ByteArrayInputStream(byteArray);
				output = new BufferedOutputStream(response.getOutputStream());
				byte[] buffer = new byte[4096];
				int length = 0;
				int i = 1;
				while ((length = input.read(buffer)) >= 0) {
					output.write(buffer, 0, length);
					System.out.println("Loading " + i);
					i++;
				}
				input.close();
				output.close();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}